<?php /* Smarty version Smarty-3.1.21-dev, created on 2016-03-23 22:04:32
         compiled from "/Users/evgeniy/sites/minsk-tokyo.dev/design/html/food-menu.tpl" */ ?>
<?php /*%%SmartyHeaderCode:91496149156e5ccb38f2308-00080962%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '3b46c5946a85fd3d3fb1b022cce548efe2c31fe0' => 
    array (
      0 => '/Users/evgeniy/sites/minsk-tokyo.dev/design/html/food-menu.tpl',
      1 => 1457904441,
      2 => 'file',
    ),
    'd99d70178b47531c5609a6ba9b17ff4ed22fd9ec' => 
    array (
      0 => '/Users/evgeniy/sites/minsk-tokyo.dev/design/html/layout.tpl',
      1 => 1457998696,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '91496149156e5ccb38f2308-00080962',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_56e5ccb39c8215_28414430',
  'variables' => 
  array (
    'config' => 0,
    'meta_title' => 0,
    'meta_description' => 0,
    'meta_keywords' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_56e5ccb39c8215_28414430')) {function content_56e5ccb39c8215_28414430($_smarty_tpl) {?><!DOCTYPE html>
<html>
<head>
	<base href="<?php echo $_smarty_tpl->tpl_vars['config']->value->root_url;?>
"/>
	<title><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['meta_title']->value, ENT_QUOTES, 'UTF-8', true);?>
</title>
	
	
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="description" content="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['meta_description']->value, ENT_QUOTES, 'UTF-8', true);?>
" />
	<meta name="keywords"    content="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['meta_keywords']->value, ENT_QUOTES, 'UTF-8', true);?>
" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1.0">
	
    <link href='https://fonts.googleapis.com/css?family=Marko+One' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Overlock:400,700' rel='stylesheet' type='text/css'>

    <link rel="stylesheet" type="text/css" href="/design/css/bootstrap.min.css" />
    <link rel="stylesheet" type="text/css" href="/design/css/vendor.css" />
    <link rel="stylesheet" type="text/css" href="/design/css/base.css" />
    <link rel="stylesheet" type="text/css" href="/design/css/additional.css" />
    <!--[if lt IE 9]>
    <link rel="stylesheet" type="text/css" href="/design/css/ie8-and-down.css" />
    <![endif]-->
    
</head>
<body>
    <div class="restaurant-minsk">
        <div class="wrapper">
            <div class="container container__mod">

                <?php echo $_smarty_tpl->getSubTemplate ('header.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>


                
    <p class="text-center">野菜がたっぷりのベラルーシ料理はビーツ、そばの実、ディル、サワークリームなどを使った料理で、日本では珍しい材料を使いつつとてもヘルシー！</p>
    <h3 class="page-title text-center _tt_upper">Appetizers</h3>
    <ul class="list-unstyled row food-menu__list">
        <li class="food-menu__item col-md-4 col-sm-4 col-xs-6">
            <a href="/design/images/food/001b.png" class="food-menu__item-link" rel="food-menu">
                <img src="/design/images/food/001.jpg" alt="">
            </a>
        </li>
        <li class="food-menu__item col-md-4 col-sm-4 col-xs-6">
            <a href="/design/images/food/002b.png" class="food-menu__item-link" rel="food-menu">
                <img src="/design/images/food/002.jpg" alt="">
            </a>
        </li>
        <li class="food-menu__item col-md-4 col-sm-4 col-xs-6">
            <a href="/design/images/food/003b.png" class="food-menu__item-link" rel="food-menu">
                <img src="/design/images/food/003.jpg" alt="">
            </a>
        </li>
        <li class="food-menu__item col-md-4 col-sm-4 col-xs-6">
            <a href="/design/images/food/004b.png" class="food-menu__item-link" rel="food-menu">
                <img src="/design/images/food/004.jpg" alt="">
            </a>
        </li>
        <li class="food-menu__item col-md-4 col-sm-4 col-xs-6">
            <a href="/design/images/food/005b.png" class="food-menu__item-link" rel="food-menu">
                <img src="/design/images/food/005.jpg" alt="">
            </a>
        </li>
        <li class="food-menu__item col-md-4 col-sm-4 col-xs-6">
            <a href="/design/images/food/006b.png" class="food-menu__item-link" rel="food-menu">
                <img src="/design/images/food/006.jpg" alt="">
            </a>
        </li>
    </ul>


                <?php echo $_smarty_tpl->getSubTemplate ('footer.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

            </div>
        </div>
    </div>
    <?php echo '<script'; ?>
 src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.2/jquery-ui.min.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="/design/js/bootstrap.min.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="/design/js/mediaelement-and-player.min.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="/design/js/picturefill.min.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="/design/js/jquery.colorbox.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
>document.createElement('picture');<?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="/design/js/main.js"><?php echo '</script'; ?>
>
    <!--[if lt IE 10]>
    <?php echo '<script'; ?>
 type="text/javascript" src="/design/js/media.match.min.js"><?php echo '</script'; ?>
>
    <![endif]-->
    <!--[if IE]><?php echo '<script'; ?>
 src="/design/js/ie.js"><?php echo '</script'; ?>
><![endif]-->

    
</body>
</html><?php }} ?>
