<?php /* Smarty version Smarty-3.1.21-dev, created on 2015-06-24 23:02:07
         compiled from "/Users/evgeniy/sites/awb/design/html/product.tpl" */ ?>
<?php /*%%SmartyHeaderCode:51654219755658ce9cd0c16-51502211%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'efdba44712869961a0ccccf4d095b0c28945ce2f' => 
    array (
      0 => '/Users/evgeniy/sites/awb/design/html/product.tpl',
      1 => 1435176080,
      2 => 'file',
    ),
    '721d29d532c8456def5ae484ceb8ede5618804b4' => 
    array (
      0 => '/Users/evgeniy/sites/awb/design/html/layout.tpl',
      1 => 1434563870,
      2 => 'file',
    ),
    'fe871ec99a977c986293050781106a843d5f91c7' => 
    array (
      0 => '/Users/evgeniy/sites/awb/design/html/blocks/get_price.tpl',
      1 => 1434563870,
      2 => 'file',
    ),
    'f0ae92599a91e9aec0f0c3978acea3f5eab5ddef' => 
    array (
      0 => '/Users/evgeniy/sites/awb/design/html/blocks/product_header.tpl',
      1 => 1433490570,
      2 => 'file',
    ),
    'a0cdcf5a1ed989b55303bc569c662a5d05c577bc' => 
    array (
      0 => '/Users/evgeniy/sites/awb/design/html/blocks/trust.tpl',
      1 => 1433410226,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '51654219755658ce9cd0c16-51502211',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_55658ce9f128d2_85919733',
  'variables' => 
  array (
    'config' => 0,
    'meta_title' => 0,
    'meta_description' => 0,
    'meta_keywords' => 0,
    'product' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_55658ce9f128d2_85919733')) {function content_55658ce9f128d2_85919733($_smarty_tpl) {?><!DOCTYPE html>
<html>
<head>
	<base href="<?php echo $_smarty_tpl->tpl_vars['config']->value->root_url;?>
"/>
	<title><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['meta_title']->value, ENT_QUOTES, 'UTF-8', true);?>
</title>
	
	
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="description" content="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['meta_description']->value, ENT_QUOTES, 'UTF-8', true);?>
" />
	<meta name="keywords"    content="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['meta_keywords']->value, ENT_QUOTES, 'UTF-8', true);?>
" />
	
    <!--[if lt IE 9]>
    <?php echo '<script'; ?>
 src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"><?php echo '</script'; ?>
>
    <![endif]-->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300' rel='stylesheet' type='text/css'>
	<link href="design/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <link href="design/css/base.css" rel="stylesheet" type="text/css"/>
    <link href="design/css/non-responsive.css" rel="stylesheet" type="text/css"/>
    <link href="design/css/slick.css" rel="stylesheet" type="text/css"/>
    <link href="design/css/revo-settings.css" rel="stylesheet" type="text/css"/>
    <link href="design/css/revo-style.css" rel="stylesheet" type="text/css"/>
	<link href="favicon.ico" rel="icon"          type="image/x-icon"/>
	<link href="favicon.ico" rel="shortcut icon" type="image/x-icon"/>
    
</head>
<body>
    <?php echo $_smarty_tpl->getSubTemplate ('header.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>


    
    <?php /*  Call merged included template "blocks/product_header.tpl" */
$_tpl_stack[] = $_smarty_tpl;
 $_smarty_tpl = $_smarty_tpl->setupInlineSubTemplate('blocks/product_header.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0, '51654219755658ce9cd0c16-51502211');
content_558b0cbf0f7211_58970431($_smarty_tpl);
$_smarty_tpl = array_pop($_tpl_stack); 
/*  End of included template "blocks/product_header.tpl" */?>
    <div class="container-fluid product-box">
        <div class="wrapper">
            <ul class="product-detail-list list-unstyled pull-left">
                <li class="product-detail-list__item">
                    <div class="product-detail-list__item-wrapper">
                        <div class="product-detail-list__item-title text-upper mt5">Цена</div>
                        <div class="product-detail-list__item-value text-bold mt15"><?php echo $_smarty_tpl->tpl_vars['product']->value->price;?>
</div>
                        <div class="product-detail-list__item-title mt10">руб./кг</div>
                    </div>
                </li>
                <li class="product-detail-list__item">
                    <div class="product-detail-list__item-wrapper">
                        <div class="product-detail-list__item-title text-upper mt5">Доставка</div>
                        <div class="product-detail-list__item-value text-bold mt15"><?php echo $_smarty_tpl->tpl_vars['product']->value->delivery;?>
</div>
                        <div class="product-detail-list__item-title mt10">рабочих<br/>дней</div>
                    </div>
                </li>
                <li class="product-detail-list__item">
                    <div class="product-detail-list__item-wrapper">
                        <div class="product-detail-list__item-title text-upper mt5">Наличие</div>
                        <div class="product-detail-list__item-value text-bold mt15"><?php echo $_smarty_tpl->tpl_vars['product']->value->availability;?>
</div>
                        <div class="product-detail-list__item-title mt10">калибры</div>
                    </div>
                </li>
                <li class="product-detail-list__item">
                    <div class="product-detail-list__item-wrapper">
                        <div class="product-detail-list__item-title text-upper mt5">Упаковка</div>
                        <div class="product-detail-list__item-value text-bold mt5">
                            <img src="design/images/product-detail-box.png" alt=""/>
                        </div>
                        <div class="product-detail-list__item-title mt5"><?php echo $_smarty_tpl->tpl_vars['product']->value->pkg;?>
</div>
                    </div>
                </li>
            </ul>
            <div style="clear: left"></div>
            <div class="product-detail-description">
                <?php echo $_smarty_tpl->tpl_vars['product']->value->body;?>

            </div>
        </div>
    </div>
    <?php /*  Call merged included template "blocks/trust.tpl" */
$_tpl_stack[] = $_smarty_tpl;
 $_smarty_tpl = $_smarty_tpl->setupInlineSubTemplate('blocks/trust.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0, '51654219755658ce9cd0c16-51502211');
content_558b0cbf13bbf2_18381132($_smarty_tpl);
$_smarty_tpl = array_pop($_tpl_stack); 
/*  End of included template "blocks/trust.tpl" */?>


    <?php if (!$_smarty_tpl->tpl_vars['product']->value->id) {?>
        <?php echo $_smarty_tpl->getSubTemplate ('blocks/trust.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

    <?php }?>

    <?php echo $_smarty_tpl->getSubTemplate ('footer.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

    <div class="complete-dialog text-center"><p>Спасибо!</p><p data-default="Ваша заявка принята!" class="complete-dialog-text">Ваша заявка принята!</p></div>

    <?php echo '<script'; ?>
 src="design/js/jquery-1.11.2.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="design/js/bootstrap.min.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="design/js/revolution.min.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="design/js/revo-tools.min.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="design/js/slick.min.js"><?php echo '</script'; ?>
>

    
        <?php echo '<script'; ?>
 type="text/javascript">
            var revApi;
            $(window).load(function(){
                initHeaderPosition();
            })

            //header position init
            function initHeaderPosition(){
                var win = $(window);
                var header = $('.navbar-fixed-top');
                function calcPosition(obj){
                    obj.stop().css({left: parseInt(obj.attr('data-top')) - win.scrollLeft()});
                    win.scroll(function(){
                        obj.stop().css({left: parseInt(obj.attr('data-top')) - win.scrollLeft()});
                    })
                }
                if(header.length){
                    header.attr('data-top',parseInt(header.css('left')));
                    calcPosition(header);
                }
            }
            $(document).ready(function() {
                $('.form__get-price, .subscription-form').on('submit', function(e){
                    e.preventDefault();
                    var $self = $(this),
                        data = $self.serialize(),
                        popup = $('.complete-dialog');

                    $.ajax({
                        url: $self.attr('action'),
                        method: 'post',
                        data: data,
                        success: function(data){
                            $self.trigger('reset');
                            popup.show();
                            setTimeout(function(){
                                popup.fadeOut('fast');
                            }, 2000);
                        }
                    });
                });

                if($('#bottomSlider').revolution == undefined)
                    revslider_showDoubleJqueryError('#bottomSlider');
                else
                    revApi = $('#bottomSlider').show()
                        .revolution({
                            dottedOverlay:"none",
                            delay:6000,
                            startwidth:1170,
                            startheight:432,
                            hideThumbs:200,
                            thumbWidth:100,
                            thumbHeight:50,
                            thumbAmount:2,
                            navigationType:"none",
                            navigationArrows:"none",
                            navigationStyle:"round",
                            touchenabled:"off",
                            onHoverStop:"off",
                            keyboardNavigation:"off",
                            navigationHAlign:"center",
                            navigationVAlign:"bottom",
                            navigationHOffset:0,
                            navigationVOffset:20,
                            soloArrowLeftHalign:"left",
                            soloArrowLeftValign:"center",
                            soloArrowLeftHOffset:20,
                            soloArrowLeftVOffset:0,
                            soloArrowRightHalign:"right",
                            soloArrowRightValign:"center",
                            soloArrowRightHOffset:20,
                            soloArrowRightVOffset:0,
                            shadow:0,
                            fullWidth:"on",
                            fullScreen:"off",
                            spinner:"spinner0",
                            stopLoop:"off",
                            stopAfterLoops:-1,
                            stopAtSlide:-1,
                            shuffle:"off",
                            autoHeight:"on",
                            forceFullWidth:"on",
                            hideThumbsOnMobile:"off",
                            hideNavDelayOnMobile:1500,
                            hideBulletsOnMobile:"off",
                            hideArrowsOnMobile:"off",
                            hideThumbsUnderResolution:0,
                            hideSliderAtLimit:0,
                            hideCaptionAtLimit:0,
                            hideAllCaptionAtLilmit:0,
                            startWithSlide:0,
                            fullScreenOffsetContainer: ""
                        });

            });
        <?php echo '</script'; ?>
>
    

    
</body>
</html><?php }} ?>
<?php /* Smarty version Smarty-3.1.21-dev, created on 2015-06-24 23:02:07
         compiled from "/Users/evgeniy/sites/awb/design/html/blocks/product_header.tpl" */ ?>
<?php if ($_valid && !is_callable('content_558b0cbf0f7211_58970431')) {function content_558b0cbf0f7211_58970431($_smarty_tpl) {?><div class="container-fluid l11 products-header clearfix" style="background-image: url(<?php if ($_smarty_tpl->tpl_vars['product']->value->cover) {?>'../<?php echo $_smarty_tpl->tpl_vars['config']->value->product_covers_dir;
echo $_smarty_tpl->tpl_vars['product']->value->cover;?>
'<?php } else { ?>'design/images/products-header.jpg'<?php }?>)">
    <div class="products-header-overlay"></div>
    <div class="products-header__inner">
        <div class="row">
            <div class="col-md-7">
                <div class="products-header__description">
                    <h1><?php echo $_smarty_tpl->tpl_vars['product']->value->name;?>
</h1>
                    <p class="products-header__description-text">
                        Страна производитель: <?php echo $_smarty_tpl->tpl_vars['product']->value->brand;?>

                    </p>
                </div>
            </div>
            <div class="col-md-5">
                <?php /*  Call merged included template "blocks/get_price.tpl" */
$_tpl_stack[] = $_smarty_tpl;
 $_smarty_tpl = $_smarty_tpl->setupInlineSubTemplate('blocks/get_price.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0, '51654219755658ce9cd0c16-51502211');
content_558b0cbf116879_38544770($_smarty_tpl);
$_smarty_tpl = array_pop($_tpl_stack); 
/*  End of included template "blocks/get_price.tpl" */?>
            </div>
        </div>
    </div>
</div><?php }} ?>
<?php /* Smarty version Smarty-3.1.21-dev, created on 2015-06-24 23:02:07
         compiled from "/Users/evgeniy/sites/awb/design/html/blocks/get_price.tpl" */ ?>
<?php if ($_valid && !is_callable('content_558b0cbf116879_38544770')) {function content_558b0cbf116879_38544770($_smarty_tpl) {?><form class="form-horizontal pull-right form__get-price" role="form" action="ajax/getPrice.php">
    <p>Получить прайс</p>
    <p>и образцы продукции</p>
    <div class="form-group">
        <label class="control-label col-sm-2" for="name">Имя *:</label>
        <div class="col-sm-12">
            <input type="text" required class="form-control" name="name" placeholder="Константин Никифоров">
            <span class="form__get-price_icon form__get-price_icon-user"></span>
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-sm-2" for="phone">Телефон *:</label>
        <div class="col-sm-12">
            <input type="text" required class="form-control" name="phone" placeholder="+7 (495) ___-__-__">
            <span class="form__get-price_icon form__get-price_icon-phone"></span>
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-sm-2" for="email">E-mail *:</label>
        <div class="col-sm-12 form__get-price_input">
            <input type="email" required class="form-control" name="email" placeholder="hello@amwellgroup.ru">
            <span class="form__get-price_icon form__get-price_icon-email"></span>
        </div>
    </div>
    <div class="form-group">
        <div class="text-center col-sm-12">
            <button type="submit" class="btn btn-default form__get-price_btn-send"></button>
        </div>
    </div>
</form><?php }} ?>
<?php /* Smarty version Smarty-3.1.21-dev, created on 2015-06-24 23:02:07
         compiled from "/Users/evgeniy/sites/awb/design/html/blocks/trust.tpl" */ ?>
<?php if ($_valid && !is_callable('content_558b0cbf13bbf2_18381132')) {function content_558b0cbf13bbf2_18381132($_smarty_tpl) {?><div class="container-fluid l6">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 text-center">
                <h2>Нам доверяют</h2>
                <img class="trust-item" src="design/images/lg1.png">
                <img class="trust-item" src="design/images/lg2.png">
                <img class="trust-item" src="design/images/lg3.png">
                <img class="trust-item" src="design/images/lg5.png">
                <img class="trust-item" src="design/images/lg4.png">
            </div>
        </div>
    </div>
</div><?php }} ?>
