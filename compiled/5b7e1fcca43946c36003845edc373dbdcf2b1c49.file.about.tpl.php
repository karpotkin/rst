<?php /* Smarty version Smarty-3.1.21-dev, created on 2015-06-15 20:17:59
         compiled from "/Users/evgeniy/sites/awb/design/html/about.tpl" */ ?>
<?php /*%%SmartyHeaderCode:17744141615566d2ae21d040-74261764%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '5b7e1fcca43946c36003845edc373dbdcf2b1c49' => 
    array (
      0 => '/Users/evgeniy/sites/awb/design/html/about.tpl',
      1 => 1434388674,
      2 => 'file',
    ),
    '721d29d532c8456def5ae484ceb8ede5618804b4' => 
    array (
      0 => '/Users/evgeniy/sites/awb/design/html/layout.tpl',
      1 => 1433707626,
      2 => 'file',
    ),
    '20004da77ee302d1b36d5e9013e2a7be8a2503b1' => 
    array (
      0 => '/Users/evgeniy/sites/awb/design/html/blocks/default_header.tpl',
      1 => 1433090847,
      2 => 'file',
    ),
    'd63dae3646c1669c2ec223b58cd06a1197a57019' => 
    array (
      0 => '/Users/evgeniy/sites/awb/design/html/blocks/team.tpl',
      1 => 1434388324,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '17744141615566d2ae21d040-74261764',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_5566d2ae2cde37_22287011',
  'variables' => 
  array (
    'config' => 0,
    'meta_title' => 0,
    'meta_description' => 0,
    'meta_keywords' => 0,
    'product' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5566d2ae2cde37_22287011')) {function content_5566d2ae2cde37_22287011($_smarty_tpl) {?><!DOCTYPE html>
<html>
<head>
	<base href="<?php echo $_smarty_tpl->tpl_vars['config']->value->root_url;?>
"/>
	<title><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['meta_title']->value, ENT_QUOTES, 'UTF-8', true);?>
</title>
	
	
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="description" content="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['meta_description']->value, ENT_QUOTES, 'UTF-8', true);?>
" />
	<meta name="keywords"    content="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['meta_keywords']->value, ENT_QUOTES, 'UTF-8', true);?>
" />
	
    <!--[if lt IE 9]>
    <?php echo '<script'; ?>
 src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"><?php echo '</script'; ?>
>
    <![endif]-->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300' rel='stylesheet' type='text/css'>
	<link href="design/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <link href="design/css/base.css" rel="stylesheet" type="text/css"/>
    <link href="design/css/non-responsive.css" rel="stylesheet" type="text/css"/>
    <link href="design/css/slick.css" rel="stylesheet" type="text/css"/>
    <link href="design/css/revo-settings.css" rel="stylesheet" type="text/css"/>
    <link href="design/css/revo-style.css" rel="stylesheet" type="text/css"/>
	<link href="favicon.ico" rel="icon"          type="image/x-icon"/>
	<link href="favicon.ico" rel="shortcut icon" type="image/x-icon"/>
    
</head>
<body>
    <?php echo $_smarty_tpl->getSubTemplate ('header.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>


    
    <?php /*  Call merged included template "blocks/default_header.tpl" */
$_tpl_stack[] = $_smarty_tpl;
 $_smarty_tpl = $_smarty_tpl->setupInlineSubTemplate('blocks/default_header.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0, '17744141615566d2ae21d040-74261764');
content_557f08c8012962_82033763($_smarty_tpl);
$_smarty_tpl = array_pop($_tpl_stack); 
/*  End of included template "blocks/default_header.tpl" */?>
    <div class="container-fluid about-box mb50">
        <div class="wrapper">
            <div class="row">
                <div class="col-md-12">
                    <div class="col-md-6">
                        <img class="about-image" src="design/images/about/goal.jpg" alt=""/>
                    </div>
                    <div class="col-md-6">
                        <h3 class="about-title text-upper">О компании</h3>
                        <div class="about-description">
                            <p>Amwell Group Ltd. - международная производственная компания.</p>
                            <p>Мы начинаем с подготовки почвы, заканчиваем доставкой фруктов, овощей и ягод на ваш склад. Проводим лабораторные исследования новых сортов. Фасуем выращенные культуры под вашим брендом.</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="col-md-6">
                        <h3 class="about-title text-upper mt50">Наши цели</h3>
                        <ul class="list-unstyled about-list">
                            <li><span class="glyphicon glyphicon-ok" aria-hidden="true"></span>Снижение цены фруктов, ягод и овощей в российском ритейле</li>
                            <li><span class="glyphicon glyphicon-ok" aria-hidden="true"></span>Максимально быстрые поставки</li>
                            <li><span class="glyphicon glyphicon-ok" aria-hidden="true"></span>Развитие сельского хозяйства в России</li>
                            <li><span class="glyphicon glyphicon-ok" aria-hidden="true"></span>Стать высокоэффективной компанией-производителем овощей, фруктов и ягод №1 в России</li>
                        </ul>
                        <p>
                            Хотите с нами? Вы представляете частного инвестора или венчурный фонд? Пишите нам на <a href="mailto:invest@amwellgroup.ru">invest@amwellgroup.ru</a>.
                        </p>
                    </div>
                    <div class="col-md-6">
                        <img class="about-image" src="design/images/about.jpg" alt=""/>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php /*  Call merged included template "blocks/team.tpl" */
$_tpl_stack[] = $_smarty_tpl;
 $_smarty_tpl = $_smarty_tpl->setupInlineSubTemplate('blocks/team.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0, '17744141615566d2ae21d040-74261764');
content_557f08c801a8f5_91406560($_smarty_tpl);
$_smarty_tpl = array_pop($_tpl_stack); 
/*  End of included template "blocks/team.tpl" */?>



    <?php if (!$_smarty_tpl->tpl_vars['product']->value->id) {?>
        <?php echo $_smarty_tpl->getSubTemplate ('blocks/trust.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

    <?php }?>

    <?php echo $_smarty_tpl->getSubTemplate ('footer.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

    <div class="complete-dialog text-center"><p>Спасибо!</p><p>Ваша заявка принята!</p></div>

    <?php echo '<script'; ?>
 src="design/js/jquery-1.11.2.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="design/js/bootstrap.min.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="design/js/revolution.min.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="design/js/revo-tools.min.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="design/js/slick.min.js"><?php echo '</script'; ?>
>

    
        <?php echo '<script'; ?>
 type="text/javascript">
            var revApi;
            $(window).load(function(){
                initHeaderPosition();
            })

            //header position init
            function initHeaderPosition(){
                var win = $(window);
                var header = $('.navbar-fixed-top');
                function calcPosition(obj){
                    obj.stop().css({left: parseInt(obj.attr('data-top')) - win.scrollLeft()});
                    win.scroll(function(){
                        obj.stop().css({left: parseInt(obj.attr('data-top')) - win.scrollLeft()});
                    })
                }
                if(header.length){
                    header.attr('data-top',parseInt(header.css('left')));
                    calcPosition(header);
                }
            }
            $(document).ready(function() {
                $('.form__get-price').on('submit', function(e){
                    e.preventDefault();
                    var $self = $(this),
                        data = $self.serialize(),
                        popup = $('.complete-dialog');
                    $.ajax({
                        url: 'ajax/getPrice.php',
                        method: 'post',
                        data: data,
                        success: function(data){
                            $self.trigger('reset');
                            popup.show();
                            setTimeout(function(){
                                popup.fadeOut('fast');
                            }, 2000);
                        }
                    });
                });

                if($('#bottomSlider').revolution == undefined)
                    revslider_showDoubleJqueryError('#bottomSlider');
                else
                    revApi = $('#bottomSlider').show()
                        .revolution({
                            dottedOverlay:"none",
                            delay:6000,
                            startwidth:1170,
                            startheight:460,
                            hideThumbs:200,
                            thumbWidth:100,
                            thumbHeight:50,
                            thumbAmount:2,
                            navigationType:"none",
                            navigationArrows:"none",
                            navigationStyle:"round",
                            touchenabled:"off",
                            onHoverStop:"off",
                            keyboardNavigation:"off",
                            navigationHAlign:"center",
                            navigationVAlign:"bottom",
                            navigationHOffset:0,
                            navigationVOffset:20,
                            soloArrowLeftHalign:"left",
                            soloArrowLeftValign:"center",
                            soloArrowLeftHOffset:20,
                            soloArrowLeftVOffset:0,
                            soloArrowRightHalign:"right",
                            soloArrowRightValign:"center",
                            soloArrowRightHOffset:20,
                            soloArrowRightVOffset:0,
                            shadow:0,
                            fullWidth:"on",
                            fullScreen:"off",
                            spinner:"spinner0",
                            stopLoop:"off",
                            stopAfterLoops:-1,
                            stopAtSlide:-1,
                            shuffle:"off",
                            autoHeight:"on",
                            forceFullWidth:"on",
                            hideThumbsOnMobile:"off",
                            hideNavDelayOnMobile:1500,
                            hideBulletsOnMobile:"off",
                            hideArrowsOnMobile:"off",
                            hideThumbsUnderResolution:0,
                            hideSliderAtLimit:0,
                            hideCaptionAtLimit:0,
                            hideAllCaptionAtLilmit:0,
                            startWithSlide:0,
                            fullScreenOffsetContainer: ""
                        });

            });
        <?php echo '</script'; ?>
>
    

    

    <?php echo '<script'; ?>
>
        $(function(){
            $('[data-text-toggle]').on('click', function(){
                $(this).next().toggle();
            });
        });
    <?php echo '</script'; ?>
>


</body>
</html><?php }} ?>
<?php /* Smarty version Smarty-3.1.21-dev, created on 2015-06-15 20:18:00
         compiled from "/Users/evgeniy/sites/awb/design/html/blocks/default_header.tpl" */ ?>
<?php if ($_valid && !is_callable('content_557f08c8012962_82033763')) {function content_557f08c8012962_82033763($_smarty_tpl) {?><div class="container-fluid l11 default-header clearfix">
    <div class="default-header__inner"></div>
</div><?php }} ?>
<?php /* Smarty version Smarty-3.1.21-dev, created on 2015-06-15 20:18:00
         compiled from "/Users/evgeniy/sites/awb/design/html/blocks/team.tpl" */ ?>
<?php if ($_valid && !is_callable('content_557f08c801a8f5_91406560')) {function content_557f08c801a8f5_91406560($_smarty_tpl) {?><div class="container-fluid company-team">
    <div class="wrapper">
        <div class="row">
            <h3 class="about-title text-upper text-center">Наша команда</h3>
            
            <ul class="list-unstyled text-center">
                <li class="company-team__item">
                    <div class="company-team__item-photo">
                        <img src="design/images/team/oleg.jpg" alt=""/>
                        <div class="company-team__item-photo_title">
                            <p class="company-team__item-photo_title-name text-upper">Олег Гусков</p>
                            <p>основатель</p>
                        </div>
                    </div>
                    <div class="company-team__social text-center">
                        <a href="https://www.facebook.com/oleg.guskov" class="company-team__social-btn company-team__social-btn-fb"></a>
                        
                        <a href="https://vk.com/oleg_guskov" class="company-team__social-btn company-team__social-btn-vk"></a>
                    </div>
                </li>
                <li class="company-team__item">
                    <div class="company-team__item-photo">
                        <img src="design/images/team/asker.jpg" alt=""/>
                        <div class="company-team__item-photo_title">
                            <p class="company-team__item-photo_title-name text-upper">Аскер Курбанов</p>
                            <p>генеральный директор</p>
                        </div>
                    </div>
                    <div class="company-team__social text-center">
                        <a href="https://www.facebook.com/asker.kurbanov" class="company-team__social-btn company-team__social-btn-fb"></a>
                        
                        <a href="https://vk.com/askerkurbanov" class="company-team__social-btn company-team__social-btn-vk"></a>
                    </div>
                </li>
                <li class="company-team__item">
                    <div class="company-team__item-photo">
                        <img src="design/images/team/chingiz.jpg" alt=""/>
                        <div class="company-team__item-photo_title">
                            <p class="company-team__item-photo_title-name text-upper">Чингиз Гизатов</p>
                            <p>директор по развитию</p>
                        </div>
                    </div>
                    <div class="company-team__social text-center">
                        
                        <a href="http://vk.com/chingiz_gizatov" class="company-team__social-btn company-team__social-btn-vk"></a>
                    </div>
                </li>
                <li class="company-team__item">
                    <div class="company-team__item-photo">
                        <img src="design/images/team/002.jpg" alt=""/>
                        <div class="company-team__item-photo_title">
                            <p class="company-team__item-photo_title-name text-upper">Светлана Моренко</p>
                            <p>директор по маркетингу</p>
                        </div>
                    </div>
                    <div class="company-team__social text-center">
                        <a href="https://www.facebook.com/100000967244246" class="company-team__social-btn company-team__social-btn-fb"></a>
                        
                        <a href="https://vk.com/id3294439" class="company-team__social-btn company-team__social-btn-vk"></a>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</div><?php }} ?>
