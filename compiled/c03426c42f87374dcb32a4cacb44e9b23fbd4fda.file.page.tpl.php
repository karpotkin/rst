<?php /* Smarty version Smarty-3.1.21-dev, created on 2016-06-01 15:45:49
         compiled from "C:\OpenServer\domains\rst\design\html\page.tpl" */ ?>
<?php /*%%SmartyHeaderCode:6589574ed8fd77df26-81000400%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'c03426c42f87374dcb32a4cacb44e9b23fbd4fda' => 
    array (
      0 => 'C:\\OpenServer\\domains\\rst\\design\\html\\page.tpl',
      1 => 1464782521,
      2 => 'file',
    ),
    'bcd154d41235933679b1ff005029fd5fb45de6b8' => 
    array (
      0 => 'C:\\OpenServer\\domains\\rst\\design\\html\\layout.tpl',
      1 => 1464782521,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '6589574ed8fd77df26-81000400',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'config' => 0,
    'meta_title' => 0,
    'meta_description' => 0,
    'meta_keywords' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_574ed8fd8af637_34898938',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_574ed8fd8af637_34898938')) {function content_574ed8fd8af637_34898938($_smarty_tpl) {?><!DOCTYPE html>
<html>
<head>
	<base href="<?php echo $_smarty_tpl->tpl_vars['config']->value->root_url;?>
"/>
	<title><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['meta_title']->value, ENT_QUOTES, 'UTF-8', true);?>
</title>
	
	
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="description" content="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['meta_description']->value, ENT_QUOTES, 'UTF-8', true);?>
" />
	<meta name="keywords"    content="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['meta_keywords']->value, ENT_QUOTES, 'UTF-8', true);?>
" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1.0">
	
    <link href='https://fonts.googleapis.com/css?family=Marko+One' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Overlock:400,700' rel='stylesheet' type='text/css'>

    <link rel="stylesheet" type="text/css" href="/design/css/bootstrap.min.css" />
    <link rel="stylesheet" type="text/css" href="/design/css/vendor.css" />
    <link rel="stylesheet" type="text/css" href="/design/css/base.css" />
    <link rel="stylesheet" type="text/css" href="/design/css/additional.css" />
    <!--[if lt IE 9]>
    <link rel="stylesheet" type="text/css" href="/design/css/ie8-and-down.css" />
    <![endif]-->
    
</head>
<body>
    <div class="restaurant-minsk">
        <div class="wrapper">
            <div class="container container__mod">

                <?php echo $_smarty_tpl->getSubTemplate ('header.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>


                
    <div class="row">
        <div class="<?php if ($_smarty_tpl->tpl_vars['page']->value->image) {?>col-md-4<?php }?> col-xs-12">
            <?php if ($_smarty_tpl->tpl_vars['page']->value->url!=='aboutus') {?>
                <h3 class="page-title text-center _tt_upper"><?php echo $_smarty_tpl->tpl_vars['page']->value->header;?>
</h3>
            <?php }?>
            <div class="page-body text-center"><?php echo $_smarty_tpl->tpl_vars['page']->value->page_text;?>
</div>
        </div>
        <?php if ($_smarty_tpl->tpl_vars['page']->value->image) {?>
            <div class="col-md-8 col-xs-12">
                <img src="/files/pages/original/<?php echo $_smarty_tpl->tpl_vars['page']->value->image;?>
" alt="">
            </div>
        <?php }?>
    </div>


                <?php echo $_smarty_tpl->getSubTemplate ('footer.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

            </div>
        </div>
    </div>
    <?php echo '<script'; ?>
 src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.2/jquery-ui.min.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="/design/js/bootstrap.min.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="/design/js/mediaelement-and-player.min.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="/design/js/picturefill.min.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="/design/js/jquery.colorbox.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
>document.createElement('picture');<?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="/design/js/main.js"><?php echo '</script'; ?>
>
    <!--[if lt IE 10]>
    <?php echo '<script'; ?>
 type="text/javascript" src="/design/js/media.match.min.js"><?php echo '</script'; ?>
>
    <![endif]-->
    <!--[if IE]><?php echo '<script'; ?>
 src="/design/js/ie.js"><?php echo '</script'; ?>
><![endif]-->

    
</body>
</html><?php }} ?>
