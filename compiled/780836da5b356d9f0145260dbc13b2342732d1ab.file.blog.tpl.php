<?php /* Smarty version Smarty-3.1.21-dev, created on 2015-06-08 00:04:12
         compiled from "/Users/evgeniy/sites/awb/design/html/blog.tpl" */ ?>
<?php /*%%SmartyHeaderCode:4185604875566d3ec1fe9a1-49357790%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '780836da5b356d9f0145260dbc13b2342732d1ab' => 
    array (
      0 => '/Users/evgeniy/sites/awb/design/html/blog.tpl',
      1 => 1433274718,
      2 => 'file',
    ),
    '721d29d532c8456def5ae484ceb8ede5618804b4' => 
    array (
      0 => '/Users/evgeniy/sites/awb/design/html/layout.tpl',
      1 => 1433707626,
      2 => 'file',
    ),
    '20004da77ee302d1b36d5e9013e2a7be8a2503b1' => 
    array (
      0 => '/Users/evgeniy/sites/awb/design/html/blocks/default_header.tpl',
      1 => 1433090847,
      2 => 'file',
    ),
    'a303aff54530bd55655a6ba3cad6502b13efbe2b' => 
    array (
      0 => '/Users/evgeniy/sites/awb/design/html/blocks/twitter.tpl',
      1 => 1433151972,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '4185604875566d3ec1fe9a1-49357790',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_5566d3ec4500b1_57146765',
  'variables' => 
  array (
    'config' => 0,
    'meta_title' => 0,
    'meta_description' => 0,
    'meta_keywords' => 0,
    'product' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5566d3ec4500b1_57146765')) {function content_5566d3ec4500b1_57146765($_smarty_tpl) {?><?php if (!is_callable('smarty_modifier_date_format')) include '/Users/evgeniy/sites/awb/libs/plugins/modifier.date_format.php';
?><!DOCTYPE html>
<html>
<head>
	<base href="<?php echo $_smarty_tpl->tpl_vars['config']->value->root_url;?>
"/>
	<title><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['meta_title']->value, ENT_QUOTES, 'UTF-8', true);?>
</title>
	
	
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="description" content="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['meta_description']->value, ENT_QUOTES, 'UTF-8', true);?>
" />
	<meta name="keywords"    content="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['meta_keywords']->value, ENT_QUOTES, 'UTF-8', true);?>
" />
	
    <!--[if lt IE 9]>
    <?php echo '<script'; ?>
 src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"><?php echo '</script'; ?>
>
    <![endif]-->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300' rel='stylesheet' type='text/css'>
	<link href="design/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <link href="design/css/base.css" rel="stylesheet" type="text/css"/>
    <link href="design/css/non-responsive.css" rel="stylesheet" type="text/css"/>
    <link href="design/css/slick.css" rel="stylesheet" type="text/css"/>
    <link href="design/css/revo-settings.css" rel="stylesheet" type="text/css"/>
    <link href="design/css/revo-style.css" rel="stylesheet" type="text/css"/>
	<link href="favicon.ico" rel="icon"          type="image/x-icon"/>
	<link href="favicon.ico" rel="shortcut icon" type="image/x-icon"/>
    
</head>
<body>
    <?php echo $_smarty_tpl->getSubTemplate ('header.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>


    
    <?php /*  Call merged included template "blocks/default_header.tpl" */
$_tpl_stack[] = $_smarty_tpl;
 $_smarty_tpl = $_smarty_tpl->setupInlineSubTemplate('blocks/default_header.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0, '4185604875566d3ec1fe9a1-49357790');
content_5574b1cc8db594_59418028($_smarty_tpl);
$_smarty_tpl = array_pop($_tpl_stack); 
/*  End of included template "blocks/default_header.tpl" */?>

    <div class="container-fluid news-box">
        <div class="wrapper">
            <div class="row">
                <div class="col-md-8">
                    <?php if ($_smarty_tpl->tpl_vars['posts']->value) {?>
                        <?php  $_smarty_tpl->tpl_vars['post'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['post']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['posts']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['post']->key => $_smarty_tpl->tpl_vars['post']->value) {
$_smarty_tpl->tpl_vars['post']->_loop = true;
?>
                            <div class="news-item">
                                <?php if ($_smarty_tpl->tpl_vars['post']->value->image) {?>
                                <div class="news-item__image-box">
                                    <img class="news-item__image" src="files/news/<?php echo $_smarty_tpl->tpl_vars['post']->value->image;?>
" alt=""/>
                                    <div class="news-item__date">
                                        <div class="news-item__date-day"><?php echo smarty_modifier_date_format($_smarty_tpl->tpl_vars['post']->value->date,"%d");?>
</div>
                                        <div><?php echo smarty_modifier_date_format($_smarty_tpl->tpl_vars['post']->value->date,"%B");?>
</div>
                                    </div>
                                </div>
                                <?php }?>
                                <h2 class="text-upper news-item__title"><a href="news/<?php echo $_smarty_tpl->tpl_vars['post']->value->category_id;?>
/<?php echo $_smarty_tpl->tpl_vars['post']->value->url;?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['post']->value->name, ENT_QUOTES, 'UTF-8', true);?>
</a></h2>
                                <a href="news/<?php echo $_smarty_tpl->tpl_vars['post']->value->category_id;?>
" class="news-category"><?php echo $_smarty_tpl->tpl_vars['categoriesArray']->value[$_smarty_tpl->tpl_vars['post']->value->category_id];?>
</a>
                                <div>
                                    <div>
                                        <?php echo $_smarty_tpl->tpl_vars['post']->value->annotation;?>

                                    </div>
                                </div>
                                <?php if ($_smarty_tpl->tpl_vars['post']->value->text) {?>
                                    <div><?php echo $_smarty_tpl->tpl_vars['post']->value->text;?>
</div>
                                <?php }?>
                            </div>
                        <?php } ?>
                    <?php } else { ?>
                        В категории нет новостей
                    <?php }?>
                </div>
                <div class="col-md-4">
                    <?php /*  Call merged included template "blocks/twitter.tpl" */
$_tpl_stack[] = $_smarty_tpl;
 $_smarty_tpl = $_smarty_tpl->setupInlineSubTemplate('blocks/twitter.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0, '4185604875566d3ec1fe9a1-49357790');
content_5574b1cc92ba06_50930111($_smarty_tpl);
$_smarty_tpl = array_pop($_tpl_stack); 
/*  End of included template "blocks/twitter.tpl" */?>
                    <div class="sidebar-box green-separator__top mt20">
                        <div class="sidebar-box__wrapper">
                            <h2 class="text-upper">Важные новости</h2>
                            <ul class="list-unstyled">
                                <?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['get_posts'][0][0]->get_posts_plugin(array('var'=>'important_posts','limit'=>3,'important'=>true),$_smarty_tpl);?>

                                <?php if ($_smarty_tpl->tpl_vars['important_posts']->value) {?>
                                    <div class="important-news row">
                                        <?php  $_smarty_tpl->tpl_vars['post'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['post']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['important_posts']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['post']->key => $_smarty_tpl->tpl_vars['post']->value) {
$_smarty_tpl->tpl_vars['post']->_loop = true;
?>
                                            <div class="important-news_item clearfix">
                                                <?php if ($_smarty_tpl->tpl_vars['post']->value->image) {?>
                                                    <div class="col-xs-4">
                                                        <div class="important-news__image" style="background-image: url('<?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['resize_news'][0][0]->resize_modifier_news($_smarty_tpl->tpl_vars['post']->value->image,90,70);?>
');"></div>
                                                    </div>
                                                <?php }?>
                                                <div class="col-xs-8">
                                                    <p class="important-news__title mb10"><?php echo $_smarty_tpl->tpl_vars['post']->value->name;?>
</p>
                                                    <p class="important-news__date text-right"><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['date'][0][0]->date_modifier($_smarty_tpl->tpl_vars['post']->value->date);?>
</p>
                                                    <a class="text-right category-link" href="news/<?php echo $_smarty_tpl->tpl_vars['post']->value->category_id;?>
">Новости компании</a></p>
                                                </div>
                                            </div>
                                        <?php } ?>
                                    </div>
                                <?php }?>
                            </ul>
                        </div>
                    </div>
                    <div class="news-categories green-separator__top mt20">
                        <div class="news-categories__wrapper">
                            <h2 class="text-upper">Категории</h2>
                            <ul class="list-unstyled">
                                <?php  $_smarty_tpl->tpl_vars['cat'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['cat']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['news_categories']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['cat']->key => $_smarty_tpl->tpl_vars['cat']->value) {
$_smarty_tpl->tpl_vars['cat']->_loop = true;
?>
                                    <li class="news-categories__item">
                                        <a class="news-categories__link" href="news/<?php echo $_smarty_tpl->tpl_vars['cat']->value->url;?>
"><?php echo $_smarty_tpl->tpl_vars['cat']->value->name;?>
 (<?php echo $_smarty_tpl->tpl_vars['cat']->value->count;?>
)</a>
                                    </li>
                                <?php } ?>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        
    </div>



    <?php if (!$_smarty_tpl->tpl_vars['product']->value->id) {?>
        <?php echo $_smarty_tpl->getSubTemplate ('blocks/trust.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

    <?php }?>

    <?php echo $_smarty_tpl->getSubTemplate ('footer.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

    <div class="complete-dialog text-center"><p>Спасибо!</p><p>Ваша заявка принята!</p></div>

    <?php echo '<script'; ?>
 src="design/js/jquery-1.11.2.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="design/js/bootstrap.min.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="design/js/revolution.min.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="design/js/revo-tools.min.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="design/js/slick.min.js"><?php echo '</script'; ?>
>

    
        <?php echo '<script'; ?>
 type="text/javascript">
            var revApi;
            $(window).load(function(){
                initHeaderPosition();
            })

            //header position init
            function initHeaderPosition(){
                var win = $(window);
                var header = $('.navbar-fixed-top');
                function calcPosition(obj){
                    obj.stop().css({left: parseInt(obj.attr('data-top')) - win.scrollLeft()});
                    win.scroll(function(){
                        obj.stop().css({left: parseInt(obj.attr('data-top')) - win.scrollLeft()});
                    })
                }
                if(header.length){
                    header.attr('data-top',parseInt(header.css('left')));
                    calcPosition(header);
                }
            }
            $(document).ready(function() {
                $('.form__get-price').on('submit', function(e){
                    e.preventDefault();
                    var $self = $(this),
                        data = $self.serialize(),
                        popup = $('.complete-dialog');
                    $.ajax({
                        url: 'ajax/getPrice.php',
                        method: 'post',
                        data: data,
                        success: function(data){
                            $self.trigger('reset');
                            popup.show();
                            setTimeout(function(){
                                popup.fadeOut('fast');
                            }, 2000);
                        }
                    });
                });

                if($('#bottomSlider').revolution == undefined)
                    revslider_showDoubleJqueryError('#bottomSlider');
                else
                    revApi = $('#bottomSlider').show()
                        .revolution({
                            dottedOverlay:"none",
                            delay:6000,
                            startwidth:1170,
                            startheight:460,
                            hideThumbs:200,
                            thumbWidth:100,
                            thumbHeight:50,
                            thumbAmount:2,
                            navigationType:"none",
                            navigationArrows:"none",
                            navigationStyle:"round",
                            touchenabled:"off",
                            onHoverStop:"off",
                            keyboardNavigation:"off",
                            navigationHAlign:"center",
                            navigationVAlign:"bottom",
                            navigationHOffset:0,
                            navigationVOffset:20,
                            soloArrowLeftHalign:"left",
                            soloArrowLeftValign:"center",
                            soloArrowLeftHOffset:20,
                            soloArrowLeftVOffset:0,
                            soloArrowRightHalign:"right",
                            soloArrowRightValign:"center",
                            soloArrowRightHOffset:20,
                            soloArrowRightVOffset:0,
                            shadow:0,
                            fullWidth:"on",
                            fullScreen:"off",
                            spinner:"spinner0",
                            stopLoop:"off",
                            stopAfterLoops:-1,
                            stopAtSlide:-1,
                            shuffle:"off",
                            autoHeight:"on",
                            forceFullWidth:"on",
                            hideThumbsOnMobile:"off",
                            hideNavDelayOnMobile:1500,
                            hideBulletsOnMobile:"off",
                            hideArrowsOnMobile:"off",
                            hideThumbsUnderResolution:0,
                            hideSliderAtLimit:0,
                            hideCaptionAtLimit:0,
                            hideAllCaptionAtLilmit:0,
                            startWithSlide:0,
                            fullScreenOffsetContainer: ""
                        });

            });
        <?php echo '</script'; ?>
>
    

    
</body>
</html><?php }} ?>
<?php /* Smarty version Smarty-3.1.21-dev, created on 2015-06-08 00:04:12
         compiled from "/Users/evgeniy/sites/awb/design/html/blocks/default_header.tpl" */ ?>
<?php if ($_valid && !is_callable('content_5574b1cc8db594_59418028')) {function content_5574b1cc8db594_59418028($_smarty_tpl) {?><div class="container-fluid l11 default-header clearfix">
    <div class="default-header__inner"></div>
</div><?php }} ?>
<?php /* Smarty version Smarty-3.1.21-dev, created on 2015-06-08 00:04:12
         compiled from "/Users/evgeniy/sites/awb/design/html/blocks/twitter.tpl" */ ?>
<?php if ($_valid && !is_callable('content_5574b1cc92ba06_50930111')) {function content_5574b1cc92ba06_50930111($_smarty_tpl) {?><div class="twitter-box green-separator__top">
    <h2 class="twitter-title">Мы в Twitter</h2>
    <ul class="twitter-list list-unstyled">
        <?php  $_smarty_tpl->tpl_vars['tw'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['tw']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['tweets']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['tw']->key => $_smarty_tpl->tpl_vars['tw']->value) {
$_smarty_tpl->tpl_vars['tw']->_loop = true;
?>
            <li class="twitter-item clearfix">
                <a class="twitter-item__link" target="_blank" href="https://twitter.com/<?php echo $_smarty_tpl->tpl_vars['tw']->value->user->screen_name;?>
/status/<?php echo $_smarty_tpl->tpl_vars['tw']->value->id;?>
">
                    <?php echo $_smarty_tpl->tpl_vars['tw']->value->text;?>

                </a>
                <div class="mt10">
                    <a class="twitter-item__user text-green" href="https://twitter.com/<?php echo $_smarty_tpl->tpl_vars['tw']->value->user->screen_name;?>
">@<?php echo $_smarty_tpl->tpl_vars['tw']->value->user->screen_name;?>
</a>
                    <div class="twitter-date pull-right"><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['date'][0][0]->date_modifier($_smarty_tpl->tpl_vars['tw']->value->created_at);?>
</div>
                </div>
            </li>
        <?php } ?>
    </ul>
</div><?php }} ?>
