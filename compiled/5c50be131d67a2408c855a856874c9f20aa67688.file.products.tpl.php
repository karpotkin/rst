<?php /* Smarty version Smarty-3.1.21-dev, created on 2015-07-02 14:11:55
         compiled from "/Users/evgeniy/sites/awb/design/html/products.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1159183340556514c40eb4c4-06302686%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '5c50be131d67a2408c855a856874c9f20aa67688' => 
    array (
      0 => '/Users/evgeniy/sites/awb/design/html/products.tpl',
      1 => 1435822888,
      2 => 'file',
    ),
    '721d29d532c8456def5ae484ceb8ede5618804b4' => 
    array (
      0 => '/Users/evgeniy/sites/awb/design/html/layout.tpl',
      1 => 1434563870,
      2 => 'file',
    ),
    '20004da77ee302d1b36d5e9013e2a7be8a2503b1' => 
    array (
      0 => '/Users/evgeniy/sites/awb/design/html/blocks/default_header.tpl',
      1 => 1433090847,
      2 => 'file',
    ),
    '4d573270e98f218c5a643d274e2acc6ead026e41' => 
    array (
      0 => '/Users/evgeniy/sites/awb/design/html/blocks/countries.tpl',
      1 => 1435821483,
      2 => 'file',
    ),
    '64ec18fd1c3b0ce30fd7fee6c9beb1b8c41926ba' => 
    array (
      0 => '/Users/evgeniy/sites/awb/design/html/blocks/categories.tpl',
      1 => 1435835483,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1159183340556514c40eb4c4-06302686',
  'function' => 
  array (
    'categories_tree' => 
    array (
      'parameter' => 
      array (
        'level' => 0,
      ),
      'compiled' => '',
    ),
  ),
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_556514c442d291_64495504',
  'variables' => 
  array (
    'config' => 0,
    'meta_title' => 0,
    'meta_description' => 0,
    'meta_keywords' => 0,
    'product' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_556514c442d291_64495504')) {function content_556514c442d291_64495504($_smarty_tpl) {?><!DOCTYPE html>
<html>
<head>
	<base href="<?php echo $_smarty_tpl->tpl_vars['config']->value->root_url;?>
"/>
	<title><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['meta_title']->value, ENT_QUOTES, 'UTF-8', true);?>
</title>
	
	
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="description" content="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['meta_description']->value, ENT_QUOTES, 'UTF-8', true);?>
" />
	<meta name="keywords"    content="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['meta_keywords']->value, ENT_QUOTES, 'UTF-8', true);?>
" />
	
    <!--[if lt IE 9]>
    <?php echo '<script'; ?>
 src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"><?php echo '</script'; ?>
>
    <![endif]-->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300' rel='stylesheet' type='text/css'>
	<link href="design/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <link href="design/css/base.css" rel="stylesheet" type="text/css"/>
    <link href="design/css/non-responsive.css" rel="stylesheet" type="text/css"/>
    <link href="design/css/slick.css" rel="stylesheet" type="text/css"/>
    <link href="design/css/revo-settings.css" rel="stylesheet" type="text/css"/>
    <link href="design/css/revo-style.css" rel="stylesheet" type="text/css"/>
	<link href="favicon.ico" rel="icon"          type="image/x-icon"/>
	<link href="favicon.ico" rel="shortcut icon" type="image/x-icon"/>
    
</head>
<body>
    <?php echo $_smarty_tpl->getSubTemplate ('header.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>


    
    
    <?php /*  Call merged included template "blocks/default_header.tpl" */
$_tpl_stack[] = $_smarty_tpl;
 $_smarty_tpl = $_smarty_tpl->setupInlineSubTemplate('blocks/default_header.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0, '1159183340556514c40eb4c4-06302686');
content_55951c7bdfd585_21240681($_smarty_tpl);
$_smarty_tpl = array_pop($_tpl_stack); 
/*  End of included template "blocks/default_header.tpl" */?>

    <div class="container-fluid products-box">
        <h3 class="text-center">Наша продукция</h3>
        <div class="row">
            <div class="col-md-2">
                <div class="box">
                    <?php /*  Call merged included template "blocks/countries.tpl" */
$_tpl_stack[] = $_smarty_tpl;
 $_smarty_tpl = $_smarty_tpl->setupInlineSubTemplate('blocks/countries.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0, '1159183340556514c40eb4c4-06302686');
content_55951c7be03685_50829485($_smarty_tpl);
$_smarty_tpl = array_pop($_tpl_stack); 
/*  End of included template "blocks/countries.tpl" */?>
                </div>
                <div class="box mt20">
                    <?php /*  Call merged included template "blocks/categories.tpl" */
$_tpl_stack[] = $_smarty_tpl;
 $_smarty_tpl = $_smarty_tpl->setupInlineSubTemplate('blocks/categories.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0, '1159183340556514c40eb4c4-06302686');
content_55951c7be3a687_71400340($_smarty_tpl);
$_smarty_tpl = array_pop($_tpl_stack); 
/*  End of included template "blocks/categories.tpl" */?>
                </div>
            </div>
            <div class="col-md-10">
                <div class="products__list">
                    <?php if ($_smarty_tpl->tpl_vars['products']->value) {?>
                        <?php  $_smarty_tpl->tpl_vars['p'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['p']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['products']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['p']->key => $_smarty_tpl->tpl_vars['p']->value) {
$_smarty_tpl->tpl_vars['p']->_loop = true;
?>
                            <div class="product-item text-center">
                                <a href="products/<?php echo $_smarty_tpl->tpl_vars['p']->value->url;?>
" class="text-bold product-name"><?php echo $_smarty_tpl->tpl_vars['p']->value->name;?>
</a>
                                <a href="" class="product-country"><?php echo $_smarty_tpl->tpl_vars['p']->value->brand;?>
</a>
                                <a href="products/<?php echo $_smarty_tpl->tpl_vars['p']->value->url;?>
" class="product-image mt10" style="background-image: url(<?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['resize'][0][0]->resize_modifier($_smarty_tpl->tpl_vars['p']->value->image->filename,250,250);?>
);"></a>
                            </div>
                        <?php } ?>
                    <?php } else { ?>
                        <h2>Ничего не найдено</h2>
                    <?php }?>
                </div>
            </div>
        </div>
    </div>


    <?php if (!$_smarty_tpl->tpl_vars['product']->value->id) {?>
        <?php echo $_smarty_tpl->getSubTemplate ('blocks/trust.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

    <?php }?>

    <?php echo $_smarty_tpl->getSubTemplate ('footer.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

    <div class="complete-dialog text-center"><p>Спасибо!</p><p data-default="Ваша заявка принята!" class="complete-dialog-text">Ваша заявка принята!</p></div>

    <?php echo '<script'; ?>
 src="design/js/jquery-1.11.2.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="design/js/bootstrap.min.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="design/js/revolution.min.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="design/js/revo-tools.min.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="design/js/slick.min.js"><?php echo '</script'; ?>
>

    
        <?php echo '<script'; ?>
 type="text/javascript">
            var revApi;
            $(window).load(function(){
                initHeaderPosition();
            })

            //header position init
            function initHeaderPosition(){
                var win = $(window);
                var header = $('.navbar-fixed-top');
                function calcPosition(obj){
                    obj.stop().css({left: parseInt(obj.attr('data-top')) - win.scrollLeft()});
                    win.scroll(function(){
                        obj.stop().css({left: parseInt(obj.attr('data-top')) - win.scrollLeft()});
                    })
                }
                if(header.length){
                    header.attr('data-top',parseInt(header.css('left')));
                    calcPosition(header);
                }
            }
            $(document).ready(function() {
                $('.form__get-price, .subscription-form').on('submit', function(e){
                    e.preventDefault();
                    var $self = $(this),
                        data = $self.serialize(),
                        popup = $('.complete-dialog');

                    $.ajax({
                        url: $self.attr('action'),
                        method: 'post',
                        data: data,
                        success: function(data){
                            $self.trigger('reset');
                            popup.show();
                            setTimeout(function(){
                                popup.fadeOut('fast');
                            }, 2000);
                        }
                    });
                });

                if($('#bottomSlider').revolution == undefined)
                    revslider_showDoubleJqueryError('#bottomSlider');
                else
                    revApi = $('#bottomSlider').show()
                        .revolution({
                            dottedOverlay:"none",
                            delay:6000,
                            startwidth:1170,
                            startheight:432,
                            hideThumbs:200,
                            thumbWidth:100,
                            thumbHeight:50,
                            thumbAmount:2,
                            navigationType:"none",
                            navigationArrows:"none",
                            navigationStyle:"round",
                            touchenabled:"off",
                            onHoverStop:"off",
                            keyboardNavigation:"off",
                            navigationHAlign:"center",
                            navigationVAlign:"bottom",
                            navigationHOffset:0,
                            navigationVOffset:20,
                            soloArrowLeftHalign:"left",
                            soloArrowLeftValign:"center",
                            soloArrowLeftHOffset:20,
                            soloArrowLeftVOffset:0,
                            soloArrowRightHalign:"right",
                            soloArrowRightValign:"center",
                            soloArrowRightHOffset:20,
                            soloArrowRightVOffset:0,
                            shadow:0,
                            fullWidth:"on",
                            fullScreen:"off",
                            spinner:"spinner0",
                            stopLoop:"off",
                            stopAfterLoops:-1,
                            stopAtSlide:-1,
                            shuffle:"off",
                            autoHeight:"on",
                            forceFullWidth:"on",
                            hideThumbsOnMobile:"off",
                            hideNavDelayOnMobile:1500,
                            hideBulletsOnMobile:"off",
                            hideArrowsOnMobile:"off",
                            hideThumbsUnderResolution:0,
                            hideSliderAtLimit:0,
                            hideCaptionAtLimit:0,
                            hideAllCaptionAtLilmit:0,
                            startWithSlide:0,
                            fullScreenOffsetContainer: ""
                        });

            });
        <?php echo '</script'; ?>
>
    

    
</body>
</html><?php }} ?>
<?php /* Smarty version Smarty-3.1.21-dev, created on 2015-07-02 14:11:55
         compiled from "/Users/evgeniy/sites/awb/design/html/blocks/default_header.tpl" */ ?>
<?php if ($_valid && !is_callable('content_55951c7bdfd585_21240681')) {function content_55951c7bdfd585_21240681($_smarty_tpl) {?><div class="container-fluid l11 default-header clearfix">
    <div class="default-header__inner"></div>
</div><?php }} ?>
<?php /* Smarty version Smarty-3.1.21-dev, created on 2015-07-02 14:11:55
         compiled from "/Users/evgeniy/sites/awb/design/html/blocks/countries.tpl" */ ?>
<?php if ($_valid && !is_callable('content_55951c7be03685_50829485')) {function content_55951c7be03685_50829485($_smarty_tpl) {?><div class="products__countries">
    <div class="text-bold">Выберите страну производителя:</div>
    <ul class="products__countries-list list-unstyled">
        <li class="products__countries-item <?php if (!$_smarty_tpl->tpl_vars['brand']->value->id) {?>products__countries-item_active<?php }?>">
            <a class="products__countries-item_link" href="/products">Все</a>
        </li>
        <?php  $_smarty_tpl->tpl_vars['c'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['c']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['countries']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['c']->key => $_smarty_tpl->tpl_vars['c']->value) {
$_smarty_tpl->tpl_vars['c']->_loop = true;
?>
            <li class="products__countries-item <?php if ($_smarty_tpl->tpl_vars['c']->value->id==$_smarty_tpl->tpl_vars['brand']->value->id) {?>products__countries-item_active<?php }?>">
                <a class="products__countries-item_link" href="/countries/<?php if ($_smarty_tpl->tpl_vars['category']->value->url) {
echo $_smarty_tpl->tpl_vars['category']->value->url;?>
/<?php }
echo $_smarty_tpl->tpl_vars['c']->value->url;?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['c']->value->name, ENT_QUOTES, 'UTF-8', true);?>
</a>
            </li>
        <?php } ?>
    </ul>
</div><?php }} ?>
<?php /* Smarty version Smarty-3.1.21-dev, created on 2015-07-02 14:11:55
         compiled from "/Users/evgeniy/sites/awb/design/html/blocks/categories.tpl" */ ?>
<?php if ($_valid && !is_callable('content_55951c7be3a687_71400340')) {function content_55951c7be3a687_71400340($_smarty_tpl) {?><div class="products__categories">
    <div class="text-bold">Выберите тип продукта:</div>
    <?php if (!function_exists('smarty_template_function_categories_tree')) {
    function smarty_template_function_categories_tree($_smarty_tpl,$params) {
    $saved_tpl_vars = $_smarty_tpl->tpl_vars;
    foreach ($_smarty_tpl->smarty->template_functions['categories_tree']['parameter'] as $key => $value) {$_smarty_tpl->tpl_vars[$key] = new Smarty_variable($value);};
    foreach ($params as $key => $value) {$_smarty_tpl->tpl_vars[$key] = new Smarty_variable($value);}?>
        <?php if ($_smarty_tpl->tpl_vars['categories']->value) {?>
            <ul class="products__categories-list list-unstyled">
                <?php if ($_smarty_tpl->tpl_vars['level']->value==0) {?>
                    <li class="products__categories-item <?php if (!$_smarty_tpl->tpl_vars['category']->value->id) {?>products__categories-item_active<?php }?>">
                        <a class="products__categories-item_link" href="/products">Все</a>
                    </li>
                <?php }?>
                <?php  $_smarty_tpl->tpl_vars['cat'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['cat']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['categories']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['cat']->key => $_smarty_tpl->tpl_vars['cat']->value) {
$_smarty_tpl->tpl_vars['cat']->_loop = true;
?>
                    <?php if ($_smarty_tpl->tpl_vars['cat']->value->visible) {?>
                        <li class="products__categories-item <?php if ($_smarty_tpl->tpl_vars['category']->value->id==$_smarty_tpl->tpl_vars['cat']->value->id) {?>products__categories-item_active<?php }?>">
                            <a class="products__categories-item_link" href="/catalog/<?php echo $_smarty_tpl->tpl_vars['cat']->value->url;?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['cat']->value->name, ENT_QUOTES, 'UTF-8', true);?>
</a>
                            <?php smarty_template_function_categories_tree($_smarty_tpl,array('categories'=>$_smarty_tpl->tpl_vars['cat']->value->subcategories,'level'=>$_smarty_tpl->tpl_vars['level']->value+1));?>

                        </li>
                    <?php }?>
                <?php } ?>
            </ul>
        <?php }?>
    <?php $_smarty_tpl->tpl_vars = $saved_tpl_vars;
foreach (Smarty::$global_tpl_vars as $key => $value) if(!isset($_smarty_tpl->tpl_vars[$key])) $_smarty_tpl->tpl_vars[$key] = $value;}}?>

    <?php smarty_template_function_categories_tree($_smarty_tpl,array('categories'=>$_smarty_tpl->tpl_vars['categories']->value));?>

</div><?php }} ?>
