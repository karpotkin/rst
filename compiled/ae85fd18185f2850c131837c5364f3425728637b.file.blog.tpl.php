<?php /* Smarty version Smarty-3.1.21-dev, created on 2016-03-15 02:41:51
         compiled from "/Users/evgeniy/sites/minsk-tokyo.dev/design/html/blog.tpl" */ ?>
<?php /*%%SmartyHeaderCode:211123925756e73d72549500-80170691%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'ae85fd18185f2850c131837c5364f3425728637b' => 
    array (
      0 => '/Users/evgeniy/sites/minsk-tokyo.dev/design/html/blog.tpl',
      1 => 1457995925,
      2 => 'file',
    ),
    'd99d70178b47531c5609a6ba9b17ff4ed22fd9ec' => 
    array (
      0 => '/Users/evgeniy/sites/minsk-tokyo.dev/design/html/layout.tpl',
      1 => 1457998696,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '211123925756e73d72549500-80170691',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_56e73d7260ef43_49323976',
  'variables' => 
  array (
    'config' => 0,
    'meta_title' => 0,
    'meta_description' => 0,
    'meta_keywords' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_56e73d7260ef43_49323976')) {function content_56e73d7260ef43_49323976($_smarty_tpl) {?><?php if (!is_callable('smarty_modifier_date_format')) include '/Users/evgeniy/sites/minsk-tokyo.dev/libs/plugins/modifier.date_format.php';
?><!DOCTYPE html>
<html>
<head>
	<base href="<?php echo $_smarty_tpl->tpl_vars['config']->value->root_url;?>
"/>
	<title><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['meta_title']->value, ENT_QUOTES, 'UTF-8', true);?>
</title>
	
	
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="description" content="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['meta_description']->value, ENT_QUOTES, 'UTF-8', true);?>
" />
	<meta name="keywords"    content="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['meta_keywords']->value, ENT_QUOTES, 'UTF-8', true);?>
" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1.0">
	
    <link href='https://fonts.googleapis.com/css?family=Marko+One' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Overlock:400,700' rel='stylesheet' type='text/css'>

    <link rel="stylesheet" type="text/css" href="/design/css/bootstrap.min.css" />
    <link rel="stylesheet" type="text/css" href="/design/css/vendor.css" />
    <link rel="stylesheet" type="text/css" href="/design/css/base.css" />
    <link rel="stylesheet" type="text/css" href="/design/css/additional.css" />
    <!--[if lt IE 9]>
    <link rel="stylesheet" type="text/css" href="/design/css/ie8-and-down.css" />
    <![endif]-->
    
</head>
<body>
    <div class="restaurant-minsk">
        <div class="wrapper">
            <div class="container container__mod">

                <?php echo $_smarty_tpl->getSubTemplate ('header.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>


                

    <h3 class="page-title text-center _tt_upper"><?php echo $_smarty_tpl->tpl_vars['page']->value->header;?>
</h3>

    <?php if ($_smarty_tpl->tpl_vars['posts']->value) {?>
        <?php  $_smarty_tpl->tpl_vars['post'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['post']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['posts']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['post']->key => $_smarty_tpl->tpl_vars['post']->value) {
$_smarty_tpl->tpl_vars['post']->_loop = true;
?>
            <div class="news-list__item">
                <h4 class="page-title news-list__item-title"><?php echo smarty_modifier_date_format($_smarty_tpl->tpl_vars['post']->value->date,"%d/%m/%Y");?>
 - <?php echo $_smarty_tpl->tpl_vars['post']->value->name;?>
</h4>
                <div class="news-list__item-text"><?php echo $_smarty_tpl->tpl_vars['post']->value->annotation;?>
</div>
            </div>
        <?php } ?>
    <?php } else { ?>
        В категории нет новостей
    <?php }?>



                <?php echo $_smarty_tpl->getSubTemplate ('footer.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

            </div>
        </div>
    </div>
    <?php echo '<script'; ?>
 src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.2/jquery-ui.min.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="/design/js/bootstrap.min.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="/design/js/mediaelement-and-player.min.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="/design/js/picturefill.min.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="/design/js/jquery.colorbox.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
>document.createElement('picture');<?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="/design/js/main.js"><?php echo '</script'; ?>
>
    <!--[if lt IE 10]>
    <?php echo '<script'; ?>
 type="text/javascript" src="/design/js/media.match.min.js"><?php echo '</script'; ?>
>
    <![endif]-->
    <!--[if IE]><?php echo '<script'; ?>
 src="/design/js/ie.js"><?php echo '</script'; ?>
><![endif]-->

    
</body>
</html><?php }} ?>
