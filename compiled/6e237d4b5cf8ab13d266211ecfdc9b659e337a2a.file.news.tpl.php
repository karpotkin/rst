<?php /* Smarty version Smarty-3.1.21-dev, created on 2015-05-29 01:04:35
         compiled from "/Users/evgeniy/sites/awb/design/html/blocks/news.tpl" */ ?>
<?php /*%%SmartyHeaderCode:4487819675564c7a32deec8-33500498%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '6e237d4b5cf8ab13d266211ecfdc9b659e337a2a' => 
    array (
      0 => '/Users/evgeniy/sites/awb/design/html/blocks/news.tpl',
      1 => 1432850649,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '4487819675564c7a32deec8-33500498',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_5564c7a32eba14_71373567',
  'variables' => 
  array (
    'last_posts' => 0,
    'post' => 0,
    'cnt' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5564c7a32eba14_71373567')) {function content_5564c7a32eba14_71373567($_smarty_tpl) {?><div id="mainNews" class="col-md-6 carousel slide carousel-fade" data-ride="carousel" data-interval="false">
    <h3>Новости с наших плантаций</h3>
    <div class="row">
        <?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['get_posts'][0][0]->get_posts_plugin(array('var'=>'last_posts','limit'=>3),$_smarty_tpl);?>

        <?php if ($_smarty_tpl->tpl_vars['last_posts']->value) {?>
            <div class="carousel-inner">
                <?php  $_smarty_tpl->tpl_vars['post'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['post']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['last_posts']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
 $_smarty_tpl->tpl_vars['post']->index=-1;
foreach ($_from as $_smarty_tpl->tpl_vars['post']->key => $_smarty_tpl->tpl_vars['post']->value) {
$_smarty_tpl->tpl_vars['post']->_loop = true;
 $_smarty_tpl->tpl_vars['post']->index++;
 $_smarty_tpl->tpl_vars['post']->first = $_smarty_tpl->tpl_vars['post']->index === 0;
?>
                    <div class="main__news-item item <?php if ($_smarty_tpl->tpl_vars['post']->first) {?>active<?php }?>">
                        <div class="col-xs-4">
                            <img src="design/images/apples.png">
                        </div>
                        <div class="col-xs-8 news">
                            <p class="main__news-carousel-title"><?php echo $_smarty_tpl->tpl_vars['post']->value->name;?>
</p>
                            <div class="main__news-carousel-description"><?php echo $_smarty_tpl->tpl_vars['post']->value->annotation;?>
</div>
                            <p class="date"><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['date'][0][0]->date_modifier($_smarty_tpl->tpl_vars['post']->value->date);?>
</p>
                            <p class="main__news-carousel-category category"><a class="main__news-carousel-category_link" href="news">Новости компании</a></p>
                        </div>
                    </div>
                <?php } ?>
            </div>
            <ol class="carousel-indicators main__news-carousel_indicator">
                <?php $_smarty_tpl->tpl_vars['cnt'] = new Smarty_Variable;$_smarty_tpl->tpl_vars['cnt']->step = 1;$_smarty_tpl->tpl_vars['cnt']->total = (int) ceil(($_smarty_tpl->tpl_vars['cnt']->step > 0 ? count($_smarty_tpl->tpl_vars['last_posts']->value)-1+1 - (0) : 0-(count($_smarty_tpl->tpl_vars['last_posts']->value)-1)+1)/abs($_smarty_tpl->tpl_vars['cnt']->step));
if ($_smarty_tpl->tpl_vars['cnt']->total > 0) {
for ($_smarty_tpl->tpl_vars['cnt']->value = 0, $_smarty_tpl->tpl_vars['cnt']->iteration = 1;$_smarty_tpl->tpl_vars['cnt']->iteration <= $_smarty_tpl->tpl_vars['cnt']->total;$_smarty_tpl->tpl_vars['cnt']->value += $_smarty_tpl->tpl_vars['cnt']->step, $_smarty_tpl->tpl_vars['cnt']->iteration++) {
$_smarty_tpl->tpl_vars['cnt']->first = $_smarty_tpl->tpl_vars['cnt']->iteration == 1;$_smarty_tpl->tpl_vars['cnt']->last = $_smarty_tpl->tpl_vars['cnt']->iteration == $_smarty_tpl->tpl_vars['cnt']->total;?>
                    <li data-target="#mainNews" data-slide-to="<?php echo $_smarty_tpl->tpl_vars['cnt']->value;?>
" <?php if ($_smarty_tpl->tpl_vars['cnt']->value==0) {?>class="active"<?php }?>></li>
                <?php }} ?>
            </ol>
        <?php }?>
    </div>
</div><?php }} ?>
