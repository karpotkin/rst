<?php /* Smarty version Smarty-3.1.21-dev, created on 2015-06-07 22:49:56
         compiled from "/Users/evgeniy/sites/awb/design/html/sliders/bottom.tpl" */ ?>
<?php /*%%SmartyHeaderCode:15302290905564c7a32ccd82-90631020%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '6f91dc85879c3d201da3917ef352b53a143cda27' => 
    array (
      0 => '/Users/evgeniy/sites/awb/design/html/sliders/bottom.tpl',
      1 => 1433509861,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '15302290905564c7a32ccd82-90631020',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_5564c7a32d2a29_51312131',
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5564c7a32d2a29_51312131')) {function content_5564c7a32d2a29_51312131($_smarty_tpl) {?><!-- start bottom slider -->
<div class="rev_slider_wrapper fullwidthbanner-container bottom-slider">
    <div id="bottomSlider" class="rev_slider fullwidthabanner bottom-slider__inner">
        <ul>
            <li data-transition="random" data-slotamount="7" data-masterspeed="300" >
                <!-- MAIN IMAGE -->
                <img src="design/images/transparent.png"  alt=""  data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat">
                <!-- LAYERS -->
                <!-- LAYER NR. 1 -->
                <div class="tp-caption sft"
                     data-x="-155"
                     data-y="-49"
                     data-speed="600"
                     data-start="500"
                     data-easing="Power3.easeInOut"
                     data-splitin="none"
                     data-splitout="none"
                     data-elementdelay="0.1"
                     data-endelementdelay="0.1"
                     data-endspeed="300"
                     style="z-index: 2;">
                    <img src="design/images/slider/left_001.png" alt="">
                </div>

                <!-- LAYER NR. 2 -->
                <div class="tp-caption sfb"
                     data-x="right" data-hoffset="150"
                     data-y="center" data-voffset="-3"
                     data-speed="600"
                     data-start="800"
                     data-easing="Power3.easeInOut"
                     data-splitin="none"
                     data-splitout="none"
                     data-elementdelay="0.1"
                     data-endelementdelay="0.1"
                     data-endspeed="300"
                     style="z-index: 3;">
                    <img src="design/images/slider/right_001.png" alt="">
                </div>
            </li>
            <li data-transition="random" data-slotamount="7" data-masterspeed="300" >
                <!-- MAIN IMAGE -->
                <img src="design/images/transparent.png" alt="" data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat">
                <!-- LAYERS -->

                <!-- LAYER NR. 1 -->
                <div class="tp-caption sft stt"
                     data-x="200"
                     data-y="-50"
                     data-speed="900"
                     data-start="1100"
                     data-easing="Power0.easeInOut"
                     data-splitin="none"
                     data-splitout="none"
                     data-elementdelay="0.1"
                     data-endelementdelay="0.1"
                     data-endspeed="300"
                     style="z-index: 2;">
                    <img src="design/images/slider/right_002.png" alt="" data-ww="1086" data-hh="550">
                </div>

                <!-- LAYER NR. 2 -->
                <div class="tp-caption sft"
                     data-x="-190"
                     data-y="-50"
                     data-speed="900"
                     data-start="1400"
                     data-easing="Power3.easeInOut"
                     data-splitin="none"
                     data-splitout="none"
                     data-elementdelay="0.2"
                     data-endelementdelay="0.1"
                     data-endspeed="300"
                     style="z-index: 3;">
                    <img src="design/images/slider/left_002.png" alt="">
                </div>
            </li>
            <li data-transition="random" data-slotamount="7" data-masterspeed="300" >
                <img src="design/images/scope/004.jpg" alt="" data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat">
            </li>
        </ul>
        <div class="tp-bannertimer"></div>
    </div>
</div>
<!-- end bottom slider --><?php }} ?>
