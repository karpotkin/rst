<?php /* Smarty version Smarty-3.1.21-dev, created on 2016-06-01 15:08:20
         compiled from "C:\OpenServer\domains\rst\design\html\blocks\menu.tpl" */ ?>
<?php /*%%SmartyHeaderCode:21533574ed0345afe44-71156469%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '0f1e2164df22fb80957671fc2d3a04540e25d6c4' => 
    array (
      0 => 'C:\\OpenServer\\domains\\rst\\design\\html\\blocks\\menu.tpl',
      1 => 1464782521,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '21533574ed0345afe44-71156469',
  'function' => 
  array (
    'pages_menu' => 
    array (
      'parameter' => 
      array (
        'level' => 0,
      ),
      'compiled' => '',
    ),
  ),
  'variables' => 
  array (
    'pages' => 0,
    'level' => 0,
    'p' => 0,
    'page' => 0,
  ),
  'has_nocache_code' => 0,
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_574ed03462b142_63599201',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_574ed03462b142_63599201')) {function content_574ed03462b142_63599201($_smarty_tpl) {?><div class="main-menu">
    <?php if (!function_exists('smarty_template_function_pages_menu')) {
    function smarty_template_function_pages_menu($_smarty_tpl,$params) {
    $saved_tpl_vars = $_smarty_tpl->tpl_vars;
    foreach ($_smarty_tpl->smarty->template_functions['pages_menu']['parameter'] as $key => $value) {$_smarty_tpl->tpl_vars[$key] = new Smarty_variable($value);};
    foreach ($params as $key => $value) {$_smarty_tpl->tpl_vars[$key] = new Smarty_variable($value);}?>
        <?php if ($_smarty_tpl->tpl_vars['pages']->value) {?>
            <ul class="<?php if ($_smarty_tpl->tpl_vars['level']->value==0) {?>nav navbar-nav main-menu__list<?php } else { ?>dropdown-menu<?php }?>">
                <?php  $_smarty_tpl->tpl_vars['p'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['p']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['pages']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['p']->key => $_smarty_tpl->tpl_vars['p']->value) {
$_smarty_tpl->tpl_vars['p']->_loop = true;
?>
                    <?php if ($_smarty_tpl->tpl_vars['p']->value->menu_id==2) {?>
                        <?php if ($_smarty_tpl->tpl_vars['p']->value->subcategories) {?>
                            <li role="presentation" class="main-menu__item dropdown<?php if ($_smarty_tpl->tpl_vars['page']->value->id==$_smarty_tpl->tpl_vars['p']->value->id) {?> active<?php }?>">
                                <a class="<?php if ($_smarty_tpl->tpl_vars['level']->value==0) {?>_tt_upper <?php }?>main-menu__item-link dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="#">
                                    <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['p']->value->name, ENT_QUOTES, 'UTF-8', true);?>

                                    <span class="main-menu__item-caret"></span>
                                </a>
                                <?php smarty_template_function_pages_menu($_smarty_tpl,array('pages'=>$_smarty_tpl->tpl_vars['p']->value->subcategories,'level'=>$_smarty_tpl->tpl_vars['level']->value+1));?>

                            </li>
                        <?php } else { ?>
                            <li role="presentation" class="main-menu__item<?php if ($_smarty_tpl->tpl_vars['page']->value->id==$_smarty_tpl->tpl_vars['p']->value->id) {?> active<?php }?>">
                                <a class="<?php if ($_smarty_tpl->tpl_vars['level']->value==0) {?>_tt_upper <?php }?>main-menu__item-link" href="<?php echo $_smarty_tpl->tpl_vars['p']->value->url;?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['p']->value->name, ENT_QUOTES, 'UTF-8', true);?>
</a>
                            </li>
                        <?php }?>
                    <?php }?>
                <?php } ?>
            </ul>
        <?php }?>
    <?php $_smarty_tpl->tpl_vars = $saved_tpl_vars;
foreach (Smarty::$global_tpl_vars as $key => $value) if(!isset($_smarty_tpl->tpl_vars[$key])) $_smarty_tpl->tpl_vars[$key] = $value;}}?>

    <?php smarty_template_function_pages_menu($_smarty_tpl,array('pages'=>$_smarty_tpl->tpl_vars['pages']->value));?>

</div><?php }} ?>
