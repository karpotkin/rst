<?php /* Smarty version Smarty-3.1.21-dev, created on 2016-06-09 09:30:35
         compiled from "/Users/evgeniy/sites/minsk-tokyo.dev/design/html/footer.tpl" */ ?>
<?php /*%%SmartyHeaderCode:141484581056e3321f528b34-02051189%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '5876a2ec0c908d4518b108ac73d6e01277607f72' => 
    array (
      0 => '/Users/evgeniy/sites/minsk-tokyo.dev/design/html/footer.tpl',
      1 => 1465453834,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '141484581056e3321f528b34-02051189',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_56e3321f57fde9_32814882',
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_56e3321f57fde9_32814882')) {function content_56e3321f57fde9_32814882($_smarty_tpl) {?><div class="footer">
    <div class="navbar-collapse collapse footer-menu">
        <ul class="nav navbar-nav footer-menu__list">
            <li class="footer-menu__item">
                <a class="_tt_upper footer-menu__item-link" href="#">Company</a>
            </li>
            <li class="footer-menu__item active">
                <a class="_tt_upper footer-menu__item-link" href="#">Contact</a>
            </li>
            <li class="footer-menu__item active">
                <a class="_tt_upper footer-menu__item-link" href="#">Credits</a>
            </li>
        </ul>
        <ul class="list-unstyled footer-menu__social text-center">
            <li class="footer-menu__social-item">
                <a class="_tt_upper footer-menu__social-item-link" href="https://www.facebook.com/pages/%E3%83%99%E3%83%A9%E3%83%AB%E3%83%BC%E3%82%B7%E3%81%AE%E5%AE%B6%E5%BA%AD%E6%96%99%E7%90%86-%E3%83%9F%E3%83%B3%E3%82%B9%E3%82%AF%E3%81%AE%E5%8F%B0%E6%89%80/145609245474795">
                    <i class="fa fa-facebook"></i>
                </a>
            </li>
            <li class="footer-menu__social-item">
                <a class="_tt_upper footer-menu__social-item-link" href="#">
                    <i class="fa fa-youtube"></i>
                </a>
            </li>
        </ul>
    </div>
</div><?php }} ?>
