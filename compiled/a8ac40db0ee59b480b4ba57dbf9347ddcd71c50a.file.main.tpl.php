<?php /* Smarty version Smarty-3.1.21-dev, created on 2015-06-17 21:01:47
         compiled from "/Users/evgeniy/sites/awb/design/html/main.tpl" */ ?>
<?php /*%%SmartyHeaderCode:12463351695564bd11935865-33626550%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'a8ac40db0ee59b480b4ba57dbf9347ddcd71c50a' => 
    array (
      0 => '/Users/evgeniy/sites/awb/design/html/main.tpl',
      1 => 1433707824,
      2 => 'file',
    ),
    '721d29d532c8456def5ae484ceb8ede5618804b4' => 
    array (
      0 => '/Users/evgeniy/sites/awb/design/html/layout.tpl',
      1 => 1434563870,
      2 => 'file',
    ),
    '73a398ece01e870d385602ca43afd39d40c3e6a7' => 
    array (
      0 => '/Users/evgeniy/sites/awb/design/html/sliders/top.tpl',
      1 => 1433418307,
      2 => 'file',
    ),
    '6f91dc85879c3d201da3917ef352b53a143cda27' => 
    array (
      0 => '/Users/evgeniy/sites/awb/design/html/sliders/bottom.tpl',
      1 => 1434530022,
      2 => 'file',
    ),
    '6e237d4b5cf8ab13d266211ecfdc9b659e337a2a' => 
    array (
      0 => '/Users/evgeniy/sites/awb/design/html/blocks/news.tpl',
      1 => 1432850649,
      2 => 'file',
    ),
    'c711d79b118d14b5812ce728a15a8f080eb4ca3b' => 
    array (
      0 => '/Users/evgeniy/sites/awb/design/html/blocks/instagram.tpl',
      1 => 1432850649,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '12463351695564bd11935865-33626550',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_5564bd11c05553_52422027',
  'variables' => 
  array (
    'config' => 0,
    'meta_title' => 0,
    'meta_description' => 0,
    'meta_keywords' => 0,
    'product' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5564bd11c05553_52422027')) {function content_5564bd11c05553_52422027($_smarty_tpl) {?><!DOCTYPE html>
<html>
<head>
	<base href="<?php echo $_smarty_tpl->tpl_vars['config']->value->root_url;?>
"/>
	<title><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['meta_title']->value, ENT_QUOTES, 'UTF-8', true);?>
</title>
	
	
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="description" content="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['meta_description']->value, ENT_QUOTES, 'UTF-8', true);?>
" />
	<meta name="keywords"    content="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['meta_keywords']->value, ENT_QUOTES, 'UTF-8', true);?>
" />
	
    <!--[if lt IE 9]>
    <?php echo '<script'; ?>
 src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"><?php echo '</script'; ?>
>
    <![endif]-->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300' rel='stylesheet' type='text/css'>
	<link href="design/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <link href="design/css/base.css" rel="stylesheet" type="text/css"/>
    <link href="design/css/non-responsive.css" rel="stylesheet" type="text/css"/>
    <link href="design/css/slick.css" rel="stylesheet" type="text/css"/>
    <link href="design/css/revo-settings.css" rel="stylesheet" type="text/css"/>
    <link href="design/css/revo-style.css" rel="stylesheet" type="text/css"/>
	<link href="favicon.ico" rel="icon"          type="image/x-icon"/>
	<link href="favicon.ico" rel="shortcut icon" type="image/x-icon"/>
    
</head>
<body>
    <?php echo $_smarty_tpl->getSubTemplate ('header.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>


    

    <?php /*  Call merged included template "sliders/top.tpl" */
$_tpl_stack[] = $_smarty_tpl;
 $_smarty_tpl = $_smarty_tpl->setupInlineSubTemplate('sliders/top.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0, '12463351695564bd11935865-33626550');
content_5581b60b0d0893_12884005($_smarty_tpl);
$_smarty_tpl = array_pop($_tpl_stack); 
/*  End of included template "sliders/top.tpl" */?>

    <div class="container-fluid l3 trends-container">
        <div class="container steps">
            <div class="row">
                <div class="col-xs-12">
                    <h2>Основные направления бизнеса</h2>
                </div>
            </div>
            <div class="row fruits">
                <div class="col-sm-3 gr">
                    <img src="design/images/fruit1.png">
                    <p class="name_group trends-title">Выращивание</p>
                    <p class="trends-description">Свои плантации в Марокко, Эквадоре, Узбекистане, Турции и еще 3 странах. Вы получаете минимальную цену от крупного производителя</p>
                </div>
                <div class="col-sm-3 gr">
                    <img src="design/images/fruit2.png">
                    <p class="name_group trends-title">Сбор и подготовка</p>
                    <p class="trends-description">Свое оборудование от морозильных тоннелей до линии мойки и калибровки.</p>
                </div>
                <div class="col-sm-3 gr">
                    <img src="design/images/fruit3.png">
                    <p class="name_group trends-title">Упаковка</p>
                    <p class="trends-description">Множество видов упаковки, в том числе под вашим брендом! Вы получите продукт под своим брендом без наценки!</p>
                </div>
                <div class="col-sm-3 gr">
                    <img src="design/images/fruit4.png">
                    <p class="name_group trends-title">Доставка</p>
                    <p class="trends-description">Собственная логистическая система + партнерские контракты. Вы получите продукт максимально быстро!</p>
                </div>
            </div>
        </div>
    </div>

    <?php /*  Call merged included template "sliders/bottom.tpl" */
$_tpl_stack[] = $_smarty_tpl;
 $_smarty_tpl = $_smarty_tpl->setupInlineSubTemplate('sliders/bottom.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0, '12463351695564bd11935865-33626550');
content_5581b60b0df6e0_86225356($_smarty_tpl);
$_smarty_tpl = array_pop($_tpl_stack); 
/*  End of included template "sliders/bottom.tpl" */?>

    <div class="container-fluid l5">
        <div class="container">
            <div class="row">
                <?php /*  Call merged included template "blocks/news.tpl" */
$_tpl_stack[] = $_smarty_tpl;
 $_smarty_tpl = $_smarty_tpl->setupInlineSubTemplate('blocks/news.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0, '12463351695564bd11935865-33626550');
content_5581b60b0ea195_57915067($_smarty_tpl);
$_smarty_tpl = array_pop($_tpl_stack); 
/*  End of included template "blocks/news.tpl" */?>
                <?php /*  Call merged included template "blocks/instagram.tpl" */
$_tpl_stack[] = $_smarty_tpl;
 $_smarty_tpl = $_smarty_tpl->setupInlineSubTemplate('blocks/instagram.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0, '12463351695564bd11935865-33626550');
content_5581b60b13f052_88951257($_smarty_tpl);
$_smarty_tpl = array_pop($_tpl_stack); 
/*  End of included template "blocks/instagram.tpl" */?>
            </div>
        </div>
    </div>



    <?php if (!$_smarty_tpl->tpl_vars['product']->value->id) {?>
        <?php echo $_smarty_tpl->getSubTemplate ('blocks/trust.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

    <?php }?>

    <?php echo $_smarty_tpl->getSubTemplate ('footer.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

    <div class="complete-dialog text-center"><p>Спасибо!</p><p data-default="Ваша заявка принята!" class="complete-dialog-text">Ваша заявка принята!</p></div>

    <?php echo '<script'; ?>
 src="design/js/jquery-1.11.2.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="design/js/bootstrap.min.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="design/js/revolution.min.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="design/js/revo-tools.min.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="design/js/slick.min.js"><?php echo '</script'; ?>
>

    
        <?php echo '<script'; ?>
 type="text/javascript">
            var revApi;
            $(window).load(function(){
                initHeaderPosition();
            })

            //header position init
            function initHeaderPosition(){
                var win = $(window);
                var header = $('.navbar-fixed-top');
                function calcPosition(obj){
                    obj.stop().css({left: parseInt(obj.attr('data-top')) - win.scrollLeft()});
                    win.scroll(function(){
                        obj.stop().css({left: parseInt(obj.attr('data-top')) - win.scrollLeft()});
                    })
                }
                if(header.length){
                    header.attr('data-top',parseInt(header.css('left')));
                    calcPosition(header);
                }
            }
            $(document).ready(function() {
                $('.form__get-price, .subscription-form').on('submit', function(e){
                    e.preventDefault();
                    var $self = $(this),
                        data = $self.serialize(),
                        popup = $('.complete-dialog');

                    $.ajax({
                        url: $self.attr('action'),
                        method: 'post',
                        data: data,
                        success: function(data){
                            $self.trigger('reset');
                            popup.show();
                            setTimeout(function(){
                                popup.fadeOut('fast');
                            }, 2000);
                        }
                    });
                });

                if($('#bottomSlider').revolution == undefined)
                    revslider_showDoubleJqueryError('#bottomSlider');
                else
                    revApi = $('#bottomSlider').show()
                        .revolution({
                            dottedOverlay:"none",
                            delay:6000,
                            startwidth:1170,
                            startheight:432,
                            hideThumbs:200,
                            thumbWidth:100,
                            thumbHeight:50,
                            thumbAmount:2,
                            navigationType:"none",
                            navigationArrows:"none",
                            navigationStyle:"round",
                            touchenabled:"off",
                            onHoverStop:"off",
                            keyboardNavigation:"off",
                            navigationHAlign:"center",
                            navigationVAlign:"bottom",
                            navigationHOffset:0,
                            navigationVOffset:20,
                            soloArrowLeftHalign:"left",
                            soloArrowLeftValign:"center",
                            soloArrowLeftHOffset:20,
                            soloArrowLeftVOffset:0,
                            soloArrowRightHalign:"right",
                            soloArrowRightValign:"center",
                            soloArrowRightHOffset:20,
                            soloArrowRightVOffset:0,
                            shadow:0,
                            fullWidth:"on",
                            fullScreen:"off",
                            spinner:"spinner0",
                            stopLoop:"off",
                            stopAfterLoops:-1,
                            stopAtSlide:-1,
                            shuffle:"off",
                            autoHeight:"on",
                            forceFullWidth:"on",
                            hideThumbsOnMobile:"off",
                            hideNavDelayOnMobile:1500,
                            hideBulletsOnMobile:"off",
                            hideArrowsOnMobile:"off",
                            hideThumbsUnderResolution:0,
                            hideSliderAtLimit:0,
                            hideCaptionAtLimit:0,
                            hideAllCaptionAtLilmit:0,
                            startWithSlide:0,
                            fullScreenOffsetContainer: ""
                        });

            });
        <?php echo '</script'; ?>
>
    

    
    <?php echo '<script'; ?>
>
        /*$(function(){
            function autoResize() {
                var $width = $(window).width();
                if($width > 1200){
                    $('.main-slider__item').css('height', $width * (25/100));
                }else {
                    $('.main-slider__item').css('height', 600);
                }
            }
            window.onresize = autoResize;
            autoResize();
        });*/
    <?php echo '</script'; ?>
>

</body>
</html><?php }} ?>
<?php /* Smarty version Smarty-3.1.21-dev, created on 2015-06-17 21:01:47
         compiled from "/Users/evgeniy/sites/awb/design/html/sliders/top.tpl" */ ?>
<?php if ($_valid && !is_callable('content_5581b60b0d0893_12884005')) {function content_5581b60b0d0893_12884005($_smarty_tpl) {?><div class="container-fluid l2">
    <div id="mainTop" class="carousel slide" data-ride="carousel">
        <div class="row">
            <!-- Indicators -->
            <ol class="carousel-indicators">
                <li data-target="#mainTop" data-slide-to="0" class="active"></li>
                <li data-target="#mainTop" data-slide-to="1" class=""></li>
                <li data-target="#mainTop" data-slide-to="2" class=""></li>
            </ol>
            <div class="carousel-inner">
                <div class="item active main-slider__item" style="background-image: url('/design/images/slider/main/001.jpg')">
                    <div class="main-slider__item-overlay"></div>
                    <div class="container">
                        <div class="carousel-caption">
                            <h1>Мы знаем всё о фруктах, овощах и ягодах</h1>
                        </div>
                    </div>
                </div>
                <div class="item main-slider__item" style="background-image: url('/design/images/slider/main/004.jpg')">
                    <div class="main-slider__item-overlay"></div>
                    <div class="container">
                        <div class="carousel-caption">
                            <h1>Собственные поля в 7 странах на 3 континентах</h1>
                        </div>
                    </div>
                </div>
                <div class="item main-slider__item" style="background-image: url('/design/images/slider/main/003.jpg')">
                    <div class="main-slider__item-overlay"></div>
                    <div class="container">
                        <div class="carousel-caption">
                            <h1>По итогам 2014 года - в среднем около 112 тонн отгрузок в сутки</h1>
                        </div>
                    </div>
                </div>
            </div>
            <a class="left carousel-control" href="#mainTop" data-slide="prev"><span class="glyphicon glyphicon-chevron-left"></span></a>
            <a class="right carousel-control" href="#mainTop" data-slide="next"><span class="glyphicon glyphicon-chevron-right"></span></a>
        </div>
    </div>
</div><?php }} ?>
<?php /* Smarty version Smarty-3.1.21-dev, created on 2015-06-17 21:01:47
         compiled from "/Users/evgeniy/sites/awb/design/html/sliders/bottom.tpl" */ ?>
<?php if ($_valid && !is_callable('content_5581b60b0df6e0_86225356')) {function content_5581b60b0df6e0_86225356($_smarty_tpl) {?><!-- start bottom slider -->
<div class="rev_slider_wrapper fullwidthbanner-container bottom-slider">
    <div id="bottomSlider" class="rev_slider fullwidthabanner bottom-slider__inner">
        <ul>
            
            <li data-transition="random" data-slotamount="7" data-masterspeed="300" >
                <img src="design/images/slider/bottom/001.jpg" alt="" data-bgposition="center top" data-bgfit="contain" data-bgrepeat="no-repeat">
            </li>
            <li data-transition="random" data-slotamount="7" data-masterspeed="300" >
                <img src="design/images/slider/bottom/002_4.jpg" alt="" data-bgposition="center top" data-bgfit="contain" data-bgrepeat="no-repeat">
            </li>
            
        </ul>
        <div class="tp-bannertimer"></div>
    </div>
</div>
<!-- end bottom slider --><?php }} ?>
<?php /* Smarty version Smarty-3.1.21-dev, created on 2015-06-17 21:01:47
         compiled from "/Users/evgeniy/sites/awb/design/html/blocks/news.tpl" */ ?>
<?php if ($_valid && !is_callable('content_5581b60b0ea195_57915067')) {function content_5581b60b0ea195_57915067($_smarty_tpl) {?><div id="mainNews" class="col-md-6 carousel slide carousel-fade" data-ride="carousel" data-interval="false">
    <h3>Новости с наших плантаций</h3>
    <div class="row">
        <?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['get_posts'][0][0]->get_posts_plugin(array('var'=>'last_posts','limit'=>3),$_smarty_tpl);?>

        <?php if ($_smarty_tpl->tpl_vars['last_posts']->value) {?>
            <div class="carousel-inner">
                <?php  $_smarty_tpl->tpl_vars['post'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['post']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['last_posts']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
 $_smarty_tpl->tpl_vars['post']->index=-1;
foreach ($_from as $_smarty_tpl->tpl_vars['post']->key => $_smarty_tpl->tpl_vars['post']->value) {
$_smarty_tpl->tpl_vars['post']->_loop = true;
 $_smarty_tpl->tpl_vars['post']->index++;
 $_smarty_tpl->tpl_vars['post']->first = $_smarty_tpl->tpl_vars['post']->index === 0;
?>
                    <div class="main__news-item item <?php if ($_smarty_tpl->tpl_vars['post']->first) {?>active<?php }?>">
                        <div class="col-xs-4">
                            <img src="design/images/apples.png">
                        </div>
                        <div class="col-xs-8 news">
                            <p class="main__news-carousel-title"><?php echo $_smarty_tpl->tpl_vars['post']->value->name;?>
</p>
                            <div class="main__news-carousel-description"><?php echo $_smarty_tpl->tpl_vars['post']->value->annotation;?>
</div>
                            <p class="date"><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['date'][0][0]->date_modifier($_smarty_tpl->tpl_vars['post']->value->date);?>
</p>
                            <p class="main__news-carousel-category category"><a class="main__news-carousel-category_link" href="news">Новости компании</a></p>
                        </div>
                    </div>
                <?php } ?>
            </div>
            <ol class="carousel-indicators main__news-carousel_indicator">
                <?php $_smarty_tpl->tpl_vars['cnt'] = new Smarty_Variable;$_smarty_tpl->tpl_vars['cnt']->step = 1;$_smarty_tpl->tpl_vars['cnt']->total = (int) ceil(($_smarty_tpl->tpl_vars['cnt']->step > 0 ? count($_smarty_tpl->tpl_vars['last_posts']->value)-1+1 - (0) : 0-(count($_smarty_tpl->tpl_vars['last_posts']->value)-1)+1)/abs($_smarty_tpl->tpl_vars['cnt']->step));
if ($_smarty_tpl->tpl_vars['cnt']->total > 0) {
for ($_smarty_tpl->tpl_vars['cnt']->value = 0, $_smarty_tpl->tpl_vars['cnt']->iteration = 1;$_smarty_tpl->tpl_vars['cnt']->iteration <= $_smarty_tpl->tpl_vars['cnt']->total;$_smarty_tpl->tpl_vars['cnt']->value += $_smarty_tpl->tpl_vars['cnt']->step, $_smarty_tpl->tpl_vars['cnt']->iteration++) {
$_smarty_tpl->tpl_vars['cnt']->first = $_smarty_tpl->tpl_vars['cnt']->iteration == 1;$_smarty_tpl->tpl_vars['cnt']->last = $_smarty_tpl->tpl_vars['cnt']->iteration == $_smarty_tpl->tpl_vars['cnt']->total;?>
                    <li data-target="#mainNews" data-slide-to="<?php echo $_smarty_tpl->tpl_vars['cnt']->value;?>
" <?php if ($_smarty_tpl->tpl_vars['cnt']->value==0) {?>class="active"<?php }?>></li>
                <?php }} ?>
            </ol>
        <?php }?>
    </div>
</div><?php }} ?>
<?php /* Smarty version Smarty-3.1.21-dev, created on 2015-06-17 21:01:47
         compiled from "/Users/evgeniy/sites/awb/design/html/blocks/instagram.tpl" */ ?>
<?php if ($_valid && !is_callable('content_5581b60b13f052_88951257')) {function content_5581b60b13f052_88951257($_smarty_tpl) {?><div class="col-md-6 inst">
    <div class="row">
        <h3>Наш Instagram</h3>
        <p>#amwellgroup</p>
        <div class="main__instagram-slider" data-slick='{"slidesToShow": 3, "slidesToScroll": 1, "arrows": true, "autoplay": true, "autoplaySpeed": 7000}'>
            <?php  $_smarty_tpl->tpl_vars['insta'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['insta']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['instagram']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['insta']->key => $_smarty_tpl->tpl_vars['insta']->value) {
$_smarty_tpl->tpl_vars['insta']->_loop = true;
?>
                <a class="main__instagram-item" href="<?php echo $_smarty_tpl->tpl_vars['insta']->value->link;?>
">
                    <img class="main__instagram-item_img" src="<?php echo $_smarty_tpl->tpl_vars['insta']->value->images->low_resolution->url;?>
" alt=""/>
                </a>
            <?php } ?>
        </div>
    </div>
</div><?php }} ?>
