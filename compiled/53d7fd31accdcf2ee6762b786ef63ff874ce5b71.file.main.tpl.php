<?php /* Smarty version Smarty-3.1.21-dev, created on 2016-06-09 09:34:18
         compiled from "/Users/evgeniy/sites/minsk-tokyo.dev/design/html/main.tpl" */ ?>
<?php /*%%SmartyHeaderCode:189510803956e332a242f4f9-77095648%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '53d7fd31accdcf2ee6762b786ef63ff874ce5b71' => 
    array (
      0 => '/Users/evgeniy/sites/minsk-tokyo.dev/design/html/main.tpl',
      1 => 1457852994,
      2 => 'file',
    ),
    'd99d70178b47531c5609a6ba9b17ff4ed22fd9ec' => 
    array (
      0 => '/Users/evgeniy/sites/minsk-tokyo.dev/design/html/layout.tpl',
      1 => 1465454055,
      2 => 'file',
    ),
    '937e645bb05b432ad7a3b6ac3ac08c531ce7c4e2' => 
    array (
      0 => '/Users/evgeniy/sites/minsk-tokyo.dev/design/html/sliders/main.tpl',
      1 => 1465453465,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '189510803956e332a242f4f9-77095648',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_56e332a255a5b2_05192561',
  'variables' => 
  array (
    'config' => 0,
    'meta_title' => 0,
    'meta_description' => 0,
    'meta_keywords' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_56e332a255a5b2_05192561')) {function content_56e332a255a5b2_05192561($_smarty_tpl) {?><!DOCTYPE html>
<html>
<head>
	<base href="<?php echo $_smarty_tpl->tpl_vars['config']->value->root_url;?>
"/>
	<title><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['meta_title']->value, ENT_QUOTES, 'UTF-8', true);?>
</title>
	
	
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="description" content="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['meta_description']->value, ENT_QUOTES, 'UTF-8', true);?>
" />
	<meta name="keywords"    content="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['meta_keywords']->value, ENT_QUOTES, 'UTF-8', true);?>
" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1.0">
	
    <link href='https://fonts.googleapis.com/css?family=Marko+One' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Overlock:400,700' rel='stylesheet' type='text/css'>

    <link rel="stylesheet" type="text/css" href="/design/css/bootstrap.min.css" />
    <link rel="stylesheet" type="text/css" href="/design/css/font-awesome.min.css" />
    <link rel="stylesheet" type="text/css" href="/design/css/vendor.css" />
    <link rel="stylesheet" type="text/css" href="/design/css/base.css" />
    <link rel="stylesheet" type="text/css" href="/design/css/additional.css" />
    <!--[if lt IE 9]>
    <link rel="stylesheet" type="text/css" href="/design/css/ie8-and-down.css" />
    <![endif]-->
    
</head>
<body>
    <div class="restaurant-minsk">
        <div class="wrapper">
            <div class="container container__mod">

                <?php echo $_smarty_tpl->getSubTemplate ('header.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>


                

    <div id="main-container" class="row">

        <?php /*  Call merged included template "sliders/main.tpl" */
$_tpl_stack[] = $_smarty_tpl;
 $_smarty_tpl = $_smarty_tpl->setupInlineSubTemplate('sliders/main.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0, '189510803956e332a242f4f9-77095648');
content_57590dea544209_44545247($_smarty_tpl);
$_smarty_tpl = array_pop($_tpl_stack); 
/*  End of included template "sliders/main.tpl" */?>

    </div><!--END #MAIN-CONTAINER-->



                <?php echo $_smarty_tpl->getSubTemplate ('footer.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

            </div>
        </div>
    </div>
    <?php echo '<script'; ?>
 src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.2/jquery-ui.min.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="/design/js/bootstrap.min.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="/design/js/mediaelement-and-player.min.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="/design/js/picturefill.min.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="/design/js/jquery.colorbox.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
>document.createElement('picture');<?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="/design/js/main.js"><?php echo '</script'; ?>
>
    <!--[if lt IE 10]>
    <?php echo '<script'; ?>
 type="text/javascript" src="/design/js/media.match.min.js"><?php echo '</script'; ?>
>
    <![endif]-->
    <!--[if IE]><?php echo '<script'; ?>
 src="/design/js/ie.js"><?php echo '</script'; ?>
><![endif]-->

    


</body>
</html><?php }} ?>
<?php /* Smarty version Smarty-3.1.21-dev, created on 2016-06-09 09:34:18
         compiled from "/Users/evgeniy/sites/minsk-tokyo.dev/design/html/sliders/main.tpl" */ ?>
<?php if ($_valid && !is_callable('content_57590dea544209_44545247')) {function content_57590dea544209_44545247($_smarty_tpl) {?><div class="node node--restaurant node--promoted main__slider view-mode-full  node--full node--restaurant--full col-sm-12 clearfix">
    <div class="field field-name-field-slides main__slider-list">
        <img src="/design/images/slider/01.jpg" class="main__slider-item" width="1400" height="620" alt="" title="" />
        <div class="field field-name-field-alignment">center</div>
        <img src="/design/images/slider/02.png" class="main__slider-item" width="467" height="620" title="" />
        <div class="field field-name-field-alignment">left</div>
        <img src="/design/images/slider/03.png" class="main__slider-item" width="935" height="620" title="" />
        <div class="field field-name-field-alignment">right</div>
        <img src="/design/images/slider/04.jpg" class="main__slider-item" width="1400" height="620" alt="" title="" />
        <div class="field field-name-field-alignment">center</div>
        <img src="/design/images/slider/05.png" class="main__slider-item" width="467" height="620" title="" />
        <div class="field field-name-field-alignment">right</div>
        <img src="/design/images/slider/06.png" class="main__slider-item" width="935" height="620" title="" />
        <div class="field field-name-field-alignment">left</div>
        <img src="/design/images/slider/07.jpg" class="main__slider-item" width="1400" height="620" alt="" title="" />
        <div class="field field-name-field-alignment">center</div>
        <img src="/design/images/slider/08.png" class="main__slider-item" width="467" height="620" title="" />
        <div class="field field-name-field-alignment">left</div>
        <img src="/design/images/slider/09.png" class="main__slider-item" width="935" height="620" title="" />
        <div class="field field-name-field-alignment">right</div>
        <img src="/design/images/slider/10.jpg" class="main__slider-item" width="1400" height="620" alt="" title="" />
        <div class="field field-name-field-alignment">center</div>
        <img src="/design/images/slider/11.png" class="main__slider-item" width="467" height="620" title="" />
        <div class="field field-name-field-alignment">left</div>
        <img src="/design/images/slider/12.png" class="main__slider-item" width="935" height="620" title="" />
        <div class="field field-name-field-alignment">right</div>
        <img src="/design/images/slider/13.jpg" class="main__slider-item" width="1400" height="620" alt="" title="" />
        <div class="field field-name-field-alignment">center</div>
    </div>
</div><?php }} ?>
