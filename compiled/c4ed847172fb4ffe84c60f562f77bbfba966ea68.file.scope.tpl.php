<?php /* Smarty version Smarty-3.1.21-dev, created on 2015-06-08 11:14:06
         compiled from "/Users/evgeniy/sites/awb/design/html/scope.tpl" */ ?>
<?php /*%%SmartyHeaderCode:17547183485566d3dfb10eb1-92021534%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'c4ed847172fb4ffe84c60f562f77bbfba966ea68' => 
    array (
      0 => '/Users/evgeniy/sites/awb/design/html/scope.tpl',
      1 => 1433751138,
      2 => 'file',
    ),
    '721d29d532c8456def5ae484ceb8ede5618804b4' => 
    array (
      0 => '/Users/evgeniy/sites/awb/design/html/layout.tpl',
      1 => 1433707626,
      2 => 'file',
    ),
    '20004da77ee302d1b36d5e9013e2a7be8a2503b1' => 
    array (
      0 => '/Users/evgeniy/sites/awb/design/html/blocks/default_header.tpl',
      1 => 1433090847,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '17547183485566d3dfb10eb1-92021534',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_5566d3dfbe8622_78714274',
  'variables' => 
  array (
    'config' => 0,
    'meta_title' => 0,
    'meta_description' => 0,
    'meta_keywords' => 0,
    'product' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5566d3dfbe8622_78714274')) {function content_5566d3dfbe8622_78714274($_smarty_tpl) {?><!DOCTYPE html>
<html>
<head>
	<base href="<?php echo $_smarty_tpl->tpl_vars['config']->value->root_url;?>
"/>
	<title><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['meta_title']->value, ENT_QUOTES, 'UTF-8', true);?>
</title>
	
	
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="description" content="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['meta_description']->value, ENT_QUOTES, 'UTF-8', true);?>
" />
	<meta name="keywords"    content="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['meta_keywords']->value, ENT_QUOTES, 'UTF-8', true);?>
" />
	
    <!--[if lt IE 9]>
    <?php echo '<script'; ?>
 src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"><?php echo '</script'; ?>
>
    <![endif]-->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300' rel='stylesheet' type='text/css'>
	<link href="design/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <link href="design/css/base.css" rel="stylesheet" type="text/css"/>
    <link href="design/css/non-responsive.css" rel="stylesheet" type="text/css"/>
    <link href="design/css/slick.css" rel="stylesheet" type="text/css"/>
    <link href="design/css/revo-settings.css" rel="stylesheet" type="text/css"/>
    <link href="design/css/revo-style.css" rel="stylesheet" type="text/css"/>
	<link href="favicon.ico" rel="icon"          type="image/x-icon"/>
	<link href="favicon.ico" rel="shortcut icon" type="image/x-icon"/>
    
</head>
<body>
    <?php echo $_smarty_tpl->getSubTemplate ('header.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>


    
    <?php /*  Call merged included template "blocks/default_header.tpl" */
$_tpl_stack[] = $_smarty_tpl;
 $_smarty_tpl = $_smarty_tpl->setupInlineSubTemplate('blocks/default_header.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0, '17547183485566d3dfb10eb1-92021534');
content_55754ecead6ae0_10987567($_smarty_tpl);
$_smarty_tpl = array_pop($_tpl_stack); 
/*  End of included template "blocks/default_header.tpl" */?>

    <div class="container-fluid scope-box">
        <div class="wrapper">
            <div class="row">
                <ul class="list-unstyled scope-list">
                    <li class="scope-item">
                        <p class="scope-item__description">
                            <span class="scope-item__description-l1">В 7 странах мира</span>
                            <span class="scope-item__description-l2">более 1800 ГА</span>
                            <span class="scope-item__description-l3">фруктовых, овощных и ягодных плантаций</span>
                        </p>
                        <img class="scope-item__image" src="design/images/scope/001.jpg" />
                        <div class="scope-item__title">
                            Плантация клубники. Чили
                        </div>
                    </li>
                    <li class="scope-item">
                        <p class="scope-item__description">
                            <span class="scope-item__description-l2" style="letter-spacing: 13px;">Фасовка</span>
                            <span class="scope-item__description-l1_mod" style="font-size: 50px;letter-spacing: 10px;">Калибровка</span>
                            <span class="scope-item__description-l3">Поставка под брендом заказчика.</span>
                        </p>
                        <img class="scope-item__image" src="design/images/scope/005.jpg" />
                        <div class="scope-item__title">
                            Калибровка яблок. Израиль
                        </div>
                    </li>
                    <li class="scope-item">
                        <p class="scope-item__description">
                            <span class="scope-item__description-l2" style="letter-spacing: 11px;">Логистика</span>
                            <span class="scope-item__description-l1_mod" style="font-size: 50px;">прямо с плантации</span>
                            <span class="scope-item__description-l1_mod" style="font-size: 50px;letter-spacing: -2.8px;">до любой точки мира</span>
                        </p>
                        <img class="scope-item__image" src="design/images/scope/003.jpg" />
                        <div class="scope-item__title">
                            Погрузка в порту Касабланки. Марокко
                        </div>
                    </li>
                    <li class="scope-item">
                        <div class="scope-item__description scope-item__description_mod">
                            <div class="col-md-6">
                                <span class="scope-item__description-l2">Экскурсии</span>
                                <span class="scope-item__description-l1_mod" style="font-size: 50px; letter-spacing: 3.5px;">на плантации</span>
                                <span class="scope-item__description-l1_mod" style="font-size: 50px; letter-spacing: 2.5px;">производство</span>
                            </div>
                            <div class="col-md-6 scope-item__description-info">
                                Наши плантации открыты для всех. Мы готовы показать процесс производства: от выращивания до погрузки продукта.
                            </div>
                        </div>
                        <img class="scope-item__image" src="design/images/scope/004.jpg" />
                        <div class="scope-item__title">
                            Сбор мандаринов. Китай, провинция Хунань
                        </div>
                    </li>
                    <li class="scope-item">
                        <div class="scope-item__description scope-item__description_mod">
                            <div class="col-md-7">
                                <span class="scope-item__description-l2">Финансовые</span>
                                <span class="scope-item__description-l2" style="letter-spacing: -2.3px;">возможности</span>
                            </div>
                            <div class="col-md-5 scope-item__description-info">
                                Для постоянных клиентов - отсрочка платежа, аккредитив, рассрочка.
                            </div>
                        </div>
                        <img class="scope-item__image" src="design/images/scope/002.jpg" />
                    </li>
                </ul>
            </div>
        </div>
    </div>


    <?php if (!$_smarty_tpl->tpl_vars['product']->value->id) {?>
        <?php echo $_smarty_tpl->getSubTemplate ('blocks/trust.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

    <?php }?>

    <?php echo $_smarty_tpl->getSubTemplate ('footer.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

    <div class="complete-dialog text-center"><p>Спасибо!</p><p>Ваша заявка принята!</p></div>

    <?php echo '<script'; ?>
 src="design/js/jquery-1.11.2.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="design/js/bootstrap.min.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="design/js/revolution.min.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="design/js/revo-tools.min.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="design/js/slick.min.js"><?php echo '</script'; ?>
>

    
        <?php echo '<script'; ?>
 type="text/javascript">
            var revApi;
            $(window).load(function(){
                initHeaderPosition();
            })

            //header position init
            function initHeaderPosition(){
                var win = $(window);
                var header = $('.navbar-fixed-top');
                function calcPosition(obj){
                    obj.stop().css({left: parseInt(obj.attr('data-top')) - win.scrollLeft()});
                    win.scroll(function(){
                        obj.stop().css({left: parseInt(obj.attr('data-top')) - win.scrollLeft()});
                    })
                }
                if(header.length){
                    header.attr('data-top',parseInt(header.css('left')));
                    calcPosition(header);
                }
            }
            $(document).ready(function() {
                $('.form__get-price').on('submit', function(e){
                    e.preventDefault();
                    var $self = $(this),
                        data = $self.serialize(),
                        popup = $('.complete-dialog');
                    $.ajax({
                        url: 'ajax/getPrice.php',
                        method: 'post',
                        data: data,
                        success: function(data){
                            $self.trigger('reset');
                            popup.show();
                            setTimeout(function(){
                                popup.fadeOut('fast');
                            }, 2000);
                        }
                    });
                });

                if($('#bottomSlider').revolution == undefined)
                    revslider_showDoubleJqueryError('#bottomSlider');
                else
                    revApi = $('#bottomSlider').show()
                        .revolution({
                            dottedOverlay:"none",
                            delay:6000,
                            startwidth:1170,
                            startheight:460,
                            hideThumbs:200,
                            thumbWidth:100,
                            thumbHeight:50,
                            thumbAmount:2,
                            navigationType:"none",
                            navigationArrows:"none",
                            navigationStyle:"round",
                            touchenabled:"off",
                            onHoverStop:"off",
                            keyboardNavigation:"off",
                            navigationHAlign:"center",
                            navigationVAlign:"bottom",
                            navigationHOffset:0,
                            navigationVOffset:20,
                            soloArrowLeftHalign:"left",
                            soloArrowLeftValign:"center",
                            soloArrowLeftHOffset:20,
                            soloArrowLeftVOffset:0,
                            soloArrowRightHalign:"right",
                            soloArrowRightValign:"center",
                            soloArrowRightHOffset:20,
                            soloArrowRightVOffset:0,
                            shadow:0,
                            fullWidth:"on",
                            fullScreen:"off",
                            spinner:"spinner0",
                            stopLoop:"off",
                            stopAfterLoops:-1,
                            stopAtSlide:-1,
                            shuffle:"off",
                            autoHeight:"on",
                            forceFullWidth:"on",
                            hideThumbsOnMobile:"off",
                            hideNavDelayOnMobile:1500,
                            hideBulletsOnMobile:"off",
                            hideArrowsOnMobile:"off",
                            hideThumbsUnderResolution:0,
                            hideSliderAtLimit:0,
                            hideCaptionAtLimit:0,
                            hideAllCaptionAtLilmit:0,
                            startWithSlide:0,
                            fullScreenOffsetContainer: ""
                        });

            });
        <?php echo '</script'; ?>
>
    

    
</body>
</html><?php }} ?>
<?php /* Smarty version Smarty-3.1.21-dev, created on 2015-06-08 11:14:06
         compiled from "/Users/evgeniy/sites/awb/design/html/blocks/default_header.tpl" */ ?>
<?php if ($_valid && !is_callable('content_55754ecead6ae0_10987567')) {function content_55754ecead6ae0_10987567($_smarty_tpl) {?><div class="container-fluid l11 default-header clearfix">
    <div class="default-header__inner"></div>
</div><?php }} ?>
