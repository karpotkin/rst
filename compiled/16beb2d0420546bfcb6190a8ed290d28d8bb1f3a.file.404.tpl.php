<?php /* Smarty version Smarty-3.1.21-dev, created on 2015-06-17 21:17:47
         compiled from "/Users/evgeniy/sites/awb/design/html/404.tpl" */ ?>
<?php /*%%SmartyHeaderCode:20820063765564bc8dd58d91-84525101%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '16beb2d0420546bfcb6190a8ed290d28d8bb1f3a' => 
    array (
      0 => '/Users/evgeniy/sites/awb/design/html/404.tpl',
      1 => 1419538316,
      2 => 'file',
    ),
    '721d29d532c8456def5ae484ceb8ede5618804b4' => 
    array (
      0 => '/Users/evgeniy/sites/awb/design/html/layout.tpl',
      1 => 1434563870,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '20820063765564bc8dd58d91-84525101',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_5564bc8de52609_05700226',
  'variables' => 
  array (
    'config' => 0,
    'meta_title' => 0,
    'meta_description' => 0,
    'meta_keywords' => 0,
    'product' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5564bc8de52609_05700226')) {function content_5564bc8de52609_05700226($_smarty_tpl) {?><!DOCTYPE html>
<html>
<head>
	<base href="<?php echo $_smarty_tpl->tpl_vars['config']->value->root_url;?>
"/>
	<title><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['meta_title']->value, ENT_QUOTES, 'UTF-8', true);?>
</title>
	
	
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="description" content="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['meta_description']->value, ENT_QUOTES, 'UTF-8', true);?>
" />
	<meta name="keywords"    content="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['meta_keywords']->value, ENT_QUOTES, 'UTF-8', true);?>
" />
	
    <!--[if lt IE 9]>
    <?php echo '<script'; ?>
 src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"><?php echo '</script'; ?>
>
    <![endif]-->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300' rel='stylesheet' type='text/css'>
	<link href="design/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <link href="design/css/base.css" rel="stylesheet" type="text/css"/>
    <link href="design/css/non-responsive.css" rel="stylesheet" type="text/css"/>
    <link href="design/css/slick.css" rel="stylesheet" type="text/css"/>
    <link href="design/css/revo-settings.css" rel="stylesheet" type="text/css"/>
    <link href="design/css/revo-style.css" rel="stylesheet" type="text/css"/>
	<link href="favicon.ico" rel="icon"          type="image/x-icon"/>
	<link href="favicon.ico" rel="shortcut icon" type="image/x-icon"/>
    
</head>
<body>
    <?php echo $_smarty_tpl->getSubTemplate ('header.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>


    

    <?php if (!$_smarty_tpl->tpl_vars['product']->value->id) {?>
        <?php echo $_smarty_tpl->getSubTemplate ('blocks/trust.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

    <?php }?>

    <?php echo $_smarty_tpl->getSubTemplate ('footer.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

    <div class="complete-dialog text-center"><p>Спасибо!</p><p data-default="Ваша заявка принята!" class="complete-dialog-text">Ваша заявка принята!</p></div>

    <?php echo '<script'; ?>
 src="design/js/jquery-1.11.2.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="design/js/bootstrap.min.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="design/js/revolution.min.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="design/js/revo-tools.min.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="design/js/slick.min.js"><?php echo '</script'; ?>
>

    
        <?php echo '<script'; ?>
 type="text/javascript">
            var revApi;
            $(window).load(function(){
                initHeaderPosition();
            })

            //header position init
            function initHeaderPosition(){
                var win = $(window);
                var header = $('.navbar-fixed-top');
                function calcPosition(obj){
                    obj.stop().css({left: parseInt(obj.attr('data-top')) - win.scrollLeft()});
                    win.scroll(function(){
                        obj.stop().css({left: parseInt(obj.attr('data-top')) - win.scrollLeft()});
                    })
                }
                if(header.length){
                    header.attr('data-top',parseInt(header.css('left')));
                    calcPosition(header);
                }
            }
            $(document).ready(function() {
                $('.form__get-price, .subscription-form').on('submit', function(e){
                    e.preventDefault();
                    var $self = $(this),
                        data = $self.serialize(),
                        popup = $('.complete-dialog');

                    $.ajax({
                        url: $self.attr('action'),
                        method: 'post',
                        data: data,
                        success: function(data){
                            $self.trigger('reset');
                            popup.show();
                            setTimeout(function(){
                                popup.fadeOut('fast');
                            }, 2000);
                        }
                    });
                });

                if($('#bottomSlider').revolution == undefined)
                    revslider_showDoubleJqueryError('#bottomSlider');
                else
                    revApi = $('#bottomSlider').show()
                        .revolution({
                            dottedOverlay:"none",
                            delay:6000,
                            startwidth:1170,
                            startheight:432,
                            hideThumbs:200,
                            thumbWidth:100,
                            thumbHeight:50,
                            thumbAmount:2,
                            navigationType:"none",
                            navigationArrows:"none",
                            navigationStyle:"round",
                            touchenabled:"off",
                            onHoverStop:"off",
                            keyboardNavigation:"off",
                            navigationHAlign:"center",
                            navigationVAlign:"bottom",
                            navigationHOffset:0,
                            navigationVOffset:20,
                            soloArrowLeftHalign:"left",
                            soloArrowLeftValign:"center",
                            soloArrowLeftHOffset:20,
                            soloArrowLeftVOffset:0,
                            soloArrowRightHalign:"right",
                            soloArrowRightValign:"center",
                            soloArrowRightHOffset:20,
                            soloArrowRightVOffset:0,
                            shadow:0,
                            fullWidth:"on",
                            fullScreen:"off",
                            spinner:"spinner0",
                            stopLoop:"off",
                            stopAfterLoops:-1,
                            stopAtSlide:-1,
                            shuffle:"off",
                            autoHeight:"on",
                            forceFullWidth:"on",
                            hideThumbsOnMobile:"off",
                            hideNavDelayOnMobile:1500,
                            hideBulletsOnMobile:"off",
                            hideArrowsOnMobile:"off",
                            hideThumbsUnderResolution:0,
                            hideSliderAtLimit:0,
                            hideCaptionAtLimit:0,
                            hideAllCaptionAtLilmit:0,
                            startWithSlide:0,
                            fullScreenOffsetContainer: ""
                        });

            });
        <?php echo '</script'; ?>
>
    

    
</body>
</html><?php }} ?>
