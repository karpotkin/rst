<?php /* Smarty version Smarty-3.1.21-dev, created on 2015-06-08 09:56:01
         compiled from "/Users/evgeniy/sites/awb/design/html/contact.tpl" */ ?>
<?php /*%%SmartyHeaderCode:7167748065566d32eef9126-12120396%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '26630911d4406f7f67993dd9d3cf4651af9e204b' => 
    array (
      0 => '/Users/evgeniy/sites/awb/design/html/contact.tpl',
      1 => 1433746312,
      2 => 'file',
    ),
    '721d29d532c8456def5ae484ceb8ede5618804b4' => 
    array (
      0 => '/Users/evgeniy/sites/awb/design/html/layout.tpl',
      1 => 1433707626,
      2 => 'file',
    ),
    '5bfb7e8bcb7847953ee3f24332678892d045bce5' => 
    array (
      0 => '/Users/evgeniy/sites/awb/design/html/blocks/contact_form.tpl',
      1 => 1433124736,
      2 => 'file',
    ),
    '685c6d53fa31d72eee851b973e6bebfcd0f36e29' => 
    array (
      0 => '/Users/evgeniy/sites/awb/design/html/blocks/contact_header.tpl',
      1 => 1433409831,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '7167748065566d32eef9126-12120396',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_5566d32f084b81_60150718',
  'variables' => 
  array (
    'config' => 0,
    'meta_title' => 0,
    'meta_description' => 0,
    'meta_keywords' => 0,
    'product' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5566d32f084b81_60150718')) {function content_5566d32f084b81_60150718($_smarty_tpl) {?><!DOCTYPE html>
<html>
<head>
	<base href="<?php echo $_smarty_tpl->tpl_vars['config']->value->root_url;?>
"/>
	<title><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['meta_title']->value, ENT_QUOTES, 'UTF-8', true);?>
</title>
	
	
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="description" content="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['meta_description']->value, ENT_QUOTES, 'UTF-8', true);?>
" />
	<meta name="keywords"    content="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['meta_keywords']->value, ENT_QUOTES, 'UTF-8', true);?>
" />
	
    <!--[if lt IE 9]>
    <?php echo '<script'; ?>
 src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"><?php echo '</script'; ?>
>
    <![endif]-->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300' rel='stylesheet' type='text/css'>
	<link href="design/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <link href="design/css/base.css" rel="stylesheet" type="text/css"/>
    <link href="design/css/non-responsive.css" rel="stylesheet" type="text/css"/>
    <link href="design/css/slick.css" rel="stylesheet" type="text/css"/>
    <link href="design/css/revo-settings.css" rel="stylesheet" type="text/css"/>
    <link href="design/css/revo-style.css" rel="stylesheet" type="text/css"/>
	<link href="favicon.ico" rel="icon"          type="image/x-icon"/>
	<link href="favicon.ico" rel="shortcut icon" type="image/x-icon"/>
    
</head>
<body>
    <?php echo $_smarty_tpl->getSubTemplate ('header.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>


    
    <?php /*  Call merged included template "blocks/contact_header.tpl" */
$_tpl_stack[] = $_smarty_tpl;
 $_smarty_tpl = $_smarty_tpl->setupInlineSubTemplate('blocks/contact_header.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0, '7167748065566d32eef9126-12120396');
content_55753c811d9384_16672956($_smarty_tpl);
$_smarty_tpl = array_pop($_tpl_stack); 
/*  End of included template "blocks/contact_header.tpl" */?>

    <div class="container-fluid contact-box">
        <h3 class="text-center text-upper">Свяжитесь с нами</h3>
        <p class="text-center">Мы всегда открыты. Ждем ваших вопросов, предложений, отзывов.</p>
        <div class="contact-wrapper mt50 mb50">
            <div class="row">
                <div class="col-md-4">
                    <h5 class="text-upper text-bold">Контакты в Москве:</h5>
                    <ul class="footer-menu__list footer-menu__list-contacts contact-menu__list list-unstyled">
                        <li class="footer-menu__item contact-menu__item">
                            <p><span class="footer-menu__item-icon contact-menu__item-icon street"></span>г. Москва, Кутузовский пр-т, 36-к.4</p>
                        </li>
                        <li class="footer-menu__item contact-menu__item">
                            <p><span class="footer-menu__item-icon contact-menu__item-icon phone"></span>+7 (495) 786-10-80</p>
                        </li>
                        <li class="footer-menu__item contact-menu__item">
                            <p class="green"><span class="footer-menu__item-icon contact-menu__item-icon email"></span>hello@amwellgroup.ru</p>
                        </li>
                        <li class="footer-menu__item contact-menu__item">
                            <p class="green"><span class="footer-menu__item-icon contact-menu__item-icon skype"></span>amwellgroup</p>
                        </li>
                    </ul>
                </div>
                <div class="col-md-4">
                    <h5 class="text-upper text-bold">Контакты в Марокко:</h5>
                    <ul class="footer-menu__list footer-menu__list-contacts contact-menu__list list-unstyled">
                        <li class="footer-menu__item contact-menu__item">
                            <p><span class="footer-menu__item-icon contact-menu__item-icon street"></span>г. Марракеш, Марокко, <br/>Rue de la Liberté, 40000</p>
                        </li>
                        <li class="footer-menu__item contact-menu__item">
                            <p class="green"><span class="footer-menu__item-icon contact-menu__item-icon email"></span>mc@amwellgroup.com</p>
                        </li>
                    </ul>
                </div>
                <div class="col-md-4">
                    <h5 class="text-upper text-bold">Контакты в Эквадор:</h5>
                    <ul class="footer-menu__list footer-menu__list-contacts contact-menu__list list-unstyled">
                        <li class="footer-menu__item contact-menu__item">
                            <p><span class="footer-menu__item-icon contact-menu__item-icon street"></span>г. Кито, Эквадор, <br/>Avenida Río Amazonas, EC170135</p>
                        </li>
                        <li class="footer-menu__item contact-menu__item">
                            <p class="green"><span class="footer-menu__item-icon contact-menu__item-icon email"></span>eq@amwellgroup.com</p>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>


    <?php if (!$_smarty_tpl->tpl_vars['product']->value->id) {?>
        <?php echo $_smarty_tpl->getSubTemplate ('blocks/trust.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

    <?php }?>

    <?php echo $_smarty_tpl->getSubTemplate ('footer.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

    <div class="complete-dialog text-center"><p>Спасибо!</p><p>Ваша заявка принята!</p></div>

    <?php echo '<script'; ?>
 src="design/js/jquery-1.11.2.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="design/js/bootstrap.min.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="design/js/revolution.min.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="design/js/revo-tools.min.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="design/js/slick.min.js"><?php echo '</script'; ?>
>

    
        <?php echo '<script'; ?>
 type="text/javascript">
            var revApi;
            $(window).load(function(){
                initHeaderPosition();
            })

            //header position init
            function initHeaderPosition(){
                var win = $(window);
                var header = $('.navbar-fixed-top');
                function calcPosition(obj){
                    obj.stop().css({left: parseInt(obj.attr('data-top')) - win.scrollLeft()});
                    win.scroll(function(){
                        obj.stop().css({left: parseInt(obj.attr('data-top')) - win.scrollLeft()});
                    })
                }
                if(header.length){
                    header.attr('data-top',parseInt(header.css('left')));
                    calcPosition(header);
                }
            }
            $(document).ready(function() {
                $('.form__get-price').on('submit', function(e){
                    e.preventDefault();
                    var $self = $(this),
                        data = $self.serialize(),
                        popup = $('.complete-dialog');
                    $.ajax({
                        url: 'ajax/getPrice.php',
                        method: 'post',
                        data: data,
                        success: function(data){
                            $self.trigger('reset');
                            popup.show();
                            setTimeout(function(){
                                popup.fadeOut('fast');
                            }, 2000);
                        }
                    });
                });

                if($('#bottomSlider').revolution == undefined)
                    revslider_showDoubleJqueryError('#bottomSlider');
                else
                    revApi = $('#bottomSlider').show()
                        .revolution({
                            dottedOverlay:"none",
                            delay:6000,
                            startwidth:1170,
                            startheight:460,
                            hideThumbs:200,
                            thumbWidth:100,
                            thumbHeight:50,
                            thumbAmount:2,
                            navigationType:"none",
                            navigationArrows:"none",
                            navigationStyle:"round",
                            touchenabled:"off",
                            onHoverStop:"off",
                            keyboardNavigation:"off",
                            navigationHAlign:"center",
                            navigationVAlign:"bottom",
                            navigationHOffset:0,
                            navigationVOffset:20,
                            soloArrowLeftHalign:"left",
                            soloArrowLeftValign:"center",
                            soloArrowLeftHOffset:20,
                            soloArrowLeftVOffset:0,
                            soloArrowRightHalign:"right",
                            soloArrowRightValign:"center",
                            soloArrowRightHOffset:20,
                            soloArrowRightVOffset:0,
                            shadow:0,
                            fullWidth:"on",
                            fullScreen:"off",
                            spinner:"spinner0",
                            stopLoop:"off",
                            stopAfterLoops:-1,
                            stopAtSlide:-1,
                            shuffle:"off",
                            autoHeight:"on",
                            forceFullWidth:"on",
                            hideThumbsOnMobile:"off",
                            hideNavDelayOnMobile:1500,
                            hideBulletsOnMobile:"off",
                            hideArrowsOnMobile:"off",
                            hideThumbsUnderResolution:0,
                            hideSliderAtLimit:0,
                            hideCaptionAtLimit:0,
                            hideAllCaptionAtLilmit:0,
                            startWithSlide:0,
                            fullScreenOffsetContainer: ""
                        });

            });
        <?php echo '</script'; ?>
>
    

    
    <?php echo '<script'; ?>
 src="http://api-maps.yandex.ru/2.0/?load=package.full&lang=ru-RU" type="text/javascript"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 type="text/javascript">
        function initMaps() {
            var cntr = [55.750727, 37.562272]; //32.219596,52.767002
            var myMap = new ymaps.Map('contactMap', {
                center:cntr,
                zoom:16,
                behaviors:["default"]
            });

            var myPlacemark = new ymaps.Placemark([55.750727, 37.562272],
                {
                    hintContent: 'Метка Amwell Group'
                },
                {
                    iconImageHref:'/design/images/pin.png',
                    iconImageSize:[38, 69]
                }
            );
            myMap.geoObjects.add(myPlacemark);
        }
        ymaps.ready(initMaps);
    <?php echo '</script'; ?>
>

</body>
</html><?php }} ?>
<?php /* Smarty version Smarty-3.1.21-dev, created on 2015-06-08 09:56:01
         compiled from "/Users/evgeniy/sites/awb/design/html/blocks/contact_header.tpl" */ ?>
<?php if ($_valid && !is_callable('content_55753c811d9384_16672956')) {function content_55753c811d9384_16672956($_smarty_tpl) {?><div class="container-fluid contact-header clearfix">
    <div id="contactMap"></div>
    <div class="contact-header__inner">
        <div class="row">
            <div class="col-md-12">
                <?php /*  Call merged included template "blocks/contact_form.tpl" */
$_tpl_stack[] = $_smarty_tpl;
 $_smarty_tpl = $_smarty_tpl->setupInlineSubTemplate('blocks/contact_form.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0, '7167748065566d32eef9126-12120396');
content_55753c811deea3_36954763($_smarty_tpl);
$_smarty_tpl = array_pop($_tpl_stack); 
/*  End of included template "blocks/contact_form.tpl" */?>
            </div>
        </div>
    </div>
</div><?php }} ?>
<?php /* Smarty version Smarty-3.1.21-dev, created on 2015-06-08 09:56:01
         compiled from "/Users/evgeniy/sites/awb/design/html/blocks/contact_form.tpl" */ ?>
<?php if ($_valid && !is_callable('content_55753c811deea3_36954763')) {function content_55753c811deea3_36954763($_smarty_tpl) {?><form class="form-horizontal pull-right form__get-price" role="form">
    <p>Свяжитесь с нами</p>
    <div class="form-group">
        <label class="control-label col-sm-2" for="name">Имя *:</label>
        <div class="col-sm-12">
            <input type="text" required class="form-control" name="name" placeholder="Константин Никифоров">
            <span class="form__get-price_icon form__get-price_icon-user"></span>
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-sm-2" for="email">E-mail *:</label>
        <div class="col-sm-12 form__get-price_input">
            <input type="email" required class="form-control" name="email" placeholder="hello@amwellgroup.ru">
            <span class="form__get-price_icon form__get-price_icon-email"></span>
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-sm-2" for="email">Сообщение *:</label>
        <div class="col-md-12">
            <textarea class="form-control" name="message" id="message" placeholder="Задайте интересующие вас вопросы" cols="30" rows="4"></textarea>
        </div>
    </div>
    <div class="form-group">
        <div class="text-center col-sm-12">
            <button type="submit" class="btn btn-default contact-btn-send"></button>
        </div>
    </div>
</form><?php }} ?>
