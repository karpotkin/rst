<?php /* Smarty version Smarty-3.1.21-dev, created on 2015-06-17 21:14:21
         compiled from "/Users/evgeniy/sites/awb/design/html/footer.tpl" */ ?>
<?php /*%%SmartyHeaderCode:7893245495564bc8de67271-29535623%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '70d4f034f22d7173dff9e387634b8c177871abfc' => 
    array (
      0 => '/Users/evgeniy/sites/awb/design/html/footer.tpl',
      1 => 1434564674,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '7893245495564bc8de67271-29535623',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_5564bc8de80525_59715713',
  'variables' => 
  array (
    'pages' => 0,
    'p' => 0,
    'page' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5564bc8de80525_59715713')) {function content_5564bc8de80525_59715713($_smarty_tpl) {?><?php if (!is_callable('smarty_modifier_date_format')) include '/Users/evgeniy/sites/awb/libs/plugins/modifier.date_format.php';
?><div class="container-fluid l7">
    <div class="container foot_links">
        <div class="row text-center">
            <div class="footer-menu-box subscription-box">
                <h3 class="footer-menu__title">Рассылка</h3>
                <p>Подпишитесь на рассылку новостей от Amwell Group.</p>
                <form role="form" class="subscription-form" action="ajax/subscribe.php">
                    <div class="form-group">
                        <button type="submit" class="subscription-btn">
                            <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
                        </button>
                        <input type="email" class="subscription-input form-control" required="required" id="exampleInputEmail1" name="email" placeholder="Ваш Email...">
                    </div>
                </form>
            </div>
            <div class="footer-menu-box">
                <h3 class="footer-menu__title">Информация</h3>
                <ul class="footer-menu__list">
                    <li><a class="footer-menu__link" href="about">О компании</a></li>
                    <li><a class="footer-menu__link" href="contacts">Свяжитесь с нами</a></li>
                    <?php  $_smarty_tpl->tpl_vars['p'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['p']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['pages']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['p']->key => $_smarty_tpl->tpl_vars['p']->value) {
$_smarty_tpl->tpl_vars['p']->_loop = true;
?>
                        <?php if ($_smarty_tpl->tpl_vars['p']->value->menu_id==3) {?>
                            <li class="footer-menu__item" <?php if ($_smarty_tpl->tpl_vars['page']->value->id==$_smarty_tpl->tpl_vars['p']->value->id) {?>class="active"<?php }?>>
                                <a class="footer-menu__link" href="<?php echo $_smarty_tpl->tpl_vars['p']->value->url;?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['p']->value->name, ENT_QUOTES, 'UTF-8', true);?>
</a>
                            </li>
                        <?php }?>
                    <?php } ?>
                </ul>
            </div>
            <div class="footer-menu-box">
                <h3 class="footer-menu__title">возможности</h3>
                <ul class="footer-menu__list">
                    <?php  $_smarty_tpl->tpl_vars['p'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['p']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['pages']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['p']->key => $_smarty_tpl->tpl_vars['p']->value) {
$_smarty_tpl->tpl_vars['p']->_loop = true;
?>
                        <?php if ($_smarty_tpl->tpl_vars['p']->value->menu_id==4) {?>
                            <li class="footer-menu__item" <?php if ($_smarty_tpl->tpl_vars['page']->value->id==$_smarty_tpl->tpl_vars['p']->value->id) {?>class="active"<?php }?>>
                                <a class="footer-menu__link" href="<?php echo $_smarty_tpl->tpl_vars['p']->value->url;?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['p']->value->name, ENT_QUOTES, 'UTF-8', true);?>
</a>
                            </li>
                        <?php }?>
                    <?php } ?>
                </ul>
            </div>
            <div class="footer-menu-box">
                <h3 class="footer-menu__title">мы в соц сетях</h3>
                <ul class="footer-menu__list">
                    <li class="footer-menu__item"><a class="footer-menu__link" href="#">ВКонтакте</a></li>
                    <li class="footer-menu__item"><a class="footer-menu__link" href="#">Одноклассники</a></li>
                    <li class="footer-menu__item"><a class="footer-menu__link" href="#">Facebook</a></li>
                    <li class="footer-menu__item"><a class="footer-menu__link" href="#">Instagram</a></li>
                </ul>
            </div>
            <div class="footer-menu-box footer-menu__contacts">
                <h3 class="footer-menu__title">Контакты</h3>
                <ul class="footer-menu__list footer-menu__list-contacts list-unstyled">
                    <li class="footer-menu__item">
                        <p><span class="footer-menu__item-icon street"></span>г. Москва, Кутузовский пр-т, 36-к.4</p>
                    </li>
                    <li class="footer-menu__item">
                        <p><span class="footer-menu__item-icon phone"></span>+7 (495) 786-10-80</p>
                    </li>
                    <li class="footer-menu__item">
                        <p class="green"><span class="footer-menu__item-icon email"></span><a href="mailto:hello@amwellgroup.ru">hello@amwellgroup.ru</a></p>
                    </li>
                    <li class="footer-menu__item">
                        <p class="green"><span class="footer-menu__item-icon skype"></span><a href="skype:amwellgroup">amwellgroup</a></p>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<div class="container-fluid l8 footer">
    <div class="container">
        <div class="row">
            <div class="copyright pull-left">
                <p>Copyright © <?php echo smarty_modifier_date_format(time(),"%Y");?>
. All rights reserved</p>
            </div>
            <div class="social pull-right">
                <a class="footer__social-btn socicon" href="#">b</a>
                <a class="footer__social-btn socicon" href="#">a</a>
                <a class="footer__social-btn socicon" href="#">;</a>
                <a class="footer__social-btn socicon" href="#">.</a>
            </div>
        </div>
    </div>
</div><?php }} ?>
