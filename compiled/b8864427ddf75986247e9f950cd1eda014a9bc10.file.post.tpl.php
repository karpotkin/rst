<?php /* Smarty version Smarty-3.1.21-dev, created on 2015-06-01 08:54:55
         compiled from "/Users/evgeniy/sites/awb/design/html/post.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1645894137556bf304452674-33310403%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'b8864427ddf75986247e9f950cd1eda014a9bc10' => 
    array (
      0 => '/Users/evgeniy/sites/awb/design/html/post.tpl',
      1 => 1433138094,
      2 => 'file',
    ),
    '721d29d532c8456def5ae484ceb8ede5618804b4' => 
    array (
      0 => '/Users/evgeniy/sites/awb/design/html/layout.tpl',
      1 => 1433089679,
      2 => 'file',
    ),
    '20004da77ee302d1b36d5e9013e2a7be8a2503b1' => 
    array (
      0 => '/Users/evgeniy/sites/awb/design/html/blocks/default_header.tpl',
      1 => 1433090847,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1645894137556bf304452674-33310403',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_556bf3044f7819_15742667',
  'variables' => 
  array (
    'config' => 0,
    'meta_title' => 0,
    'meta_description' => 0,
    'meta_keywords' => 0,
    'product' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_556bf3044f7819_15742667')) {function content_556bf3044f7819_15742667($_smarty_tpl) {?><!DOCTYPE html>
<html>
<head>
	<base href="<?php echo $_smarty_tpl->tpl_vars['config']->value->root_url;?>
"/>
	<title><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['meta_title']->value, ENT_QUOTES, 'UTF-8', true);?>
</title>
	
	
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="description" content="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['meta_description']->value, ENT_QUOTES, 'UTF-8', true);?>
" />
	<meta name="keywords"    content="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['meta_keywords']->value, ENT_QUOTES, 'UTF-8', true);?>
" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
	
	
    <!--[if lt IE 9]>
    <?php echo '<script'; ?>
 src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"><?php echo '</script'; ?>
>
    <![endif]-->
	<link href="design/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <link href="design/css/base.css" rel="stylesheet" type="text/css"/>
    <link href="design/css/non-responsive.css" rel="stylesheet" type="text/css"/>
    <link href="design/css/slick.css" rel="stylesheet" type="text/css"/>
    <link href="design/css/revo-settings.css" rel="stylesheet" type="text/css"/>
    <link href="design/css/revo-style.css" rel="stylesheet" type="text/css"/>
	<link href="favicon.ico" rel="icon"          type="image/x-icon"/>
	<link href="favicon.ico" rel="shortcut icon" type="image/x-icon"/>
    
</head>
<body>
    <?php echo $_smarty_tpl->getSubTemplate ('header.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>


    
    <?php /*  Call merged included template "blocks/default_header.tpl" */
$_tpl_stack[] = $_smarty_tpl;
 $_smarty_tpl = $_smarty_tpl->setupInlineSubTemplate('blocks/default_header.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0, '1645894137556bf304452674-33310403');
content_556bf3aff1eab7_17688440($_smarty_tpl);
$_smarty_tpl = array_pop($_tpl_stack); 
/*  End of included template "blocks/default_header.tpl" */?>

    <div class="container-fluid news-box">
        <div class="wrapper">
            <div class="row">
                <h3><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['post']->value->name, ENT_QUOTES, 'UTF-8', true);?>
</h3>
                <p><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['date'][0][0]->date_modifier($_smarty_tpl->tpl_vars['post']->value->date);?>
</p>
                <div>
                    <?php echo $_smarty_tpl->tpl_vars['post']->value->annotation;?>

                    <?php echo $_smarty_tpl->tpl_vars['post']->value->text;?>

                </div>
            </div>
        </div>
    </div>



    <?php echo $_smarty_tpl->getSubTemplate ('sliders/bottom.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>


    <div class="container-fluid l5">
        <div class="container">
            <div class="row">
                <?php echo $_smarty_tpl->getSubTemplate ('blocks/news.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

                <?php echo $_smarty_tpl->getSubTemplate ('blocks/instagram.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

            </div>
        </div>
    </div>

    <?php if (!$_smarty_tpl->tpl_vars['product']->value->id) {?>
        <?php echo $_smarty_tpl->getSubTemplate ('blocks/trust.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

    <?php }?>

    <?php echo $_smarty_tpl->getSubTemplate ('footer.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

    <div class="complete-dialog text-center"><p>Спасибо!</p><p>Ваша заявка принята!</p></div>

    <?php echo '<script'; ?>
 src="design/js/jquery-1.11.2.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="design/js/bootstrap.min.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="design/js/revolution.min.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="design/js/revo-tools.min.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="design/js/slick.min.js"><?php echo '</script'; ?>
>

    
        <?php echo '<script'; ?>
 type="text/javascript">
            var revApi;
            $(window).load(function(){
                initHeaderPosition();
            })

            //header position init
            function initHeaderPosition(){
                var win = $(window);
                var header = $('.navbar-fixed-top');
                function calcPosition(obj){
                    obj.stop().css({left: parseInt(obj.attr('data-top')) - win.scrollLeft()});
                    win.scroll(function(){
                        obj.stop().css({left: parseInt(obj.attr('data-top')) - win.scrollLeft()});
                    })
                }
                if(header.length){
                    header.attr('data-top',parseInt(header.css('left')));
                    calcPosition(header);
                }
            }
            $(document).ready(function() {
                $('.form__get-price').on('submit', function(e){
                    e.preventDefault();
                    var $self = $(this),
                        data = $self.serialize(),
                        popup = $('.complete-dialog');
                    $.ajax({
                        url: 'ajax/getPrice.php',
                        method: 'post',
                        data: data,
                        success: function(data){
                            $self.trigger('reset');
                            popup.show();
                            setTimeout(function(){
                                popup.fadeOut('fast');
                            }, 2000);
                        }
                    });
                });

                if($('#bottomSlider').revolution == undefined)
                    revslider_showDoubleJqueryError('#bottomSlider');
                else
                    revApi = $('#bottomSlider').show()
                        .revolution({
                            dottedOverlay:"none",
                            delay:6000,
                            startwidth:1170,
                            startheight:460,
                            hideThumbs:200,
                            thumbWidth:100,
                            thumbHeight:50,
                            thumbAmount:2,
                            navigationType:"none",
                            navigationArrows:"none",
                            navigationStyle:"round",
                            touchenabled:"off",
                            onHoverStop:"off",
                            keyboardNavigation:"off",
                            navigationHAlign:"center",
                            navigationVAlign:"bottom",
                            navigationHOffset:0,
                            navigationVOffset:20,
                            soloArrowLeftHalign:"left",
                            soloArrowLeftValign:"center",
                            soloArrowLeftHOffset:20,
                            soloArrowLeftVOffset:0,
                            soloArrowRightHalign:"right",
                            soloArrowRightValign:"center",
                            soloArrowRightHOffset:20,
                            soloArrowRightVOffset:0,
                            shadow:0,
                            fullWidth:"on",
                            fullScreen:"off",
                            spinner:"spinner0",
                            stopLoop:"off",
                            stopAfterLoops:-1,
                            stopAtSlide:-1,
                            shuffle:"off",
                            autoHeight:"on",
                            forceFullWidth:"on",
                            hideThumbsOnMobile:"off",
                            hideNavDelayOnMobile:1500,
                            hideBulletsOnMobile:"off",
                            hideArrowsOnMobile:"off",
                            hideThumbsUnderResolution:0,
                            hideSliderAtLimit:0,
                            hideCaptionAtLimit:0,
                            hideAllCaptionAtLilmit:0,
                            startWithSlide:0,
                            fullScreenOffsetContainer: ""
                        });

            });
        <?php echo '</script'; ?>
>
    

    
</body>
</html><?php }} ?>
<?php /* Smarty version Smarty-3.1.21-dev, created on 2015-06-01 08:54:55
         compiled from "/Users/evgeniy/sites/awb/design/html/blocks/default_header.tpl" */ ?>
<?php if ($_valid && !is_callable('content_556bf3aff1eab7_17688440')) {function content_556bf3aff1eab7_17688440($_smarty_tpl) {?><div class="container-fluid l11 default-header clearfix">
    <div class="default-header__inner"></div>
</div><?php }} ?>
