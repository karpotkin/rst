<?PHP

require_once('View.php');

class ScopeView extends View
{

	function fetch()
	{
		
		$this->design->assign('meta_title', $this->page->meta_title);
		$this->design->assign('meta_keywords', $this->page->meta_keywords);
		$this->design->assign('meta_description', $this->page->meta_description);
		
		$this->body = $this->design->fetch('scope.tpl');
		return $this->body;
	}
	
	

}
