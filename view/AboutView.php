<?PHP

/**
 * Simpla CMS
 *
 * @copyright 	2011 Denis Pikusov
 * @link 		http://simplacms.ru
 * @author 		Denis Pikusov
 *
 * Этот класс использует шаблон products.tpl
 *
 */
 
require_once('View.php');

class AboutView extends View
{
 	/**
	 *
	 * Отображение списка товаров
	 *
	 */	
	function fetch()
	{
		
		$this->design->assign('meta_title', $this->page->meta_title);
		$this->design->assign('meta_keywords', $this->page->meta_keywords);
		$this->design->assign('meta_description', $this->page->meta_description);
		
		$this->body = $this->design->fetch('about.tpl');
		return $this->body;
	}
	
	

}
