<?PHP

require_once('View.php');

class PageView extends View
{
	function fetch()
	{
		$url = $this->request->get('page_url', 'string');

		$page = $this->pages->getPage($url);
var_dump($url);
		$page_parent = $this->pages->get_page_parent($page->parent_id);

		$this->design->assign('page_parent', $page_parent);

		//$parents = $this->pages->get_categories_tree();
  	
		//$this->design->assign('parents', $parents);
		
		// Отображать скрытые страницы только админу
		if(empty($page) || (!$page->visible && empty($_SESSION['admin']))){
            header("HTTP/1.0 404 Not Found");
			return $this->design->fetch('404.tpl');
		}else{
			$this->design->assign('page', $page);
			$this->design->assign('meta_title', $page->meta_title);
			$this->design->assign('meta_keywords', $page->meta_keywords);
			$this->design->assign('meta_description', $page->meta_description);
			
			return $this->design->fetch('page.tpl');
		}

	}
}