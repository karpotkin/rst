<?php

require_once('Master.php');

class Pages extends Master
{

	/*
	*
	* Функция возвращает страницу по ее id или url (в зависимости от типа)
	* @param $id id или url страницы
	*
	*/
	
	// Список указателей на категории в дереве категорий (ключ = id категории)
	private $all_categories;
	// Дерево категорий
	private $categories_tree;
	
	
	// Функция возвращает дерево категорий
	public function get_categories_tree($filter = array())
	{
		if(!isset($this->categories_tree))
			$this->init_categories($filter);
			
		return $this->categories_tree;
	}
	
	public function get_page($id)
	{
		if(gettype($id) == 'string')
			$where = $this->db->placehold(' WHERE url=? ', $id);
		else
			$where = $this->db->placehold(' WHERE id=? ', intval($id));
		
		$query = "SELECT * FROM __pages $where LIMIT 1";

		$this->db->query($query);
		$page = $this->db->result();

		if($page) $page->description = $this->getPageDescriptions($page->id);

		return $page;
	}

    public function getPage($id, $language_id = 2)
    {
        if(gettype($id) == 'string')
            $where = $this->db->placehold(' WHERE url=? ', $id);
        else
            $where = $this->db->placehold(' WHERE id=? ', intval($id));

        $query = "SELECT * FROM __pages p LEFT JOIN __pages_description pd on pd.page_id = p.id $where and language_id = $language_id LIMIT 1";

        $this->db->query($query);
        return $this->db->result();
    }

	public function getPageDescriptions($page_id) {

		$this->db->query('SELECT * FROM __pages_description WHERE page_id = ?', (int)$page_id);

		return $this->db->results();

	}

	public function get_page_parent($parent_id)
	{

		$where = $this->db->placehold(' WHERE id=? ', intval($parent_id));
		
		$query = "SELECT p.id, parent_id, url, header, name, meta_title, meta_description, meta_keywords, body, menu_id, position, visible, image, page_text
				  FROM __pages p
		          LEFT JOIN __pages_description pd on pd.page_id = p.id
		          $where and pd.language_id = 1 LIMIT 1";

		$this->db->query($query);
		return $this->db->result();
	}
	
	/*
	*
	* Функция возвращает массив страниц, удовлетворяющих фильтру
	* @param $filter
	*
	*/
	
	
	public function get_pages($filter = array())
	{	
		$menu_filter = '';
		$visible_filter = '';
		$pages = array();

		if(isset($filter['menu_id']))
			$menu_filter = $this->db->placehold('AND menu_id in (?@)', (array)$filter['menu_id']);

		$filter['language_id'] = isset($filter['language_id']) ? $filter['language_id'] : 2;
		$language_filter = $this->db->placehold('language_id = ?', intval($filter['language_id']));

		if(isset($filter['visible']))
			$visible_filter = $this->db->placehold('AND visible = ?', intval($filter['visible']));
		
		$query = "SELECT p.id, parent_id, url, header, name, meta_title, meta_description, meta_keywords, body, menu_id, position, visible, image
		          FROM __pages p
		          LEFT JOIN __pages_description pd on pd.page_id = p.id
		          WHERE $language_filter $menu_filter $visible_filter ORDER BY position";

		$this->db->query($query);
		
		foreach($this->db->results() as $page)
			$pages[$page->id] = $page;
			
		return $pages;
	}

	/*
	*
	* Создание страницы
	*
	*/	
	public function add_page($page)
	{	
		$query = $this->db->placehold('INSERT INTO __pages SET ?%', $page);

		if(!$this->db->query($query))
			return false;

		$id = $this->db->insert_id();
		$this->db->query("UPDATE __pages SET position=id WHERE id=?", $id);	
		return $id;
	}

    public function add_image($id, $filename)
    {
        $this->db->query("UPDATE __pages SET image = ? WHERE id = ?", $filename, $id);
    }

	public function addPageDescription($description)
	{
		$query = $this->db->placehold('INSERT INTO __pages_description SET ?%', $description);

		if(!$this->db->query($query))
			return false;

		$id = $this->db->insert_id();

		return $id;
	}
	
	/*
	*
	* Обновить страницу
	*
	*/
	public function update_page($id, $page)
	{	
		$query = $this->db->placehold('UPDATE __pages SET ?% WHERE id in (?@)', $page, (array)$id);
		if(!$this->db->query($query))
			return false;
		return $id;
	}


	public function updatePageDescription($id, $description)
	{
		$query = $this->db->placehold('UPDATE __pages_description SET ?% WHERE id in (?@)', $description, (array)$id);
		if(!$this->db->query($query))
			return false;
		return $id;
	}
	
	/*
	*
	* Удалить страницу
	*
	*/	
	public function delete_page($id)
	{
		if(!empty($id))
		{
			$query = $this->db->placehold("DELETE FROM __pages WHERE id=? LIMIT 1", intval($id));
			if($this->db->query($query))
				return true;
		}
		return false;
	}	
	
	/*
	*
	* Функция возвращает массив меню
	*
	*/
	public function get_menus()
	{
		$menus = array();
		$query = "SELECT * FROM __menu ORDER BY position";
		$this->db->query($query);
		foreach($this->db->results() as $menu)
			$menus[$menu->id] = $menu;
		return $menus;
	}
	
	/*
	*
	* Функция возвращает меню по id
	* @param $id
	*
	*/
	public function get_menu($menu_id)
	{	
		$query = $this->db->placehold("SELECT * FROM __menu WHERE id=? LIMIT 1", intval($menu_id));
		$this->db->query($query);
		return $this->db->result();
	}
	
	
	private function init_categories($filter)
	{
		$visible_filter = '';

		$filter['language_id'] = isset($filter['language_id']) ? $filter['language_id'] : 2;
		$language_filter = $this->db->placehold('language_id = ?', intval($filter['language_id']));

		if(isset($filter['visible']))
			$visible_filter = $this->db->placehold('AND visible = ?', intval($filter['visible']));

		// Дерево категорий
		$tree = new stdClass();
		$tree->subcategories = array();
		
		// Указатели на узлы дерева
		$pointers = array();
		$pointers[0] = &$tree;
		$pointers[0]->path = array();

		// Выбираем все категории
		$query = $this->db->placehold("SELECT p.id, parent_id, url, header, name, meta_title, meta_description, meta_keywords, body, menu_id, position, visible
									   FROM __pages p
		          					   LEFT JOIN __pages_description pd on pd.page_id = p.id
									   WHERE $language_filter $visible_filter ORDER BY p.parent_id, p.position");

		$this->db->query($query);
		$categories = $this->db->results();
				
		$finish = false;
		// Не кончаем, пока не кончатся категории, или пока ниодну из оставшихся некуда приткнуть
		while(!empty($categories)  && !$finish)
		{
			$flag = false;
			// Проходим все выбранные категории
			foreach($categories as $k=>$category)
			{
				if(isset($pointers[$category->parent_id]))
				{
					// В дерево категорий (через указатель) добавляем текущую категорию
					$pointers[$category->id] = $pointers[$category->parent_id]->subcategories[] = $category;
					
					// Путь к текущей категории
					$curr = $pointers[$category->id];
					$pointers[$category->id]->path = array_merge((array)$pointers[$category->parent_id]->path, array($curr));
					
					// Убираем использованную категорию из массива категорий
					unset($categories[$k]);
					$flag = true;
				}
			}
			if(!$flag) $finish = true;
		}
		
		// Для каждой категории id всех ее деток узнаем
		$ids = array_reverse(array_keys($pointers));
		foreach($ids as $id)
		{
			if($id>0)
			{
				$pointers[$id]->children[] = $id;

				if(isset($pointers[$pointers[$id]->parent_id]->children))
					$pointers[$pointers[$id]->parent_id]->children = array_merge($pointers[$id]->children, $pointers[$pointers[$id]->parent_id]->children);
				else
					$pointers[$pointers[$id]->parent_id]->children = $pointers[$id]->children;
			}
		}
		unset($pointers[0]);
		unset($ids);

		$this->categories_tree = $tree->subcategories;
		$this->all_categories = $pointers;	
	}
	
}
