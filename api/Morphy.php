<?php

require_once('Simpla.php');
require_once('Morphy/src/common.php');

class Morphy extends Simpla
{

    public $morphy;

    public function __construct()
    {
        parent::__construct();
        // set some options
        $opts = array(
            // storage type, follow types supported
            // PHPMORPHY_STORAGE_FILE - use file operations(fread, fseek) for dictionary access, this is very slow...
            // PHPMORPHY_STORAGE_SHM - load dictionary in shared memory(using shmop php extension), this is preferred mode
            // PHPMORPHY_STORAGE_MEM - load dict to memory each time when phpMorphy intialized, this useful when shmop ext. not activated. Speed same as for PHPMORPHY_STORAGE_SHM type
            'storage' => PHPMORPHY_STORAGE_FILE,
            // Enable prediction by suffix
            'predict_by_suffix' => true, 
            // Enable prediction by prefix
            'predict_by_db' => true,
            // TODO: comment this
            'graminfo_as_text' => true,
        );

        // Path to directory where dictionaries located
        $dir = 'Morphy/dicts';
        $lang = 'ru_RU';

        // Create phpMorphy instance
        try {
            $this->morphy = new phpMorphy($dir, $lang, $opts);
        } catch(phpMorphy_Exception $e) {
            die('Error occured while creating phpMorphy instance: ' . PHP_EOL . $e);
        }

    }

    public function convert($words, $parent = null){

        if($parent){
            $parent = trim( mb_convert_case($parent, MB_CASE_UPPER, "UTF-8") );
            $tmp = explode(" ", $parent);
            if(count($tmp) > 1){
                $parent = end($tmp);
            }
            $gender = $this->morphy->castFormByGramInfo($parent, null, array('ЕД', 'ИМ'), false);
            var_dump($gender);
            /*foreach ($gender->getByPartOfSpeech('С') as $g) {
                var_dump($g->getBaseForm());
            }*/
        }

        $words = mb_convert_case($words, MB_CASE_UPPER, "UTF-8");
        $words = explode(" ", $words);


        foreach($words as $word) {
            $all = $this->morphy->castFormByGramInfo($word, null, array('ЕД', 'ПР', 'ЖР'), false);
            //var_dump($all);
            $result[] = $all['form'];
        }

        return $result;
    }

    

}
