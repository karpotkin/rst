<?php

class Master
{
	// Свойства - Классы API
	private $classes = array(
		'config'     => 'Config',
		'request'    => 'Request',
		'db'         => 'Database',
		'settings'   => 'Settings',
		'design'     => 'Design',
		'response'   => 'Response',
        'slider'     => 'Slider',
		'calls'      => 'Calls',
		'money'      => 'Money',
		'pages'      => 'Pages',
		'blog'       => 'Blog',
		'articles'	 => 'Articles',
		'image'      => 'Image',
		'payment'    => 'Payment',
		'comments'   => 'Comments',
		'feedbacks'  => 'Feedbacks',
		'notify'     => 'Notify',
		'send'       => 'Send',
        'auth'       => 'Auth',
        'func'       => 'Func',
        'insta'      => 'Instagram',
        'products'   => 'Products',
        'brands'     => 'Brands',
        'categories' => 'Categories',
        'twitter'    => 'Twitter',
		'rm'		 => 'RestaurantMenu'
	);
	
	// Созданные объекты
	private static $objects = array();
	
	/**
	 * Конструктор оставим пустым, но определим его на случай обращения parent::__construct() в классах API
	 */
	public function __construct()
	{
	}

	/**
	 * Магический метод, создает нужный объект API
	 */
	public function __get($name)
	{
		// Если такой объект уже существует, возвращаем его
		if(isset(self::$objects[$name]))
		{
			return(self::$objects[$name]);
		}
		
		// Если запрошенного API не существует - ошибка
		if(!array_key_exists($name, $this->classes))
		{
			return null;
		}
		
		// Определяем имя нужного класса
		$class = $this->classes[$name];
		
		// Подключаем его
		include_once('api/'.$class.'.php');
		
		// Сохраняем для будущих обращений к нему
		self::$objects[$name] = new $class();
		
		// Возвращаем созданный объект
		return self::$objects[$name];
	}
}