<?php

require_once('Master.php');

class Calls extends Master
{
	
	public function get_calls()
	{
		
		$query = $this->db->placehold("SELECT * FROM __callme WHERE 1");
		$this->db->query($query);

		return $this->db->results();
	}

	public function update_call($id, $callback)
	{	
		$query = $this->db->placehold('UPDATE __callme SET callback=?, calltime=CURRENT_TIMESTAMP WHERE id in (?@)', $callback, (array)$id);
		if(!$this->db->query($query))
			return false;
		return $id;
	}
		
}
