<?php
/**
 * Created by PhpStorm.
 * User: evgeniy
 * Date: 27/05/15
 * Time: 00:33
 */
class Instagram {

    const API_URL = 'https://api.instagram.com/v1';

    public function fetch($token, $user = null)
    {
        $url = self::API_URL.'/users/'.($user ? $user : 'self').'/media/recent?access_token='.$token;
        
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 20);
        $result = curl_exec($ch);
        curl_close($ch);

        $result = json_decode($result);
        return $result->data;
    }
}