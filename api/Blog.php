<?php

require_once('Master.php');

class Blog extends Master
{

	/*
	*
	* Функция возвращает пост по его id или url
	* (в зависимости от типа аргумента, int - id, string - url)
	* @param $id id или url поста
	*
	*/

    private $categories = array(
        'company' => 'Новости компании',
        'policy-update' => 'Обновление политики',
        'expansion' => 'Расширение',
        'partners' => 'Сотрудничество',
        'others' => 'Прочее'
    );

    public function getPost($id, $language_id = 2)
    {
        if(gettype($id) == 'string')
            $where = $this->db->placehold(' WHERE url=? ', $id);
        else
            $where = $this->db->placehold(' WHERE id=? ', intval($id));

        $query = $this->db->placehold("SELECT b.id, url, name, annotation, text, meta_title, meta_keywords, meta_description, visible, date, image
		                               FROM __blog b
		                               LEFT JOIN __blog_description bd on bd.blog_id = b.id
		                               $where and language_id = $language_id LIMIT 1");
        $this->db->query($query);
        return $this->db->result();
    }

	public function get_post($id)
	{
		if(is_int($id))
			$where = $this->db->placehold(' WHERE id=? ', intval($id));
		else
			$where = $this->db->placehold(' WHERE url=? ', $id);

        $query = "SELECT * FROM __blog $where LIMIT 1";

        $this->db->query($query);
        $post = $this->db->result();

        if($post) $post->description = $this->getPostDescriptions($post->id);

        return $post;
	}

    public function getPostDescriptions($blog_id) {

        $this->db->query('SELECT * FROM __blog_description WHERE blog_id = ?', (int)$blog_id);

        return $this->db->results();

    }


    public function addPostDescription($description)
	{
		$query = $this->db->placehold('INSERT INTO __blog_description SET ?%', $description);

		if(!$this->db->query($query))
			return false;

		$id = $this->db->insert_id();

		return $id;
	}

    public function getCategoriesArray(){
        return $this->categories;
    }

    public function getCategories($filters = array())
    {
        $categories = $this->categories;

        if(isset($filters['count']) && $filters['count']){
            $categoryCount = $this->toArray($this->countCategoryPosts());

            $category = new stdClass();

            foreach($categories as $key => $cat){
                $category->url = $key;
                $category->name = $cat;
                $category->count = isset($categoryCount[$key]) ? $categoryCount[$key] : 0;
                $results[] = $category;
                unset($category);
            }

            return $results;
        }

        return $categories;
    }

    public function toArray($categories)
    {
        foreach($categories as $cat){
            $new[$cat->category_id] = $cat->count;
        }

        return $new;
    }

    public function countCategoryPosts()
    {
        $query = $this->db->placehold("SELECT COUNT(b.id) as count, b.category_id FROM __blog b WHERE b.category_id != '0' GROUP BY b.category_id");

        $this->db->query($query);
        return $this->db->results();
    }
	
	/*
	*
	* Функция возвращает массив постов, удовлетворяющих фильтру
	* @param $filter
	*
	*/
	public function get_posts($filter = array())
	{	
		// По умолчанию
		$limit = 1000;
		$page = 1;
		$post_id_filter = '';
		$visible_filter = '';
		$keyword_filter = '';
        $category_id_filter = '';
        $important_filter = '';

        $filter['language_id'] = isset($filter['language_id']) ? $filter['language_id'] : 2;
        $language_filter = $this->db->placehold('language_id = ?', intval($filter['language_id']));

        if(isset($filter['limit']))
			$limit = max(1, intval($filter['limit']));

		if(isset($filter['page']))
			$page = max(1, intval($filter['page']));

		if(!empty($filter['id']))
			$post_id_filter = $this->db->placehold('AND b.id in(?@)', (array)$filter['id']);

        if(!empty($filter['category_id']))
            $category_id_filter = $this->db->placehold('AND b.category_id = ?', $filter['category_id']);
			
		if(isset($filter['visible']))
			$visible_filter = $this->db->placehold('AND b.visible = ?', intval($filter['visible']));

        if(isset($filter['important']))
            $important_filter = $this->db->placehold('AND b.important = ?', intval($filter['important']));
		
		if(isset($filter['keyword']))
		{
			$keywords = explode(' ', $filter['keyword']);
			foreach($keywords as $keyword)
				$keyword_filter .= $this->db->placehold('AND (b.name LIKE "%'.mysql_real_escape_string(trim($keyword)).'%" OR b.meta_keywords LIKE "%'.mysql_real_escape_string(trim($keyword)).'%") ');
		}

		$sql_limit = $this->db->placehold(' LIMIT ?, ? ', ($page-1)*$limit, $limit);

		$query = $this->db->placehold("SELECT b.id, url, name, annotation, text,
		                                      meta_title, meta_keywords, meta_description, visible, date, image
		                                      FROM __blog b
		                                      LEFT JOIN __blog_description bd on bd.blog_id = b.id
		                                      WHERE $language_filter $post_id_filter $visible_filter $keyword_filter $category_id_filter $important_filter
		                                      ORDER BY date DESC, id DESC $sql_limit");
		
		$this->db->query($query);
		return $this->db->results();
	}
	
	
	/*
	*
	* Функция вычисляет количество постов, удовлетворяющих фильтру
	* @param $filter
	*
	*/
	public function count_posts($filter = array())
	{	
		$post_id_filter = '';
		$visible_filter = '';
		$keyword_filter = '';
		
		if(!empty($filter['id']))
			$post_id_filter = $this->db->placehold('AND b.id in(?@)', (array)$filter['id']);
			
		if(isset($filter['visible']))
			$visible_filter = $this->db->placehold('AND b.visible = ?', intval($filter['visible']));		

		if(isset($filter['keyword']))
		{
			$keywords = explode(' ', $filter['keyword']);
			foreach($keywords as $keyword)
				$keyword_filter .= $this->db->placehold('AND (b.name LIKE "%'.mysql_real_escape_string(trim($keyword)).'%" OR b.meta_keywords LIKE "%'.mysql_real_escape_string(trim($keyword)).'%") ');
		}
		
		$query = "SELECT COUNT(distinct b.id) as count
		          FROM __blog b WHERE 1 $post_id_filter $visible_filter $keyword_filter";

		if($this->db->query($query))
			return $this->db->result('count');
		else
			return false;
	}
	
	/*
	*
	* Создание поста
	* @param $post
	*
	*/	
	public function add_post($post)
	{	
		if(isset($post->date))
		{
			$date = $post->date;
			unset($post->date);
			$date_query = $this->db->placehold(', date=STR_TO_DATE(?, ?)', $date, $this->settings->date_format);
		}
		$query = $this->db->placehold("INSERT INTO __blog SET ?% $date_query", $post);
		
		if(!$this->db->query($query))
			return false;
		else
			return $this->db->insert_id();
	}
	
	
	/*
	*
	* Обновить пост(ы)
	* @param $post
	*
	*/	
	public function update_post($id, $post)
	{
		$query = $this->db->placehold("UPDATE __blog SET ?% WHERE id in(?@) LIMIT ?", $post, (array)$id, count((array)$id));
		$this->db->query($query);
		return $id;
	}

    public function updatePostDescription($id, $description)
    {
        $query = $this->db->placehold('UPDATE __blog_description SET ?% WHERE id = ?', $description, $id);
        if(!$this->db->query($query))
            return false;
        return $id;
    }

	/*
	*
	* Удалить пост
	* @param $id
	*
	*/	
	public function delete_post($id)
	{
		if(!empty($id))
		{
			$query = $this->db->placehold("DELETE FROM __blog WHERE id=? LIMIT 1", intval($id));
			if($this->db->query($query))
			{
				$query = $this->db->placehold("DELETE FROM __comments WHERE type='blog' AND object_id=? LIMIT 1", intval($id));
				if($this->db->query($query))
					return true;
			}
							
		}
		return false;
	}	
	

	/*
	*
	* Следующий пост
	* @param $post
	*
	*/	
	public function get_next_post($id)
	{
		$this->db->query("SELECT date FROM __blog WHERE id=? LIMIT 1", $id);
		$date = $this->db->result('date');

		$this->db->query("(SELECT id FROM __blog WHERE date=? AND id>? AND visible  ORDER BY id limit 1)
		                   UNION
		                  (SELECT id FROM __blog WHERE date>? AND visible ORDER BY date, id limit 1)",
		                  $date, $id, $date);
		$next_id = $this->db->result('id');
		if($next_id)
			return $this->get_post(intval($next_id));
		else
			return false; 
	}
	
	/*
	*
	* Предыдущий пост
	* @param $post
	*
	*/	
	public function get_prev_post($id)
	{
		$this->db->query("SELECT date FROM __blog WHERE id=? LIMIT 1", $id);
		$date = $this->db->result('date');

		$this->db->query("(SELECT id FROM __blog WHERE date=? AND id<? AND visible ORDER BY id DESC limit 1)
		                   UNION
		                  (SELECT id FROM __blog WHERE date<? AND visible ORDER BY date DESC, id DESC limit 1)",
		                  $date, $id, $date);
		$prev_id = $this->db->result('id');
		if($prev_id)
			return $this->get_post(intval($prev_id));
		else
			return false; 
	}
}
