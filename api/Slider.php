<?php

/**
 * Работа с товарами
 *
 * @copyright 	2011 Denis Pikusov
 * @link 		http://simplacms.ru
 * @author 		Denis Pikusov
 *
 */

require_once('Master.php');

class Slider extends Master
{
	/**
	* Функция возвращает товары
	* Возможные значения фильтра:
	* id - id товара или их массив
	* category_id - id категории или их массив
	* brand_id - id бренда или их массив
	* page - текущая страница, integer
	* limit - количество товаров на странице, integer
	* sort - порядок товаров, возможные значения: position(по умолчанию), name, price
	* keyword - ключевое слово для поиска
	* features - фильтр по свойствам товара, массив (id свойства => значение свойства)
	*/
	public function get_objects($filter = array())
	{		
		// По умолчанию
		$limit = 100;
		$page = 1;
		$category_id_filter = '';
		$object_id_filter = '';
		$features_filter = '';
		$keyword_filter = '';
		$visible_filter = '';
		$is_featured_filter = '';
		$group_by = '';
		$order = 'p.position DESC';

		if(isset($filter['limit']))
			$limit = max(1, intval($filter['limit']));

		if(isset($filter['page']))
			$page = max(1, intval($filter['page']));

		$sql_limit = $this->db->placehold(' LIMIT ?, ? ', ($page-1)*$limit, $limit);

		if(!empty($filter['id']))
			$object_id_filter = $this->db->placehold('AND p.id in(?@)', (array)$filter['id']);

		if(!empty($filter['category_id']))
		{
			$category_id_filter = $this->db->placehold('INNER JOIN __slider_categories pc ON pc.object_id = p.id AND pc.category_id in(?@)', (array)$filter['category_id']);
			$group_by = "GROUP BY p.id";
		}

		if(!empty($filter['featured']))
			$is_featured_filter = $this->db->placehold('AND p.featured=?', intval($filter['featured']));

		if(!empty($filter['visible']))
			$visible_filter = $this->db->placehold('AND p.visible=?', intval($filter['visible']));

 		if(!empty($filter['sort']))
			switch ($filter['sort'])
			{
				case 'position':
				$order = 'p.position DESC';
				break;
				case 'name':
				$order = 'p.name';
				break;
				case 'created':
				$order = 'p.created DESC';
				break;
			}

		if(!empty($filter['keyword']))
		{
			$keywords = explode(' ', $filter['keyword']);
			foreach($keywords as $keyword)
				$keyword_filter .= $this->db->placehold('AND (p.name LIKE "%'.mysql_real_escape_string(trim($keyword)).'%" OR p.meta_keywords LIKE "%'.mysql_real_escape_string(trim($keyword)).'%") ');
		}

		if(!empty($filter['features']) && !empty($filter['features']))
			foreach($filter['features'] as $feature=>$value)
				$features_filter .= $this->db->placehold('AND p.id in (SELECT object_id FROM __options WHERE feature_id=? AND value=? ) ', $feature, $value);

		$query = "SELECT  
					p.id,
					p.name,
					p.body,
					p.rating,
					p.votes,
					p.position,
					p.created as created,
					p.visible, 
					p.featured, 
					p.meta_title, 
					p.meta_keywords, 
					p.meta_description
				FROM __slider p		
				$category_id_filter 
				WHERE 
					1
					$object_id_filter
					$features_filter
					$keyword_filter
					$is_featured_filter
					$visible_filter
				$group_by
				ORDER BY $order
					$sql_limit";

		$query = $this->db->placehold($query);
		$this->db->query($query);

		return $this->db->results();
	}

	/**
	* Функция возвращает количество товаров
	* Возможные значения фильтра:
	* category_id - id категории или их массив
	* brand_id - id бренда или их массив
	* keyword - ключевое слово для поиска
	* features - фильтр по свойствам товара, массив (id свойства => значение свойства)
	*/
	public function count_objects($filter = array())
	{		
		$category_id_filter = '';
		$brand_id_filter = '';
		$keyword_filter = '';
		$visible_filter = '';
		$is_featured_filter = '';
		$discounted_filter = '';
		$features_filter = '';
		
		if(!empty($filter['category_id']))
			$category_id_filter = $this->db->placehold('INNER JOIN __slider_categories pc ON pc.object_id = p.id AND pc.category_id in(?@)', (array)$filter['category_id']);
            
		if(isset($filter['keyword']))
		{
			$keywords = explode(' ', $filter['keyword']);
			foreach($keywords as $keyword)
				$keyword_filter .= $this->db->placehold('AND (p.name LIKE "%'.mysql_real_escape_string(trim($keyword)).'%" OR p.meta_keywords LIKE "%'.mysql_real_escape_string(trim($keyword)).'%") ');
		}

		if(!empty($filter['featured']))
			$is_featured_filter = $this->db->placehold('AND p.featured=?', intval($filter['featured']));

		if(!empty($filter['visible']))
			$visible_filter = $this->db->placehold('AND p.visible=?', intval($filter['visible']));
		
		
		if(!empty($filter['features']) && !empty($filter['features']))
			foreach($filter['features'] as $feature=>$value)
				$features_filter .= $this->db->placehold('AND p.id in (SELECT object_id FROM __options WHERE feature_id=? AND value=? ) ', $feature, $value);
		
		$query = "SELECT count(distinct p.id) as count
				FROM __slider AS p
				$category_id_filter
				WHERE 1
					$keyword_filter
					$is_featured_filter
					$visible_filter
					$features_filter ";

		$this->db->query($query);	
		return $this->db->result('count');
	}


	/**
	* Функция возвращает товар по id
	* @param	$id
	* @retval	object
	*/
	public function get_object($id)
	{
		if(is_int($id))
			$filter = $this->db->placehold('AND p.id = ?', $id);
		$query = $this->db->placehold("SELECT DISTINCT
					p.id,
					p.name,
					p.body,
					p.rating,
					p.votes,
					p.position,
					p.created as created,
					p.visible, 
					p.featured, 
					p.meta_title, 
					p.meta_keywords, 
					p.meta_description,
                    p.logo
				FROM __slider AS p
                WHERE
					1 $filter 
                GROUP BY p.id
                LIMIT 1", intval($id));
		$this->db->query($query);
		$object = $this->db->result();
		return $object;
	}

	public function update_object($id, $object)
	{
		$query = $this->db->placehold("UPDATE __slider SET ?% WHERE id in (?@) LIMIT ?", $object, (array)$id, count((array)$id));
		if($this->db->query($query)){
			
        //$this->slider->update_slider_file();
            return $id;
            }
		else
			return false;
	}
	
	public function add_object($object)
	{	
		$object = (array) $object;
		var_dump($object);
		if($this->db->query("INSERT INTO __slider SET ?%", $object))
		{
			$id = $this->db->insert_id();
			$this->db->query("UPDATE __slider SET position=id WHERE id=?", $id);	
            $this->slider->update_slider_file();	
			return $id;
		}
		else
			echo "erooooooor!";
			return false;
	}
	
	
	/*
	*
	* Удалить товар
	*
	*/	
	public function delete_object($id)
	{
		if(!empty($id))
		{
			// Удаляем изображения
			$images = $this->get_images(array('object_id'=>$id));
			foreach($images as $i)
				$this->delete_image($i->id);
			
			// Удаляем категории
			$categories = $this->object_categories->get_categories(array('object_id'=>$id));
			foreach($categories as $c)
				$this->object_categories->delete_object_category($id, $c->id);
			
            // Удаляем связанные товары
			$related = $this->get_related_products($id);
			foreach($related as $r)
				$this->delete_related_product($id, $r->related_id);
            
			// Удаляем товар
			$query = $this->db->placehold("DELETE FROM __slider WHERE id=? LIMIT 1", intval($id));
			if($this->db->query($query)){
            //$this->slider->update_slider_file();
				return true;			}
		}
		return false;
	}	
	
	public function duplicate_object($id)
	{
    	$object = $this->get_object($id);
    	$object->id = null;
    	$object->created = null;

		// Сдвигаем товары вперед и вставляем копию на соседнюю позицию
    	$this->db->query('UPDATE __slider SET position=position+1 WHERE position>?', $object->position);
    	$new_id = $this->slider->add_object($object);
    	$this->db->query('UPDATE __slider SET position=? WHERE id=?', $object->position+1, $new_id);
    	
    	// Дублируем изображения
    	$images = $this->get_images(array('object_id'=>$id));
    	foreach($images as $image)
    		$this->add_image($new_id, $image->filename);
			
        // Дублируем связанные товары
		$related = $this->get_related_products($id);
        //$this->slider->update_slider_file();
		foreach($related as $r)
			$this->add_related_product($new_id, $r->related_id);
	   //$this->slider->update_slider_file();
    		
    	return $new_id;
	}
	
    
    function update_slider_file(){
        $sliders_query = mysql_query($this->db->placehold("SELECT * FROM __slider"));
        $sliders_count = mysql_num_rows($sliders_query);
        $f = fopen($this->config->root_dir.'design/win8/swf/images.xml', 'w+');
        fwrite($f, "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\r\n");
        fwrite($f, "<images>\r\n");
        for($i=0; $i<$sliders_count; $i++){
            $slider = mysql_fetch_array($sliders_query);
            $id = $slider['id'];
            $name = $slider['name'];
            $logo = $slider['logo'];
            $description = strip_tags($slider['body']);
            
            $image_query = mysql_query($this->db->placehold("SELECT filename FROM __slider_images WHERE object_id=$id"));
            $image = mysql_fetch_array($image_query);
            $filename = $image['filename'];
            
            $related_id_query = mysql_query($this->db->placehold("SELECT related_id FROM __slider_related_products WHERE slider_id=$id"));
            $related_id_array = mysql_fetch_array($related_id_query);
            $related_id = $related_id_array['related_id'];
            
            $link_query = mysql_query($this->db->placehold("SELECT url FROM __products WHERE id=$related_id"));
            $link_array = mysql_fetch_array($link_query);
            $link = $link_array['url'];
            
            $str = "<image im=\"files/originals/$filename\" logo =\"files/originals/$logo\" text1 =\"$name\"  text2 =\"$description\"  gourl =\"products/$link\" />";
            
            fwrite($f, "$str \r\n");
        }
        fwrite($f, "</images>\r\n");
        fclose($f);
    }

	
	function get_related_products($slider_id = array())
	{
		if(empty($slider_id))
			return array();

		$slider_id_filter = $this->db->placehold('AND slider_id in(?@)', (array)$slider_id);
				
		$query = $this->db->placehold("SELECT slider_id, related_id, position
					FROM __slider_related_products
					WHERE 
					1
					$slider_id_filter   
					ORDER BY position       
					");
		
		$this->db->query($query);
		return $this->db->results();
	}
    
    // Функция возвращает связанные товары
	public function add_related_product($slider_id, $related_id, $position=0)
	{
		$query = $this->db->placehold("INSERT IGNORE INTO __slider_related_products SET slider_id=?, related_id=?, position=?", $slider_id, $related_id, $position);
		$this->db->query($query);
		return $related_id;
	}
	
	// Удаление связанного товара
	public function delete_related_product($slider_id, $related_id)
	{
		$query = $this->db->placehold("DELETE FROM __slider_related_products WHERE slider_id=? AND related_id=? LIMIT 1", intval($slider_id), intval($related_id));
		$this->db->query($query);
	}
	
	
	function get_logo($filter_id)
	{		
		$object_id_filter = '';
		$group_by = '';

		if(!empty($filter_id))
			$object_id_filter = $this->db->placehold('AND i.id = ?', $filter_id);

		// images
		$query = $this->db->placehold("SELECT i.id, i.logo FROM __slider AS i WHERE 1 $object_id_filter LIMIT 1");
		$this->db->query($query);
		return $this->db->results();
	}
    
    function get_images($filter = array())
	{		
		$object_id_filter = '';
		$group_by = '';

		if(!empty($filter['object_id']))
			$object_id_filter = $this->db->placehold('AND i.object_id in(?@)', (array)$filter['object_id']);

		// images
		$query = $this->db->placehold("SELECT i.id, i.object_id, i.name, i.filename, i.position
									FROM __slider_images AS i WHERE 1 $object_id_filter $group_by ORDER BY i.object_id, i.position");
		$this->db->query($query);
		return $this->db->results();
	}
	
	public function add_image($object_id, $filename, $name = '')
	{
		$query = $this->db->placehold("SELECT id FROM __slider_images WHERE object_id=? AND filename=?", $object_id, $filename);
		$this->db->query($query);
		$id = $this->db->result('id');
		if(empty($id))
		{
			$query = $this->db->placehold("INSERT INTO __slider_images SET object_id=?, filename=?", $object_id, $filename);
			$this->db->query($query);
			$id = $this->db->insert_id();
			$query = $this->db->placehold("UPDATE __slider_images SET position=id WHERE id=?", $id);
			$this->db->query($query);
		}
		return($id);
	}
    
    public function add_logo($object_id, $filename, $name = '')
	{
            $this->resize_logo($filename, 160, 70);
			$query = $this->db->placehold("UPDATE __slider SET logo=? WHERE id=? LIMIT 1", $filename, $object_id);
			$this->db->query($query);
	}
	
	public function update_image($id, $image)
	{
	
		$query = $this->db->placehold("UPDATE __slider_images SET ?% WHERE id=?", $image, $id);
		$this->db->query($query);
		
		return($id);
	}
	
	public function delete_image($id)
	{
		$query = $this->db->placehold("SELECT filename FROM __slider_images WHERE id=?", $id);
		$this->db->query($query);
		$filename = $this->db->result('filename');
		$query = $this->db->placehold("DELETE FROM __slider_images WHERE id=? LIMIT 1", $id);
		$this->db->query($query);
		$query = $this->db->placehold("SELECT count(*) as count FROM __slider_images WHERE filename=? LIMIT 1", $filename);
		$this->db->query($query);
		$count = $this->db->result('count');
		if($count == 0)
		{			
			$file = pathinfo($filename, PATHINFO_FILENAME);
			$ext = pathinfo($filename, PATHINFO_EXTENSION);
			
			// Удалить все ресайзы
			$rezised_images = glob($this->config->root_dir.$this->config->resized_object_images_dir.$file."*.".$ext);
			if(is_array($rezised_images))
			foreach (glob($this->config->root_dir.$this->config->resized_object_images_dir.$file."*.".$ext) as $f)
				@unlink($f);

			@unlink($this->config->root_dir.$this->config->original_object_images_dir.$filename);		
		}
	}
    
    public function delete_logo($id)
	{
		$query = $this->db->placehold("SELECT logo FROM __slider WHERE id=?", $id);
		$this->db->query($query);
		$filename = $this->db->result('logo');
		$query = $this->db->placehold("UPDATE __slider SET logo='' WHERE id=? LIMIT 1", $id);
		$this->db->query($query);
			$file = pathinfo($filename, PATHINFO_FILENAME);
			$ext = pathinfo($filename, PATHINFO_EXTENSION);
			
			// Удалить все ресайзы
			$rezised_images = glob($this->config->root_dir.$this->config->resized_object_images_dir.$file."*.".$ext);
			if(is_array($rezised_images))
			foreach (glob($this->config->root_dir.$this->config->resized_object_images_dir.$file."*.".$ext) as $f)
				@unlink($f);

			@unlink($this->config->root_dir.$this->config->original_object_images_dir.$filename);
	}
    
    //Функция ресайза логотипа
    
    public function resize_logo($filename, $width, $height) { 
	/*if (!file_exists($filename)){
		die('file not found');
	}*/
	//Подключаем файл конфигурации        
	if(preg_match('/[.](jpg)$/', $filename)) {
		$im = imagecreatefromjpeg($this->config->root_dir.$this->config->original_object_images_dir. $filename);
	} else if (preg_match('/[.](gif)$/', $filename)) {
		$im = imagecreatefromgif($this->config->root_dir.$this->config->original_object_images_dir. $filename);
	} else if (preg_match('/[.](png)$/', $filename)) {
		$im = imagecreatefrompng($this->config->root_dir.$this->config->original_object_images_dir. $filename);
	} 
	//Определяем формат изображения
	$ox = imagesx($im);
	$oy = imagesy($im);
	$nx = $width;
	$ny = $height;

	$result_ratio = $nx / $ny;
	$original_ratio = $ox / $oy;

	if ($original_ratio > $result_ratio) {
		$cropped_x = $oy * $nx / $ny;
		$cropped_y = $oy;
	} else {
		$cropped_x = $ox;
		$cropped_y = $ox * $ny / $nx;
	}

	$cropped_img = imagecreatetruecolor($cropped_x, $cropped_y);

	imagecopy($cropped_img, $im, 0, 0, $ox / 2 - $cropped_x / 2, $oy / 2 - $cropped_y / 2, $cropped_x, $cropped_y);

	//imagejpeg($cropped_img, 'cropped.jpg');

	$nm = imagecreatetruecolor($nx, $ny);

	imagecopyresampled($nm, $cropped_img, 0,0,0,0,$nx,$ny,$cropped_x,$cropped_y);
    
	imagejpeg($nm, $this->config->root_dir.$this->config->original_object_images_dir."xx_".$filename);
}
		
	/*
	*
	* Следующий товар
	*
	*/	
	public function get_next_object($id)
	{
		$this->db->query("SELECT position FROM __slider WHERE id=? LIMIT 1", $id);
		$position = $this->db->result('position');
		
		$this->db->query("SELECT pc.category_id FROM __slider_categories pc WHERE object_id=? ORDER BY position LIMIT 1", $id);
		$category_id = $this->db->result('category_id');

		$query = $this->db->placehold("SELECT id FROM __slider p, __slider_categories pc
										WHERE pc.object_id=p.id AND p.position>? 
										AND pc.position=(SELECT MIN(pc2.position) FROM __slider_categories pc2 WHERE pc.object_id=pc2.object_id)
										AND pc.category_id=? 
										AND p.visible ORDER BY p.position limit 1", $position, $category_id);
		$this->db->query($query);
 
		return $this->get_object((integer)$this->db->result('id'));
	}
	
	/*
	*
	* Предыдущий товар
	*
	*/	
	public function get_prev_object($id)
	{
		$this->db->query("SELECT position FROM __slider WHERE id=? LIMIT 1", $id);
		$position = $this->db->result('position');
		
		$this->db->query("SELECT pc.category_id FROM __slider_categories pc WHERE object_id=? ORDER BY position LIMIT 1", $id);
		$category_id = $this->db->result('category_id');

		$query = $this->db->placehold("SELECT id FROM __slider p, __slider_categories pc
										WHERE pc.object_id=p.id AND p.position<? 
										AND pc.position=(SELECT MIN(pc2.position) FROM __slider_categories pc2 WHERE pc.object_id=pc2.object_id)
										AND pc.category_id=? 
										AND p.visible ORDER BY p.position DESC limit 1", $position, $category_id);
		$this->db->query($query);
 
		return $this->get_object((integer)$this->db->result('id'));	}	
}