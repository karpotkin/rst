<?php
/**
 * Created by PhpStorm.
 * User: evgeniy
 * Date: 23/12/14
 * Time: 13:33
 */
require_once('Master.php');

class Auth extends Master{

    /**
     * Проверяет, авторизован пользователь или нет
     * Возвращает true если авторизован, иначе false
     * @return boolean
     */
    public function isAuth() {
        if (isset($_SESSION["is_auth"])) { //Если сессия существует
            return $_SESSION["is_auth"]; //Возвращаем значение переменной сессии is_auth (хранит true если авторизован, false если не авторизован)
        }
        else return false;
    }

    /**
     * Авторизация пользователя
     * @param string $login
     * @param string $passwors
     */
    public function authUser($login, $password) {

        $where = $this->db->placehold(' WHERE u.login=? and u.password=?', $login, md5($password));

        $query = $this->db->placehold("SELECT u.id,u.login, u.email, u.password, u.name, u.group_id, u.enabled, u.last_ip, u.created FROM __users u $where LIMIT 1", $id);
        $this->db->query($query);
        $user = $this->db->result();

        if(empty($user)){
            $_SESSION["is_auth"] = false;
            return false;
        }else{
            $_SESSION["is_auth"] = true;
            $_SESSION["login"] = $user->login;
            return true;
        }
    }

    /**
     * Метод возвращает логин авторизованного пользователя
     */
    public function getLogin() {
        if ($this->isAuth()) { //Если пользователь авторизован
            return $_SESSION["login"]; //Возвращаем логин, который записан в сессию
        }
    }


    public function out() {
        $_SESSION = array(); //Очищаем сессию
        session_destroy(); //Уничтожаем
    }

} 