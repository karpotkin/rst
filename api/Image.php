<?php

require_once('Master.php');

class Image extends Master
{
    private	$allowed_extentions = array('png', 'gif', 'jpg', 'jpeg', 'ico');

    //идентификатор изображения
    private $image;

    //тип изображения (jpg, png, gif)
    private $type;

    public function __construct()
    {
        parent::__construct();
    }

    public function setImage($file)
    {
        $this->image = $file;
    }

    public function getImage()
    {
        return $this->image;
    }

    //функция проверяет, является ли файл изображением и устанавливает его тип
    public function setType($file)
    {
        $mime = mime_content_type($file);

        switch($mime)
        {
            case 'image/jpeg':
                $this->type = "jpg";
                break;
            case 'image/png':
                $this->type = "png";
                break;
            case 'image/gif':
                $this->type = "gif";
                break;
            default:
                return false;
        }

        return true;
    }

    public function getType()
    {
        return $this->type;
    }

    //создаёт в зависимости от типа на основе файла идентификатор изображения
    public function openImage($file)
    {
        switch($this->getType())
        {
            case 'jpg':
                return @imagecreatefromjpeg($file);
                break;
            case 'png':
                return @imagecreatefrompng($file);
                break;
            case 'gif':
                return @imagecreatefromgif($file);
                break;
            default:
                exit("File is not an image");
        }
    }

    /**
     * Создание превью изображения
     * @param $filename файл с изображением (без пути к файлу)
     * @param max_w максимальная ширина
     * @param max_h максимальная высота
     * @return $string имя файла превью
     */
    function resize($width, $height, $dir)
    {
        if(class_exists('Imagick') && $this->config->use_imagick){
            $this->image_constrain_imagick($dir, $width, $height);
        }else{
            $this->image_constrain_gd($dir, $width, $height);
        }
    }

    function resize_news($filename)
    {
        list($source_file, $width , $height, $dir, $set_watermark) = $this->get_resize_params($filename);

        // Если вайл удаленный (http://), зальем его себе
        if(substr($source_file, 0, 7) == 'http://')
        {
            // Имя оригинального файла
            if(!$original_file = $this->download_image($source_file))
                return false;

            $resized_file = $this->add_resize_params($original_file, $width, $height, $dir, $set_watermark);
        }
        else
        {
            $original_file = $source_file;
        }

        $resized_file = $this->add_resize_params($original_file, $width, $height, $dir, $set_watermark);


        // Пути к папкам с картинками
        if ($dir){
            $originals_dir = $this->config->root_dir.'files/'.$dir.'/';
            $preview_dir = $this->config->root_dir.'files/'.$dir.'/thumbs/';
        } else {
            $originals_dir = $this->config->root_dir.$this->config->news_images_dir;
            $preview_dir = $this->config->root_dir.$this->config->resized_news_images_dir;
        }



        $watermark_offet_x = $this->settings->watermark_offset_x;
        $watermark_offet_y = $this->settings->watermark_offset_y;

        $sharpen = min(100, $this->settings->images_sharpen)/100;
        $watermark_transparency =  1-min(100, $this->settings->watermark_transparency)/100;


        if($set_watermark && is_file($this->config->watermark_file))
            $watermark = $this->config->watermark_file;
        else
            $watermark = null;

        if(class_exists('Imagick') && $this->config->use_imagick){
            $this->image_constrain_imagick($originals_dir.$original_file, $preview_dir.$resized_file, $width, $height, $watermark, $watermark_offet_x, $watermark_offet_y, $watermark_transparency, $sharpen);
        }else{
            $this->image_constrain_gd($originals_dir.$original_file, $preview_dir.$resized_file, $width, $height, $watermark, $watermark_offet_x, $watermark_offet_y, $watermark_transparency);
        }


        return $preview_dir.$resized_file;
    }

    public function add_resize_params($filename, $width=0, $height=0, $dir, $set_watermark=false)
    {

        if('.' != ($dirname = pathinfo($filename,  PATHINFO_DIRNAME)))
            $file = $dirname.'/'.pathinfo($filename, PATHINFO_FILENAME);
        else
            $file = pathinfo($filename, PATHINFO_FILENAME);
        $ext = pathinfo($filename, PATHINFO_EXTENSION);

        if($width>0 || $height>0)
            $resized_filename = $file.'.'.($width>0?$width:'').'x'.($height>0?$height:'').($dir?'.'.$dir:'').($set_watermark?'.w':'').'.'.$ext;
        else
            $resized_filename = $file.'.'.($dir?'.'.$dir:'').($set_watermark?'.w.':'').$ext;

        return $resized_filename;
    }

    public function get_resize_params($filename)
    {

        if(!preg_match('/(.+)\.([0-9]*)x([0-9]*).?(\w*).?(w)?\.([^\.]+)$/', $filename, $matches))
            return false;

        $file = $matches[1];					// имя запрашиваемого файла
        $width = $matches[2];					// ширина будущего изображения
        $height = $matches[3];					// высота будущего изображения
        $dir = $matches[4];						// папка изображения
        $set_watermark = $matches[5] == 'w';	// ставить ли водяной знак
        $ext = $matches[6];						// расширение файла

        return array($file.'.'.$ext, $width, $height, $dir, $set_watermark);
    }


    public function download_image($filename)
    {
        // Заливаем только есть такой файл есть в базе
        $this->db->query('SELECT 1 FROM __images WHERE filename=? LIMIT 1', $filename);
        if(!$this->db->result())
            return false;


        // Имя оригинального файла
        $uploaded_file = array_shift(explode('?', pathinfo($filename, PATHINFO_BASENAME)));
        $uploaded_file = array_shift(explode('&', pathinfo($filename, PATHINFO_BASENAME)));
        $base = urldecode(pathinfo($uploaded_file, PATHINFO_FILENAME));
        $ext = pathinfo($uploaded_file, PATHINFO_EXTENSION);

        // Если такой файл существует, нужно придумать другое название
        $new_name = urldecode($uploaded_file);

        while(file_exists($this->config->root_dir.$this->config->original_images_dir.$new_name))
        {
            $new_base = pathinfo($new_name, PATHINFO_FILENAME);
            if(preg_match('/_([0-9]+)$/', $new_base, $parts))
                $new_name = $base.'_'.($parts[1]+1).'.'.$ext;
            else
                $new_name = $base.'_1.'.$ext;
        }
        $this->db->query('UPDATE __images SET filename=? WHERE filename=?', $new_name, $filename);

        // Перед долгим копированием займем это имя
        fclose(fopen($this->config->root_dir.$this->config->original_images_dir.$new_name, 'w'));
        copy($filename, $this->config->root_dir.$this->config->original_images_dir.$new_name);
        return $new_name;
    }

    public function upload_image($filename, $name, $path)
    {
        $path = $this->config->root_dir . $this->config->files_dir . $path;
        // Имя оригинального файла
        $uploaded_file = pathinfo($name, PATHINFO_BASENAME);
        $ext = pathinfo($uploaded_file, PATHINFO_EXTENSION);

        if(in_array(strtolower($ext), $this->allowed_extentions))
        {
            $destImage = md5(time()).'.'.$ext;
            $destOriginalPath = $path . DS . $this->config->files_original;

            if(move_uploaded_file($filename, $destOriginalPath . $destImage)){
                $destFullPath = $path . DS . 'full' . DS;
                copy($destOriginalPath . $destImage, $destFullPath . $destImage);
                $this->setImage($destFullPath . $destImage);

                return $destImage;
            }
        }

        return false;
    }

    public function save_image($inPath,$outPath)
    { 	//Download images from remote server
        $in=    fopen($inPath, "rb");
        $out=   fopen($outPath, "wb");
        while ($chunk = fread($in,8192))
        {
            fwrite($out, $chunk, 8192);
        }
        fclose($in);
        fclose($out);
    }


    /**
     * Создание превью средствами gd
     * @param $src_file исходный файл
     * @param $dst_file файл с результатом
     * @param max_w максимальная ширина
     * @param max_h максимальная высота
     * @return bool
     */
    private function image_constrain_gd($destFile, $maxWidth, $maxHeight)
    {
        $quality = 100;

        $sourceFile = $this->getImage();

        $sourceFileName  = pathinfo($sourceFile, PATHINFO_BASENAME);
        $destFile .= $sourceFileName;

        if(!$sourceFile)
            return false;

        $this->setType($sourceFile);
        $sourceImage = $this->openImage($sourceFile);

        // Размеры исходного изображения
        $width = imagesx($sourceImage);
        $height = imagesy($sourceImage);



        if(empty($width) || empty($height) || !$this->getType())
            return false;

        $srcColors = imagecolorstotal($sourceImage);

        // create destination image (indexed, if possible)
        if ($srcColors > 0 && $srcColors <= 256)
            $destImage = imagecreate($maxWidth, $maxHeight);
        else
            $destImage = imagecreatetruecolor($maxWidth, $maxHeight);

        if (empty($destImage))
            return false;

        $transparent_index = imagecolortransparent($sourceImage);
        if ($transparent_index >= 0 && $transparent_index <= 128)
        {
            $t_c = imagecolorsforindex($sourceImage, $transparent_index);
            $transparent_index = imagecolorallocate($destImage, $t_c['red'], $t_c['green'], $t_c['blue']);
            if ($transparent_index === false)
                return false;
            if (!imagefill($destImage, 0, 0, $transparent_index))
                return false;
            imagecolortransparent($destImage, $transparent_index);
        }
        // or preserve alpha transparency for png
        elseif ($this->getType() === 'png')
        {
            if (!imagealphablending($destImage, false))
                return false;
            $transparency = imagecolorallocatealpha($destImage, 0, 0, 0, 127);
            if (false === $transparency)
                return false;
            if (!imagefill($destImage, 0, 0, $transparency))
                return false;
            if (!imagesavealpha($destImage, true))
                return false;
        }

        $original_aspect = $width / $height;
        $thumb_aspect = $maxWidth / $maxHeight;

        if ( $original_aspect >= $thumb_aspect )
        {
            // If image is wider than thumbnail (in aspect ratio sense)
            $new_height = $maxHeight;
            $new_width = $width / ($height / $maxHeight);
        }
        else
        {
            // If the thumbnail is wider than the image
            $new_width = $maxWidth;
            $new_height = $height / ($width / $maxWidth);
        }
        // resample the image with new sizes
        if (!imagecopyresampled($destImage, $sourceImage, 0 - ($new_width - $maxWidth) / 2, 0 - ($new_height - $maxHeight) / 2, 0, 0, $new_width, $new_height, $width, $height))
            return false;


        // recalculate quality value for png image
        if ('png' === $this->getType())
        {
            $quality = round(($quality / 100) * 10);
            if ($quality < 1)
                $quality = 1;
            elseif ($quality > 10)
                $quality = 10;
            $quality = 10 - $quality;
        }

        // Сохраняем изображение
        switch ($this->getType())
        {
            case 'jpg':
                imageJpeg($destImage, $destFile, $quality);
                break;
            case 'gif':
                imageGif($destImage, $destFile, $quality);
                break;
            case 'png':
                imagesavealpha($destImage, true);
                imagePng($destImage, $destFile, $quality);
                break;
            default:
                return false;
        }

        return true;
    }

    private function set_image_watermark($filename)
    {
        $watermark = $this->config->root_dir.$this->config->watermark_file;

        if(!empty($watermark) && is_readable($watermark))
        {
            if(class_exists('Imagick') && $this->config->use_imagick){
                $image = new Imagick($filename);
                // Размеры исходного изображения
                $geo = $image->getImageGeometry();

                $overlay = new Imagick($watermark);
                $overlay->evaluateImage(Imagick::EVALUATE_MULTIPLY, (intval($this->settings->watermark_transparency) / 100), Imagick::CHANNEL_ALPHA);
                $overlay_compose = $overlay->getImageCompose();

                // Get the size of overlay
                $oWidth = $overlay->getImageWidth();
                $oHeight = $overlay->getImageHeight();

                $watermark_x = min(($geo['width'] - $oWidth) * $this->settings->watermark_offset_x / 100, $geo['width']);
                $watermark_y = min(($geo['height'] - $oHeight)* $this->settings->watermark_offset_y / 100, $geo['height']);

                // Анимированные gif требуют прохода по фреймам
                foreach($image as $frame)
                {
                    // Уменьшаем
                    $frame->thumbnailImage($geo['width'], $geo['height']);

                    /* Set the virtual canvas to correct size */
                    $frame->setImagePage($geo['width'], $geo['height'], 0, 0);

                    // Наводим резкость
                    if($this->settings->images_sharpen > 0)
                        $image->adaptiveSharpenImage($this->settings->images_sharpen, $this->settings->images_sharpen);

                    if(isset($overlay) && is_object($overlay))
                    {
                        $frame->compositeImage($overlay, $overlay_compose, $watermark_x, $watermark_y, imagick::COLOR_ALPHA);
                    }

                }

                $image->writeImages($filename, true);

                // Уборка
                $image->destroy();
                if(isset($overlay) && is_object($overlay))
                    $overlay->destroy();

            }else{
                $quality = 100;

                $overlay = @imagecreatefrompng($watermark);

                $this->setType($filename);
                $sourceImage = $this->openImage($filename);
                $imageType = $this->getType();

                $imageWidth = imagesx($sourceImage);
                $imageHeight = imagesy($sourceImage);

                // Get the size of overlay
                $oWidth = imagesx($overlay);
                $oHeight = imagesy($overlay);

                $watermark_x = min(($imageWidth - $oWidth) * $this->settings->watermark_offset_x / 100, $imageWidth);
                $watermark_y = min(($imageHeight - $oHeight) * $this->settings->watermark_offset_y / 100, $imageHeight);

                imagecopy($sourceImage, $overlay, $watermark_x, $watermark_y, 0, 0, $oWidth, $oHeight);

                // recalculate quality value for png image
                if ('png' === $imageType)
                {
                    $quality = round(($quality / 100) * 10);
                    if ($quality < 1)
                        $quality = 1;
                    elseif ($quality > 10)
                        $quality = 10;
                    $quality = 10 - $quality;
                }

                // Сохраняем изображение
                switch ($imageType)
                {
                    case 'jpg':
                        return imageJpeg($sourceImage, $filename, $quality);
                    case 'gif':
                        return imageGif($sourceImage, $filename, $quality);
                    case 'png':
                        imagesavealpha($sourceImage, true);
                        return imagePng($sourceImage, $filename, $quality);
                    default:
                        return false;
                }

            }
        }

        // Устанавливаем водяной знак
    }

    /**
     * Создание превью средствами imagick
     * @param $src_file исходный файл
     * @param $dst_file файл с результатом
     * @param max_w максимальная ширина
     * @param max_h максимальная высота
     * @return bool
     */
    private function image_constrain_imagick($dst_file, $max_w, $max_h)
    {
        $thumb = new Imagick();
        $src_file = $this->getImage();
        // Читаем изображение
        if(!$thumb->readImage($src_file))
            return false;

        // Размеры исходного изображения
        $geo = $thumb->getImageGeometry();

        // crop the image
        if(($geo['width'] / $max_w) < ($geo['height'] / $max_h))
        {
            $thumb->cropImage($geo['width'], floor($max_h * $geo['width'] / $max_w), 0, (($geo['height'] - ($max_h * $geo['width'] / $max_w )) / 2));
        }
        else
        {
            $thumb->cropImage(ceil($max_w * $geo['height'] / $max_h), $geo['height'], (($geo['width'] - ( $max_w * $geo['height'] / $max_h )) / 2), 0);
        }

        /*** crop the image ***/
        $thumb->ThumbnailImage($max_w,$max_h,true);


        // Убираем комменты и т.п. из картинки
        //$thumb->stripImage();

        //$thumb->setImageCompressionQuality(100);

        // Записываем картинку
        if(!$thumb->writeImages($dst_file, true))
            return false;

        //$this->set_image_watermark($dst_file);

        // Уборка
        $thumb->destroy();

        return true;
    }


    /**
     * Вычисляет размеры изображения, до которых нужно его пропорционально уменьшить, чтобы вписать в квадрат $max_w x $max_h
     * @param src_w ширина исходного изображения
     * @param src_h высота исходного изображения
     * @param max_w максимальная ширина
     * @param max_h максимальная высота
     * @return array(w, h)
     */
    function calc_contrain_size($src_w, $src_h, $max_w = 0, $max_h = 0)
    {
        if($src_w == 0 || $src_h == 0)
            return false;

        $dst_w = $src_w;
        $dst_h = $src_h;

        if($src_w > $max_w && $max_w>0)
        {
            $dst_h = $src_h * ($max_w/$src_w);
            $dst_w = $max_w;
        }
        if($dst_h > $max_h && $max_h>0)
        {
            $dst_w = $dst_w * ($max_h/$dst_h);
            $dst_h = $max_h;
        }
        return array($dst_w, $dst_h);
    }


    private function files_identical($fn1, $fn2)
    {
        $buffer_len = 1024;
        if(!$fp1 = fopen($fn1, 'rb'))
            return FALSE;

        if(!$fp2 = fopen($fn2, 'rb')) {
            fclose($fp1);
            return FALSE;
        }

        $same = TRUE;
        while (!feof($fp1) and !feof($fp2))
            if(fread($fp1, $buffer_len) !== fread($fp2, $buffer_len)) {
                $same = FALSE;
                break;
            }

        if(feof($fp1) !== feof($fp2))
            $same = FALSE;

        fclose($fp1);
        fclose($fp2);

        return $same;
    }


}