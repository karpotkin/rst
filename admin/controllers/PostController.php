<?PHP

require_once('BaseController.php');

class PostController extends BaseController
{
	public function fetch()
	{
		$post = new stdClass();

		if($this->request->method('post'))
		{

            $post = json_decode(json_encode($this->request->getPost()), FALSE);
            $post->url = strtolower($post->url);
            $post->visible = isset($post->visible) ? $post->visible : 0;
            $post->date = date('Y-m-d', strtotime($post->date));
            unset($post->session_id);

            $postDescriptions = $post->post_description;
            unset($post->post_description);

 			// Не допустить одинаковые URL разделов.
			if(($a = $this->blog->get_post($post->url)) && $a->id!=$post->id)
			{			
				$this->design->assign('message_error', 'url_exists');
			}
			else
			{
                if($image = $this->request->files('image'))
                {
                    if ($image_name = $this->image->upload_image($image['tmp_name'], $image['name'], 'news'))
                    {
                        $this->image->resize(250, 310, $this->config->root_dir . 'files/news/medium/');
                        $this->image->resize(550, 680, $this->config->root_dir . 'files/news/big/');
                        $this->image->resize(200, 200, $this->config->root_dir . 'files/news/small/');

                        $post->image  = $image['name'];
                    }
                    else
                    {
                        $this->design->assign('error', 'error uploading image');
                    }
                }

				if(empty($post->id))
				{
	  				$post->id = $this->blog->add_post($post);
                    foreach($postDescriptions as $description){
                        $description->blog_id = $post->id;
                        $this->blog->addPostDescription($description);
                    }
	  				$post = $this->blog->get_post($post->id);
					$this->design->assign('message_success', 'added');
	  			}
  	    		else
  	    		{
  	    			$this->blog->update_post($post->id, $post);
                    foreach($postDescriptions as $description){
                        $this->blog->updatePostDescription($description->id, $description);
                    }
  	    			$post = $this->blog->get_post(intval($post->id));
					$this->design->assign('message_success', 'updated');
  	    		}	

   	    		
			}
		}
		else
		{
			$post->id = isset($this->params['id']) ? $this->params['id'] : null;
			$post = $this->blog->get_post(intval($post->id));
		}
 		
		$this->design->assign('post', $post);
		
		
 	  	return $this->design->fetch('post.tpl');
	}
}