<?php

require_once('api/Master.php');

class BaseController extends Master
{
	/**
	 * @var
	 */
	public $params;

	private static $view_instance;
	
	public function __construct()
	{
		parent::__construct();

		if(self::$view_instance)
		{
			$this->params = &self::$view_instance->params;
		}
		else
		{
			self::$view_instance = $this;
		}
	}

	/**
	 * @return bool
	 */
	function fetch()
	{
		return false;
	}
}
