<?php

require_once('BaseController.php');


class IndexController extends BaseController
{
    // Конструктор
    public function __construct() {
        // Вызываем конструктор базового класса
        parent::__construct();

        if ($this->request->get('is_exit')) { //Если нажата кнопка выхода
            $this->auth->out(); //Выходим
            header("Location: /"); //Редирект после выхода
        }

        $this->design->set_templates_dir(dirname(__DIR__).DIRECTORY_SEPARATOR.'design/html');
        $this->design->set_compiled_dir(dirname(__DIR__).DIRECTORY_SEPARATOR.'design/compiled');

        $this->design->assign('settings',	$this->settings);
        $this->design->assign('config',	$this->config);

        if($this->auth->isAuth()){
            $params = array();
            $module = '';

            require_once(dirname(__DIR__).DIRECTORY_SEPARATOR.'router.php');

            $urlPath = $this->request->getCurrentUri();

            foreach($this->routes as $map){
                preg_match('/\{ \s* ([a-zA-Z][a-zA-Z0-9_-]*) \s* (?: : \s* ([^{}]*(?:\{(?-1)\}[^{}]*)*) )? \}/', '/news/{id}/edit', $matches);
                var_dump($matches);
                if(preg_match($map['pattern'], $urlPath, $matches)){
                    array_shift($matches);
                    foreach ($matches as $index => $value){
                        $params[$map['params'][$index]] = $value;
                    }
                    $module = $map['controller'];
                    break;
                }
            }

            if (is_file(__DIR__.DIRECTORY_SEPARATOR.$module.'.php'))
            {
                include_once(__DIR__.DIRECTORY_SEPARATOR.$module.'.php');

                if (class_exists($module))
                {
                    $this->module = new $module($this);
                } else {
                    return false;
                }
            } else {
                return false;
            }

            $this->params = $params;

        } else {
            $module = 'LoginController';
            require_once(__DIR__.DIRECTORY_SEPARATOR.$module.'.php');
            if (class_exists($module)){
                $this->module = new $module();
                if($this->request->method('post')){
                    $this->module->setPassword($this->request->post('password'));
                    $this->module->setLogin($this->request->post('login'));
                }
            } else {
                die("Error creating $module class");
            }
        }

    }

    function fetch()
    {
        $content = $this->module->fetch();
        $this->design->assign("content", $content);

        // Создаем текущую обертку сайта (обычно index.tpl)
        $wrapper = $this->design->smarty->getTemplateVars('wrapper');
        if(is_null($wrapper))
            $wrapper = 'index.tpl';

        if(!empty($wrapper))
            return $this->body = $this->design->fetch($wrapper);
        else
            return $this->body = $content;
    }
}