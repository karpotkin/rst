<?PHP
require_once('BaseController.php');

class PageController extends BaseController
{	
	public function fetch()
	{
		$page = new stdClass();

		if($this->request->method('POST'))
		{
			$page = json_decode(json_encode($this->request->getPost()), FALSE);
			$page->url = strtolower($page->url);
            $page->visible = isset($page->visible) ? $page->visible : 0;
			unset($page->session_id);

			$pageDescriptions = $page->page_description;
			unset($page->page_description);

			## Не допустить одинаковые URL разделов.
			if(($p = $this->pages->get_page($page->url)) && $p->id!=$page->id)
			{			
				$this->design->assign('message_error', 'url_exists');
			}
			else
			{

				if($image = $this->request->files('image'))
			  	{
                    if ($image_name = $this->image->upload_image($image['tmp_name'], $image['name'], 'pages'))
                    {
                        $this->image->resize(250, 310, $this->config->root_dir . 'files/pages/medium/');
                        $this->image->resize(550, 680, $this->config->root_dir . 'files/pages/big/');
                        $this->image->resize(200, 200, $this->config->root_dir . 'files/pages/small/');

                        $this->pages->add_image($page->id, $image_name);
                    }
					else
					{
						$this->design->assign('error', 'error uploading image');
					}
				}

				if(empty($page->id))
				{
	  				$page->id = $this->pages->add_page($page);
					foreach($pageDescriptions as $description){
						$description->page_id = $page->id;
						$this->pages->addPageDescription($description);
					}
	  				$page = $this->pages->get_page((int)$page->id);
	  				$this->design->assign('message_success', 'added');
  	    		}
  	    		else
  	    		{
  	    			$this->pages->update_page($page->id, $page);
					foreach($pageDescriptions as $description){
						$this->pages->updatePageDescription($description->id, $description);
					}
	  				$page = $this->pages->get_page((int)$page->id);
	  				$this->design->assign('message_success', 'updated');
   	    		}
			}
		}
		else
		{
			$id = isset($this->params['id']) ? $this->params['id'] : null;

			if(!empty($id))
				$page = $this->pages->get_page(intval($id));			
			else
			{
				$page->menu_id = $this->request->get('menu_id');
				$page->visible = 1;
			}
		}

		$this->design->assign('page', $page);
		
 	  	$menus = $this->pages->get_menus();
		$this->design->assign('menus', $menus);
		
	    // Текущее меню
	    if(isset($page->menu_id))
	  		$menu_id = $page->menu_id; 
	  	if(empty($menu_id) || !$menu = $this->pages->get_menu($menu_id))
	  	{
	  		$menu = reset($menus);
	  	}
	 	$this->design->assign('menu', $menu);

		$parents = $this->pages->get_categories_tree();
		//var_dump($parents);
		$this->design->assign('parents', $parents);
 	  	return $this->design->fetch('page.tpl');
	}
	
}

