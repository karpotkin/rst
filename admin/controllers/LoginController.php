<?PHP

require_once('BaseController.php');

class LoginController extends BaseController
{
    private $login;
    private $password;

    public function getLogin(){
        return $this->login;
    }

    public function setLogin($login){
        $this->login = $login;
    }

    public function getPassword(){
        return $this->password;
    }

    public function setPassword($password){
        $this->password = $password;
    }

    function fetch()
    {
        $error = '';

        if($this->request->method('post')){
            if ($this->login && $this->password) {
                if (!$this->auth->authUser($this->login, $this->password)) {
                    $error = "Не правильные логин или пароль!";
                }else{
                    Header("Location: ./");
                }
            }else{
                $error = "Ведите логин и пароль!";
            }
        }

        $this->design->assign('login', $this->login);
        $this->design->assign('password', $this->password);
        $this->design->assign('error', $error);

        $this->design->assign('meta_title', 'Авторизация');


        return $this->design->fetch('login.tpl');
    }
}