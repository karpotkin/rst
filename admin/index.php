<?PHP

chdir('..');
define ('DS', DIRECTORY_SEPARATOR);

session_start();
$_SESSION['id'] = session_id();

@ini_set('session.gc_maxlifetime', 86400); // 86400 = 24 часа
@ini_set('session.cookie_lifetime', 0); // 0 - пока браузер не закрыт

require_once('admin/controllers/IndexController.php');

// Кеширование в админке нам не нужно
Header("Cache-Control: no-cache, must-revalidate");
Header("Pragma: no-cache");

$index = new IndexController();

// Проверка сессии для защиты от xss
if(!$index->request->check_session())
{
	unset($_POST);
	trigger_error('Session expired', E_USER_WARNING);
}


print $index->fetch();
