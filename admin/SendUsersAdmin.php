<?PHP

require_once('api/Simpla.php');

############################################
# Class Properties displays a list of product parameters
############################################
class SendUsersAdmin extends Simpla
{
	function fetch()
	{	
	
		if($this->request->method('post'))
		{  	
			// Действия с выбранными
			$ids = $this->request->post('check');
			if(is_array($ids))
			switch($this->request->post('action'))
			{
			    case 'delete':
			    {
			    	foreach($ids as $id)
			    	{
			    			$this->send->delete_user($id);
					}
			        break;
			    }
			}		
	  	
			// Сортировка
			$positions = $this->request->post('positions');
	 		$ids = array_keys($positions);
			sort($positions);
			foreach($positions as $i=>$position)
				$this->features->update_feature($ids[$i], array('position'=>$position)); 

		} 
	
		$categories = $this->categories->get_categories_tree();
		$category = null;
		
		$filter = array();
		$category_id = $this->request->get('category_id', 'integer');
		if($category_id)
		{
			$category = $this->categories->get_category($category_id);
			$filter['category_id'] = $category->id;
		}
		
		$send_users = $this->send->get_users();

		$this->design->assign('users', $send_users);
		return $this->body = $this->design->fetch('sendusers.tpl');
	}
}
