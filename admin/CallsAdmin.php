<?PHP 

require_once('api/Master.php');

########################################
class CallsAdmin extends Master
{

  public function fetch()
  {
  
	$calls = $this->calls->get_calls();
	$this->design->assign('calls', $calls);



	return $this->design->fetch('calls.tpl');
  }
}


?>