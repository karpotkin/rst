<?PHP 

require_once('api/Simpla.php');

class SlidersAdmin extends Simpla
{
	function fetch()
	{		

		$filter = array();
		$filter['page'] = max(1, $this->request->get('page', 'integer'));
			
		$filter['limit'] = $this->settings->products_num_admin;
	
		// Категории
		$categories = $this->object_categories->get_categories_tree();
		$this->design->assign('categories', $categories);
		
		// Текущая категория
		$category_id = $this->request->get('category_id', 'integer'); 
		if($category_id && $category = $this->object_categories->get_category($category_id))
	  		$filter['category_id'] = $category->children;
	
		// Текущий фильтр
		if($f = $this->request->get('filter', 'string'))
		{
			if($f == 'featured')
				$filter['featured'] = 1; 
			elseif($f == 'discounted')
				$filter['discounted'] = 1; 
			$this->design->assign('filter', $f);
		}
	
		// Поиск
		$keyword = $this->request->get('keyword');
		if(!empty($keyword))
		{
	  		$filter['keyword'] = $keyword;
			$this->design->assign('keyword', $keyword);
		}
			
		// Обработка действий 	
		if($this->request->method('post'))
		{
		
			// Сортировка
			$positions = $this->request->post('positions'); 		
				$ids = array_keys($positions);
			sort($positions);
			$positions = array_reverse($positions);
			foreach($positions as $i=>$position)
				$this->slider->update_object($ids[$i], array('position'=>$position)); 
		
			
			// Действия с выбранными
			$ids = $this->request->post('check');
			if(!empty($ids))
			switch($this->request->post('action'))
			{
			    case 'delete':
			    {
				    foreach($ids as $id)
						$this->slider->delete_object($id);    
			        break;
			    }
			    case 'duplicate':
			    {
				    foreach($ids as $id)
				    	$this->slider->duplicate_object(intval($id));
			        break;
			    }
		    }			
		}

		// Отображение
		if(isset($brand))
			$this->design->assign('brand', $brand);
		if(isset($category))
			$this->design->assign('category', $category);
		
	  	$objects_count = $this->slider->count_objects($filter);
		// Показать все страницы сразу
		if($this->request->get('page') == 'all')
			$filter['limit'] = $objects_count;	
	  	
	  	$pages_count = ceil($objects_count/$filter['limit']);
	  	$filter['page'] = min($filter['page'], $pages_count);
	 	$this->design->assign('objects_count', $objects_count);
	 	$this->design->assign('pages_count', $pages_count);
	 	$this->design->assign('current_page', $filter['page']);
	 	
		$objects = array();
		foreach($this->slider->get_objects($filter) as $p)
			$objects[$p->id] = $p;
	 	
	
		if(!empty($objects))
		{
		  	
			// Товары 
			$objects_ids = array_keys($objects);
			foreach($objects as &$object)
			{
				$object->variants = array();
				$object->images = array();
				$object->properties = array();
			}
		
			$variants = $this->variants->get_variants(array('object_id'=>$objects_ids));
		
		 
			foreach($variants as &$variant)
			{
				$objects[$variant->object_id]->variants[] = $variant;
			}
		
			$images = $this->slider->get_images(array('object_id'=>$objects_ids));
			foreach($images as $image)
				$objects[$image->object_id]->images[$image->id] = $image;
            
		}
	 
		$this->design->assign('objects', $objects);
	
		return $this->design->fetch('sliders.tpl');
	}
}
