<?PHP

require_once('api/Master.php');

############################################
# Class Product - edit the static section
############################################
class ProductAdmin extends Master
{
    private	$allowed_image_extentions = array('png', 'gif', 'jpg', 'jpeg', 'ico');

	public function fetch()
	{
        $product =  new stdClass();
		$product_categories = array();
		$images = array();
		$related_products = array();
	
		if($this->request->method('post') && !empty($_POST))
		{
			$product->id = $this->request->post('id', 'integer');
			$product->name = $this->request->post('name');
			
			$product->visible = $this->request->post('visible', 'boolean');
			$product->featured = $this->request->post('featured');

			$product->brand_id = $this->request->post('brand_id', 'integer');

			$product->url = $this->request->post('url', 'string');
			$product->meta_title = $this->request->post('meta_title');
			$product->meta_keywords = $this->request->post('meta_keywords');
			$product->meta_description = $this->request->post('meta_description');
			
			$product->annotation = $this->request->post('annotation');
			$product->body = $this->request->post('body');
            $product->price = $this->request->post('price');
            $product->delivery = $this->request->post('delivery');
            $product->availability = $this->request->post('availability');
            $product->pkg = $this->request->post('pkg');
			
			// Категории товара
			$product_categories = $this->request->post('categories');
            $pc = array();
			if(is_array($product_categories))
			{
				foreach($product_categories as $c)
					$pc[]->id = $c;
				$product_categories = $pc;
			}

			// Связанные товары
			if(is_array($this->request->post('related_products')))
			{
                $rp = array();

				foreach($this->request->post('related_products') as $p)
				{
					$rp[$p]->product_id = $product->id;
					$rp[$p]->related_id = $p;
				}
				$related_products = $rp;
			}
				
			// Не допустить пустое название товара.
			if(empty($product->name))
			{			
				$this->design->assign('message_error', 'empty_name');
				$images = $this->products->get_images(array('product_id'=>$product->id));
			}
			// Не допустить одинаковые URL разделов.
			elseif(($p = $this->products->get_product($product->url)) && $p->id!=$product->id)
			{			
				$this->design->assign('message_error', 'url_exists');
				$images = $this->products->get_images(array('product_id'=>$product->id));
			}
			else
			{
				if(empty($product->id))
				{
	  				$product->id = $this->products->add_product($product);
	  				$product = $this->products->get_product($product->id);
					$this->design->assign('message_success', 'added');
	  			}
  	    		else
  	    		{
  	    			$this->products->update_product($product->id, $product);
  	    			$product = $this->products->get_product($product->id);
					$this->design->assign('message_success', 'updated');
  	    		}	
   	    		
   	    		if($product->id)
   	    		{
	   	    		// Категории товара
	   	    		$query = $this->db->placehold('DELETE FROM __products_categories WHERE product_id=?', $product->id);
	   	    		$this->db->query($query);
	 	  		    if(is_array($product_categories))
		  		    {
		  		    	foreach($product_categories as $i=>$category)
	   	    				$this->categories->add_product_category($product->id, $category->id, $i);
	  	    		}
	
					// Удаление изображений
					$images = (array)$this->request->post('images');
					$current_images = $this->products->get_images(array('product_id'=>$product->id));
					foreach($current_images as $image)
					{
						if(!in_array($image->id, $images))
	 						$this->products->delete_image($image->id);
						}
	
					// Порядок изображений
					if($images = $this->request->post('images'))
					{
	 					$i=0;
						foreach($images as $id)
						{
							$this->products->update_image($id, array('position'=>$i));
							$i++;
						}
					}
	   	    		// Загрузка изображений
		  		    if($images = $this->request->files('images'))
		  		    {
						for($i=0; $i<count($images['name']); $i++)
						{
				 			if ($image_name = $this->image->upload_image($images['tmp_name'][$i], $images['name'][$i]))
				 			{
			  	   				$this->products->add_image($product->id, $image_name);
			  	   			}
							else
							{
								$this->design->assign('error', 'error uploading image');
							}
						}
					}

                    // Удаление изображения
                    if($this->request->post('delete_cover'))
                    {
                        $this->products->delete_cover($product->id);
                    }
                    // Загрузка обложки
                    $cover = $this->request->files('cover');
                    if(!empty($cover['name']) && in_array(strtolower(pathinfo($cover['name'], PATHINFO_EXTENSION)), $this->allowed_image_extentions))
                    {
                        $this->products->delete_cover($product->id);
                        move_uploaded_file($cover['tmp_name'], $this->root_dir.$this->config->product_covers_dir.$cover['name']);
                        $this->products->update_product($product->id, array('cover'=>$cover['name']));
                    }
	   	    		// Загрузка изображений из интернета
		  		    if($images = $this->request->post('images_urls'))
		  		    {
						foreach($images as $url)
						{
							if(!empty($url) && $url != 'http://')
					 			$this->products->add_image($product->id, $url);
						}
					}
					$images = $this->products->get_images(array('product_id'=>$product->id));
					
					// Связанные товары
	   	    		$query = $this->db->placehold('DELETE FROM __related_products WHERE product_id=?', $product->id);
	   	    		$this->db->query($query);
	 	  		    if(is_array($related_products))
		  		    {
		  		    	$pos = 0;
		  		    	foreach($related_products  as $i=>$related_product)
	   	    				$this->products->add_related_product($product->id, $related_product->related_id, $pos++);
	  	    		}
  	    		}

                $product = $this->products->get_product((int)$product->id);

			}

		}
		else
		{
			$product->id = $this->request->get('id', 'integer');

			$product = $this->products->get_product(intval($product->id));

			if($product && $product->id)
			{
				// Категории товара
				$product_categories = $this->categories->get_categories(array('product_id'=>$product->id));
				
				// Изображения товара
				$images = $this->products->get_images(array('product_id'=>$product->id));

				// Связанные товары
				$related_products = $this->products->get_related_products(array('product_id'=>$product->id));
			}
			else
			{
				// Сразу активен
				$product->visible = 1;			
			}
		}
			
		if(empty($product_categories))
		{
			if($category_id = $this->request->get('category_id'))
				$product_categories[0]->id = $category_id;		
			else
				$product_categories = array(1);
		}
		if(empty($product->brand_id) && $brand_id=$this->request->get('brand_id'))
		{
			$product->brand_id = $brand_id;
		}
			
		if(!empty($related_products))
		{
			foreach($related_products as &$r_p)
				$r_products[$r_p->related_id] = &$r_p;
			$temp_products = $this->products->get_products(array('id'=>array_keys($r_products)));
			foreach($temp_products as $temp_product)
				$r_products[$temp_product->id] = $temp_product;
		
			$related_products_images = $this->products->get_images(array('product_id'=>array_keys($r_products)));
			foreach($related_products_images as $image)
			{
				$r_products[$image->product_id]->images[] = $image;
			}
		}

		$this->design->assign('product', $product);

		$this->design->assign('product_categories', $product_categories);
		$this->design->assign('product_images', $images);
		$this->design->assign('related_products', $related_products);
		
		
		// Все бренды
		$brands = $this->brands->get_brands();
		$this->design->assign('brands', $brands);
		
		// Все категории
		$categories = $this->categories->get_categories_tree();
		$this->design->assign('categories', $categories);


 	  	return $this->design->fetch('product.tpl');
	}
}