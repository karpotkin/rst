<?PHP
require_once('api/Simpla.php');

class SettingsAdmin extends Simpla
{	
	
	private $passwd_file;
	private $htaccess_file;
	
	private $allowed_image_extentions = array('png', 'gif', 'jpg', 'jpeg', 'ico');
	
	public function fetch()
	{	
		$this->passwd_file = $this->config->root_dir.'/admin/.passwd';
		$this->htaccess_file = $this->config->root_dir.'/admin/.htaccess';
		
		if($this->request->method('POST'))
		{
			$this->settings->site_name = $this->request->post('site_name');
			$this->settings->home_letter_title = $this->request->post('home_letter_title');
			$this->settings->letter_author = $this->request->post('letter_author');
			$this->settings->letter_callback = $this->request->post('letter_callback');
			
			$this->settings->letter_writeus = $this->request->post('letter_writeus');
			$this->settings->letter_full_order = $this->request->post('letter_full_order');
			$this->settings->letter_email = $this->request->post('letter_email');
			
			$this->settings->letter_send_mark = $this->request->post('letter_send_mark');
			$this->settings->thousands_separator = $this->request->post('thousands_separator');
			
			$this->settings->letter_disser = $this->request->post('letter_disser');
			$this->settings->letter_diplom = $this->request->post('letter_diplom');
			$this->settings->letter_vip = $this->request->post('letter_vip');	
			$this->settings->vuz_list = $this->request->post('vuz_list');

			$this->settings->work_type_list = $this->request->post('work_type_list');
			$this->settings->supporting_diplom_list = $this->request->post('supporting_diplom_list');
			$this->settings->supporting_report_list = $this->request->post('supporting_report_list');	
			
			// Водяной знак
			$clear_image_cache = false;
			$watermark = $this->request->files('watermark_file', 'tmp_name');
			if(!empty($watermark) && in_array(pathinfo($this->request->files('watermark_file', 'name'), PATHINFO_EXTENSION), $this->allowed_image_extentions))
			{
				if(@move_uploaded_file($watermark, $this->config->root_dir.$this->config->watermark_file))
					$clear_image_cache = true;
				else
					$this->design->assign('message_error', 'watermark_is_not_writable');
			}
			
			if($this->settings->watermark_offset_x != $this->request->post('watermark_offset_x'))
			{
				$this->settings->watermark_offset_x = $this->request->post('watermark_offset_x');
				$clear_image_cache = true;
			}
			if($this->settings->watermark_offset_y != $this->request->post('watermark_offset_y'))
			{
				$this->settings->watermark_offset_y = $this->request->post('watermark_offset_y');
				$clear_image_cache = true;
			}
			if($this->settings->watermark_transparency != $this->request->post('watermark_transparency'))
			{
				$this->settings->watermark_transparency = $this->request->post('watermark_transparency');
				$clear_image_cache = true;
			}
			if($this->settings->images_sharpen != $this->request->post('images_sharpen'))
			{
				$this->settings->images_sharpen = $this->request->post('images_sharpen');
				$clear_image_cache = true;
			}
			
			
			// Удаление заресайзеных изображений
			if($clear_image_cache)
			{
				$dir = $this->config->resized_images_dir;
				if($handle = opendir($dir))
				{
					while(false !== ($file = readdir($handle)))
					{
						if($file != "." && $file != "..")
						{
							@unlink($dir."/".$file);
						}
					}
					closedir($handle);
				}			
			}			
			$this->design->assign('message_success', 'saved');
			
			// Изменение пароля
			if($new_pass = $this->request->post('new_password'))
			{
				$this->config->set_admin_password($this->request->post('new_login'), $new_pass);
			}
		}
		
		// Текущий логин администратора
		$current_login = $this->config->get_admin_login();
		$this->design->assign('current_login', $current_login);
		
 	  	return $this->design->fetch('settings.tpl');
	}
	
}

