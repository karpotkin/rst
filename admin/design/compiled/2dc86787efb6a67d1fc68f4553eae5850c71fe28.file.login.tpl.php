<?php /* Smarty version Smarty-3.1.21-dev, created on 2016-06-01 15:25:18
         compiled from "C:\OpenServer\domains\rst\admin\design\html\login.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1308574ed42e4a95a1-14530389%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '2dc86787efb6a67d1fc68f4553eae5850c71fe28' => 
    array (
      0 => 'C:\\OpenServer\\domains\\rst\\admin\\design\\html\\login.tpl',
      1 => 1464782520,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1308574ed42e4a95a1-14530389',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'meta_title' => 0,
    'error' => 0,
    'login' => 0,
    'password' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_574ed42e517f75_11566338',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_574ed42e517f75_11566338')) {function content_574ed42e517f75_11566338($_smarty_tpl) {?><?php $_smarty_tpl->tpl_vars['wrapper'] = new Smarty_variable('', null, 1);
if ($_smarty_tpl->parent != null) $_smarty_tpl->parent->tpl_vars['wrapper'] = clone $_smarty_tpl->tpl_vars['wrapper'];?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title><?php echo $_smarty_tpl->tpl_vars['meta_title']->value;?>
</title>
    <link rel="icon" href="design/images/favicon.ico" type="image/x-icon">
    <link rel="stylesheet" type="text/css" href="design/css/bootstrap.min.css" media="screen" />
    <meta name="viewport" content="width=1024">

</head>
<body>
<style>
    html, body{
        height: 100%;
        min-height: 100%;
    }
</style>
<div class="container" style="top: 20%; position: relative;">
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-lg-4" style="margin: 0 auto; float: none;">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title">Авторизация</h3>
                </div>
                <form role="form" method="post">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12 login-box">
                                    <span class="text-danger"><?php echo $_smarty_tpl->tpl_vars['error']->value;?>
</span>
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="glyphicon glyphicon-user"></span></span>
                                        <input type="text" name="login" value="<?php echo $_smarty_tpl->tpl_vars['login']->value;?>
" class="form-control" placeholder="Логин" required autofocus />
                                    </div>
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="glyphicon glyphicon-lock"></span></span>
                                        <input type="password" value="<?php echo $_smarty_tpl->tpl_vars['password']->value;?>
" name="password" class="form-control" placeholder="Ваш пароль" required />
                                    </div>

                            </div>
                        </div>
                    </div>
                    <div class="panel-footer">
                        <div class="row">
                            <div class="col-xs-6 col-sm-6 col-md-6">
                                <button type="submit" class="btn btn-labeled btn-success">
                                    <span class="btn-label"><i class="glyphicon glyphicon-ok"></i></span>
                                    Войти
                                </button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
</body>
</html><?php }} ?>
