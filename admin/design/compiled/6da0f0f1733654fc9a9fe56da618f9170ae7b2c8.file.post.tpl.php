<?php /* Smarty version Smarty-3.1.21-dev, created on 2016-03-24 23:35:07
         compiled from "/Users/evgeniy/sites/minsk-tokyo.dev/admin/design/html/post.tpl" */ ?>
<?php /*%%SmartyHeaderCode:8034065556f44f7bb588b8-33636104%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '6da0f0f1733654fc9a9fe56da618f9170ae7b2c8' => 
    array (
      0 => '/Users/evgeniy/sites/minsk-tokyo.dev/admin/design/html/post.tpl',
      1 => 1457992916,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '8034065556f44f7bb588b8-33636104',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'post' => 0,
    'message_success' => 0,
    'message_error' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_56f44f7bcb0df2_46421635',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_56f44f7bcb0df2_46421635')) {function content_56f44f7bcb0df2_46421635($_smarty_tpl) {?><?php $_smarty_tpl->_capture_stack[0][] = array('tabs', null, null); ob_start(); ?>
	<li class="active"><a href="<?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['url'][0][0]->url_modifier(array('module'=>'BlogAdmin','id'=>null,'page'=>null),$_smarty_tpl);?>
">Новости</a></li>
<?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>

<?php if ($_smarty_tpl->tpl_vars['post']->value->id) {?>
	<?php $_smarty_tpl->tpl_vars['meta_title'] = new Smarty_variable($_smarty_tpl->tpl_vars['post']->value->name, null, 1);
if ($_smarty_tpl->parent != null) $_smarty_tpl->parent->tpl_vars['meta_title'] = clone $_smarty_tpl->tpl_vars['meta_title'];?>
<?php } else { ?>
	<?php $_smarty_tpl->tpl_vars['meta_title'] = new Smarty_variable('Новая запись', null, 1);
if ($_smarty_tpl->parent != null) $_smarty_tpl->parent->tpl_vars['meta_title'] = clone $_smarty_tpl->tpl_vars['meta_title'];?>
<?php }?>


<?php echo $_smarty_tpl->getSubTemplate ('tinymce_init.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>


<div class="page">
    <?php if ($_smarty_tpl->tpl_vars['message_success']->value) {?>
        <!-- Системное сообщение -->
        <div class="message message_success">
            <span><?php if ($_smarty_tpl->tpl_vars['message_success']->value=='added') {?>Запись добавлена<?php } elseif ($_smarty_tpl->tpl_vars['message_success']->value=='updated') {?>Запись обновлена<?php }?></span>
            <a class="link" target="_blank" href="/news/<?php echo $_smarty_tpl->tpl_vars['post']->value->url;?>
">Открыть запись на сайте</a>
            <?php if ($_GET['return']) {?>
                <a class="button" href="<?php echo $_GET['return'];?>
">Вернуться</a>
            <?php }?>
        </div>
        <!-- Системное сообщение (The End)-->
    <?php }?>

    <?php if ($_smarty_tpl->tpl_vars['message_error']->value) {?>
        <!-- Системное сообщение -->
        <div class="message message_error">
            <span><?php if ($_smarty_tpl->tpl_vars['message_error']->value=='url_exists') {?>Запись с таким адресом уже существует<?php }?></span>
            <a class="button" href="">Вернуться</a>
        </div>
        <!-- Системное сообщение (The End)-->
    <?php }?>

    <ul class="nav nav-tabs" role="tablist">
        <li class="active" role="presentation"><a href="#general" aria-controls="general" role="tab" data-toggle="tab">Основное</a></li>
        <li role="presentation"><a href="#data" aria-controls="data" role="tab" data-toggle="tab">Данные</a></li>
    </ul>

    <!-- Tab panes -->
    <!-- Основная форма -->
    <form method="post" class="form-horizontal" enctype="multipart/form-data">
        <input type="hidden" name="session_id" value="<?php echo $_SESSION['id'];?>
">
        <input name="id" type="hidden" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['post']->value->id, ENT_QUOTES, 'UTF-8', true);?>
"/>
        <div class="tab-content">
            <div role="tabpanel" class="tab-pane active" id="general">
                <ul class="nav nav-tabs" role="tablist">
                    <li class="active" role="presentation"><a href="#russian" aria-controls="russian" role="tab" data-toggle="tab">Русский</a></li>
                    <li role="presentation"><a href="#japanese" aria-controls="japanese" role="tab" data-toggle="tab">Japanese</a></li>
                </ul>
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="russian">

                        <input type="hidden" name="post_description[0][language_id]" value="1" />
                        <input type="hidden" name="post_description[0][id]" value="<?php echo $_smarty_tpl->tpl_vars['post']->value->description[0]->id;?>
" />

                        <div class="form-group required">
                            <label class="col-sm-3 control-label">Название</label>
                            <div class="col-sm-6">
                                <input class="form-control" name="post_description[0][name]" type="text" placeholder="Название" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['post']->value->description[0]->name, ENT_QUOTES, 'UTF-8', true);?>
"/>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-12 col-md-3 control-label">Краткий текст</label>
                            <div class="col-sm-12 col-md-9">
                                <textarea name="post_description[0][annotation]" rows="5" class="form-control"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['post']->value->description[0]->annotation, ENT_QUOTES, 'UTF-8', true);?>
</textarea>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-12 col-md-3 control-label">Полный текст</label>
                            <div class="col-sm-12 col-md-9">
                                <textarea name="post_description[0][text]"  class="editor_large"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['post']->value->description[0]->text, ENT_QUOTES, 'UTF-8', true);?>
</textarea>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label">Мета-тег Title</label>
                            <div class="col-sm-6">
                                <input name="post_description[0][meta_title]" class="form-control" type="text" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['post']->value->description[0]->meta_title, ENT_QUOTES, 'UTF-8', true);?>
" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label">Мета-тег Keywords</label>
                            <div class="col-sm-6">
                                <input name="post_description[0][meta_keywords]" class="form-control" type="text" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['post']->value->description[0]->meta_keywords, ENT_QUOTES, 'UTF-8', true);?>
" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label">Мета-тег Description</label>
                            <div class="col-sm-6">
                                <textarea name="post_description[0][meta_description]" class="form-control" type="text"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['post']->value->description[0]->meta_description, ENT_QUOTES, 'UTF-8', true);?>
</textarea>
                            </div>
                        </div>

                    </div>
                    <div role="tabpanel" class="tab-pane" id="japanese">

                        <input type="hidden" name="post_description[1][language_id]" value="2" />
                        <input type="hidden" name="post_description[1][id]" value="<?php echo $_smarty_tpl->tpl_vars['post']->value->description[1]->id;?>
" />

                        <div class="form-group required">
                            <label class="col-sm-3 control-label">Name</label>
                            <div class="col-sm-6">
                                <input class="form-control" name="post_description[1][name]" type="text" placeholder="Name" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['post']->value->description[1]->name, ENT_QUOTES, 'UTF-8', true);?>
"/>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-12 col-md-3 control-label">Page Annotation</label>
                            <div class="col-sm-12 col-md-9">
                                <textarea name="post_description[1][annotation]" rows="5" class="form-control"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['post']->value->description[1]->annotation, ENT_QUOTES, 'UTF-8', true);?>
</textarea>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-12 col-md-3 control-label">Page Text</label>
                            <div class="col-sm-12 col-md-9">
                                <textarea name="post_description[1][text]"  class="editor_large"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['post']->value->description[1]->text, ENT_QUOTES, 'UTF-8', true);?>
</textarea>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label">Meta Title</label>
                            <div class="col-sm-6">
                                <input name="post_description[1][meta_title]" class="form-control" type="text" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['post']->value->description[1]->meta_title, ENT_QUOTES, 'UTF-8', true);?>
" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label">Meta Keywords</label>
                            <div class="col-sm-6">
                                <input name="post_description[1][meta_keywords]" class="form-control" type="text" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['post']->value->description[1]->meta_keywords, ENT_QUOTES, 'UTF-8', true);?>
" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label">Meta Description</label>
                            <div class="col-sm-6">
                                <textarea name="post_description[1][meta_description]" class="form-control" type="text"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['post']->value->description[1]->meta_description, ENT_QUOTES, 'UTF-8', true);?>
</textarea>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <div role="tabpanel" class="tab-pane page__data-tab" id="data">

                <div class="form-group">
                    <label class="col-sm-3 control-label">Отображение</label>
                    <div class="col-sm-3">
                        <div class="checkbox">
                            <label><input name="visible" value="1" type="checkbox" <?php if ($_smarty_tpl->tpl_vars['post']->value->visible) {?>checked<?php }?>> Активна</label>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-3 control-label">Дата</label>
                    <div class="col-sm-6">
                        <input type="text" name="date" value="<?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['date'][0][0]->date_modifier($_smarty_tpl->tpl_vars['post']->value->date);?>
" class="form-control">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-3 control-label">Адрес</label>
                    <div class="col-sm-6">
                        <div class="input-group">
                            <span class="input-group-addon">http://<?php echo $_SERVER['SERVER_NAME'];?>
/news/</span>
                            <input name="url" class="page_url form-control" type="text" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['post']->value->url, ENT_QUOTES, 'UTF-8', true);?>
" />
                        </div>
                    </div>
                </div>

                <!-- Изображение страницы -->
                <div class="form-group">
                    <label class="col-sm-3 control-label">Добавить изображение</label>
                    <div class="col-sm-6">
                        <?php if ($_smarty_tpl->tpl_vars['post']->value->image) {?>
                            <img src="/files/news/small/<?php echo $_smarty_tpl->tpl_vars['post']->value->image;?>
" alt="" />
                        <?php } else { ?>
                            <input class="upload_image" name="image" type="file">
                        <?php }?>
                    </div>
                </div>
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-12 text-right">
                <button class="btn btn-success" type="submit">Сохранить</button>
            </div>
        </div>

    </form>
    <!-- Основная форма (The End) -->
</div>
<?php }} ?>
