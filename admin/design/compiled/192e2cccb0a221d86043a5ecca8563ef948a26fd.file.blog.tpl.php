<?php /* Smarty version Smarty-3.1.21-dev, created on 2016-03-24 23:40:52
         compiled from "/Users/evgeniy/sites/minsk-tokyo.dev/admin/design/html/blog.tpl" */ ?>
<?php /*%%SmartyHeaderCode:130758505756f44eb96775e2-45637706%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '192e2cccb0a221d86043a5ecca8563ef948a26fd' => 
    array (
      0 => '/Users/evgeniy/sites/minsk-tokyo.dev/admin/design/html/blog.tpl',
      1 => 1458852050,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '130758505756f44eb96775e2-45637706',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_56f44eb9762558_35095382',
  'variables' => 
  array (
    'keyword' => 0,
    'posts_count' => 0,
    'posts' => 0,
    'post' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_56f44eb9762558_35095382')) {function content_56f44eb9762558_35095382($_smarty_tpl) {?>
<?php $_smarty_tpl->_capture_stack[0][] = array('tabs', null, null); ob_start(); ?>
    <li class="active"><a href="<?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['url'][0][0]->url_modifier(array('module'=>'BlogAdmin','page'=>null,'keyword'=>null),$_smarty_tpl);?>
">Новости</a></li>
<?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>


<?php $_smarty_tpl->tpl_vars['meta_title'] = new Smarty_variable('Новости', null, 1);
if ($_smarty_tpl->parent != null) $_smarty_tpl->parent->tpl_vars['meta_title'] = clone $_smarty_tpl->tpl_vars['meta_title'];?>

<div id="main_list" class="list-container">

    
    <div class="row">
        <div class="col-sm-6">
            <h2 class="list-title">
                <?php if ($_smarty_tpl->tpl_vars['keyword']->value&&$_smarty_tpl->tpl_vars['posts_count']->value) {?>
                    <?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['plural'][0][0]->plural_modifier($_smarty_tpl->tpl_vars['posts_count']->value,'Нашлась','Нашлись','Нашлись');?>
 <?php echo $_smarty_tpl->tpl_vars['posts_count']->value;?>
 <?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['plural'][0][0]->plural_modifier($_smarty_tpl->tpl_vars['posts_count']->value,'запись','записей','записи');?>

                <?php } elseif ($_smarty_tpl->tpl_vars['posts_count']->value) {?>
                    <?php echo $_smarty_tpl->tpl_vars['posts_count']->value;?>
 <?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['plural'][0][0]->plural_modifier($_smarty_tpl->tpl_vars['posts_count']->value,'запись','записей','записи');?>
 в новостях
                <?php } else { ?>
                    Нет записей
                <?php }?>
            </h2>
        </div>
        <div class="col-sm-6 text-right">
            <a class="btn btn-default" href="<?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['url'][0][0]->url_modifier(array('path'=>'news/add'),$_smarty_tpl);?>
"><i class="fa fa-plus text-success"></i> Добавить запись</a>
        </div>
    </div>
    <?php if ($_smarty_tpl->tpl_vars['posts']->value) {?>
        <!-- Листалка страниц -->
        <?php echo $_smarty_tpl->getSubTemplate ('pagination.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

        <!-- Листалка страниц (The End) -->

        <form id="form_list" method="post" class="form-horizontal list__form">
            <input type="hidden" name="session_id" value="<?php echo $_SESSION['id'];?>
">

            <div id="list" class="form-group form-group__mod list">
                <?php  $_smarty_tpl->tpl_vars['post'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['post']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['posts']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['post']->key => $_smarty_tpl->tpl_vars['post']->value) {
$_smarty_tpl->tpl_vars['post']->_loop = true;
?>
                    <div class="<?php if (!$_smarty_tpl->tpl_vars['post']->value->visible) {?>invisible <?php }?>col-sm-12 list__item">
                        <input type="hidden" name="positions[<?php echo $_smarty_tpl->tpl_vars['post']->value->id;?>
]" value="<?php echo $_smarty_tpl->tpl_vars['post']->value->position;?>
">
                        <div class="checkbox cell">
                            <input type="checkbox" name="check[]" value="<?php echo $_smarty_tpl->tpl_vars['post']->value->id;?>
" />
                        </div>
                        <div class="name cell">
                            <a href="<?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['url'][0][0]->url_modifier(array('path'=>(('news/').($_smarty_tpl->tpl_vars['post']->value->id)).('/edit')),$_smarty_tpl);?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['post']->value->name, ENT_QUOTES, 'UTF-8', true);?>
</a>
                            <br>
                            <?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['date'][0][0]->date_modifier($_smarty_tpl->tpl_vars['post']->value->date);?>

                        </div>
                        <div class="icons cell">
                            <a class="preview" title="Предпросмотр в новом окне" href="../blog/<?php echo $_smarty_tpl->tpl_vars['post']->value->url;?>
" target="_blank"></a>
                            <a class="enable" title="Активна" href="#"></a>
                            <a class="delete" title="Удалить" href="#"></a>
                        </div>
                    </div>
                <?php } ?>
            </div>


            <div id="action" class="form-group">
                <div class="col-sm-2">
                    <label id="check_all" class="dash_link control-label">Выбрать все</label>
                </div>

                <div id="select" class="col-sm-6">
                    <select name="action" class="form-control">
                        <option value="enable">Сделать видимыми</option>
                        <option value="disable">Сделать невидимыми</option>
                        <option value="delete">Удалить</option>
                    </select>
                </div>
                <div class="col-sm-4 text-right">
                    <button id="apply_action" class="btn btn-success" type="submit">Применить</button>
                </div>

            </div>

        </form>

        <!-- Листалка страниц -->
        <?php echo $_smarty_tpl->getSubTemplate ('pagination.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

        <!-- Листалка страниц (The End) -->
    <?php }?>
</div><?php }} ?>
