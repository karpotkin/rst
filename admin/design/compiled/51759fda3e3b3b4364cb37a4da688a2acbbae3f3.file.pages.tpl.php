<?php /* Smarty version Smarty-3.1.21-dev, created on 2016-06-01 15:26:29
         compiled from "C:\OpenServer\domains\rst\admin\design\html\pages.tpl" */ ?>
<?php /*%%SmartyHeaderCode:27495574ed475322b79-78266542%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '51759fda3e3b3b4364cb37a4da688a2acbbae3f3' => 
    array (
      0 => 'C:\\OpenServer\\domains\\rst\\admin\\design\\html\\pages.tpl',
      1 => 1464782520,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '27495574ed475322b79-78266542',
  'function' => 
  array (
    'parent_select' => 
    array (
      'parameter' => 
      array (
        'level' => 0,
      ),
      'compiled' => '',
    ),
  ),
  'variables' => 
  array (
    'menu' => 0,
    'parents' => 0,
    'pars' => 0,
    'par' => 0,
    'level' => 0,
  ),
  'has_nocache_code' => 0,
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_574ed475495bc9_93017683',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_574ed475495bc9_93017683')) {function content_574ed475495bc9_93017683($_smarty_tpl) {?>
<?php ob_start();?><?php echo $_smarty_tpl->tpl_vars['menu']->value->name;?>
<?php $_tmp1=ob_get_clean();?><?php $_smarty_tpl->tpl_vars['meta_title'] = new Smarty_variable($_tmp1, null, 1);
if ($_smarty_tpl->parent != null) $_smarty_tpl->parent->tpl_vars['meta_title'] = clone $_smarty_tpl->tpl_vars['meta_title'];?>


<div id="header">
	<h1><?php echo $_smarty_tpl->tpl_vars['menu']->value->name;?>
</h1>
	<a class="add" href="<?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['url'][0][0]->url_modifier(array('module'=>'PageAdmin'),$_smarty_tpl);?>
">Добавить страницу</a>
</div>
<?php if ($_smarty_tpl->tpl_vars['parents']->value) {?>
<div id="main_list">
 
	<form id="list_form" method="post">
		<input type="hidden" name="session_id" value="<?php echo $_SESSION['id'];?>
">
		<?php if (!function_exists('smarty_template_function_parent_select')) {
    function smarty_template_function_parent_select($_smarty_tpl,$params) {
    $saved_tpl_vars = $_smarty_tpl->tpl_vars;
    foreach ($_smarty_tpl->smarty->template_functions['parent_select']['parameter'] as $key => $value) {$_smarty_tpl->tpl_vars[$key] = new Smarty_variable($value);};
    foreach ($params as $key => $value) {$_smarty_tpl->tpl_vars[$key] = new Smarty_variable($value);}?>
		<div id="list" class="sortable">

			<?php  $_smarty_tpl->tpl_vars['par'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['par']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['pars']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['par']->key => $_smarty_tpl->tpl_vars['par']->value) {
$_smarty_tpl->tpl_vars['par']->_loop = true;
?>
			<?php if ($_smarty_tpl->tpl_vars['menu']->value->id==$_smarty_tpl->tpl_vars['par']->value->menu_id) {?>
			<div class="<?php if (!$_smarty_tpl->tpl_vars['par']->value->visible) {?>invisible<?php }?> row row_mod">
				<div class="tree_row">
					<input type="hidden" name="positions[<?php echo $_smarty_tpl->tpl_vars['par']->value->id;?>
]" value="<?php echo $_smarty_tpl->tpl_vars['par']->value->position;?>
">
					<div class="move cell" style="margin-left:<?php echo $_smarty_tpl->tpl_vars['level']->value*20;?>
px"><div class="move_zone"></div></div>
			 		<div class="checkbox cell">
						<input type="checkbox" name="check[]" value="<?php echo $_smarty_tpl->tpl_vars['par']->value->id;?>
" />
					</div>
					<div class="name cell">
						<a href="<?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['url'][0][0]->url_modifier(array('path'=>(('pages/').($_smarty_tpl->tpl_vars['par']->value->id)).('/edit')),$_smarty_tpl);?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['par']->value->header, ENT_QUOTES, 'UTF-8', true);?>
</a>
					</div>
					<div class="icons cell">
						<a class="preview" title="Предпросмотр в новом окне" href="../<?php echo $_smarty_tpl->tpl_vars['par']->value->url;?>
" target="_blank"></a>
						<a class="enable" title="Активна" href="#"></a>
						<a class="delete" title="Удалить" href="#"></a>
					</div>
					<div class="clear"></div>
				</div>
				<?php smarty_template_function_parent_select($_smarty_tpl,array('pars'=>$_smarty_tpl->tpl_vars['par']->value->subcategories,'level'=>$_smarty_tpl->tpl_vars['level']->value+1));?>

			</div>
			<?php }?>
			<?php } ?>
		</div>
		<?php $_smarty_tpl->tpl_vars = $saved_tpl_vars;
foreach (Smarty::$global_tpl_vars as $key => $value) if(!isset($_smarty_tpl->tpl_vars[$key])) $_smarty_tpl->tpl_vars[$key] = $value;}}?>

		<?php smarty_template_function_parent_select($_smarty_tpl,array('pars'=>$_smarty_tpl->tpl_vars['parents']->value));?>

	
		<div id="action">
		<label id="check_all" class="dash_link">Выбрать все</label>
	
		<span id="select">
		<select name="action">
			<option value="enable">Сделать видимыми</option>
			<option value="disable">Сделать невидимыми</option>
			<option value="delete">Удалить</option>
		</select>
		</span>
	
		<input id="apply_action" class="button_green" type="submit" value="Применить">
	
		</div>
	</form>	
</div>
<?php } else { ?>
	Нет страниц
<?php }?>
<?php }} ?>
