<?php /* Smarty version Smarty-3.1.21-dev, created on 2016-03-24 23:07:12
         compiled from "/Users/evgeniy/sites/minsk-tokyo.dev/admin/design/html/page.tpl" */ ?>
<?php /*%%SmartyHeaderCode:89029213556f44188044af3-58876908%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'ebbc9f7afe2b46bbc9547e6a818449dbb40cd275' => 
    array (
      0 => '/Users/evgeniy/sites/minsk-tokyo.dev/admin/design/html/page.tpl',
      1 => 1458850021,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '89029213556f44188044af3-58876908',
  'function' => 
  array (
    'parent_select' => 
    array (
      'parameter' => 
      array (
        'level' => 0,
      ),
      'compiled' => '',
    ),
  ),
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_56f441881dfcc9_53247363',
  'variables' => 
  array (
    'menus' => 0,
    'm' => 0,
    'menu' => 0,
    'page' => 0,
    'message_success' => 0,
    'message_error' => 0,
    'pars' => 0,
    'par' => 0,
    'level' => 0,
    'parents' => 0,
  ),
  'has_nocache_code' => 0,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_56f441881dfcc9_53247363')) {function content_56f441881dfcc9_53247363($_smarty_tpl) {?><?php $_smarty_tpl->_capture_stack[0][] = array('tabs', null, null); ob_start(); ?>
	<?php  $_smarty_tpl->tpl_vars['m'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['m']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['menus']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['m']->key => $_smarty_tpl->tpl_vars['m']->value) {
$_smarty_tpl->tpl_vars['m']->_loop = true;
?>
		<li <?php if ($_smarty_tpl->tpl_vars['m']->value->id==$_smarty_tpl->tpl_vars['menu']->value->id) {?>class="active"<?php }?>><a href='index.php?module=PagesAdmin&menu_id=<?php echo $_smarty_tpl->tpl_vars['m']->value->id;?>
'><?php echo $_smarty_tpl->tpl_vars['m']->value->name;?>
</a></li>
	<?php } ?>
<?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>

<?php if ($_smarty_tpl->tpl_vars['page']->value->id) {?>
	<?php $_smarty_tpl->tpl_vars['meta_title'] = new Smarty_variable($_smarty_tpl->tpl_vars['page']->value->name, null, 1);
if ($_smarty_tpl->parent != null) $_smarty_tpl->parent->tpl_vars['meta_title'] = clone $_smarty_tpl->tpl_vars['meta_title'];?>
<?php } else { ?>
	<?php $_smarty_tpl->tpl_vars['meta_title'] = new Smarty_variable('Новая страница', null, 1);
if ($_smarty_tpl->parent != null) $_smarty_tpl->parent->tpl_vars['meta_title'] = clone $_smarty_tpl->tpl_vars['meta_title'];?>
<?php }?>

<div class="page">
	<?php if ($_smarty_tpl->tpl_vars['message_success']->value) {?>
		<!-- Системное сообщение -->
		<div class="message message_success">
			<span><?php if ($_smarty_tpl->tpl_vars['message_success']->value=='added') {?>Страница добавлена<?php } elseif ($_smarty_tpl->tpl_vars['message_success']->value=='updated') {?>Страница обновлена<?php }?></span>
			<a class="link" target="_blank" href="<?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['url'][0][0]->url_modifier(array('path'=>$_smarty_tpl->tpl_vars['page']->value->url),$_smarty_tpl);?>
">Открыть страницу на сайте</a>
			<?php if ($_GET['return']) {?>
				<a class="button" href="<?php echo $_GET['return'];?>
">Вернуться</a>
			<?php }?>
		</div>
		<!-- Системное сообщение (The End)-->
	<?php }?>

	<?php if ($_smarty_tpl->tpl_vars['message_error']->value) {?>
		<!-- Системное сообщение -->
		<div class="message message_error">
			<span><?php if ($_smarty_tpl->tpl_vars['message_error']->value=='url_exists') {?>Страница с таким адресом уже существует<?php }?></span>
			<a class="button" href="">Вернуться</a>
		</div>
		<!-- Системное сообщение (The End)-->
	<?php }?>

	<ul class="nav nav-tabs" role="tablist">
		<li class="active" role="presentation"><a href="#general" aria-controls="general" role="tab" data-toggle="tab">Основное</a></li>
		<li role="presentation"><a href="#data" aria-controls="data" role="tab" data-toggle="tab">Данные</a></li>
	</ul>

	<!-- Tab panes -->
	<!-- Основная форма -->
	<form method="post" class="form-horizontal" enctype="multipart/form-data">
		<input type="hidden" name="session_id" value="<?php echo $_SESSION['id'];?>
">
		<input type="hidden" name="menu_id" value="<?php echo $_smarty_tpl->tpl_vars['page']->value->menu_id;?>
"/>
		<input name="id" type="hidden" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['page']->value->id, ENT_QUOTES, 'UTF-8', true);?>
"/>
		<div class="tab-content">
			<div role="tabpanel" class="tab-pane active" id="general">
				<ul class="nav nav-tabs" role="tablist">
					<li class="active" role="presentation"><a href="#russian" aria-controls="russian" role="tab" data-toggle="tab">Русский</a></li>
					<li role="presentation"><a href="#japanese" aria-controls="japanese" role="tab" data-toggle="tab">Japanese</a></li>
				</ul>
				<div class="tab-content">
					<div role="tabpanel" class="tab-pane active" id="russian">

						<input type="hidden" name="page_description[0][language_id]" value="1" />
						<input type="hidden" name="page_description[0][id]" value="<?php echo $_smarty_tpl->tpl_vars['page']->value->description[0]->id;?>
" />

						<div class="form-group required">
							<label class="col-sm-3 control-label">Название</label>
							<div class="col-sm-6">
								<input class="form-control" name="page_description[0][header]" type="text" placeholder="Название" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['page']->value->description[0]->header, ENT_QUOTES, 'UTF-8', true);?>
"/>
							</div>
						</div>

						<div class="form-group">
							<label class="col-sm-3 control-label">Название пункта в меню</label>
							<div class="col-sm-6">
								<input name="page_description[0][name]" class="form-control" type="text" placeholder="Заполняется автоматически" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['page']->value->description[0]->name, ENT_QUOTES, 'UTF-8', true);?>
" />
							</div>
						</div>

						<div class="form-group">
							<label class="col-sm-12 col-md-3 control-label">Текст на странице</label>
							<div class="col-sm-12 col-md-9">
								<textarea name="page_description[0][page_text]"  class="editor_large"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['page']->value->description[0]->page_text, ENT_QUOTES, 'UTF-8', true);?>
</textarea>
							</div>
						</div>

						<div class="form-group">
							<label class="col-sm-3 control-label">Мета-тег Title</label>
							<div class="col-sm-6">
								<input name="page_description[0][meta_title]" class="form-control" type="text" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['page']->value->description[0]->meta_title, ENT_QUOTES, 'UTF-8', true);?>
" />
							</div>
						</div>

						<div class="form-group">
							<label class="col-sm-3 control-label">Мета-тег Keywords</label>
							<div class="col-sm-6">
								<input name="page_description[0][meta_keywords]" class="form-control" type="text" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['page']->value->description[0]->meta_keywords, ENT_QUOTES, 'UTF-8', true);?>
" />
							</div>
						</div>

						<div class="form-group">
							<label class="col-sm-3 control-label">Мета-тег Description</label>
							<div class="col-sm-6">
								<textarea name="page_description[0][meta_description]" class="form-control" type="text"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['page']->value->description[0]->meta_description, ENT_QUOTES, 'UTF-8', true);?>
</textarea>
							</div>
						</div>

					</div>
					<div role="tabpanel" class="tab-pane" id="japanese">

						<input type="hidden" name="page_description[1][language_id]" value="2" />
						<input type="hidden" name="page_description[1][id]" value="<?php echo $_smarty_tpl->tpl_vars['page']->value->description[1]->id;?>
" />

						<div class="form-group required">
							<label class="col-sm-3 control-label">Name</label>
							<div class="col-sm-6">
								<input class="form-control" name="page_description[1][header]" type="text" placeholder="Name" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['page']->value->description[1]->header, ENT_QUOTES, 'UTF-8', true);?>
"/>
							</div>
						</div>

						<div class="form-group">
							<label class="col-sm-3 control-label">Menu Name</label>
							<div class="col-sm-6">
								<input name="page_description[1][name]" class="form-control" type="text" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['page']->value->description[1]->name, ENT_QUOTES, 'UTF-8', true);?>
" />
							</div>
						</div>

						<div class="form-group">
							<label class="col-sm-12 col-md-3 control-label">Page Content</label>
							<div class="col-sm-12 col-md-9">
								<textarea name="page_description[1][page_text]"  class="editor_large"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['page']->value->description[1]->page_text, ENT_QUOTES, 'UTF-8', true);?>
</textarea>
							</div>
						</div>

						<div class="form-group">
							<label class="col-sm-3 control-label">Meta Title</label>
							<div class="col-sm-6">
								<input name="page_description[1][meta_title]" class="form-control" type="text" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['page']->value->description[1]->meta_title, ENT_QUOTES, 'UTF-8', true);?>
" />
							</div>
						</div>

						<div class="form-group">
							<label class="col-sm-3 control-label">Meta Keywords</label>
							<div class="col-sm-6">
								<input name="page_description[1][meta_keywords]" class="form-control" type="text" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['page']->value->description[1]->meta_keywords, ENT_QUOTES, 'UTF-8', true);?>
" />
							</div>
						</div>

						<div class="form-group">
							<label class="col-sm-3 control-label">Meta Description</label>
							<div class="col-sm-6">
								<textarea name="page_description[1][meta_description]" class="form-control" type="text"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['page']->value->description[1]->meta_description, ENT_QUOTES, 'UTF-8', true);?>
</textarea>
							</div>
						</div>

					</div>
				</div>
			</div>
			<div role="tabpanel" class="tab-pane page__data-tab" id="data">
				<div class="form-group">
					<label class="col-sm-3 control-label">Отображение</label>
					<div class="col-sm-3">
						<div class="checkbox">
							<label><input name="visible" value="1" type="checkbox" <?php if ($_smarty_tpl->tpl_vars['page']->value->visible) {?>checked<?php }?>> Активна</label>
						</div>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-3 control-label">Родительская категория</label>
					<div class="col-sm-6">
						<select name="parent_id" class="form-control">
							<option value='0'>Корневая категория</option>
							<?php if (!function_exists('smarty_template_function_parent_select')) {
    function smarty_template_function_parent_select($_smarty_tpl,$params) {
    $saved_tpl_vars = $_smarty_tpl->tpl_vars;
    foreach ($_smarty_tpl->smarty->template_functions['parent_select']['parameter'] as $key => $value) {$_smarty_tpl->tpl_vars[$key] = new Smarty_variable($value);};
    foreach ($params as $key => $value) {$_smarty_tpl->tpl_vars[$key] = new Smarty_variable($value);}?>
								<?php  $_smarty_tpl->tpl_vars['par'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['par']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['pars']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['par']->key => $_smarty_tpl->tpl_vars['par']->value) {
$_smarty_tpl->tpl_vars['par']->_loop = true;
?>
									<?php if ($_smarty_tpl->tpl_vars['page']->value->id!=$_smarty_tpl->tpl_vars['par']->value->id&&$_smarty_tpl->tpl_vars['par']->value->name!=''&&$_smarty_tpl->tpl_vars['page']->value->menu_id==$_smarty_tpl->tpl_vars['par']->value->menu_id) {?>
										<option value='<?php echo $_smarty_tpl->tpl_vars['par']->value->id;?>
' <?php if ($_smarty_tpl->tpl_vars['page']->value->parent_id==$_smarty_tpl->tpl_vars['par']->value->id) {?>selected<?php }?>><?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['sp'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['sp']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['sp']['name'] = 'sp';
$_smarty_tpl->tpl_vars['smarty']->value['section']['sp']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['level']->value) ? count($_loop) : max(0, (int) $_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['sp']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['sp']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['sp']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['sp']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['sp']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['sp']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['sp']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['sp']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['sp']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['sp']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['sp']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['sp']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['sp']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['sp']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['sp']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['sp']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['sp']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['sp']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['sp']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['sp']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['sp']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['sp']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['sp']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['sp']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['sp']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['sp']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['sp']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['sp']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['sp']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['sp']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['sp']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['sp']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['sp']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['sp']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['sp']['total']);
?>&nbsp;&nbsp;&nbsp;&nbsp;<?php endfor; endif;
echo $_smarty_tpl->tpl_vars['par']->value->name;?>
</option>
										<?php smarty_template_function_parent_select($_smarty_tpl,array('pars'=>$_smarty_tpl->tpl_vars['par']->value->subcategories,'level'=>$_smarty_tpl->tpl_vars['level']->value+1));?>

									<?php }?>
								<?php } ?>
							<?php $_smarty_tpl->tpl_vars = $saved_tpl_vars;
foreach (Smarty::$global_tpl_vars as $key => $value) if(!isset($_smarty_tpl->tpl_vars[$key])) $_smarty_tpl->tpl_vars[$key] = $value;}}?>

							<?php smarty_template_function_parent_select($_smarty_tpl,array('pars'=>$_smarty_tpl->tpl_vars['parents']->value));?>

						</select>
					</div>
				</div>

				<div class="form-group">
					<label class="col-sm-3 control-label">Адрес</label>
					<div class="col-sm-6">
						<div class="input-group">
							<span class="input-group-addon">http://<?php echo $_SERVER['SERVER_NAME'];?>
/</span>
							<input name="url" class="page_url form-control" type="text" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['page']->value->url, ENT_QUOTES, 'UTF-8', true);?>
" />
						</div>
					</div>
				</div>

				<!-- Изображение страницы -->
				<div class="form-group">
					<label class="col-sm-3 control-label">Добавить изображение</label>
					<div class="col-sm-6">
						<?php if ($_smarty_tpl->tpl_vars['page']->value->image) {?>
							<img src="/files/pages/small/<?php echo $_smarty_tpl->tpl_vars['page']->value->image;?>
" alt="" />
						<?php } else { ?>
							<input class="upload_image" name="image" type="file">
						<?php }?>
					</div>
				</div>
			</div>
		</div>

		<div class="form-group">
			<div class="col-sm-12 text-right">
				<button class="btn btn-success" type="submit">Сохранить</button>
			</div>
		</div>

	</form>
	<!-- Основная форма (The End) -->
</div>

<?php }} ?>
