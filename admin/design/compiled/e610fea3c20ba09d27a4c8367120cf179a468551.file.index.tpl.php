<?php /* Smarty version Smarty-3.1.21-dev, created on 2016-06-01 15:26:29
         compiled from "C:\OpenServer\domains\rst\admin\design\html\index.tpl" */ ?>
<?php /*%%SmartyHeaderCode:19732574ed4754dd720-84686544%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'e610fea3c20ba09d27a4c8367120cf179a468551' => 
    array (
      0 => 'C:\\OpenServer\\domains\\rst\\admin\\design\\html\\index.tpl',
      1 => 1464782520,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '19732574ed4754dd720-84686544',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'meta_title' => 0,
    'content' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_574ed47550ac00_29132609',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_574ed47550ac00_29132609')) {function content_574ed47550ac00_29132609($_smarty_tpl) {?><?php if (!is_callable('smarty_modifier_date_format')) include 'C:\\OpenServer\\domains\\rst\\libs\\plugins\\modifier.date_format.php';
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title><?php echo $_smarty_tpl->tpl_vars['meta_title']->value;?>
</title>
    <link rel="icon" href="/admin/design/images/favicon.ico" type="image/x-icon">
    <link rel="stylesheet" type="text/css" href="/admin/design/css/bootstrap.min.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="/admin/design/css/font-awesome.min.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="/admin/design/js/jquery/jquery-ui.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="/admin/design/css/style.css" />
    <meta name="viewport" content="width=1024">

</head>
<body>

<!-- Вся страница -->
<div id="main">

    <!-- Главное меню -->
    <ul class="nav nav-pills clearfix" style="margin: 20px 0; ">
        <li><a href="<?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['url'][0][0]->url_modifier(array('path'=>'pages'),$_smarty_tpl);?>
">Страницы</a></li>
        <li><a href="<?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['url'][0][0]->url_modifier(array('path'=>'news'),$_smarty_tpl);?>
">Новости</a></li>
        <li><a href="<?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['url'][0][0]->url_modifier(array('path'=>'restaurant-menu'),$_smarty_tpl);?>
">Меню</a></li>
        
    </ul>
    <!-- Главное меню (The End)-->


    <!-- Таб меню -->
    <ul id="tab_menu" class="nav nav-tabs">
        <?php echo Smarty::$_smarty_vars['capture']['tabs'];?>

    </ul>
    <!-- Таб меню (The End)-->

    <!-- Основная часть страницы -->
    <div id="middle">
        <?php echo $_smarty_tpl->tpl_vars['content']->value;?>

    </div>
    <!-- Основная часть страницы (The End) -->

    <!-- Подвал сайта -->
    <div id="footer">
        &copy; <?php echo smarty_modifier_date_format(time(),'%Y');?>

    </div>
    <!-- Подвал сайта (The End)-->

</div>
<!-- Вся страница (The End)-->
<?php echo '<script'; ?>
 src="/admin/design/js/jquery-1.12.1.min.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="/admin/design/js/jquery/jquery.form.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="/admin/design/js/jquery/jquery-ui.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="/admin/design/js/bootstrap.min.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="/admin/design/js/tiny_mce/plugins/smimage/smplugins.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="/admin/design/js/tiny_mce/tiny_mce.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="/admin/design/js/main.js"><?php echo '</script'; ?>
>
</body>
</html>
<?php }} ?>
