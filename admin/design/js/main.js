(function($){
    $(function() {
        // Сортировка списка
        $(".sortable").sortable({
            items:".row",
            handle: ".move_zone",
            tolerance:"pointer",
            scrollSensitivity:40,
            opacity:0.7,
            axis: "y",
            update:function()
            {
                $("#list_form input[name*='check']").attr('checked', false);
                $("#list_form").ajaxSubmit();
            }
        });


        // Выделить все
        $("#check_all").click(function() {
            $('#list input[type="checkbox"][name*="check"]').attr('checked', 1-$('#list input[type="checkbox"][name*="check"]').attr('checked'));
        });

        // Удалить
        $("a.delete").on('click', function(e) {
            e.preventDefault();
            var $self = $(this);
            var $form = $self.closest("form");

            $('#list input[type="checkbox"][name*="check"]').attr('checked', false);
            $self.closest(".list__item").find('input[type="checkbox"][name*="check"]').attr('checked', true);
            $form.find('select[name="action"] option[value=delete]').attr('selected', true);
            $form.submit();
        });


        // Показать
        $("a.enable").click(function() {
            var icon        = $(this);
            var line        = icon.closest(".row");
            var id          = line.find('input[type="checkbox"][name*="check"]').val();
            var state       = line.hasClass('invisible')?1:0;
            icon.addClass('loading_icon');
            $.ajax({
                type: 'POST',
                url: 'ajax/update_object.php',
                data: {'object': 'page', 'id': id, 'values': {'visible': state}, 'session_id': '{/literal}{$smarty.session.id}{literal}'},
                success: function(data){
                    icon.removeClass('loading_icon');
                    if(state)
                        line.removeClass('invisible');
                    else
                        line.addClass('invisible');
                },
                dataType: 'json'
            });
            return false;
        });


        $("form").submit(function() {
            if($('select[name="action"]').val()=='delete' && !confirm('Подтвердите удаление'))
                return false;
        });

        tinyMCE.init({
            // General options
            mode : "specific_textareas",
                editor_selector : /editor/,
                theme : "advanced",
                language : "ru",
                theme_advanced_path : false,
                apply_source_formatting : true,
                plugins : "jaretypograph,smimage,smeditimage,smexplorer,safari,spellchecker,style,table,save,advimage,advlink,autolink,inlinepopups,media,contextmenu,paste,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras",
                relative_urls : false,
                remove_script_host : true,
                convert_urls : true,
                verify_html: false,
                remove_linebreaks : false,
                width: '100%',
                height: '400',
                spellchecker_languages : "+Russian=ru,+English=en",

                // Theme options
                theme_advanced_toolbar_location : "top",
                theme_advanced_toolbar_align : "left",
                theme_advanced_statusbar_location : "bottom",
                theme_advanced_resizing : true,

                file_browser_callback : "SMPlugins",
                plugin_smexplorer_directory : "{$config->subfolder}files/uploads",
                plugin_smimage_directory : "{$config->subfolder}files/uploads",

                setup : function(ed) {
                if(typeof set_meta == 'function')
                {
                    ed.onKeyUp.add(set_meta);
                    ed.onChange.add(set_meta);
                }
            }

        });
    });

    $(function() {

        $('input[name="date"]').datepicker({
            regional:'ru'
        });

        // Автозаполнение мета-тегов
        menu_item_name_touched = true;
        meta_title_touched = true;
        meta_keywords_touched = true;
        meta_description_touched = true;
        url_touched = true;

        if($('input[name="menu_item_name"]').val() == generate_menu_item_name() || $('input[name="name"]').val() == '')
            menu_item_name_touched = false;
        if($('input[name="meta_title"]').val() == generate_meta_title() || $('input[name="meta_title"]').val() == '')
            meta_title_touched = false;
        if($('input[name="meta_keywords"]').val() == generate_meta_keywords() || $('input[name="meta_keywords"]').val() == '')
            meta_keywords_touched = false;
        if($('textarea[name="meta_description"]').val() == generate_meta_description() || $('textarea[name="meta_description"]').val() == '')
            meta_description_touched = false;
        if($('input[name="url"]').val() == generate_url())
            url_touched = false;

        $('input[name="name"]').change(function() { menu_item_name_touched = true; });
        $('input[name="meta_title"]').change(function() { meta_title_touched = true; });
        $('input[name="meta_keywords"]').change(function() { meta_keywords_touched = true; });
        $('textarea[name="meta_description"]').change(function() { meta_description_touched = true; });
        $('input[name="url"]').change(function() { url_touched = true; });

        $('input[name="header"]').keyup(function() { set_meta(); });

        // Удаление изображений
        $(".images a.delete").on('click', function() {
            $(this).closest("li").fadeOut(200, function() { $(this).remove(); });
            return false;
        });
        // Загрузить изображение с компьютера
        $('#upload_image').click(function() {
            $("<input class='upload_image' name=images[] type=file>").appendTo('div#add_image').focus().click();
        });
    });

    function set_meta()
    {
        if(!menu_item_name_touched)
            $('input[name="name"]').val(generate_menu_item_name());
        if(!meta_title_touched)
            $('input[name="meta_title"]').val(generate_meta_title());
        if(!meta_keywords_touched)
            $('input[name="meta_keywords"]').val(generate_meta_keywords());
        if(!meta_description_touched)
        {
            descr = $('textarea[name="meta_description"]');
            descr.val(generate_meta_description());
            descr.scrollTop(descr.outerHeight());
        }
        if(!url_touched)
            $('input[name="url"]').val(generate_url());
    }

    function generate_menu_item_name()
    {
        name = $('input[name="header"]').val();
        return name;
    }

    function generate_meta_title()
    {
        name = $('input[name="header"]').val();
        return name;
    }

    function generate_meta_keywords()
    {
        name = $('input[name="header"]').val();
        return name;
    }

    function generate_meta_description()
    {
        if(typeof(tinyMCE) === 'undefined') return;

        if(typeof(tinyMCE.get("page_text")) =='object')
        {
            //description = tinyMCE.get("page_text").getContent().replace(/(<([^>]+)>)/ig," ").replace(/(\&nbsp;)/ig," ").replace(/^\s+|\s+$/g, '').substr(0, 512);
            //return description;
        }
        //else
        //return $('textarea[name="page_text"]').val().replace(/(<([^>]+)>)/ig," ").replace(/(\&nbsp;)/ig," ").replace(/^\s+|\s+$/g, '').substr(0, 512);
    }

    function generate_url()
    {
        url = $('input[name="header"]').val();
        url = url.replace(/[\s]+/gi, '-');
        url = translit(url);
        url = url.replace(/[^0-9a-z_\-]+/gi, '').toLowerCase();
        return url;
    }

    function translit(str)
    {
        var ru=("А-а-Б-б-В-в-Ґ-ґ-Г-г-Д-д-Е-е-Ё-ё-Є-є-Ж-ж-З-з-И-и-І-і-Ї-ї-Й-й-К-к-Л-л-М-м-Н-н-О-о-П-п-Р-р-С-с-Т-т-У-у-Ф-ф-Х-х-Ц-ц-Ч-ч-Ш-ш-Щ-щ-Ъ-ъ-Ы-ы-Ь-ь-Э-э-Ю-ю-Я-я").split("-")
        var en=("A-a-B-b-V-v-G-g-G-g-D-d-E-e-E-e-E-e-ZH-zh-Z-z-I-i-I-i-I-i-J-j-K-k-L-l-M-m-N-n-O-o-P-p-R-r-S-s-T-t-U-u-F-f-H-h-TS-ts-CH-ch-SH-sh-SCH-sch-'-'-Y-y-'-'-E-e-YU-yu-YA-ya").split("-")
        var res = '';
        for(var i=0, l=str.length; i<l; i++)
        {
            var s = str.charAt(i), n = ru.indexOf(s);
            if(n >= 0) { res += en[n]; }
            else { res += s; }
        }
        return res;
    }

    $(function() {

        // Раскраска строк
        function colorize()
        {
            $("#list div.row:even").addClass('even');
            $("#list div.row:odd").removeClass('even');
        }
        // Раскрасить строки сразу
        colorize();

        // Выделить все
        $("#check_all").click(function() {
            $('#list input[type="checkbox"][name*="check"]').attr('checked', 1-$('#list input[type="checkbox"][name*="check"]').attr('checked'));
        });

        // Скрыт/Видим
        $("a.enable").click(function() {
            var icon        = $(this);
            var line        = icon.closest(".row");
            var id          = line.find('input[type="checkbox"][name*="check"]').val();
            var state       = line.hasClass('invisible')?1:0;
            icon.addClass('loading_icon');
            $.ajax({
                type: 'POST',
                url: 'ajax/update_object.php',
                data: {'object': 'blog', 'id': id, 'values': {'visible': state}, 'session_id': '{/literal}{$smarty.session.id}{literal}'},
                success: function(data){
                    icon.removeClass('loading_icon');
                    if(state)
                        line.removeClass('invisible');
                    else
                        line.addClass('invisible');
                },
                dataType: 'json'
            });
            return false;
        });
    });
})(jQuery);