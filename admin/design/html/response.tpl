{* Вкладки *}

{capture name=tabs}

	<li class="active"><a href="index.php?module=FeedsAdmin">Отзывы</a></li>

{/capture}





{* Title *}

{$meta_title='Отзывы' scope=parent}



{* Поиск *}

{if $comments || $keyword}

<form method="get">

<div id="search">

	<input type="hidden" name="module" value='CommentsAdmin'>

	<input class="search" type="text" name="keyword" value="{$keyword|escape}" />

	<input class="search_button" type="submit" value=""/>

</div>

</form>

{/if}





{* Заголовок *}

<div id="header">

	{if $keyword && $responses_count}

	<h1>{$responses_count|plural:'Нашелся':'Нашлось':'Нашлись'} {$comments_count} {$comments_count|plural:'отзыв':'отзывов':'отзыва'}</h1> 

	{/if}

</div>	





{if $responses}

<div id="main_list">

	

	<!-- Листалка страниц -->

	{include file='pagination.tpl'}	

	<!-- Листалка страниц (The End) -->

	

	<form id="list_form" method="post">

	<input type="hidden" name="session_id" value="{$smarty.session.id}">

	

		<div id="list" class="sortable">

			{foreach $responses as $response}

			<div class="{if !$response->approved}unapproved{/if} row">

		 		<div class="checkbox cell">

					<input type="checkbox" name="check[]" value="{$response->id}"/>				

				</div>

				<div class="name cell">

					<div class="comment_name">

					{$response->name|escape}

					<a class="approve" href="#">Одобрить</a>

					</div>

					<div class="comment_text">

					{$response->text|escape|nl2br}

					</div>

					<div class="comment_info">

					Отзыв добавлен {$response->date|date} в {$response->date|time}

					</div>

				</div>

				<div class="icons cell">

					<a class="delete" title="Удалить" href="#"></a>

				</div>

				<div class="clear"></div>

			</div>

			{/foreach}

		</div>

	

		<div id="action">

		Выбрать <label id="check_all" class="dash_link">все</label> или <label id="check_unapproved" class="dash_link">ожидающие</label>

	

		<span id="select">

		<select name="action">

			<option value="approve">Одобрить</option>

			<option value="delete">Удалить</option>

		</select>

		</span>

	

		<input id="apply_action" class="button_green" type="submit" value="Применить">



	</div>

	</form>

	

	<!-- Листалка страниц -->

	{include file='pagination.tpl'}	

	<!-- Листалка страниц (The End) -->

		

</div>

{else}

Нет отзывов

{/if}



<!-- Меню -->

<div id="right_menu">

	

	<!-- Категории товаров -->

	<ul>

	<li {if !$type}class="selected"{/if}><a href="{url type=null}">Все отзывы</a></li>

	</ul>

	<!-- Категории товаров (The End)-->

	

</div>

<!-- Меню  (The End) -->



{literal}

<script>

$(function() {



	// Раскраска строк

	function colorize()

	{

		$("#list div.row:even").addClass('even');

		$("#list div.row:odd").removeClass('even');

	}

	// Раскрасить строки сразу

	colorize();

	

	// Выделить все

	$("#check_all").click(function() {

		$('#list input[type="checkbox"][name*="check"]').attr('checked', 1-$('#list input[type="checkbox"][name*="check"]').attr('checked'));

	});	



	// Выделить ожидающие

	$("#check_unapproved").click(function() {

		$('#list .unapproved input[type="checkbox"][name*="check"]').attr('checked', 1-$('#list .unapproved input[type="checkbox"][name*="check"]').attr('checked'));

	});	



	// Удалить 

	$("a.delete").click(function() {

		$('#list input[type="checkbox"][name*="check"]').attr('checked', false);

		$(this).closest(".row").find('input[type="checkbox"][name*="check"]').attr('checked', true);

		$(this).closest("form").find('select[name="action"] option[value=delete]').attr('selected', true);

		$(this).closest("form").submit();

	});

	

	// Одобрить

	$("a.approve").click(function() {

		var line        = $(this).closest(".row");

		var id          = line.find('input[type="checkbox"][name*="check"]').val();

		$.ajax({

			type: 'POST',

			url: 'ajax/update_comment.php',

			data: {'object': 'comment', 'id': id, 'values': {'approved': 1}, 'session_id': '{/literal}{$smarty.session.id}{literal}'},

			success: function(data){

				line.removeClass('unapproved');

			},

			dataType: 'json'

		});	

		return false;	

	});

	

	$("form#list_form").submit(function() {

		if($('#list_form select[name="action"]').val()=='delete' && !confirm('Подтвердите удаление'))

			return false;	

	});	

 	

});



</script>

{/literal}