{capture name=tabs}
	<li class="active"><a href="{url module=BlogAdmin id=null page=null}">Новости</a></li>
{/capture}

{if $post->id}
	{$meta_title = $post->name scope=parent}
{else}
	{$meta_title = 'Новая запись' scope=parent}
{/if}

{* Подключаем Tiny MCE *}
{include file='tinymce_init.tpl'}

<div class="page">
    {if $message_success}
        <!-- Системное сообщение -->
        <div class="message message_success">
            <span>{if $message_success == 'added'}Запись добавлена{elseif $message_success == 'updated'}Запись обновлена{/if}</span>
            <a class="link" target="_blank" href="/news/{$post->url}">Открыть запись на сайте</a>
            {if $smarty.get.return}
                <a class="button" href="{$smarty.get.return}">Вернуться</a>
            {/if}
        </div>
        <!-- Системное сообщение (The End)-->
    {/if}

    {if $message_error}
        <!-- Системное сообщение -->
        <div class="message message_error">
            <span>{if $message_error == 'url_exists'}Запись с таким адресом уже существует{/if}</span>
            <a class="button" href="">Вернуться</a>
        </div>
        <!-- Системное сообщение (The End)-->
    {/if}

    <ul class="nav nav-tabs" role="tablist">
        <li class="active" role="presentation"><a href="#general" aria-controls="general" role="tab" data-toggle="tab">Основное</a></li>
        <li role="presentation"><a href="#data" aria-controls="data" role="tab" data-toggle="tab">Данные</a></li>
    </ul>

    <!-- Tab panes -->
    <!-- Основная форма -->
    <form method="post" class="form-horizontal" enctype="multipart/form-data">
        <input type="hidden" name="session_id" value="{$smarty.session.id}">
        <input name="id" type="hidden" value="{$post->id|escape}"/>
        <div class="tab-content">
            <div role="tabpanel" class="tab-pane active" id="general">
                <ul class="nav nav-tabs" role="tablist">
                    <li class="active" role="presentation"><a href="#russian" aria-controls="russian" role="tab" data-toggle="tab">Русский</a></li>
                    <li role="presentation"><a href="#japanese" aria-controls="japanese" role="tab" data-toggle="tab">Japanese</a></li>
                </ul>
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="russian">

                        <input type="hidden" name="post_description[0][language_id]" value="1" />
                        <input type="hidden" name="post_description[0][id]" value="{$post->description[0]->id}" />

                        <div class="form-group required">
                            <label class="col-sm-3 control-label">Название</label>
                            <div class="col-sm-6">
                                <input class="form-control" name="post_description[0][name]" type="text" placeholder="Название" value="{$post->description[0]->name|escape}"/>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-12 col-md-3 control-label">Краткий текст</label>
                            <div class="col-sm-12 col-md-9">
                                <textarea name="post_description[0][annotation]" rows="5" class="form-control">{$post->description[0]->annotation|escape}</textarea>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-12 col-md-3 control-label">Полный текст</label>
                            <div class="col-sm-12 col-md-9">
                                <textarea name="post_description[0][text]"  class="editor_large">{$post->description[0]->text|escape}</textarea>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label">Мета-тег Title</label>
                            <div class="col-sm-6">
                                <input name="post_description[0][meta_title]" class="form-control" type="text" value="{$post->description[0]->meta_title|escape}" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label">Мета-тег Keywords</label>
                            <div class="col-sm-6">
                                <input name="post_description[0][meta_keywords]" class="form-control" type="text" value="{$post->description[0]->meta_keywords|escape}" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label">Мета-тег Description</label>
                            <div class="col-sm-6">
                                <textarea name="post_description[0][meta_description]" class="form-control" type="text">{$post->description[0]->meta_description|escape}</textarea>
                            </div>
                        </div>

                    </div>
                    <div role="tabpanel" class="tab-pane" id="japanese">

                        <input type="hidden" name="post_description[1][language_id]" value="2" />
                        <input type="hidden" name="post_description[1][id]" value="{$post->description[1]->id}" />

                        <div class="form-group required">
                            <label class="col-sm-3 control-label">Name</label>
                            <div class="col-sm-6">
                                <input class="form-control" name="post_description[1][name]" type="text" placeholder="Name" value="{$post->description[1]->name|escape}"/>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-12 col-md-3 control-label">Page Annotation</label>
                            <div class="col-sm-12 col-md-9">
                                <textarea name="post_description[1][annotation]" rows="5" class="form-control">{$post->description[1]->annotation|escape}</textarea>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-12 col-md-3 control-label">Page Text</label>
                            <div class="col-sm-12 col-md-9">
                                <textarea name="post_description[1][text]"  class="editor_large">{$post->description[1]->text|escape}</textarea>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label">Meta Title</label>
                            <div class="col-sm-6">
                                <input name="post_description[1][meta_title]" class="form-control" type="text" value="{$post->description[1]->meta_title|escape}" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label">Meta Keywords</label>
                            <div class="col-sm-6">
                                <input name="post_description[1][meta_keywords]" class="form-control" type="text" value="{$post->description[1]->meta_keywords|escape}" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label">Meta Description</label>
                            <div class="col-sm-6">
                                <textarea name="post_description[1][meta_description]" class="form-control" type="text">{$post->description[1]->meta_description|escape}</textarea>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <div role="tabpanel" class="tab-pane page__data-tab" id="data">

                <div class="form-group">
                    <label class="col-sm-3 control-label">Отображение</label>
                    <div class="col-sm-3">
                        <div class="checkbox">
                            <label><input name="visible" value="1" type="checkbox" {if $post->visible}checked{/if}> Активна</label>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-3 control-label">Дата</label>
                    <div class="col-sm-6">
                        <input type="text" name="date" value="{$post->date|date}" class="form-control">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-3 control-label">Адрес</label>
                    <div class="col-sm-6">
                        <div class="input-group">
                            <span class="input-group-addon">http://{$smarty.server.SERVER_NAME}/news/</span>
                            <input name="url" class="page_url form-control" type="text" value="{$post->url|escape}" />
                        </div>
                    </div>
                </div>

                <!-- Изображение страницы -->
                <div class="form-group">
                    <label class="col-sm-3 control-label">Добавить изображение</label>
                    <div class="col-sm-6">
                        {if $post->image}
                            <img src="/files/news/small/{$post->image}" alt="" />
                        {else}
                            <input class="upload_image" name="image" type="file">
                        {/if}
                    </div>
                </div>
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-12 text-right">
                <button class="btn btn-success" type="submit">Сохранить</button>
            </div>
        </div>

    </form>
    <!-- Основная форма (The End) -->
</div>
