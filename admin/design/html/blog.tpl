{* Вкладки *}
{capture name=tabs}
    <li class="active"><a href="{url module=BlogAdmin page=null keyword=null}">Новости</a></li>
{/capture}

{* Title *}
{$meta_title='Новости' scope=parent}

<div id="main_list" class="list-container">

    {* Заголовок *}
    <div class="row">
        <div class="col-sm-6">
            <h2 class="list-title">
                {if $keyword && $posts_count}
                    {$posts_count|plural:'Нашлась':'Нашлись':'Нашлись'} {$posts_count} {$posts_count|plural:'запись':'записей':'записи'}
                {elseif $posts_count}
                    {$posts_count} {$posts_count|plural:'запись':'записей':'записи'} в новостях
                {else}
                    Нет записей
                {/if}
            </h2>
        </div>
        <div class="col-sm-6 text-right">
            <a class="btn btn-default" href="{url path='news/add'}"><i class="fa fa-plus text-success"></i> Добавить запись</a>
        </div>
    </div>
    {if $posts}
        <!-- Листалка страниц -->
        {include file='pagination.tpl'}
        <!-- Листалка страниц (The End) -->

        <form id="form_list" method="post" class="form-horizontal list__form">
            <input type="hidden" name="session_id" value="{$smarty.session.id}">

            <div id="list" class="form-group form-group__mod list">
                {foreach $posts as $post}
                    <div class="{if !$post->visible}invisible {/if}col-sm-12 list__item">
                        <input type="hidden" name="positions[{$post->id}]" value="{$post->position}">
                        <div class="checkbox cell">
                            <input type="checkbox" name="check[]" value="{$post->id}" />
                        </div>
                        <div class="name cell">
                            <a href="{url path='news/'|cat:$post->id|cat:'/edit'}">{$post->name|escape}</a>
                            <br>
                            {$post->date|date}
                        </div>
                        <div class="icons cell">
                            <a class="preview" title="Предпросмотр в новом окне" href="../blog/{$post->url}" target="_blank"></a>
                            <a class="enable" title="Активна" href="#"></a>
                            <a class="delete" title="Удалить" href="#"></a>
                        </div>
                    </div>
                {/foreach}
            </div>


            <div id="action" class="form-group">
                <div class="col-sm-2">
                    <label id="check_all" class="dash_link control-label">Выбрать все</label>
                </div>

                <div id="select" class="col-sm-6">
                    <select name="action" class="form-control">
                        <option value="enable">Сделать видимыми</option>
                        <option value="disable">Сделать невидимыми</option>
                        <option value="delete">Удалить</option>
                    </select>
                </div>
                <div class="col-sm-4 text-right">
                    <button id="apply_action" class="btn btn-success" type="submit">Применить</button>
                </div>

            </div>

        </form>

        <!-- Листалка страниц -->
        {include file='pagination.tpl'}
        <!-- Листалка страниц (The End) -->
    {/if}
</div>