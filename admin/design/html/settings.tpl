{capture name=tabs}
		<li class="active"><a href="index.php?module=SettingsAdmin">Настройки</a></li>
{/capture}
 
{$meta_title = "Настройки" scope=parent}

{if $message_success}
<!-- Системное сообщение -->
<div class="message message_success">
	<span>{if $message_success == 'saved'}Настройки сохранены{/if}</span>
	{if $smarty.get.return}
	<a class="button" href="{$smarty.get.return}">Вернуться</a>
	{/if}
</div>
<!-- Системное сообщение (The End)-->
{/if}

{if $message_error}
<!-- Системное сообщение -->
<div class="message message_error">
	<span>{if $message_error == 'watermark_is_not_writable'}Установите права на запись для файла {$config->watermark_file}{/if}</span>
	<a class="button" href="">Вернуться</a>
</div>
<!-- Системное сообщение (The End)-->
{/if}
			

<!-- Основная форма -->
<form method=post id=product enctype="multipart/form-data">
<input type=hidden name="session_id" class="class="form-control"" value="{$smarty.session.id}">
			
		<!-- Параметры -->
		<div class="block">
			<div style="display: inline-block; vertical-align: top;">
			<h2>Настройки сайта</h2>
			<ul>
				<li><label class=property>Имя сайта</label><input name="site_name" class="form-control" type="text" value="{$settings->site_name|escape}" /></li>
				{*<li><label class=property>Заголовок письма на главной</label><input name="home_letter_title" class="form-control" type="text" value="{$settings->home_letter_title|escape}" /></li>
				<li><label class=property>Заголовок письма "Регистрация автора"</label><input name="letter_author" class="form-control" type="text" value="{$settings->letter_author|escape}" /></li>
				<li><label class=property>Заголовок письма "Обратный звонок"</label><input name="letter_callback" class="form-control" type="text" value="{$settings->letter_callback|escape}" /></li>
				<li><label class=property>Заголовок письма "Написать нам"</label><input name="letter_writeus" class="form-control" type="text" value="{$settings->letter_writeus|escape}" /></li>
				<li><label class=property>Заголовок письма полного заказа</label><input name="letter_full_order" class="form-control" type="text" value="{$settings->letter_full_order|escape}" /></li>
				<li><label class=property>Заголовок письма "Отправить на оценку"</label><input name="letter_send_mark" class="form-control" type="text" value="{$settings->letter_send_mark|escape}" /></li>
				<li><label class=property>Заголовок письма "Заказать диплом"</label><input name="letter_diplom" class="form-control" type="text" value="{$settings->letter_diplom|escape}" /></li>
				<li><label class=property>Заголовок письма "Заказать диссертацию"</label><input name="letter_disser" class="form-control" type="text" value="{$settings->letter_disser|escape}" /></li>
				<li><label class=property>Заголовок письма "VIP клиент"</label><input name="letter_vip" class="form-control" type="text" value="{$settings->letter_vip|escape}" /></li>*}
				<li><label class=property>Email, куда будут приходить письма</label><input name="letter_email" class="form-control" type="text" value="{$settings->letter_email|escape}" /></li>
			</ul>
			</div>

			{*<div style="display: inline-block; vertical-align: top; width: 420px;">
				<h2>Список ВУЗов</h2>
				<p>Внимание! Каждый ВУЗ с новой строчки.</p>
				<textarea name="vuz_list" class="form-control" rows="10">{$settings->vuz_list}</textarea>
			</div>

			<div style="display: inline-block; vertical-align: top; width: 420px;">
				<h2>Краткое описание</h2>
				<p>Типы работ. Каждый пункт с новой строчки</p>
				<textarea name="work_type_list" class="form-control" rows="10">{$settings->work_type_list}</textarea>
				<p>Сопроводительные документы(диплом). Каждый пункт с новой строчки</p>
				<textarea name="supporting_diplom_list" class="form-control" rows="10">{$settings->supporting_diplom_list}</textarea>
				<p>Сопроводительные документы(отчет). Каждый пункт с новой строчки</p>
				<textarea name="supporting_report_list" class="form-control" rows="10">{$settings->supporting_report_list}</textarea>
			</div>
		</div>
		<!-- Параметры (The End)-->*}
		
		<!-- Параметры -->
		{*<div class="block layer">
			<h2><a class="dash_link"id="change_password">Изменить пароль администратора</a></h2>
			<ul id="change_password_form">
				<li><label class=property>Новый логин</label><input name="new_login" class="form-control" type="text" value="{$current_login|escape}" /></li>
				<li><label class=property>Новый пароль</label><input name="new_password" class="form-control" type="text" value="" /></li>
			</ul>
		</div>*}
		<!-- Параметры (The End)-->
		
		<input class="button_green button_save" type="submit" name="save" value="Сохранить" />
			
	<!-- Левая колонка свойств товара (The End)--> 
	
</form>
<!-- Основная форма (The End) -->

{literal}
<script>
$(function() {
	$('#change_password_form').hide();
	$('#change_password').click(function() {
		$('#change_password_form').show();
	});
});
</script>
{/literal}
