{* Title *}
{$meta_title = {$menu->name} scope=parent}

{* Заголовок *}
<div id="header">
	<h1>{$menu->name}</h1>
	<a class="add" href="{url module=PageAdmin}">Добавить страницу</a>
</div>
{if $parents}
<div id="main_list">
 
	<form id="list_form" method="post">
		<input type="hidden" name="session_id" value="{$smarty.session.id}">
		{function name=parent_select level=0}
		<div id="list" class="sortable">

			{foreach $pars as $par}
			{if $menu->id == $par->menu_id}
			<div class="{if !$par->visible}invisible{/if} row row_mod">
				<div class="tree_row">
					<input type="hidden" name="positions[{$par->id}]" value="{$par->position}">
					<div class="move cell" style="margin-left:{$level*20}px"><div class="move_zone"></div></div>
			 		<div class="checkbox cell">
						<input type="checkbox" name="check[]" value="{$par->id}" />
					</div>
					<div class="name cell">
						<a href="{url path='pages/'|cat:$par->id|cat:'/edit'}">{$par->header|escape}</a>
					</div>
					<div class="icons cell">
						<a class="preview" title="Предпросмотр в новом окне" href="../{$par->url}" target="_blank"></a>
						<a class="enable" title="Активна" href="#"></a>
						<a class="delete" title="Удалить" href="#"></a>
					</div>
					<div class="clear"></div>
				</div>
				{parent_select pars=$par->subcategories level=$level+1}
			</div>
			{/if}
			{/foreach}
		</div>
		{/function}
		{parent_select pars=$parents}
	
		<div id="action">
		<label id="check_all" class="dash_link">Выбрать все</label>
	
		<span id="select">
		<select name="action">
			<option value="enable">Сделать видимыми</option>
			<option value="disable">Сделать невидимыми</option>
			<option value="delete">Удалить</option>
		</select>
		</span>
	
		<input id="apply_action" class="button_green" type="submit" value="Применить">
	
		</div>
	</form>	
</div>
{else}
	Нет страниц
{/if}
