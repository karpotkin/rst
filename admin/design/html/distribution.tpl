{capture name=tabs}
		<li class="active"><a href="index.php?module=DistributionsAdmin">Рассылки</a></li>
		<li><a href="index.php?module=SendUsersAdmin">Подписчики</a></li>
{/capture}

{if $feature->id}
{$meta_title = $feature->name scope=parent}
{else}
{$meta_title = 'Новое свойство' scope=parent}
{/if}

{* Подключаем Tiny MCE *}
{include file='tinymce_init_small.tpl'}

{* On document load *}
{literal}
<script>

$(function() {
	// Добавление 
</script>
{/literal}

{if $message_success}
<!-- Системное сообщение -->
<div class="message message_success">
	<span>{if $message_success=='added'}Свойство добавлено{elseif $message_success=='updated'}Свойство обновлено{else}{$message_success}{/if}</span>
	{if $smarty.get.return}
	<a class="button" href="{$smarty.get.return}">Вернуться</a>
	{/if}
</div>
<!-- Системное сообщение (The End)-->
{/if}

{if $message_error}
<!-- Системное сообщение -->
<div class="message message_error">
	<span>{$message_error}</span>
	<a class="button" href="">Вернуться</a>
</div>
<!-- Системное сообщение (The End)-->
{/if}

<!-- Основная форма -->
<form method=post id="product">

	<div id="name">			
		<h2>Тема</h2>
		<input class="name" name=subject type="text" value="{$distribution->subject|escape}"/> 
		<input name=id type="hidden" value="{$distribution->id|escape}"/> 			
		
		<h2>Сообщение:</h2>
<textarea name="body" style="width:450px; height: 200px;">
{$distribution->body}
</textarea> 
	</div> 
       
		<input type=hidden name='session_id' value='{$smarty.session.id}'>
		<input class="button_green" type="submit" name="" value="Сохранить" />
		<a href="{url module=DistributeAdmin id=$distribution->id return=$smarty.server.REQUEST_URI}">
			<input class="button_green" type="button" name="" value="Перейти к отправке" />
		</a>
		
	</div>
	<!-- Правая колонка свойств товара (The End)--> 
	

</form>
<!-- Основная форма (The End) -->

