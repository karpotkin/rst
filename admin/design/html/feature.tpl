{capture name=tabs}
		<li><a href="index.php?module=ProductsAdmin">Товары</a></li>
		<li><a href="index.php?module=CategoriesAdmin">Категории</a></li>
		<li><a href="index.php?module=BrandsAdmin">Бренды</a></li>
		<li class="active"><a href="index.php?module=FeaturesAdmin">Свойства</a></li>
{/capture}

{if $feature->id}
{$meta_title = $feature->name scope=parent}
{else}
{$meta_title = 'Новое свойство' scope=parent}
{/if}

{* Подключаем Tiny MCE *}
{include file='tinymce_init_small.tpl'}

{* On document load *}
{literal}
<script>

$(function() {
	// Добавление 
    

function generate_meta_description(name)
{
	if(typeof(tinyMCE.get(name)) =='object')
	{
		description = tinyMCE.get(name).getContent().substr(0, 512);
		return description;
	}
	else
		return $('textarea[name=body]').val();
}
	

function refresh_tinymce(){
	    tinyMCE.init({
     mode : "specific_textareas",
	editor_selector : /editor/,
	theme : "advanced",
	language : "ru",
	theme_advanced_path : false,
	apply_source_formatting : false,
	plugins : "jaretypograph,smimage,smeditimage,smexplorer,safari,spellchecker,style,table,save,advimage,advlink,autolink,inlinepopups,media,contextmenu,paste,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras",
	relative_urls : false,
	remove_script_host : true,
	convert_urls : true,
	verify_html: false,
    remove_linebreaks : false,
    width : "420", 
    height : "150",
    content_css :"../design/{$settings->theme}/css/style.css",
    spellchecker_languages : "+Russian=ru,+English=en",
            
	// Theme options
	theme_advanced_buttons1 : "bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,smimage,|,code",
	theme_advanced_buttons2 : "",
	theme_advanced_buttons3 : "",
	theme_advanced_buttons4 : "",
	theme_advanced_toolbar_location : "top",
	theme_advanced_toolbar_align : "left",
	theme_advanced_statusbar_location : "bottom",
	theme_advanced_resizing : true,

	file_browser_callback : "SMPlugins",
	plugin_smexplorer_directory : "{$config->subfolder}files/uploads",
	plugin_smimage_directory : "{$config->subfolder}files/uploads",
	
	setup : function(ed) {
		if(typeof set_meta == 'function')
		{
			ed.onKeyUp.add(set_meta);
			ed.onChange.add(set_meta);
		}
	}
    });
}

	var variant = $('#new_variant').clone(true);
	$('#new_variant').remove();
	$('#add_new_variant').click(function() {	
			$(variant).clone(true).appendTo('.prop_ul').attr('class', 'variant_'+(variants_count+new_variants)).fadeIn('slow');
			$('textarea[name=body]').attr('name', 'body_'+(variants_count+new_variants))
			new_variants++;
			refresh_tinymce();
		return false;		
	});

	$('.del_variant').click(function(){
        var id = $(this).parent().attr('variant_id');
        $(this).parent().remove();
    });

    $('.qasw').click(function(){

    	clear_variants(f_id);
    	for (var i = 0; i < new_variants+variants_count; i++) {
    		var decr = generate_meta_description("body_"+i);
    		var temp = Array('0', $('.variant_'+i+' > input').val(), decr, 0);
    		variants[i] = temp;
    	};
    	for(var i in variants){
    		if(variants[i][1] != null && variants[i][2] != null){
    		update_variants(f_id, variants, i);
    	}
    	}
    });
});

function update_variants(f_id, variants, i){
	$.ajax({
  		type: "POST",
  		url: "ajax/update_features.php",
  		data: "clear=0&feature_id="+f_id+"&variant_title="+variants[i][1]+"&variant_description="+variants[i][2]
		});

    	alert(variants[i]);
}

function clear_variants(f_id){
	$.ajax({
  		type: "POST",
  		url: "ajax/update_features.php",
  		data: "clear=1&feature_id="+f_id+"&variant_title=''&variant_description=''"
		});
}
</script>
{/literal}

{if $message_success}
<!-- Системное сообщение -->
<div class="message message_success">
	<span>{if $message_success=='added'}Свойство добавлено{elseif $message_success=='updated'}Свойство обновлено{else}{$message_success}{/if}</span>
	{if $smarty.get.return}
	<a class="button" href="{$smarty.get.return}">Вернуться</a>
	{/if}
</div>
<!-- Системное сообщение (The End)-->
{/if}

{if $message_error}
<!-- Системное сообщение -->
<div class="message message_error">
	<span>{$message_error}</span>
	<a class="button" href="">Вернуться</a>
</div>
<!-- Системное сообщение (The End)-->
{/if}

<!-- Основная форма -->
<form method=post id=product onsubmit="$('.qasw').click();">

	<div id="name">
		<input class="name" name=name type="text" value="{$feature->name|escape}"/> 
		<input name=id type="hidden" value="{$feature->id|escape}"/> 
	</div> 

	<!-- Левая колонка свойств товара -->
	<div id="column_left">
			
		<!-- Категории -->	
		<div class="block">
			<h2>Использовать в категориях</h2>
					<select class=multiple_categories multiple name="feature_categories[]">
						{function name=category_select selected_id=$product_category level=0}
						{foreach from=$categories item=category}
								<option value='{$category->id}' {if in_array($category->id, $feature_categories)}selected{/if} category_name='{$category->single_name}'>{section name=sp loop=$level}&nbsp;&nbsp;&nbsp;&nbsp;{/section}{$category->name}</option>
								{category_select categories=$category->subcategories selected_id=$selected_id  level=$level+1}
						{/foreach}
						{/function}
						{category_select categories=$categories}
					</select>
		</div>
 
	</div>
	<!-- Левая колонка свойств товара (The End)--> 
	
	<!-- Правая колонка свойств товара -->	
	<div id="column_right">
		
		<!-- Параметры страницы -->
		<div class="block">
			<h2>Настройки свойства</h2>
			<ul>
				<li><input type=checkbox name=in_filter id=in_filter {if $feature->in_filter}checked{/if} value="1"> <label for=in_filter>Использовать в фильтре</label></li>
			</ul>
		</div>
		<!-- Параметры страницы (The End)-->

        <!-- Варианты свойства -->
		{$variants|print_r}
		<div class="block layer">
			<h2>Варианты свойства</h2>
			<ul class="prop_ul">
<script>
	var variants = new Array();
	var f_id = {$feature->id};
	var variants_count = {$variants|count}
	var new_variants = 0;
	{foreach from=$variants key=i item=vr}
		variants[{$i}]  = Array({foreach from=$vr item=v}'{$v}', {/foreach}0);
	{/foreach}
</script>
				{foreach from=$variants key=i item=variant}
					<li variant_id={$variant->id} class=variant_{$i}>
						<input type="text" value="{$variant->title}" />

						<textarea name="body_{$i}"  class="editor_large">{$page->body|escape}{$variant->description}</textarea>
						<a class="del_variant" href="#">
							<img src="design/images/cross-circle-frame.png" alt="" />
						</a>
					</li>
				{/foreach}
					<li id=new_variant>
						<input type="text" />

						<textarea name="body"  class="editor_large">{$page->body|escape}</textarea>
					</li>
			</ul>
			<!-- Новый вариант -->
			<span class="add"><i class="dash_link" id="add_new_variant">Добавить вариант</i></span>	
		</div>
		
		<!-- Варианты свойства (The End)-->
        
		<input type=hidden name='session_id' value='{$smarty.session.id}'>
		<input class="button_green" type="submit" name="" value="Сохранить" />
		
	</div>
	<!-- Правая колонка свойств товара (The End)--> 
	

</form>

		<input type="button" name="" value="блобло" class="qasw"/>
<!-- Основная форма (The End) -->

