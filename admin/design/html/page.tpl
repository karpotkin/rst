{capture name=tabs}
	{foreach from=$menus item=m}
		<li {if $m->id == $menu->id}class="active"{/if}><a href='index.php?module=PagesAdmin&menu_id={$m->id}'>{$m->name}</a></li>
	{/foreach}
{/capture}

{if $page->id}
	{$meta_title = $page->name scope=parent}
{else}
	{$meta_title = 'Новая страница' scope=parent}
{/if}

<div class="page">
	{if $message_success}
		<!-- Системное сообщение -->
		<div class="message message_success">
			<span>{if $message_success == 'added'}Страница добавлена{elseif $message_success == 'updated'}Страница обновлена{/if}</span>
			<a class="link" target="_blank" href="{url path=$page->url}">Открыть страницу на сайте</a>
			{if $smarty.get.return}
				<a class="button" href="{$smarty.get.return}">Вернуться</a>
			{/if}
		</div>
		<!-- Системное сообщение (The End)-->
	{/if}

	{if $message_error}
		<!-- Системное сообщение -->
		<div class="message message_error">
			<span>{if $message_error == 'url_exists'}Страница с таким адресом уже существует{/if}</span>
			<a class="button" href="">Вернуться</a>
		</div>
		<!-- Системное сообщение (The End)-->
	{/if}

	<ul class="nav nav-tabs" role="tablist">
		<li class="active" role="presentation"><a href="#general" aria-controls="general" role="tab" data-toggle="tab">Основное</a></li>
		<li role="presentation"><a href="#data" aria-controls="data" role="tab" data-toggle="tab">Данные</a></li>
	</ul>

	<!-- Tab panes -->
	<!-- Основная форма -->
	<form method="post" class="form-horizontal" enctype="multipart/form-data">
		<input type="hidden" name="session_id" value="{$smarty.session.id}">
		<input type="hidden" name="menu_id" value="{$page->menu_id}"/>
		<input name="id" type="hidden" value="{$page->id|escape}"/>
		<div class="tab-content">
			<div role="tabpanel" class="tab-pane active" id="general">
				<ul class="nav nav-tabs" role="tablist">
					<li class="active" role="presentation"><a href="#russian" aria-controls="russian" role="tab" data-toggle="tab">Русский</a></li>
					<li role="presentation"><a href="#japanese" aria-controls="japanese" role="tab" data-toggle="tab">Japanese</a></li>
				</ul>
				<div class="tab-content">
					<div role="tabpanel" class="tab-pane active" id="russian">

						<input type="hidden" name="page_description[0][language_id]" value="1" />
						<input type="hidden" name="page_description[0][id]" value="{$page->description[0]->id}" />

						<div class="form-group required">
							<label class="col-sm-3 control-label">Название</label>
							<div class="col-sm-6">
								<input class="form-control" name="page_description[0][header]" type="text" placeholder="Название" value="{$page->description[0]->header|escape}"/>
							</div>
						</div>

						<div class="form-group">
							<label class="col-sm-3 control-label">Название пункта в меню</label>
							<div class="col-sm-6">
								<input name="page_description[0][name]" class="form-control" type="text" placeholder="Заполняется автоматически" value="{$page->description[0]->name|escape}" />
							</div>
						</div>

						<div class="form-group">
							<label class="col-sm-12 col-md-3 control-label">Текст на странице</label>
							<div class="col-sm-12 col-md-9">
								<textarea name="page_description[0][page_text]"  class="editor_large">{$page->description[0]->page_text|escape}</textarea>
							</div>
						</div>

						<div class="form-group">
							<label class="col-sm-3 control-label">Мета-тег Title</label>
							<div class="col-sm-6">
								<input name="page_description[0][meta_title]" class="form-control" type="text" value="{$page->description[0]->meta_title|escape}" />
							</div>
						</div>

						<div class="form-group">
							<label class="col-sm-3 control-label">Мета-тег Keywords</label>
							<div class="col-sm-6">
								<input name="page_description[0][meta_keywords]" class="form-control" type="text" value="{$page->description[0]->meta_keywords|escape}" />
							</div>
						</div>

						<div class="form-group">
							<label class="col-sm-3 control-label">Мета-тег Description</label>
							<div class="col-sm-6">
								<textarea name="page_description[0][meta_description]" class="form-control" type="text">{$page->description[0]->meta_description|escape}</textarea>
							</div>
						</div>

					</div>
					<div role="tabpanel" class="tab-pane" id="japanese">

						<input type="hidden" name="page_description[1][language_id]" value="2" />
						<input type="hidden" name="page_description[1][id]" value="{$page->description[1]->id}" />

						<div class="form-group required">
							<label class="col-sm-3 control-label">Name</label>
							<div class="col-sm-6">
								<input class="form-control" name="page_description[1][header]" type="text" placeholder="Name" value="{$page->description[1]->header|escape}"/>
							</div>
						</div>

						<div class="form-group">
							<label class="col-sm-3 control-label">Menu Name</label>
							<div class="col-sm-6">
								<input name="page_description[1][name]" class="form-control" type="text" value="{$page->description[1]->name|escape}" />
							</div>
						</div>

						<div class="form-group">
							<label class="col-sm-12 col-md-3 control-label">Page Content</label>
							<div class="col-sm-12 col-md-9">
								<textarea name="page_description[1][page_text]"  class="editor_large">{$page->description[1]->page_text|escape}</textarea>
							</div>
						</div>

						<div class="form-group">
							<label class="col-sm-3 control-label">Meta Title</label>
							<div class="col-sm-6">
								<input name="page_description[1][meta_title]" class="form-control" type="text" value="{$page->description[1]->meta_title|escape}" />
							</div>
						</div>

						<div class="form-group">
							<label class="col-sm-3 control-label">Meta Keywords</label>
							<div class="col-sm-6">
								<input name="page_description[1][meta_keywords]" class="form-control" type="text" value="{$page->description[1]->meta_keywords|escape}" />
							</div>
						</div>

						<div class="form-group">
							<label class="col-sm-3 control-label">Meta Description</label>
							<div class="col-sm-6">
								<textarea name="page_description[1][meta_description]" class="form-control" type="text">{$page->description[1]->meta_description|escape}</textarea>
							</div>
						</div>

					</div>
				</div>
			</div>
			<div role="tabpanel" class="tab-pane page__data-tab" id="data">
				<div class="form-group">
					<label class="col-sm-3 control-label">Отображение</label>
					<div class="col-sm-3">
						<div class="checkbox">
							<label><input name="visible" value="1" type="checkbox" {if $page->visible}checked{/if}> Активна</label>
						</div>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-3 control-label">Родительская категория</label>
					<div class="col-sm-6">
						<select name="parent_id" class="form-control">
							<option value='0'>Корневая категория</option>
							{function name=parent_select level=0}
								{foreach from=$pars item=par}
									{if $page->id != $par->id && $par->name != '' && $page->menu_id == $par->menu_id}
										<option value='{$par->id}' {if $page->parent_id == $par->id}selected{/if}>{section name=sp loop=$level}&nbsp;&nbsp;&nbsp;&nbsp;{/section}{$par->name}</option>
										{parent_select pars=$par->subcategories level=$level+1}
									{/if}
								{/foreach}
							{/function}
							{parent_select pars=$parents}
						</select>
					</div>
				</div>

				<div class="form-group">
					<label class="col-sm-3 control-label">Адрес</label>
					<div class="col-sm-6">
						<div class="input-group">
							<span class="input-group-addon">http://{$smarty.server.SERVER_NAME}/</span>
							<input name="url" class="page_url form-control" type="text" value="{$page->url|escape}" />
						</div>
					</div>
				</div>

				<!-- Изображение страницы -->
				<div class="form-group">
					<label class="col-sm-3 control-label">Добавить изображение</label>
					<div class="col-sm-6">
						{if $page->image}
							<img src="/files/pages/small/{$page->image}" alt="" />
						{else}
							<input class="upload_image" name="image" type="file">
						{/if}
					</div>
				</div>
			</div>
		</div>

		<div class="form-group">
			<div class="col-sm-12 text-right">
				<button class="btn btn-success" type="submit">Сохранить</button>
			</div>
		</div>

	</form>
	<!-- Основная форма (The End) -->
</div>

