<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>{$meta_title}</title>
    <link rel="icon" href="/admin/design/images/favicon.ico" type="image/x-icon">
    <link rel="stylesheet" type="text/css" href="/admin/design/css/bootstrap.min.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="/admin/design/css/font-awesome.min.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="/admin/design/js/jquery/jquery-ui.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="/admin/design/css/style.css" />
    <meta name="viewport" content="width=1024">

</head>
<body>

<!-- Вся страница -->
<div id="main">

    <!-- Главное меню -->
    <ul class="nav nav-pills clearfix" style="margin: 20px 0; ">
        <li><a href="{url path='pages'}">Страницы</a></li>
        <li><a href="{url path='news'}">Новости</a></li>
        <li><a href="{url path='restaurant-menu'}">Меню</a></li>
        {*
        <li><a href="index.php?module=ProductsAdmin">Товары</a></li>
        <li><a href="index.php?module=SettingsAdmin">Настройки</a></li>
        *}
    </ul>
    <!-- Главное меню (The End)-->


    <!-- Таб меню -->
    <ul id="tab_menu" class="nav nav-tabs">
        {$smarty.capture.tabs}
    </ul>
    <!-- Таб меню (The End)-->

    <!-- Основная часть страницы -->
    <div id="middle">
        {$content}
    </div>
    <!-- Основная часть страницы (The End) -->

    <!-- Подвал сайта -->
    <div id="footer">
        &copy; {$smarty.now|date_format:'%Y'}
    </div>
    <!-- Подвал сайта (The End)-->

</div>
<!-- Вся страница (The End)-->
<script src="/admin/design/js/jquery-1.12.1.min.js"></script>
<script src="/admin/design/js/jquery/jquery.form.js"></script>
<script src="/admin/design/js/jquery/jquery-ui.js"></script>
<script src="/admin/design/js/bootstrap.min.js"></script>
<script src="/admin/design/js/tiny_mce/plugins/smimage/smplugins.js"></script>
<script src="/admin/design/js/tiny_mce/tiny_mce.js"></script>
<script src="/admin/design/js/main.js"></script>
</body>
</html>
