{$wrapper = '' scope=parent}
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>{$meta_title}</title>
    <link rel="icon" href="design/images/favicon.ico" type="image/x-icon">
    <link rel="stylesheet" type="text/css" href="design/css/bootstrap.min.css" media="screen" />
    <meta name="viewport" content="width=1024">

</head>
<body>
<style>
    html, body{
        height: 100%;
        min-height: 100%;
    }
</style>
<div class="container" style="top: 20%; position: relative;">
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-lg-4" style="margin: 0 auto; float: none;">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title">Авторизация</h3>
                </div>
                <form role="form" method="post">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12 login-box">
                                    <span class="text-danger">{$error}</span>
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="glyphicon glyphicon-user"></span></span>
                                        <input type="text" name="login" value="{$login}" class="form-control" placeholder="Логин" required autofocus />
                                    </div>
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="glyphicon glyphicon-lock"></span></span>
                                        <input type="password" value="{$password}" name="password" class="form-control" placeholder="Ваш пароль" required />
                                    </div>

                            </div>
                        </div>
                    </div>
                    <div class="panel-footer">
                        <div class="row">
                            <div class="col-xs-6 col-sm-6 col-md-6">
                                <button type="submit" class="btn btn-labeled btn-success">
                                    <span class="btn-label"><i class="glyphicon glyphicon-ok"></i></span>
                                    Войти
                                </button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
</body>
</html>