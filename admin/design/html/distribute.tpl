{capture name=tabs}
		<li class="active"><a href="index.php?module=DistributionsAdmin">Рассылки</a></li>
		<li><a href="index.php?module=SendUsersAdmin">Подписчики</a></li>
{/capture}

{if $feature->id}
{$meta_title = $feature->name scope=parent}
{else}
{$meta_title = 'Новое свойство' scope=parent}
{/if}

{* Подключаем Tiny MCE *}
{include file='tinymce_init_small.tpl'}

{* On document load *}
{literal}
<script type="text/javascript">

function getCaretPos(objName) {
  var el = document.getElementById(objName);
  if (el.selectionStart) { 
    return el.selectionStart; 
  }
  return 0;
}

//setCaretPosition( document.getElementById("add_name"), 3 );

/*function setCaretPosition(ctrl, pos) {
    if(ctrl.setSelectionRange) {
        ctrl.focus();
        ctrl.setSelectionRange(pos,pos);
        //alert(ctrl.setSelectionRange(pos,pos));
    }
    else if (ctrl.createTextRange) {
        var range = ctrl.createTextRange();
        range.collapse(true);
        range.moveEnd('character', pos);
        range.moveStart('character', pos);
        //alert(range.moveStart('character', pos));
        range.select();
    }
}*/




$('document').ready(function(){
		$('input#select_all').click(function(){
			$('select option').attr('selected', 'selected');
		});


		$('input#add_name').click(function(){
			var cursor_pos = getCaretPos('body');
			//alert(cursor_pos);
			var text = $('#body').val();
			var temp1 = text.substring(0, cursor_pos);
			var temp2 = text.substring(cursor_pos);
			text = temp1+" {name} "+temp2;
			$('#body').val(text);
		});
	});
</script>
{/literal}

{if $message_success}
<!-- Системное сообщение -->
<div class="message message_success">
	<span>{if $message_success=='added'}Свойство добавлено{elseif $message_success=='updated'}Свойство обновлено{else}{$message_success}{/if}</span>
	{if $smarty.get.return}
	<a class="button" href="{$smarty.get.return}">Вернуться</a>
	{/if}
</div>
<!-- Системное сообщение (The End)-->
{/if}

{if $message_error}
<!-- Системное сообщение -->
<div class="message message_error">
	<span>{$message_error}</span>
	<a class="button" href="">Вернуться</a>
</div>
<!-- Системное сообщение (The End)-->
{/if}

<!-- Основная форма -->
{$distribution|var_dump}
<form method="post" id="product">
{$us|var_dump}
	<div id="name">			
		<div style="width:50%; float:left">
			<h2>Подписчики:</h2>
			<select class=multiple_categories multiple name="selected_users[]" style="width:450px; height:300px;">
				{foreach $users as $user}
					<option value='{$user->id}' {if in_array($user->id, $distribution->users)}selected{/if}>{$user->fio}[{$user->email}]</option>
				{/foreach}
			</select>
		</div>
		<div style="width:50%; float:left">
			<h2>Тема:</h2>
			<input class="name" type="text" name=subject style="font-size:26px; width: 450px;" value="{$distribution->subject|escape}" />
			<br/>
			<br/>
			<h2>Сообщение:</h2>
			<input type="button" class="button_green" id="add_name" value="Вставить имя">
			<textarea name="body" id="body" style="width:450px; height: 200px;">{$distribution->body}</textarea><br>
		</div>
		<input name=id type="hidden" value="{$distribution->id|escape}"/> 	
		<input type=hidden name='session_id' value='{$smarty.session.id}'>
		<input class="button_green" type="button" id="select_all" value="Выбрать всех" />
		<input class="button_green" type="submit" name="" value="Отправить" />
	</div>
	

</form>
<!-- Основная форма (The End) -->
