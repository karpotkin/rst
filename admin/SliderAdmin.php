<?PHP

require_once('api/Simpla.php');

############################################
# Class Object - edit the static section
############################################
class SliderAdmin extends Simpla
{
	public function fetch()
	{
		$images = array();
		$related_products = array();
	
		if($this->request->method('post') && !empty($_POST))
		{
			$object->id = $this->request->post('id', 'integer');
			$object->name = $this->request->post('name');
			$object->logo = $this->request->post('logo');
			
			$object->visible = $this->request->post('visible', 'boolean');
			$object->meta_title = $this->request->post('meta_title');
			$object->meta_keywords = $this->request->post('meta_keywords');
			$object->meta_description = $this->request->post('meta_description');
            
			$object->body = $this->request->post('body');
			
			// Связанные товары
			if(is_array($this->request->post('related_products')))
			{
				foreach($this->request->post('related_products') as $p)
				{
					$rp[$p]->slider_id = $object->id;
					$rp[$p]->related_id = $p;
				}
				$related_products = $rp;
			}
				
			// Не допустить пустое название товара.
			if(empty($object->name))
			{			
				$this->design->assign('message_error', 'empty_name');
				$images = $this->slider->get_images(array('object_id'=>$object->id));
			}
			else
			{
				if(empty($object->id))
				{
	  				$object->id = $this->slider->add_object($object);
	  				$object = $this->slider->get_object($object->id);
					$this->design->assign('message_success', 'added');
	  			}
  	    		else
  	    		{
  	    			$this->slider->update_object($object->id, $object);
  	    			$object = $this->slider->get_object($object->id);
					$this->design->assign('message_success', 'updated');
  	    		}	
   	    		
   	    		if($object->id)
   	    		{
	   	    		// Категории товара
	   	    		$query = $this->db->placehold('DELETE FROM __objects_categories WHERE object_id=?', $object->id);
	   	    		$this->db->query($query);
	 	  		    if(is_array($object_categories))
		  		    {
		  		    	foreach($object_categories as $i=>$category)
	   	    				$this->object_categories->add_object_category($object->id, $category->id, $i);
	  	    		}
	
					// Удаление изображений
					$images = (array)$this->request->post('images');
					$current_images = $this->slider->get_images(array('object_id'=>$object->id));
					foreach($current_images as $image)
					{
						if(!in_array($image->id, $images))
	 						$this->slider->delete_image($image->id);
						}
	
					// Порядок изображений
					if($images = $this->request->post('images'))
					{
	 					$i=0;
						foreach($images as $id)
						{
							$this->slider->update_image($id, array('position'=>$i));
							$i++;
						}
					}
	   	    		// Загрузка изображений
		  		    if($images = $this->request->files('images'))
		  		    {
						for($i=0; $i<count($images['name']); $i++)
						{
				 			if ($image_name = $this->image->upload_image($images['tmp_name'][$i], $images['name'][$i]))
				 			{
			  	   				$this->slider->add_image($object->id, $image_name);
			  	   			}
							else
							{
								$this->design->assign('error', 'error uploading image');
							}
						}
					}
                    
                    
                    // Удаление логтипа
	 						$this->slider->delete_logo($object->id);
                        
                        // Загрузка логотипа
		  		    if($logo = $this->request->files('logo'))
		  		    {
				 			if ($logo_name = $this->image->upload_image($logo['tmp_name'], $logo['name']))
				 			{
			  	   				$this->slider->add_logo($object->id, $logo_name);
			  	   			}
							else
							{
								$this->design->assign('error', 'error uploading image');
							}
					}
                    
					$images = $this->slider->get_images(array('object_id'=>$object->id));
                    $logo = $this->slider->get_logo($object->id);
                    // Связанные товары
	   	    		$query = $this->db->placehold('DELETE FROM __slider_related_products WHERE slider_id=?', $object->id);
	   	    		$this->db->query($query);
	 	  		    if(is_array($related_products))
		  		    {
		  		    	$pos = 0;
		  		    	foreach($related_products  as $i=>$related_product)
	   	    				$this->slider->add_related_product($object->id, $related_product->related_id, $pos++);
	  	    		}
  	    		}
			}
			
			//header('Location: '.$this->request->url(array('message_success'=>'updated')));
		}
		else
		{
			$object->id = $this->request->get('id', 'integer');
			$object = $this->slider->get_object(intval($object->id));

			if($object && $object->id)
			{
				// Связанные товары
				$related_products = $this->slider->get_related_products(array('slider_id'=>$object->id));
				// Категории товара
				$object_categories = $this->object_categories->get_categories(array('object_id'=>$object->id));
                
				// Изображения товара
				$images = $this->slider->get_images(array('object_id'=>$object->id));
                
                //Логотип
                $logo = $this->slider->get_logo($object->id);
			}
			else
			{
				// Сразу активен
				$object->visible = 1;			
			}
		}
			
		if(empty($object_categories))
		{
			if($category_id = $this->request->get('category_id'))
				$object_categories[0]->id = $category_id;		
			else
				$object_categories = array(1);
		}
        
        if(!empty($related_products))
		{
			foreach($related_products as &$r_p)
				$r_products[$r_p->related_id] = &$r_p;
			$temp_products = $this->products->get_products(array('id'=>array_keys($r_products)));
			foreach($temp_products as $temp_product)
				$r_products[$temp_product->id] = $temp_product;
		
			$related_products_images = $this->products->get_images(array('product_id'=>array_keys($r_products)));
			foreach($related_products_images as $image)
			{
				$r_products[$image->product_id]->images[] = $image;
			}
		}
			
            

		$this->design->assign('object', $object);

		$this->design->assign('object_categories', $object_categories);
		$this->design->assign('object_images', $images);
		$this->design->assign('logo', $logo);
		$this->design->assign('related_products', $related_products);
		
		// Все категории
		$categories = $this->object_categories->get_categories_tree();
		$this->design->assign('categories', $categories);
		
		//$this->design->assign('message_success', $this->request->get('message_success', 'string'));
		//$this->design->assign('message_error',   $this->request->get('message_error', 'string'));

 	  	return $this->design->fetch('slider.tpl');
	}
}