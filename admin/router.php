<?php

$this->routes = array(

    array(
        'pattern' => '/restaurant-menu',
        'controller' => 'RestaurantMenuController'
    ),
    array(
        'pattern' => '~^/?$~',
        'controller' => 'PagesController'
    ),
    array(
        'pattern' => '~^/pages/?$~',
        'controller' => 'PagesController'
    ),
    array(
        'pattern' => '~^/pages/([^/]+)/?$~',
        'controller' => 'PageController',
        'params' => array('id')
    ),
    array(
        'pattern' => '/news',
        'controller' => 'NewsController'
    ),
    array(
        'pattern' => '/news/{id}/edit',
        'controller' => 'PostController',
        'params' => array('id')
    )
);

?>