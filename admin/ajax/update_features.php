<?php
	chdir('../..');
	require_once('api/Simpla.php');
	$simpla = new Simpla();
	$variant_title = $_POST['variant_title'];
	$variant_description = $_POST['variant_description'];
	$clear = $_POST['clear'];
	$feature_id = $_POST['feature_id'];
	if($clear == 1){
		$simpla->db->query("DELETE FROM __features_data WHERE feature_id = ?", $feature_id);
		$simpla->db->results();
	}else{
	$simpla->db->query("INSERT INTO __features_data SET feature_id=?, title=?, description=?", $feature_id, $variant_title, $variant_description);
	$simpla->db->results();
	}
?>