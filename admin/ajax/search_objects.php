<?php
	chdir('../..');
	require_once('api/Simpla.php');
	$simpla = new Simpla();
	$limit = 100;
	
	$keyword = $simpla->request->get('query', 'string');
	
	$simpla->db->query('SELECT o.id, o.name, i.filename as image FROM __objects o
	                    LEFT JOIN __object_images i ON i.object_id=o.id AND i.position=(SELECT MIN(position) FROM __object_images WHERE object_id=o.id LIMIT 1)
	                    WHERE o.name LIKE "%'.mysql_real_escape_string($keyword).'%" ORDER BY o.name LIMIT ?', $limit);
	$objects = $simpla->db->results();

	foreach($objects as $object)
	{
		if(!empty($object->image))
			{
				$object->image = $simpla->design->resize_modifier($object->image, 35, 35);
				$objects_names[] = $object->name;
			}
		else
			$objects_names[] = $object->name;	
				
		$objects_data[] = $object;
	}

	$res->query = $keyword;
	$res->suggestions = $objects_names;
	$res->data = $objects_data;
	header("Content-type: application/json; charset=UTF-8");
	header("Cache-Control: must-revalidate");
	header("Pragma: no-cache");
	header("Expires: -1");		
	print json_encode($res);
