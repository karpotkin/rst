<?PHP 

require_once('api/Simpla.php');

########################################
class FeedsAdmin extends Simpla
{


  function fetch()
  {
  
 	$filter = array();
  	$filter['page'] = max(1, $this->request->get('page', 'integer'));
  		
  	$filter['limit'] = 40;
 

    // Поиск
  	$keyword = $this->request->get('keyword', 'string');
  	if(!empty($keyword))
  	{
	  	$filter['keyword'] = $keyword;
 		$this->design->assign('keyword', $keyword);
	}

  
  	// Обработка действий 	
  	if($this->request->method('post'))
  	{
		
		// Действия с выбранными
		$ids = $this->request->post('check');
		if(!empty($ids) && is_array($ids))
		switch($this->request->post('action'))
		{
		    case 'approve':
		    {
				foreach($ids as $id)
					$this->response->update_response($id, array('approved'=>1));    
		        break;
		    }
		    case 'delete':
		    {
				foreach($ids as $id)
					$this->response->delete_response($id);    
		        break;
		    }
		}		
		
 	}

  

	// Отображение
  	$responses_count = $this->response->count_responses($filter);
	// Показать все страницы сразу
	if($this->request->get('page') == 'all')
		$filter['limit'] = $comments_count;	
  	$responses = $this->response->get_responses($filter, true);
  	
  	
  	
 	$this->design->assign('pages_count', ceil($responses_count/$filter['limit']));
 	$this->design->assign('current_page', $filter['page']);

 	$this->design->assign('responses', $responses);
 	$this->design->assign('responses_count', $responses_count);

	return $this->design->fetch('response.tpl');
  }
}


?>