<?PHP
require_once('api/Simpla.php');

class SendUserAdmin extends Simpla
{

	function fetch()
	{
		if($this->request->method('post'))
		{
			$user->id = $this->request->post('id', 'integer');
			$user->fio = $this->request->post('name');
			$user->email = $this->request->post('mail');
			$user->phone = $this->request->post('phone');

			if(empty($user->id))
			{
  				$user->id = $this->send->add_user($user);
  				$user = $this->send->get_user($user->id);
				$this->design->assign('message_success', 'added');
  			}
			else
			{
				$this->send->update_user($user->id, $user);
  				$user = $this->send->get_user($user->id);
				$this->design->assign('message_success', 'updated');
			}
		}
		else
		{
			$user->id = $this->request->get('id', 'integer');
  			$user = $this->send->get_user($user->id);
		}


		$this->design->assign('user', $user);
		return $this->body = $this->design->fetch('senduser.tpl');
	}
}




