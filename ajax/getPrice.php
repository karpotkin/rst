<?PHP

session_start();
chdir('..');
require_once('api/Simpla.php');
$simpla = new Simpla();

if($simpla->request->method('post')){
    $to = $simpla->settings->letter_email;

    $subject = 'Запрос на получение прайса';
    $name = "Имя: ".$simpla->request->post('name')."\n";
    $phone = "Телефон: ".$simpla->request->post('phone')."\n";
    $email = "Email: ".$simpla->request->post('email')."\n";

    $msg = $name;
    $msg .= $phone;
    $msg .= $email;

    $EOL = "\r\n"; // ограничитель строк, некоторые почтовые сервера требуют \n - подобрать опытным путём
    $boundary     = "--".md5(uniqid(time()));  // любая строка, которой не будет ниже в потоке данных.
    $headers    = "MIME-Version: 1.0;$EOL";
    $headers   .= "Content-Type: multipart/mixed; boundary=\"$boundary\"$EOL";
    $headers   .= "From: amwellgroup.ru <info@amwellgroup.ru>\r\n";
    $multipart  = "--$boundary$EOL";
    $multipart .= "Content-Type: text/plain; charset=utf-8$EOL";
    $multipart .= "Content-Transfer-Encoding: base64$EOL";
    $multipart .= $EOL; // раздел между заголовками и телом html-части
    $multipart .= chunk_split(base64_encode($msg));

    @mail($to, $subject, $multipart, $headers);

    $data['success'] =  true;

    print json_encode($data);
}