/**
 * Created by evgeniy on 12/03/16.
 */
(function($){

    $(function(){
        var $sliderItem = $('.field-name-field-slides');
        var $sliderItemArray = $sliderItem.find('img');
        var e = 3250;
        var i = 1e3;

        $sliderItem.find('.field-name-field-alignment').each(function() {
            var $self = $(this);
            $self.prev().addClass($self.text() + '-align');
        });

        function slideShow(){
            $sliderItemArray.each(function(n) {
                var $self = $(this);
                var cnt = n + 1;

                if(cnt === 1) {
                    $sliderItemArray.last().removeClass('active');
                    $self.nextAll('img').first().removeAttr('style');
                    $self.addClass('active').fadeIn(i);

                } else {
                    $self.delay(e * n).queue(function() {
                        var $me = $(this);

                        $me.addClass('active')
                            .fadeIn(i)
                            .prevAll('img')
                            .removeClass('active');

                        $me.nextAll('img').removeAttr('style');

                        $me.dequeue();

                        if($sliderItemArray.length === cnt) {
                            $sliderItemArray.first().css('display', '');
                            setTimeout(function(){
                                slideShow();
                            }, e);
                        }
                    });
                }
            });
        }

        slideShow();

        $('.food-menu__item-link').colorbox({arrowKey: false, closeButton: false, })
    });
	
	$('.food-hack__body').css('width', $('.food-menu__item').width());
	$('.food-hack__body').css('top', $('.food-menu__item').height()-340);
	
	function menuSize() {
		$('.food-hack__body').css('width', $('.food-menu__item').width());
		$('.food-hack__body').css('top', $('.food-menu__item').height()-340);
	}
	
	$(window).on('resize', menuSize);
	
	$('a[href^="#"]').click(function(){
			var el = $(this).attr('href');
			$('body').animate({
				scrollTop: $(el).offset().top}, 2000);
			return false; 
	});
	

})(jQuery);