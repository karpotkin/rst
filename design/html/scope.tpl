{extends file='layout.tpl'}
{block name='main'}
    {include file='blocks/default_header.tpl'}

    <div class="container-fluid scope-box">
        <div class="wrapper">
            <div class="row">
                <ul class="list-unstyled scope-list">
                    <li class="scope-item">
                        <p class="scope-item__description">
                            <span class="scope-item__description-l1">В 7 странах мира</span>
                            <span class="scope-item__description-l2">более 1800 ГА</span>
                            <span class="scope-item__description-l3">фруктовых, овощных и ягодных плантаций</span>
                        </p>
                        <img class="scope-item__image" src="design/images/scope/001.jpg" />
                        <div class="scope-item__title">
                            Плантация клубники. Чили
                        </div>
                    </li>
                    <li class="scope-item">
                        <p class="scope-item__description">
                            <span class="scope-item__description-l2" style="letter-spacing: 13px;">Фасовка</span>
                            <span class="scope-item__description-l1_mod" style="font-size: 50px;letter-spacing: 10px;">Калибровка</span>
                            <span class="scope-item__description-l3">Поставка под брендом заказчика.</span>
                        </p>
                        <img class="scope-item__image" src="design/images/scope/005.jpg" />
                        <div class="scope-item__title">
                            Калибровка яблок. Израиль
                        </div>
                    </li>
                    <li class="scope-item">
                        <p class="scope-item__description">
                            <span class="scope-item__description-l2" style="letter-spacing: 11px;">Логистика</span>
                            <span class="scope-item__description-l1_mod" style="font-size: 50px;">прямо с плантации</span>
                            <span class="scope-item__description-l1_mod" style="font-size: 50px;letter-spacing: -2.8px;">до любой точки мира</span>
                        </p>
                        <img class="scope-item__image" src="design/images/scope/003.jpg" />
                        <div class="scope-item__title">
                            Погрузка в порту Касабланки. Марокко
                        </div>
                    </li>
                    <li class="scope-item">
                        <div class="scope-item__description scope-item__description_mod">
                            <div class="col-md-6">
                                <span class="scope-item__description-l2">Экскурсии</span>
                                <span class="scope-item__description-l1_mod" style="font-size: 50px; letter-spacing: 3.5px;">на плантации</span>
                                <span class="scope-item__description-l1_mod" style="font-size: 50px; letter-spacing: 2.5px;">производство</span>
                            </div>
                            <div class="col-md-6 scope-item__description-info">
                                Наши плантации открыты для всех. Мы готовы показать процесс производства: от выращивания до погрузки продукта.
                            </div>
                        </div>
                        <img class="scope-item__image" src="design/images/scope/004.jpg" />
                        <div class="scope-item__title">
                            Сбор мандаринов. Китай, провинция Хунань
                        </div>
                    </li>
                    <li class="scope-item">
                        <div class="scope-item__description scope-item__description_mod">
                            <div class="col-md-7">
                                <span class="scope-item__description-l2">Финансовые</span>
                                <span class="scope-item__description-l2" style="letter-spacing: -2.3px;">возможности</span>
                            </div>
                            <div class="col-md-5 scope-item__description-info">
                                Для постоянных клиентов - отсрочка платежа, аккредитив, рассрочка.
                            </div>
                        </div>
                        <img class="scope-item__image" src="design/images/scope/002.jpg" />
                    </li>
                </ul>
            </div>
        </div>
    </div>
{/block}