{* О нас *}
{extends file='layout.tpl'}
{block name='main'}
    {include file='blocks/contact_header.tpl'}

    <div class="container-fluid contact-box">
        <h3 class="text-center text-upper">Свяжитесь с нами</h3>
        <p class="text-center">Мы всегда открыты. Ждем ваших вопросов, предложений, отзывов.</p>
        <div class="contact-wrapper mt50 mb50">
            <div class="row">
                <div class="col-md-4">
                    <h5 class="text-upper text-bold">Контакты в Москве:</h5>
                    <ul class="footer-menu__list footer-menu__list-contacts contact-menu__list list-unstyled">
                        <li class="footer-menu__item contact-menu__item">
                            <p><span class="footer-menu__item-icon contact-menu__item-icon street"></span>г. Москва, Кутузовский пр-т, 36-к.4</p>
                        </li>
                        <li class="footer-menu__item contact-menu__item">
                            <p><span class="footer-menu__item-icon contact-menu__item-icon phone"></span>+7 (495) 786-10-80</p>
                        </li>
                        <li class="footer-menu__item contact-menu__item">
                            <p class="green"><span class="footer-menu__item-icon contact-menu__item-icon email"></span>hello@amwellgroup.ru</p>
                        </li>
                        <li class="footer-menu__item contact-menu__item">
                            <p class="green"><span class="footer-menu__item-icon contact-menu__item-icon skype"></span>amwellgroup</p>
                        </li>
                    </ul>
                </div>
                <div class="col-md-4">
                    <h5 class="text-upper text-bold">Контакты в Марокко:</h5>
                    <ul class="footer-menu__list footer-menu__list-contacts contact-menu__list list-unstyled">
                        <li class="footer-menu__item contact-menu__item">
                            <p><span class="footer-menu__item-icon contact-menu__item-icon street"></span>г. Марракеш, Марокко, <br/>Rue de la Liberté, 40000</p>
                        </li>
                        <li class="footer-menu__item contact-menu__item">
                            <p class="green"><span class="footer-menu__item-icon contact-menu__item-icon email"></span>mc@amwellgroup.com</p>
                        </li>
                    </ul>
                </div>
                <div class="col-md-4">
                    <h5 class="text-upper text-bold">Контакты в Эквадор:</h5>
                    <ul class="footer-menu__list footer-menu__list-contacts contact-menu__list list-unstyled">
                        <li class="footer-menu__item contact-menu__item">
                            <p><span class="footer-menu__item-icon contact-menu__item-icon street"></span>г. Кито, Эквадор, <br/>Avenida Río Amazonas, EC170135</p>
                        </li>
                        <li class="footer-menu__item contact-menu__item">
                            <p class="green"><span class="footer-menu__item-icon contact-menu__item-icon email"></span>eq@amwellgroup.com</p>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
{/block}
{block name='footerScript'}
    <script src="http://api-maps.yandex.ru/2.0/?load=package.full&lang=ru-RU" type="text/javascript"></script>
    <script type="text/javascript">
        function initMaps() {
            var cntr = [55.750727, 37.562272]; //32.219596,52.767002
            var myMap = new ymaps.Map('contactMap', {
                center:cntr,
                zoom:16,
                behaviors:["default"]
            });

            var myPlacemark = new ymaps.Placemark([55.750727, 37.562272],
                {
                    hintContent: 'Метка Amwell Group'
                },
                {
                    iconImageHref:'/design/images/pin.png',
                    iconImageSize:[38, 69]
                }
            );
            myMap.geoObjects.add(myPlacemark);
        }
        ymaps.ready(initMaps);
    </script>
{/block}