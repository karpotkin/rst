{extends file='layout.tpl'}
{block name='main'}
    {include file='blocks/product_header.tpl'}
    <div class="container-fluid product-box">
        <div class="wrapper">
            <ul class="product-detail-list list-unstyled pull-left">
                <li class="product-detail-list__item">
                    <div class="product-detail-list__item-wrapper">
                        <div class="product-detail-list__item-title text-upper mt5">Цена</div>
                        <div class="product-detail-list__item-value text-bold mt15">{$product->price}</div>
                        <div class="product-detail-list__item-title mt10">руб./кг</div>
                    </div>
                </li>
                <li class="product-detail-list__item">
                    <div class="product-detail-list__item-wrapper">
                        <div class="product-detail-list__item-title text-upper mt5">Доставка</div>
                        <div class="product-detail-list__item-value text-bold mt15">{$product->delivery}</div>
                        <div class="product-detail-list__item-title mt10">рабочих<br/>дней</div>
                    </div>
                </li>
                <li class="product-detail-list__item">
                    <div class="product-detail-list__item-wrapper">
                        <div class="product-detail-list__item-title text-upper mt5">Наличие</div>
                        <div class="product-detail-list__item-value text-bold mt15">{$product->availability}</div>
                        <div class="product-detail-list__item-title mt10">калибры</div>
                    </div>
                </li>
                <li class="product-detail-list__item">
                    <div class="product-detail-list__item-wrapper">
                        <div class="product-detail-list__item-title text-upper mt5">Упаковка</div>
                        <div class="product-detail-list__item-value text-bold mt5">
                            <img src="design/images/product-detail-box.png" alt=""/>
                        </div>
                        <div class="product-detail-list__item-title mt5">{$product->pkg}</div>
                    </div>
                </li>
            </ul>
            <div style="clear: left"></div>
            <div class="product-detail-description">
                {$product->body}
            </div>
        </div>
    </div>
    {include file='blocks/trust.tpl'}
{/block}