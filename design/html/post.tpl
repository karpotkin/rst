{* Страница отдельной записи новости *}
{* Страница отдельной записи новости *}
{extends file='layout.tpl'}
{block name='main'}
    {include file='blocks/default_header.tpl'}

    <div class="container-fluid news-box">
        <div class="wrapper">
            <div class="row">
                <h3>{$post->name|escape}</h3>
                <p>{$post->date|date}</p>
                <div>
                    {$post->annotation}
                    {$post->text}
                </div>
            </div>
        </div>
    </div>

{/block}