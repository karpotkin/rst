{* News *}
{extends file='layout.tpl'}
{block name='main'}
    <div class="row">
        <div class="col-md-4 col-xs-12">
            <h3 class="page-title text-left _tt_upper">{$page->header}</h3>

            <div class="page-body news__container">
                {if $posts}
                    {foreach $posts as $post}
                        <div class="news-list__item">
                            <h4 class="page-title news-list__item-title">{$post->date|date_format:"%d/%m/%Y"} - {$post->name}</h4>
                            <div class="news-list__item-text">{$post->annotation}</div>
                        </div>
                    {/foreach}
                {else}
                    Empty
                {/if}
            </div>
        </div>
        {if $page->image}
            <div class="col-md-8 col-xs-12">
                <img src="/files/pages/original/{$page->image}" alt="">
            </div>
        {/if}
    </div>
{/block}