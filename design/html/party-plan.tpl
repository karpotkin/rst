{* Party Plan *}
{extends file='layout.tpl'}
{block name='main'}
    {include file='blocks/contact_header.tpl'}

    <div class="container-fluid party-plan-box">
        <h3 class="text-center text-upper">{$page->header}</h3>
        <div class="party-plan__description">
            <p>ミンスクの台所では、8名様以上40名様以下の団体様向けにおすすめのパーティープランをご用意致します。</p>
            <p>皆様でお料理をシェアーしながらディナーを楽しめるプランを3つご用意しました。</p>
            <p>お客様のご希望の量、予算に合わせてお選び下さい。<p>
            <p>ご希望に応じてメニュー内容の変更が可能です。</p>
            <p>下記のプラン以外にも少人数パーティー用のコースをご用意できますのでお気軽にお問合せ下さい。</p>
            <p>8名様以上の団体様にはグルジアワインを1本サービスさせていただきます。</p>
        </div>
        <div class="contact-wrapper mt50 mb50">
            <div class="row">
                <div class="col-md-4">
                    <h5 class="text-upper text-bold">Контакты в Москве:</h5>
                    <ul class="footer-menu__list footer-menu__list-contacts contact-menu__list list-unstyled">
                        <li class="footer-menu__item contact-menu__item">
                            <p><span class="footer-menu__item-icon contact-menu__item-icon street"></span>г. Москва, Кутузовский пр-т, 36-к.4</p>
                        </li>
                        <li class="footer-menu__item contact-menu__item">
                            <p><span class="footer-menu__item-icon contact-menu__item-icon phone"></span>+7 (495) 786-10-80</p>
                        </li>
                        <li class="footer-menu__item contact-menu__item">
                            <p class="green"><span class="footer-menu__item-icon contact-menu__item-icon email"></span>hello@amwellgroup.ru</p>
                        </li>
                        <li class="footer-menu__item contact-menu__item">
                            <p class="green"><span class="footer-menu__item-icon contact-menu__item-icon skype"></span>amwellgroup</p>
                        </li>
                    </ul>
                </div>
                <div class="col-md-4">
                    <h5 class="text-upper text-bold">Контакты в Марокко:</h5>
                    <ul class="footer-menu__list footer-menu__list-contacts contact-menu__list list-unstyled">
                        <li class="footer-menu__item contact-menu__item">
                            <p><span class="footer-menu__item-icon contact-menu__item-icon street"></span>г. Марракеш, Марокко, <br/>Rue de la Liberté, 40000</p>
                        </li>
                        <li class="footer-menu__item contact-menu__item">
                            <p class="green"><span class="footer-menu__item-icon contact-menu__item-icon email"></span>mc@amwellgroup.com</p>
                        </li>
                    </ul>
                </div>
                <div class="col-md-4">
                    <h5 class="text-upper text-bold">Контакты в Эквадор:</h5>
                    <ul class="footer-menu__list footer-menu__list-contacts contact-menu__list list-unstyled">
                        <li class="footer-menu__item contact-menu__item">
                            <p><span class="footer-menu__item-icon contact-menu__item-icon street"></span>г. Кито, Эквадор, <br/>Avenida Río Amazonas, EC170135</p>
                        </li>
                        <li class="footer-menu__item contact-menu__item">
                            <p class="green"><span class="footer-menu__item-icon contact-menu__item-icon email"></span>eq@amwellgroup.com</p>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
{/block}
{block name='footerScript'}
{/block}