<div class="breadcrumbs {$modificator}">
    <a href="/">Главная</a>
    <span class="separator">&rarr;</span>
    {if $page_parent->name !=''}
        <a href="{$page_parent->url}">{$page_parent->name}</a>
        <span class="separator">&rarr;</span>
    {/if}
    <span>{$page->name}</span>
    {if $post}
        <a href="articles">Статьи</a>
        <span class="separator">&rarr;</span>
        <span>{$post->name}</span>
    {/if}
</div>