<div class="row">
    <div class="col-xs-4"></div>
    <div class="col-xs-4">
        <a href="/" class="brand-heading logo"></a>
    </div>
    <div class="col-xs-4">
        <div class="pull-right">
            <ul class="list-unstyled header__lang text-center">
                <li class="header__lang-item"><a href="#">JP</a></li>
                <li class="header__lang-item"><a href="#">RU</a></li>
            </ul>
            <ul class="list-unstyled header__phones text-center">
                <li class="header__phones-item">TEL：03-3586-6600</li>
            </ul>
        </div>
    </div>
</div>

{include file='blocks/menu.tpl'}