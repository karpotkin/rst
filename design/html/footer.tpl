<div class="footer">
    <div class="navbar-collapse collapse footer-menu">
        <ul class="nav navbar-nav footer-menu__list">
            <li class="footer-menu__item">
                <a class="_tt_upper footer-menu__item-link" href="/company">Company</a>
            </li>
            <li class="footer-menu__item active">
                <a class="_tt_upper footer-menu__item-link" href="#">Contact</a>
            </li>
            <li class="footer-menu__item active">
                <a class="_tt_upper footer-menu__item-link" href="/credits">Credits</a>
            </li>
        </ul>
        <ul class="list-unstyled footer-menu__social text-center">
            <li class="footer-menu__social-item">
                <a class="_tt_upper footer-menu__social-item-link" href="https://www.facebook.com/pages/%E3%83%99%E3%83%A9%E3%83%AB%E3%83%BC%E3%82%B7%E3%81%AE%E5%AE%B6%E5%BA%AD%E6%96%99%E7%90%86-%E3%83%9F%E3%83%B3%E3%82%B9%E3%82%AF%E3%81%AE%E5%8F%B0%E6%89%80/145609245474795">
                    <i class="fa fa-facebook"></i>
                </a>
            </li>
            <li class="footer-menu__social-item">
                <a class="_tt_upper footer-menu__social-item-link" href="#">
                    <i class="fa fa-youtube"></i>
                </a>
            </li>
        </ul>
    </div>
</div>