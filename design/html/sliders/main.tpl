<div class="node node--restaurant node--promoted main__slider view-mode-full  node--full node--restaurant--full col-sm-12 clearfix">
    <div class="field field-name-field-slides main__slider-list">
        <img src="/design/images/slider/01.jpg" class="main__slider-item" width="1400" height="620" alt="" title="" />
        <div class="field field-name-field-alignment">center</div>
        <img src="/design/images/slider/02.png" class="main__slider-item" width="467" height="620" title="" />
        <div class="field field-name-field-alignment">left</div>
        <img src="/design/images/slider/03.png" class="main__slider-item" width="935" height="620" title="" />
        <div class="field field-name-field-alignment">right</div>
        <img src="/design/images/slider/04.jpg" class="main__slider-item" width="1400" height="620" alt="" title="" />
        <div class="field field-name-field-alignment">center</div>
        <img src="/design/images/slider/05.png" class="main__slider-item" width="467" height="620" title="" />
        <div class="field field-name-field-alignment">right</div>
        <img src="/design/images/slider/06.png" class="main__slider-item" width="935" height="620" title="" />
        <div class="field field-name-field-alignment">left</div>
        <img src="/design/images/slider/07.jpg" class="main__slider-item" width="1400" height="620" alt="" title="" />
        <div class="field field-name-field-alignment">center</div>
        <img src="/design/images/slider/08.png" class="main__slider-item" width="467" height="620" title="" />
        <div class="field field-name-field-alignment">left</div>
        <img src="/design/images/slider/09.png" class="main__slider-item" width="935" height="620" title="" />
        <div class="field field-name-field-alignment">right</div>
        <img src="/design/images/slider/10.jpg" class="main__slider-item" width="1400" height="620" alt="" title="" />
        <div class="field field-name-field-alignment">center</div>
        <img src="/design/images/slider/11.png" class="main__slider-item" width="467" height="620" title="" />
        <div class="field field-name-field-alignment">left</div>
        <img src="/design/images/slider/12.png" class="main__slider-item" width="935" height="620" title="" />
        <div class="field field-name-field-alignment">right</div>
        <img src="/design/images/slider/13.jpg" class="main__slider-item" width="1400" height="620" alt="" title="" />
        <div class="field field-name-field-alignment">center</div>
    </div>
</div>