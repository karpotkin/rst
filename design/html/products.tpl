{* Список товаров *}
{extends file='layout.tpl'}
{block name='main'}
    {*include file='blocks/products_header.tpl'*}
    {include file='blocks/default_header.tpl'}

    <div class="container-fluid products-box">
        <h3 class="text-center">Наша продукция</h3>
        <div class="row">
            <div class="col-md-2">
                <div class="box">
                    {include file='blocks/countries.tpl'}
                </div>
                <div class="box mt20">
                    {include file='blocks/categories.tpl'}
                </div>
            </div>
            <div class="col-md-10">
                <div class="products__list">
                    {if $products}
                        {foreach $products as $p}
                            <div class="product-item text-center">
                                <a href="products/{$p->url}" class="text-bold product-name">{$p->name}</a>
                                <a href="" class="product-country">{$p->brand}</a>
                                <a href="products/{$p->url}" class="product-image mt10" style="background-image: url({$p->image->filename|resize:250:250});"></a>
                            </div>
                        {/foreach}
                    {else}
                        <h2>Ничего не найдено</h2>
                    {/if}
                </div>
            </div>
        </div>
    </div>
{/block}