{extends file='layout.tpl'}
{block name='main'}
    <div class="row">
        <div class="{if $page->url === 'media'}col-md-3{elseif $page->image}col-md-4{/if} col-xs-12">
            {if !$page->url|in_array:['aboutus', 'media']}
                <h3 class="page-title text-left _tt_upper">{$page->header}</h3>
            {/if}
            {if $page->url === 'media'}
                <div class="text-left">
                    <h3 class="page-title text-left _tt_upper">
                        <a href="/tvprograms">Tv Programs</a>
                    </h3>
                    <div class="media__separator"></div>
                    <h3 class="page-title text-left _tt_upper mt-20">
                        <a href="/radio">Radio</a>
                    </h3>
                    <div class="media__separator"></div>
                    <h3 class="page-title text-left _tt_upper mt-20">
                        <a href="#">Magazine</a>
                    </h3>
                </div>
            {/if}
            <div class="page-body page__{$page->url} text-center">{$page->page_text}</div>
        </div>
        {if $page->image}
            <div class="{if $page->url === 'media'}col-md-9{else}col-md-8{/if} col-xs-12">
                <img src="/files/pages/original/{$page->image}" alt="">
            </div>
        {/if}
    </div>
{/block}