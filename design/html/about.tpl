{* О компании *}
{extends file='layout.tpl'}
{block name='main'}
    {include file='blocks/default_header.tpl'}
    <div class="container-fluid about-box mb50">
        <div class="wrapper">
            <div class="row">
                <div class="col-md-12">
                    <div class="col-md-6">
                        <img class="about-image" src="design/images/about/lea.jpg" alt=""/>
                    </div>
                    <div class="col-md-6">
                        <h3 class="about-title text-upper">О компании</h3>
                        <div class="about-description">
                            <p>Amwell Group Ltd. - международная производственная компания.</p>
                            <p>Мы начинаем с подготовки почвы, заканчиваем доставкой фруктов, овощей и ягод на ваш склад. Проводим лабораторные исследования новых сортов. Фасуем выращенные культуры под вашим брендом.</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="col-md-6">
                        <h3 class="about-title text-upper mt50">Наши цели</h3>
                        <ul class="list-unstyled about-list">
                            <li><span class="glyphicon glyphicon-ok" aria-hidden="true"></span>Снижение цены фруктов, ягод и овощей в российском ритейле</li>
                            <li><span class="glyphicon glyphicon-ok" aria-hidden="true"></span>Максимально быстрые поставки</li>
                            <li><span class="glyphicon glyphicon-ok" aria-hidden="true"></span>Развитие сельского хозяйства в России</li>
                            <li><span class="glyphicon glyphicon-ok" aria-hidden="true"></span>Стать высокоэффективной компанией-производителем овощей, фруктов и ягод №1 в России</li>
                        </ul>
                        <p>
                            Хотите с нами? Вы представляете частного инвестора или венчурный фонд? Пишите нам на <a href="mailto:invest@amwellgroup.ru">invest@amwellgroup.ru</a>.
                        </p>
                    </div>
                    <div class="col-md-6">
                        <img class="about-image" src="design/images/about/goal.jpg" alt=""/>
                    </div>
                </div>
            </div>
        </div>
    </div>

    {include file='blocks/team.tpl'}

{/block}
{block name='footerScript'}
{literal}
    <script>
        $(function(){
            $('[data-text-toggle]').on('click', function(){
                $(this).next().toggle();
            });
        });
    </script>
{/literal}
{/block}