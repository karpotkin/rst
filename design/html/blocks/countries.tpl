<div class="products__countries">
    <div class="text-bold">Выберите страну производителя:</div>
    <ul class="products__countries-list list-unstyled">
        <li class="products__countries-item {if !$brand->id}products__countries-item_active{/if}">
            <a class="products__countries-item_link" href="/products">Все</a>
        </li>
        {foreach $countries as $c}
            <li class="products__countries-item {if $c->id == $brand->id}products__countries-item_active{/if}">
                <a class="products__countries-item_link" href="/countries/{if $category->url}{$category->url}/{/if}{$c->url}">{$c->name|escape}</a>
            </li>
        {/foreach}
    </ul>
</div>