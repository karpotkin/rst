<div class="container-fluid contact-header clearfix">
    <div id="contactMap"></div>
    <div class="contact-header__inner">
        <div class="row">
            <div class="col-md-12">
                {include file='blocks/contact_form.tpl'}
            </div>
        </div>
    </div>
</div>