<div class="container-fluid company-team">
    <div class="wrapper">
        <div class="row">
            <h3 class="about-title text-upper text-center">Наша команда</h3>
            {*<p class="text-center mt20 mb50">
                Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut.
            </p>*}
            <ul class="list-unstyled text-center">
                <li class="company-team__item">
                    <div class="company-team__item-photo">
                        <img src="design/images/team/oleg.jpg" alt=""/>
                        <div class="company-team__item-photo_title">
                            <p class="company-team__item-photo_title-name text-upper">Олег Гусков</p>
                            <p>основатель</p>
                        </div>
                    </div>
                    <div class="company-team__social text-center">
                        <a href="https://www.facebook.com/oleg.guskov" class="company-team__social-btn company-team__social-btn-fb"></a>
                        {*<a href="#" class="company-team__social-btn company-team__social-btn-twitter"></a>*}
                        <a href="https://vk.com/oleg_guskov" class="company-team__social-btn company-team__social-btn-vk"></a>
                    </div>
                </li>
                <li class="company-team__item">
                    <div class="company-team__item-photo">
                        <img src="design/images/team/asker.jpg" alt=""/>
                        <div class="company-team__item-photo_title">
                            <p class="company-team__item-photo_title-name text-upper">Аскер Курбанов</p>
                            <p>генеральный директор</p>
                        </div>
                    </div>
                    <div class="company-team__social text-center">
                        <a href="https://www.facebook.com/asker.kurbanov" class="company-team__social-btn company-team__social-btn-fb"></a>
                        {*<a href="#" class="company-team__social-btn company-team__social-btn-twitter"></a>*}
                        <a href="https://vk.com/askerkurbanov" class="company-team__social-btn company-team__social-btn-vk"></a>
                    </div>
                </li>
                <li class="company-team__item">
                    <div class="company-team__item-photo">
                        <img src="design/images/team/chingiz.jpg" alt=""/>
                        <div class="company-team__item-photo_title">
                            <p class="company-team__item-photo_title-name text-upper">Чингиз Гизатов</p>
                            <p>директор по развитию</p>
                        </div>
                    </div>
                    <div class="company-team__social text-center">
                        {*<a href="#" class="company-team__social-btn company-team__social-btn-fb"></a>
                        <a href="#" class="company-team__social-btn company-team__social-btn-twitter"></a>*}
                        <a href="http://vk.com/chingiz_gizatov" class="company-team__social-btn company-team__social-btn-vk"></a>
                    </div>
                </li>
                <li class="company-team__item">
                    <div class="company-team__item-photo">
                        <img src="design/images/team/002.jpg" alt=""/>
                        <div class="company-team__item-photo_title">
                            <p class="company-team__item-photo_title-name text-upper">Светлана Моренко</p>
                            <p>директор по маркетингу</p>
                        </div>
                    </div>
                    <div class="company-team__social text-center">
                        <a href="https://www.facebook.com/100000967244246" class="company-team__social-btn company-team__social-btn-fb"></a>
                        {*<a href="#" class="company-team__social-btn company-team__social-btn-twitter"></a>*}
                        <a href="https://vk.com/id3294439" class="company-team__social-btn company-team__social-btn-vk"></a>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</div>