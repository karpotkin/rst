<div class="main-menu">
    {function name=pages_menu level=0}
        {if $pages}
            <ul class="{if $level == 0}nav navbar-nav main-menu__list{else}dropdown-menu{/if}">
                {foreach $pages as $p}
                    {if $p->menu_id == 2}
                        {if $p->subcategories}
                            <li role="presentation" class="main-menu__item dropdown{if $page->id == $p->id} active{/if}">
                                <a class="{if $level == 0}_tt_upper {/if}main-menu__item-link dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="#">
                                    {$p->name|escape}
                                    <span class="main-menu__item-caret"></span>
                                </a>
                                {pages_menu pages=$p->subcategories level=$level+1}
                            </li>
                        {else}
                            <li role="presentation" class="main-menu__item{if $page->id == $p->id} active{/if}">
                                <a class="{if $level == 0}_tt_upper {/if}main-menu__item-link" href="{$p->url}">{$p->name|escape}</a>
                            </li>
                        {/if}
                    {/if}
                {/foreach}
            </ul>
        {/if}
    {/function}
    {pages_menu pages=$pages}
</div>