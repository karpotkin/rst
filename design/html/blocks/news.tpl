<div id="mainNews" class="col-md-6 carousel slide carousel-fade" data-ride="carousel" data-interval="false">
    <h3>Новости с наших плантаций</h3>
    <div class="row">
        {get_posts var=last_posts limit=3}
        {if $last_posts}
            <div class="carousel-inner">
                {foreach $last_posts as $post}
                    <div class="main__news-item item {if $post@first}active{/if}">
                        <div class="col-xs-4">
                            <img src="design/images/apples.png">
                        </div>
                        <div class="col-xs-8 news">
                            <p class="main__news-carousel-title">{$post->name}</p>
                            <div class="main__news-carousel-description">{$post->annotation}</div>
                            <p class="date">{$post->date|date}</p>
                            <p class="main__news-carousel-category category"><a class="main__news-carousel-category_link" href="news">Новости компании</a></p>
                        </div>
                    </div>
                {/foreach}
            </div>
            <ol class="carousel-indicators main__news-carousel_indicator">
                {for $cnt=0 to $last_posts|@count - 1}
                    <li data-target="#mainNews" data-slide-to="{$cnt}" {if $cnt == 0}class="active"{/if}></li>
                {/for}
            </ol>
        {/if}
    </div>
</div>