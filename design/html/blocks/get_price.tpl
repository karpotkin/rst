<form class="form-horizontal pull-right form__get-price" role="form" action="ajax/getPrice.php">
    <p>Получить прайс</p>
    <p>и образцы продукции</p>
    <div class="form-group">
        <label class="control-label col-sm-2" for="name">Имя *:</label>
        <div class="col-sm-12">
            <input type="text" required class="form-control" name="name" placeholder="Константин Никифоров">
            <span class="form__get-price_icon form__get-price_icon-user"></span>
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-sm-2" for="phone">Телефон *:</label>
        <div class="col-sm-12">
            <input type="text" required class="form-control" name="phone" placeholder="+7 (495) ___-__-__">
            <span class="form__get-price_icon form__get-price_icon-phone"></span>
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-sm-2" for="email">E-mail *:</label>
        <div class="col-sm-12 form__get-price_input">
            <input type="email" required class="form-control" name="email" placeholder="hello@amwellgroup.ru">
            <span class="form__get-price_icon form__get-price_icon-email"></span>
        </div>
    </div>
    <div class="form-group">
        <div class="text-center col-sm-12">
            <button type="submit" class="btn btn-default form__get-price_btn-send"></button>
        </div>
    </div>
</form>