<div class="container-fluid l11 products-header clearfix" style="background-image: url({if $product->cover}'../{$config->product_covers_dir}{$product->cover}'{else}'design/images/products-header.jpg'{/if})">
    <div class="products-header-overlay"></div>
    <div class="products-header__inner">
        <div class="row">
            <div class="col-md-7">
                <div class="products-header__description">
                    <h1>{$product->name}</h1>
                    <p class="products-header__description-text">
                        Страна производитель: {$product->brand}
                    </p>
                </div>
            </div>
            <div class="col-md-5">
                {include file='blocks/get_price.tpl'}
            </div>
        </div>
    </div>
</div>