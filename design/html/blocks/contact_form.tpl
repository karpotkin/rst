<form class="form-horizontal pull-right form__get-price" role="form">
    <p>Свяжитесь с нами</p>
    <div class="form-group">
        <label class="control-label col-sm-2" for="name">Имя *:</label>
        <div class="col-sm-12">
            <input type="text" required class="form-control" name="name" placeholder="Константин Никифоров">
            <span class="form__get-price_icon form__get-price_icon-user"></span>
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-sm-2" for="email">E-mail *:</label>
        <div class="col-sm-12 form__get-price_input">
            <input type="email" required class="form-control" name="email" placeholder="hello@amwellgroup.ru">
            <span class="form__get-price_icon form__get-price_icon-email"></span>
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-sm-2" for="email">Сообщение *:</label>
        <div class="col-md-12">
            <textarea class="form-control" name="message" id="message" placeholder="Задайте интересующие вас вопросы" cols="30" rows="4"></textarea>
        </div>
    </div>
    <div class="form-group">
        <div class="text-center col-sm-12">
            <button type="submit" class="btn btn-default contact-btn-send"></button>
        </div>
    </div>
</form>