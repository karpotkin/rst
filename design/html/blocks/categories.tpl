<div class="products__categories">
    <div class="text-bold">Выберите тип продукта:</div>
    {function name=categories_tree level=0}
        {if $categories}
            <ul class="products__categories-list list-unstyled">
                {if $level == 0}
                    <li class="products__categories-item {if !$category->id}products__categories-item_active{/if}">
                        <a class="products__categories-item_link" href="/products">Все</a>
                    </li>
                {/if}
                {foreach $categories as $cat}
                    {if $cat->visible}
                        <li class="products__categories-item {if $category->id == $cat->id}products__categories-item_active{/if}">
                            <a class="products__categories-item_link" href="/catalog/{$cat->url}">{$cat->name|escape}</a>
                            {categories_tree categories=$cat->subcategories level=$level+1}
                        </li>
                    {/if}
                {/foreach}
            </ul>
        {/if}
    {/function}
    {categories_tree categories=$categories}
</div>