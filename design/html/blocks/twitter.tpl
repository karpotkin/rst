<div class="twitter-box green-separator__top">
    <h2 class="twitter-title">Мы в Twitter</h2>
    <ul class="twitter-list list-unstyled">
        {foreach $tweets as $tw}
            <li class="twitter-item clearfix">
                <a class="twitter-item__link" target="_blank" href="https://twitter.com/{$tw->user->screen_name}/status/{$tw->id}">
                    {$tw->text}
                </a>
                <div class="mt10">
                    <a class="twitter-item__user text-green" href="https://twitter.com/{$tw->user->screen_name}">@{$tw->user->screen_name}</a>
                    <div class="twitter-date pull-right">{$tw->created_at|date}</div>
                </div>
            </li>
        {/foreach}
    </ul>
</div>