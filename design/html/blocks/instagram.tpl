<div class="col-md-6 inst">
    <div class="row">
        <h3>Наш Instagram</h3>
        <p>#amwellgroup</p>
        <div class="main__instagram-slider" data-slick='{ldelim}"slidesToShow": 3, "slidesToScroll": 1, "arrows": true, "autoplay": true, "autoplaySpeed": 7000{rdelim}'>
            {foreach $instagram as $insta}
                <a class="main__instagram-item" href="{$insta->link}">
                    <img class="main__instagram-item_img" src="{$insta->images->low_resolution->url}" alt=""/>
                </a>
            {/foreach}
        </div>
    </div>
</div>