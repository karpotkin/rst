{extends file='layout.tpl'}
{block name='main'}
    <p class="text-center jp_font">野菜がたっぷりのベラルーシ料理はビーツ、そばの実、ディル、サワークリームなどを使った料理で、日本では珍しい材料を使いつつとてもヘルシー！</p>
	
	<div class="menu__second">
		<div class="col-md-2 col-sm-4 col-xs-6 menu__second_bg">
			<a href="/menu/#APPETIZERS">
				<div class="menu__second_text menu__second_text_first">APPETIZERS</div>
				<div class="menu__second_text jp_font">前菜</div>
			</a>	
		</div>
		<div class="col-md-2 col-sm-4 col-xs-6 menu__second_bg">
			<a href="/menu/#SOUPS">
				<div class="menu__second_text menu__second_text_first">SOUPS & BREAD</div>
				<div class="menu__second_text jp_font">スープとパン</div>	
			</a>					
		</div>
		<div class="col-md-2 col-sm-4 col-xs-6 menu__second_bg">
			<a href="/menu/#HOT">
				<div class="menu__second_text menu__second_text_first">HOT APPETIZERS</div>
				<div class="menu__second_text jp_font">温前菜</div>		
			</a>								
		</div>
		<div class="col-md-2 col-sm-4 col-xs-6 menu__second_bg">
			<a href="/menu/#FISH">		
				<div class="menu__second_text menu__second_text_first">FISH</div>
				<div class="menu__second_text jp_font">魚料理</div>	
			</a>				
		</div>
		<div class="col-md-2 col-sm-4 col-xs-6 menu__second_bg">
			<a href="/menu/#MEAT">		
				<div class="menu__second_text menu__second_text_first">MEAT DISHES</div>
				<div class="menu__second_text jp_font">肉料理</div>	
			</a>				
		</div>
		<div class="col-md-2 col-sm-4 col-xs-6 menu__second_bg">
			<a href="/menu/#DESSERTS">		
				<div class="menu__second_text menu__second_text_first">DESSERTS</div>
				<div class="menu__second_text jp_font">デザート</div>	
			</a>				
		</div>		
	</div>
	
	<div class="clear"></div>
	<a name="APPETIZERS"></a>
    <h3 class="page-title text-center _tt_upper"><span class="jp_font">前菜</a> / Appetizers</h3>
    <ul class="list-unstyled row food-menu__list">
        <li class="food-menu__item col-md-4 col-sm-4 col-xs-6">         
			<a href="/design/images/food/full/1.jpg" class="food-menu__item-link" rel="nofollow">			
				<div>
					<img src="/design/images/food/1.jpg" alt="">
				</div>
				
				<div class="food-hack__text">	
					<div class="food-hack__body">
						<div class="food-menu__en_title">Fresh Tomatoes with Garlic Cheese</div>
						<div class="food-menu__jp_title jp_font">
							フレッシュトマトのガーリック <br>
							チーズ
						</div>	
					</div>
				</div>			
			</a>			
        </li>
        <li class="food-menu__item col-md-4 col-sm-4 col-xs-6">         
			<a href="/design/images/food/full/2.jpg" class="food-menu__item-link" rel="nofollow">			
				<div>
					<img src="/design/images/food/2.jpg" alt="">
				</div>
				
				<div class="food-hack__text">	
					<div class="food-hack__body">
						<div class="food-menu__en_title">Country Style Salad with Sour Cream</div>
						<div class="food-menu__jp_title jp_font">
							カントリー風サラダ　サワークリームソース
						</div>	
					</div>
				</div>			
			</a>			
        </li>       
        <li class="food-menu__item col-md-4 col-sm-4 col-xs-6">         
			<a href="/design/images/food/full/3.jpg" class="food-menu__item-link" rel="nofollow">			
				<div>
					<img src="/design/images/food/3.jpg" alt="">
				</div>
				
				<div class="food-hack__text">	
					<div class="food-hack__body">
						<div class="food-menu__en_title">Green Salad VITAMIN</div>
						<div class="food-menu__jp_title jp_font">
							グリーンサラダ「ビタミン」
						</div>	
					</div>
				</div>			
			</a>			
        </li> 	
        <li class="food-menu__item col-md-4 col-sm-4 col-xs-6">         
			<a href="/design/images/food/full/4.jpg" class="food-menu__item-link" rel="nofollow">			
				<div>
					<img src="/design/images/food/4.jpg" alt="">
				</div>
				
				<div class="food-hack__text">	
					<div class="food-hack__body">
						<div class="food-menu__en_title">Marinated Herring</div>
						<div class="food-menu__jp_title jp_font">
							ニシンの白ワイン漬け
						</div>	
					</div>
				</div>			
			</a>			
        </li> 
        <li class="food-menu__item col-md-4 col-sm-4 col-xs-6">         
			<a href="/design/images/food/full/5.jpg" class="food-menu__item-link" rel="nofollow">			
				<div>
					<img src="/design/images/food/5.jpg" alt="">
				</div>
				
				<div class="food-hack__text">	
					<div class="food-hack__body">
						<div class="food-menu__en_title">Crepe Rolls with Smoked Salmon</div>
						<div class="food-menu__jp_title jp_font">
							スモークサーモンのクレープ巻き
						</div>	
					</div>
				</div>			
			</a>			
        </li> 
        <li class="food-menu__item col-md-4 col-sm-4 col-xs-6">         
			<a href="/design/images/food/full/6.jpg" class="food-menu__item-link" rel="nofollow">			
				<div>
					<img src="/design/images/food/6.jpg" alt="">
				</div>
				
				<div class="food-hack__text">	
					<div class="food-hack__body">
						<div class="food-menu__en_title">Pickled Mushrooms</div>
						<div class="food-menu__jp_title jp_font">
							きのこのピクルス
						</div>	
					</div>
				</div>			
			</a>			
        </li> 
        <li class="food-menu__item col-md-4 col-sm-4 col-xs-6">         
			<a href="/design/images/food/full/7.jpg" class="food-menu__item-link" rel="nofollow">			
				<div>
					<img src="/design/images/food/7.jpg" alt="">
				</div>
				
				<div class="food-hack__text">	
					<div class="food-hack__body">
						<div class="food-menu__en_title">Mixed Pickles</div>
						<div class="food-menu__jp_title jp_font">
							ピクルスの盛り合わせ
						</div>	
					</div>
				</div>			
			</a>			
        </li>
        <li class="food-menu__item col-md-4 col-sm-4 col-xs-6">         
			<a href="/design/images/food/full/8.jpg" class="food-menu__item-link" rel="nofollow">			
				<div>
					<img src="/design/images/food/8.jpg" alt="">
				</div>
				
				<div class="food-hack__text">	
					<div class="food-hack__body">
						<div class="food-menu__en_title">Liver Pate</div>
						<div class="food-menu__jp_title jp_font">
							レバーペースト
						</div>	
					</div>
				</div>			
			</a>			
        </li>
        <li class="food-menu__item col-md-4 col-sm-4 col-xs-6">         
			<a href="/design/images/food/full/9.jpg" class="food-menu__item-link" rel="nofollow">			
				<div>
					<img src="/design/images/food/9.jpg" alt="">
				</div>
				
				<div class="food-hack__text">	
					<div class="food-hack__body">
						<div class="food-menu__en_title">Assorted Cold Meat ZAKUSKA</div>
						<div class="food-menu__jp_title jp_font">
							「ザクスカ」 （牛タン、自家製ハムなどの本日の盛り合わせ）
						</div>	
					</div>
				</div>			
			</a>			
        </li>		
        <li class="food-menu__item col-md-4 col-sm-4 col-xs-6">         
			<a href="/design/images/food/full/10.jpg" class="food-menu__item-link" rel="nofollow">			
				<div>
					<img src="/design/images/food/10.jpg" alt="">
				</div>
				
				<div class="food-hack__text">	
					<div class="food-hack__body">
						<div class="food-menu__en_title">Potato Salad OLIVIE</div>
						<div class="food-menu__jp_title jp_font">
							ポテトサラダ「オリヴィエ」
						</div>	
					</div>
				</div>			
			</a>			
        </li>		
        <li class="food-menu__item col-md-4 col-sm-4 col-xs-6">         
			<a href="/design/images/food/full/11.jpg" class="food-menu__item-link" rel="nofollow">			
				<div>
					<img src="/design/images/food/11.jpg" alt="">
				</div>
				
				<div class="food-hack__text">	
					<div class="food-hack__body">
						<div class="food-menu__en_title">Marinated Herring and Beet Salad SELYODKA POD SHUBOI</div>
						<div class="food-menu__jp_title jp_font">								ニシンとビーツのサラダ「毛皮のコートを着たニシン」（ポテト、卵、ワイン漬けのニシン、ビーツなどをミルフィーユ状に重ねた色鮮やかなサラダ）
						</div>	
					</div>
				</div>			
			</a>			
        </li>	
        <li class="food-menu__item col-md-4 col-sm-4 col-xs-6">         
			<a href="/design/images/food/full/12.jpg" class="food-menu__item-link" rel="nofollow">			
				<div>
					<img src="/design/images/food/12.jpg" alt="">
				</div>
				
				<div class="food-hack__text">	
					<div class="food-hack__body">
						<div class="food-menu__en_title">Lecho</div>
						<div class="food-menu__jp_title jp_font">										  レーチョ（パプリカのトマトソースマリネ）
						</div>	
					</div>
				</div>			
			</a>			
        </li>	
        <li class="food-menu__item col-md-4 col-sm-4 col-xs-6">         
			<a href="/design/images/food/full/13.jpg" class="food-menu__item-link" rel="nofollow">			
				<div>
					<img src="/design/images/food/13.jpg" alt="">
				</div>
				
				<div class="food-hack__text">	
					<div class="food-hack__body">
						<div class="food-menu__en_title">Marinated Vegetable Salad</div>
						<div class="food-menu__jp_title jp_font">										  色とりどり野菜のマリネ　　
						</div>	
					</div>
				</div>			
			</a>			
        </li>
        <li class="food-menu__item col-md-4 col-sm-4 col-xs-6">         
			<a href="/design/images/food/full/14.jpg" class="food-menu__item-link" rel="nofollow">			
				<div>
					<img src="/design/images/food/14.jpg" alt="">
				</div>
				
				<div class="food-hack__text">	
					<div class="food-hack__body">
						<div class="food-menu__en_title">Smoked Mackerel</div>
						<div class="food-menu__jp_title jp_font">										 
						自家製サバの燻製
						</div>	
					</div>
				</div>			
			</a>			
        </li>	
        <li class="food-menu__item col-md-4 col-sm-4 col-xs-6">         
			<a href="/design/images/food/full/14.jpg" class="food-menu__item-link" rel="nofollow">			
				<div>
					<img src="/design/images/food/14.jpg" alt="">
				</div>
				
				<div class="food-hack__text">	
					<div class="food-hack__body">
						<div class="food-menu__en_title">Smoked Salmon and Caviar Canapés</div>
						<div class="food-menu__jp_title jp_font">										 
						スモークサーモンとキャビヤのカナペ
						</div>	
					</div>
				</div>			
			</a>			
        </li>
        <li class="food-menu__item col-md-4 col-sm-4 col-xs-6">         
			<a href="/design/images/food/full/48.jpg" class="food-menu__item-link" rel="nofollow">			
				<div>
					<img src="/design/images/food/48.jpg" alt="">
				</div>
				
				<div class="food-hack__text">	
					<div class="food-hack__body">
						<div class="food-menu__en_title">Mushroom Spread</div>
						<div class="food-menu__jp_title jp_font">										 
						きのこのイクラ　　　　　　　　　　　　　　　　　　　
						</div>	
					</div>
				</div>			
			</a>			
        </li>	
        <li class="food-menu__item col-md-4 col-sm-4 col-xs-6">         
			<a href="/design/images/food/full/49.jpg" class="food-menu__item-link" rel="nofollow">			
				<div>
					<img src="/design/images/food/49.jpg" alt="">
				</div>
				
				<div class="food-hack__text">	
					<div class="food-hack__body">
						<div class="food-menu__en_title">Eggplant Rolls stuffed with Carrot and Garlic Cheese</div>
						<div class="food-menu__jp_title jp_font">										 
						人参とガーリックチーズのナス包み　　
						</div>	
					</div>
				</div>			
			</a>			
        </li>
        <li class="food-menu__item col-md-4 col-sm-4 col-xs-6">         
			<a href="/design/images/food/full/50.jpg" class="food-menu__item-link" rel="nofollow">			
				<div>
					<img src="/design/images/food/50.jpg" alt="">
				</div>
				
				<div class="food-hack__text">	
					<div class="food-hack__body">
						<div class="food-menu__en_title">Assorted Meat Aspic</div>
						<div class="food-menu__jp_title jp_font">										 
						ホロデーツ（豚肉、牛肉と鶏肉を煮たブイヨンをゼリーにしたもの）　
						</div>	
					</div>
				</div>			
			</a>			
        </li>
        <li class="food-menu__item col-md-4 col-sm-4 col-xs-6">         
			<a href="/design/images/food/full/51.jpg" class="food-menu__item-link" rel="nofollow">			
				<div>
					<img src="/design/images/food/51.jpg" alt="">
				</div>
				
				<div class="food-hack__text">	
					<div class="food-hack__body">
						<div class="food-menu__en_title">Eggplant and Walnut salad</div>
						<div class="food-menu__jp_title jp_font">										 
						ナスとくるみのサラダ
						</div>	
					</div>
				</div>			
			</a>			
        </li>		
        <li class="food-menu__item col-md-4 col-sm-4 col-xs-6">         
			<a href="/design/images/food/full/53.jpg" class="food-menu__item-link" rel="nofollow">			
				<div>
					<img src="/design/images/food/53.jpg" alt="">
				</div>
				
				<div class="food-hack__text">	
					<div class="food-hack__body">
						<div class="food-menu__en_title">Cod Liver and Egg Salad</div>
						<div class="food-menu__jp_title jp_font">										 
						タラ肝とたまごのサラダ
						</div>	
					</div>
				</div>			
			</a>			
        </li>			
        <li class="food-menu__item col-md-4 col-sm-4 col-xs-6">         
			<a href="/design/images/food/full/54.jpg" class="food-menu__item-link" rel="nofollow">			
				<div>
					<img src="/design/images/food/54.jpg" alt="">
				</div>
				
				<div class="food-hack__text">	
					<div class="food-hack__body">
						<div class="food-menu__en_title">Marinated Tomatoes</div>
						<div class="food-menu__jp_title jp_font">										 
						トマトのマリネ
						</div>	
					</div>
				</div>			
			</a>			
        </li>	
        <li class="food-menu__item col-md-4 col-sm-4 col-xs-6">         
			<a href="/design/images/food/full/55.jpg" class="food-menu__item-link" rel="nofollow">			
				<div>
					<img src="/design/images/food/55.jpg" alt="">
				</div>
				
				<div class="food-hack__text">	
					<div class="food-hack__body">
						<div class="food-menu__en_title">Assorted Smoked Fish</div>
						<div class="food-menu__jp_title jp_font">										 
						燻製魚の盛り合わせ
						</div>	
					</div>
				</div>			
			</a>			
        </li>		
        <li class="food-menu__item col-md-4 col-sm-4 col-xs-6">         
			<a href="/design/images/food/full/56.jpg" class="food-menu__item-link" rel="nofollow">			
				<div>
					<img src="/design/images/food/56.jpg" alt="">
				</div>
				
				<div class="food-hack__text">	
					<div class="food-hack__body">
						<div class="food-menu__en_title">Rabbit Pate with Dill Sauce</div>
						<div class="food-menu__jp_title jp_font">										 
						ウサギのペースト　ディルソース
						</div>	
					</div>
				</div>			
			</a>			
        </li>		
    </ul>

	<div class="clear"></div>
	<a name="SOUPS"></a>
    <h3 class="page-title text-center _tt_upper"><span class="jp_font">スープとパン</span> / Soups & Bread</h3>
    <ul class="list-unstyled row food-menu__list">
        <li class="food-menu__item col-md-4 col-sm-4 col-xs-6">         
			<a href="/design/images/food/full/16.jpg" class="food-menu__item-link" rel="nofollow">			
				<div>
					<img src="/design/images/food/16.jpg" alt="">
				</div>
				
				<div class="food-hack__text">	
					<div class="food-hack__body">
						<div class="food-menu__en_title">BORSCHT</div>
						<div class="food-menu__jp_title jp_font">
							「ボルシチ」
						</div>	
					</div>
				</div>			
			</a>			
        </li>
        <li class="food-menu__item col-md-4 col-sm-4 col-xs-6">         
			<a href="/design/images/food/full/17.jpg" class="food-menu__item-link" rel="nofollow">			
				<div>
					<img src="/design/images/food/17.jpg" alt="">
				</div>
				
				<div class="food-hack__text">	
					<div class="food-hack__body">
						<div class="food-menu__en_title">Pickles and Mushroom Soup RASSOLNIK</div>
						<div class="food-menu__jp_title jp_font">
							「ラッソルニク」（ピクルスの酸味が効いたキノコと麦のスープ)  
						</div>	
					</div>
				</div>			
			</a>			
        </li>		
        <li class="food-menu__item col-md-4 col-sm-4 col-xs-6">         
			<a href="/design/images/food/full/57.jpg" class="food-menu__item-link" rel="nofollow">			
				<div>
					<img src="/design/images/food/57.jpg" alt="">
				</div>
				
				<div class="food-hack__text">	
					<div class="food-hack__body">
						<div class="food-menu__en_title">Summer Special Cold Borscht HALADNIK</div>
						<div class="food-menu__jp_title jp_font">
							ハラドニク（夏限定　冷製ボルシチ）  
						</div>	
					</div>
				</div>			
			</a>			
        </li>		
        <li class="food-menu__item col-md-4 col-sm-4 col-xs-6">         
			<a href="/design/images/food/full/18.jpg" class="food-menu__item-link" rel="nofollow">			
				<div>
					<img src="/design/images/food/18.jpg" alt="">
				</div>
				
				<div class="food-hack__text">	
					<div class="food-hack__body">
						<div class="food-menu__en_title">Assorted Bread</div>
						<div class="food-menu__jp_title jp_font">
							パン（３種１人前）  
						</div>	
					</div>
				</div>			
			</a>			
        </li>	
        <li class="food-menu__item col-md-4 col-sm-4 col-xs-6">         
			<a href="/design/images/food/full/19.jpg" class="food-menu__item-link" rel="nofollow">			
				<div>
					<img src="/design/images/food/19.jpg" alt="">
				</div>
				
				<div class="food-hack__text">	
					<div class="food-hack__body">
						<div class="food-menu__en_title">Rye Bread</div>
						<div class="food-menu__jp_title jp_font">
							ライ麦パン  
						</div>	
					</div>
				</div>			
			</a>			
        </li>			
        <li class="food-menu__item col-md-4 col-sm-4 col-xs-6">         
			<a href="/design/images/food/full/20.jpg" class="food-menu__item-link" rel="nofollow">			
				<div>
					<img src="/design/images/food/20.jpg" alt="">
				</div>
				
				<div class="food-hack__text">	
					<div class="food-hack__body">
						<div class="food-menu__en_title">Baked Piroshki</div>
						<div class="food-menu__jp_title jp_font">
							焼きピロシキ　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　  
						</div>	
					</div>
				</div>			
			</a>			
        </li>					
    </ul>		
	
	<div class="clear"></div>
	<a name="HOT"></a>
    <h3 class="page-title text-center _tt_upper"><span class="jp_font">温前菜</span> / Hot Appetizers</h3>
    <ul class="list-unstyled row food-menu__list">
        <li class="food-menu__item col-md-4 col-sm-4 col-xs-6">         
			<a href="/design/images/food/full/21.jpg" class="food-menu__item-link" rel="nofollow">			
				<div>
					<img src="/design/images/food/21.jpg" alt="">
				</div>
				
				<div class="food-hack__text">	
					<div class="food-hack__body">
						<div class="food-menu__en_title">Sautéed Potatoes with Mushroom</div>
						<div class="food-menu__jp_title jp_font">
							ジャガイモときのこのソテー
						</div>	
					</div>
				</div>			
			</a>			
        </li>
        <li class="food-menu__item col-md-4 col-sm-4 col-xs-6">         
			<a href="/design/images/food/full/22.jpg" class="food-menu__item-link" rel="nofollow">			
				<div>
					<img src="/design/images/food/22.jpg" alt="">
				</div>
				
				<div class="food-hack__text">	
					<div class="food-hack__body">
						<div class="food-menu__en_title">Potato and Liver ZRAZY</div>
						<div class="food-menu__jp_title jp_font">
							ジャガイモとレバーの「ズラズイ」 
						</div>	
					</div>
				</div>			
			</a>			
        </li>		
        <li class="food-menu__item col-md-4 col-sm-4 col-xs-6">         
			<a href="/design/images/food/full/23.jpg" class="food-menu__item-link" rel="nofollow">			
				<div>
					<img src="/design/images/food/23.jpg" alt="">
				</div>
				
				<div class="food-hack__text">	
					<div class="food-hack__body">
						<div class="food-menu__en_title">Mushroom and Cheese Omelet</div>
						<div class="food-menu__jp_title jp_font">
							きのことチーズのオムレツ  
						</div>	
					</div>
				</div>			
			</a>			
        </li>	
        <li class="food-menu__item col-md-4 col-sm-4 col-xs-6">         
			<a href="/design/images/food/full/24.jpg" class="food-menu__item-link" rel="nofollow">			
				<div>
					<img src="/design/images/food/24.jpg" alt="">
				</div>
				
				<div class="food-hack__text">	
					<div class="food-hack__body">
						<div class="food-menu__en_title">Filled Crepes BLINCHIKI</div>
						<div class="food-menu__jp_title jp_font">
							クレープ巻き　「ブリンチキ」 
						</div>	
					</div>
				</div>			
			</a>			
        </li>	
        <li class="food-menu__item col-md-4 col-sm-4 col-xs-6">         
			<a href="/design/images/food/full/25.jpg" class="food-menu__item-link" rel="nofollow">			
				<div>
					<img src="/design/images/food/25.jpg" alt="">
				</div>
				
				<div class="food-hack__text">	
					<div class="food-hack__body">
						<div class="food-menu__en_title">Potato Pancakes DRANIKI</div>
						<div class="food-menu__jp_title jp_font">
							ポテトパンケーキ　「ドラニキ」 
						</div>	
					</div>
				</div>			
			</a>			
        </li>
        <li class="food-menu__item col-md-4 col-sm-4 col-xs-6">         
			<a href="/design/images/food/full/26.jpg" class="food-menu__item-link" rel="nofollow">			
				<div>
					<img src="/design/images/food/26.jpg" alt="">
				</div>
				
				<div class="food-hack__text">	
					<div class="food-hack__body">
						<div class="food-menu__en_title">Potato Pancake stuffed with fried Mushroom and Cabbage BABKA</div>
						<div class="food-menu__jp_title jp_font">
							「バプカ」（きのことキャベツを挟んだハッシュドポテト)         
						</div>	
					</div>
				</div>			
			</a>			
        </li>		
        <li class="food-menu__item col-md-4 col-sm-4 col-xs-6">         
			<a href="/design/images/food/full/27.jpg" class="food-menu__item-link" rel="nofollow">			
				<div>
					<img src="/design/images/food/27.jpg" alt="">
				</div>
				
				<div class="food-hack__text">	
					<div class="food-hack__body">
						<div class="food-menu__en_title">Zucchini sautéed in Sour Cream Sauce</div>
						<div class="food-menu__jp_title jp_font">
							ズッキーニのサワークリーム煮         
						</div>	
					</div>
				</div>			
			</a>			
        </li>		
        <li class="food-menu__item col-md-4 col-sm-4 col-xs-6">         
			<a href="/design/images/food/full/28.jpg" class="food-menu__item-link" rel="nofollow">			
				<div>
					<img src="/design/images/food/28.jpg" alt="">
				</div>
				
				<div class="food-hack__text">	
					<div class="food-hack__body">
						<div class="food-menu__en_title">Grandma’s Cabbage Rolls GOLUBTSY</div>
						<div class="food-menu__jp_title jp_font">
							おばあちゃんのロールキャベツ         
						</div>	
					</div>
				</div>			
			</a>			
        </li>	
        <li class="food-menu__item col-md-4 col-sm-4 col-xs-6">         
			<a href="/design/images/food/full/29.jpg" class="food-menu__item-link" rel="nofollow">			
				<div>
					<img src="/design/images/food/29.jpg" alt="">
				</div>
				
				<div class="food-hack__text">	
					<div class="food-hack__body">
						<div class="food-menu__en_title">Paprika stuffed with Meat</div>
						<div class="food-menu__jp_title jp_font">
							パプリカの肉詰め         
						</div>	
					</div>
				</div>			
			</a>			
        </li>	
        <li class="food-menu__item col-md-4 col-sm-4 col-xs-6">         
			<a href="/design/images/food/full/30.jpg" class="food-menu__item-link" rel="nofollow">			
				<div>
					<img src="/design/images/food/30.jpg" alt="">
				</div>
				
				<div class="food-hack__text">	
					<div class="food-hack__body">
						<div class="food-menu__en_title">Pot Stew GORSHOK with Buckwheat and Porcini</div>
						<div class="food-menu__jp_title jp_font">
							そばの実とポルチーニ茸の壷焼き「ゴルショク」      
						</div>	
					</div>
				</div>			
			</a>			
        </li>	
        <li class="food-menu__item col-md-4 col-sm-4 col-xs-6">         
			<a href="/design/images/food/full/31.jpg" class="food-menu__item-link" rel="nofollow">			
				<div>
					<img src="/design/images/food/31.jpg" alt="">
				</div>
				
				<div class="food-hack__text">	
					<div class="food-hack__body">
						<div class="food-menu__en_title">Russian Style Meat Dumplings PELMENI</div>
						<div class="food-menu__jp_title jp_font">
							ロシア風水餃子「ペリメーニ」（７ケ入）   
						</div>	
					</div>
				</div>			
			</a>			
        </li>
        <li class="food-menu__item col-md-4 col-sm-4 col-xs-6">         
			<a href="/design/images/food/full/58.jpg" class="food-menu__item-link" rel="nofollow">			
				<div>
					<img src="/design/images/food/58.jpg" alt="">
				</div>
				
				<div class="food-hack__text">	
					<div class="food-hack__body">
						<div class="food-menu__en_title">Vareniki</div>
						<div class="food-menu__jp_title jp_font">
							ヴァレニキ   
						</div>	
					</div>
				</div>			
			</a>			
        </li>		
        <li class="food-menu__item col-md-4 col-sm-4 col-xs-6">         
			<a href="/design/images/food/full/59.jpg" class="food-menu__item-link" rel="nofollow">			
				<div>
					<img src="/design/images/food/59.jpg" alt="">
				</div>
				
				<div class="food-hack__text">	
					<div class="food-hack__body">
						<div class="food-menu__en_title">Draniki with Mushroom Machanka</div>
						<div class="food-menu__jp_title jp_font">
							きのこのマチャンカ（ポテトパンケーキ「ドラニキ」、きのことサワークリームソース）  
						</div>	
					</div>
				</div>			
			</a>			
        </li>		
        <li class="food-menu__item col-md-4 col-sm-4 col-xs-6">         
			<a href="/design/images/food/full/59.jpg" class="food-menu__item-link" rel="nofollow">			
				<div>
					<img src="/design/images/food/59.jpg" alt="">
				</div>
				
				<div class="food-hack__text">	
					<div class="food-hack__body">
						<div class="food-menu__en_title">Baked Eggplant</div>
						<div class="food-menu__jp_title jp_font">
							ナスのオーブン焼き  
						</div>	
					</div>
				</div>			
			</a>			
        </li>					
    </ul>	

	<div class="clear"></div>
	<a name="FISH"></a>
    <h3 class="page-title text-center _tt_upper"><span class="jp_font">魚料理</span> / Fish</h3>
    <ul class="list-unstyled row food-menu__list">
        <li class="food-menu__item col-md-4 col-sm-4 col-xs-6">         
			<a href="/design/images/food/full/32.jpg" class="food-menu__item-link" rel="nofollow">			
				<div>
					<img src="/design/images/food/32.jpg" alt="">
				</div>
				
				<div class="food-hack__text">	
					<div class="food-hack__body">
						<div class="food-menu__en_title">Fried Capelins</div>
						<div class="food-menu__jp_title jp_font">
							ししゃものシンプルソテー
						</div>	
					</div>
				</div>			
			</a>			
        </li>
        <li class="food-menu__item col-md-4 col-sm-4 col-xs-6">         
			<a href="/design/images/food/full/33.jpg" class="food-menu__item-link" rel="nofollow">			
				<div>
					<img src="/design/images/food/33.jpg" alt="">
				</div>
				
				<div class="food-hack__text">	
					<div class="food-hack__body">
						<div class="food-menu__en_title">Salmon Croquettes</div>
						<div class="food-menu__jp_title jp_font">
							サーモンのコロッケ
						</div>	
					</div>
				</div>			
			</a>			
        </li>		
        <li class="food-menu__item col-md-4 col-sm-4 col-xs-6">         
			<a href="/design/images/food/full/34.jpg" class="food-menu__item-link" rel="nofollow">			
				<div>
					<img src="/design/images/food/34.jpg" alt="">
				</div>
				
				<div class="food-hack__text">	
					<div class="food-hack__body">
						<div class="food-menu__en_title">Roasted Salmon with Cream Sauce</div>
						<div class="food-menu__jp_title jp_font">
							サーモンのソテー　サワークリームソース
						</div>	
					</div>
				</div>			
			</a>			
        </li>		
        <li class="food-menu__item col-md-4 col-sm-4 col-xs-6">         
			<a href="/design/images/food/full/35.jpg" class="food-menu__item-link" rel="nofollow">			
				<div>
					<img src="/design/images/food/35.jpg" alt="">
				</div>
				
				<div class="food-hack__text">	
					<div class="food-hack__body">
						<div class="food-menu__en_title">Broiled Sea Bass</div>
						<div class="food-menu__jp_title jp_font">
							スズキのホイル焼き 
						</div>	
					</div>
				</div>			
			</a>			
        </li>	
        <li class="food-menu__item col-md-4 col-sm-4 col-xs-6">         
			<a href="/design/images/food/full/61.jpg" class="food-menu__item-link" rel="nofollow">			
				<div>
					<img src="/design/images/food/61.jpg" alt="">
				</div>
				
				<div class="food-hack__text">	
					<div class="food-hack__body">
						<div class="food-menu__en_title">Pot Stew GORSHOK with Sea Bass, Vegetables and Cheese</div>
						<div class="food-menu__jp_title jp_font">
							スズキのソテー、炒め野菜とチーズの壷焼き「ゴルショク」
						</div>	
					</div>
				</div>			
			</a>			
        </li>							
    </ul>	

	<div class="clear"></div>
	<a name="MEAT"></a>
    <h3 class="page-title text-center _tt_upper"><span class="jp_font">肉料理</span> / Meat Dishes</h3>
    <ul class="list-unstyled row food-menu__list">
        <li class="food-menu__item col-md-4 col-sm-4 col-xs-6">         
			<a href="/design/images/food/full/36.jpg" class="food-menu__item-link" rel="nofollow">			
				<div>
					<img src="/design/images/food/36.jpg" alt="">
				</div>
				
				<div class="food-hack__text">	
					<div class="food-hack__body">
						<div class="food-menu__en_title">Roasted Chicken with Tomato and Horseradish Sauce</div>
						<div class="food-menu__jp_title jp_font">
							チキンマリネのハーブ焼き
						</div>	
					</div>
				</div>			
			</a>			
        </li>
        <li class="food-menu__item col-md-4 col-sm-4 col-xs-6">         
			<a href="/design/images/food/full/37.jpg" class="food-menu__item-link" rel="nofollow">			
				<div>
					<img src="/design/images/food/37.jpg" alt="">
				</div>
				
				<div class="food-hack__text">	
					<div class="food-hack__body">
						<div class="food-menu__en_title">Kiev Style Chicken Cutlet</div>
						<div class="food-menu__jp_title jp_font">
							キエフ風チキンカツレツ（切ると中から熱々のハーブバターソースが流れ出るウクライナの伝統料理） 
						</div>	
					</div>
				</div>			
			</a>			
        </li>		
        <li class="food-menu__item col-md-4 col-sm-4 col-xs-6">         
			<a href="/design/images/food/full/38.jpg" class="food-menu__item-link" rel="nofollow">			
				<div>
					<img src="/design/images/food/38.jpg" alt="">
				</div>
				
				<div class="food-hack__text">	
					<div class="food-hack__body">
						<div class="food-menu__en_title">Sour Cream Pork Stew MACHANKA</div>
						<div class="food-menu__jp_title jp_font">
							豚肉のサワークリーム煮「マチャンカ」（クレープにソースをつけて食べるベラルーシの郷土料理） 
						</div>	
					</div>
				</div>			
			</a>			
        </li>		
        <li class="food-menu__item col-md-4 col-sm-4 col-xs-6">         
			<a href="/design/images/food/full/39.jpg" class="food-menu__item-link" rel="nofollow">			
				<div>
					<img src="/design/images/food/39.jpg" alt="">
				</div>
				
				<div class="food-hack__text">	
					<div class="food-hack__body">
						<div class="food-menu__en_title">Beef Steak with Cranberry Sauce</div>
						<div class="food-menu__jp_title jp_font">
							牛フィレステーキ　コケモモのソース 
						</div>	
					</div>
				</div>			
			</a>			
        </li>	
        <li class="food-menu__item col-md-4 col-sm-4 col-xs-6">         
			<a href="/design/images/food/full/40.jpg" class="food-menu__item-link" rel="nofollow">			
				<div>
					<img src="/design/images/food/40.jpg" alt="">
				</div>
				
				<div class="food-hack__text">	
					<div class="food-hack__body">
						<div class="food-menu__en_title">Oven Baked Beef Steak with Cheese and Vegetables</div>
						<div class="food-menu__jp_title jp_font">
							ピョートル大帝のステーキ（薄くたたいたお肉に野菜とチーズをのせたステーキ）  
						</div>	
					</div>
				</div>			
			</a>			
        </li>			
        <li class="food-menu__item col-md-4 col-sm-4 col-xs-6">         
			<a href="/design/images/food/full/20.jpg" class="food-menu__item-link" rel="nofollow">			
				<div>
					<img src="/design/images/food/20.jpg" alt="">
				</div>
				
				<div class="food-hack__text">	
					<div class="food-hack__body">
						<div class="food-menu__en_title">Beef Stroganoff (side dish at your choice: mashed potatoes, buckwheat, saffron rice)</div>
						<div class="food-menu__jp_title jp_font">
						ビーフストロガノフ（付け合せをマッシュポテト、そばの実、サフランライスからお選び下さい）　　　　　　　　  
						</div>	
					</div>
				</div>			
			</a>			
        </li>
        <li class="food-menu__item col-md-4 col-sm-4 col-xs-6">         
			<a href="/design/images/food/full/62.jpg" class="food-menu__item-link" rel="nofollow">			
				<div>
					<img src="/design/images/food/62.jpg" alt="">
				</div>
				
				<div class="food-hack__text">	
					<div class="food-hack__body">
						<div class="food-menu__en_title">Braised Duck with Onion Sauce</div>
						<div class="food-menu__jp_title jp_font">
						鴨のオニオンソース煮　　　　　　　　  
						</div>	
					</div>
				</div>			
			</a>			
        </li>		
    </ul>	

	<div class="clear"></div>
	<a name="DESSERTS"></a>
    <h3 class="page-title text-center _tt_upper"><span class="jp_font">ホームメイドデザート</span> / Homemade</h3>
    <ul class="list-unstyled row food-menu__list">
        <li class="food-menu__item col-md-4 col-sm-4 col-xs-6">         
			<a href="/design/images/food/full/41.jpg" class="food-menu__item-link" rel="nofollow">			
				<div>
					<img src="/design/images/food/41.jpg" alt="">
				</div>
				
				<div class="food-hack__text">	
					<div class="food-hack__body">
						<div class="food-menu__en_title">Chocolate Cake BIRD’S MILK</div>
						<div class="food-menu__jp_title jp_font">
							「小鳥のミルク」（レモンとセモリナ粉のクリームをはさんだチョコレートケーキ)
						</div>	
					</div>
				</div>			
			</a>			
        </li>	
        <li class="food-menu__item col-md-4 col-sm-4 col-xs-6">         
			<a href="/design/images/food/full/43.jpg" class="food-menu__item-link" rel="nofollow">			
				<div>
					<img src="/design/images/food/43.jpg" alt="">
				</div>
				
				<div class="food-hack__text">	
					<div class="food-hack__body">
						<div class="food-menu__en_title">Spicy Apple Cake SHARLOTKA</div>
						<div class="food-menu__jp_title jp_font">
							アップルケーキ「シャルロトカ」（シナモンの味が効いたスポンジケーキ） 
						</div>	
					</div>
				</div>			
			</a>			
        </li>		
        <li class="food-menu__item col-md-4 col-sm-4 col-xs-6">         
			<a href="/design/images/food/full/44.jpg" class="food-menu__item-link" rel="nofollow">			
				<div>
					<img src="/design/images/food/44.jpg" alt="">
				</div>
				
				<div class="food-hack__text">	
					<div class="food-hack__body">
						<div class="food-menu__en_title">Fried Cookies HVOROST</div>
						<div class="food-menu__jp_title jp_font">
							「ホヴォロスト」（ほんのり甘いサクサクの揚げクッキー）  
						</div>	
					</div>
				</div>			
			</a>			
        </li>	
        <li class="food-menu__item col-md-4 col-sm-4 col-xs-6">         
			<a href="/design/images/food/full/45.jpg" class="food-menu__item-link" rel="nofollow">			
				<div>
					<img src="/design/images/food/45.jpg" alt="">
				</div>
				
				<div class="food-hack__text">	
					<div class="food-hack__body">
						<div class="food-menu__en_title">Cottage Cheese Dumplings LAZY VARENIKI</div>
						<div class="food-menu__jp_title jp_font">
							怠け者のヴァレニキ（カッテージチーズの入ったニョッキのような温かいデザート）
						</div>	
					</div>
				</div>			
			</a>			
        </li>			
        <li class="food-menu__item col-md-4 col-sm-4 col-xs-6">         
			<a href="/design/images/food/full/46.jpg" class="food-menu__item-link" rel="nofollow">			
				<div>
					<img src="/design/images/food/46.jpg" alt="">
				</div>
				
				<div class="food-hack__text">	
					<div class="food-hack__body">
						<div class="food-menu__en_title">Crepes BLINCHIKI filled with Cottage Cheese and Raisin</div>
						<div class="food-menu__jp_title jp_font">
							カッテージチーズのクレープ巻き　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　  
						</div>	
					</div>
				</div>			
			</a>			
        </li>
        <li class="food-menu__item col-md-4 col-sm-4 col-xs-6">         
			<a href="/design/images/food/full/47.jpg" class="food-menu__item-link" rel="nofollow">			
				<div>
					<img src="/design/images/food/47.jpg" alt="">
				</div>
				
				<div class="food-hack__text">	
					<div class="food-hack__body">
						<div class="food-menu__en_title">Ice Cream</div>
						<div class="food-menu__jp_title jp_font">
							アイスクリーム　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　  
						</div>	
					</div>
				</div>			
			</a>			
        </li>	
        <li class="food-menu__item col-md-4 col-sm-4 col-xs-6">         
			<a href="/design/images/food/full/63.jpg" class="food-menu__item-link" rel="nofollow">			
				<div>
					<img src="/design/images/food/63.jpg" alt="">
				</div>
				
				<div class="food-hack__text">	
					<div class="food-hack__body">
						<div class="food-menu__en_title">Plum Mousse</div>
						<div class="food-menu__jp_title jp_font">
							プラムのムース　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　  
						</div>	
					</div>
				</div>			
			</a>			
        </li>			
        <li class="food-menu__item col-md-4 col-sm-4 col-xs-6">         
			<a href="/design/images/food/full/64.jpg" class="food-menu__item-link" rel="nofollow">			
				<div>
					<img src="/design/images/food/64.jpg" alt="">
				</div>
				
				<div class="food-hack__text">	
					<div class="food-hack__body">
						<div class="food-menu__en_title">Rhubarb and Apple Tart</div>
						<div class="food-menu__jp_title jp_font">
							ルバーブとアップルのタルト　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　  
						</div>	
					</div>
				</div>			
			</a>			
        </li>		
		
    </ul>		
	
{/block}