<!DOCTYPE html>
<html>
<head>
	<base href="{$config->root_url}"/>
	<title>{$meta_title|escape}</title>
	
	{* Meta *}
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="description" content="{$meta_description|escape}" />
	<meta name="keywords"    content="{$meta_keywords|escape}" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1.0">
	{* Styles *}
    <link href='https://fonts.googleapis.com/css?family=Marko+One' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Overlock:400,700' rel='stylesheet' type='text/css'>

    <link rel="stylesheet" type="text/css" href="/design/css/bootstrap.min.css" />
    <link rel="stylesheet" type="text/css" href="/design/css/font-awesome.min.css" />
    <link rel="stylesheet" type="text/css" href="/design/css/vendor.css" />
    <link rel="stylesheet" type="text/css" href="/design/css/base.css" />
    <link rel="stylesheet" type="text/css" href="/design/css/additional.css" />
    <!--[if lt IE 9]>
    <link rel="stylesheet" type="text/css" href="/design/css/ie8-and-down.css" />
    <![endif]-->
    {block name='headCSS'}{/block}
</head>
<body>
    <div class="restaurant-minsk">
        <div class="wrapper">
            <div class="container container__mod">

                {include file='header.tpl'}

                {block name='main'}{/block}

                {include file='footer.tpl'}
            </div>
        </div>
    </div>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.2/jquery-ui.min.js"></script>
    <script src="/design/js/bootstrap.min.js"></script>
    <script src="/design/js/mediaelement-and-player.min.js"></script>
    <script src="/design/js/picturefill.min.js"></script>
    <script src="/design/js/jquery.colorbox.js"></script>
    <script>document.createElement('picture');</script>
    <script src="/design/js/main.js"></script>
    <!--[if lt IE 10]>
    <script type="text/javascript" src="/design/js/media.match.min.js"></script>
    <![endif]-->
    <!--[if IE]><script src="/design/js/ie.js"></script><![endif]-->

    {block name='footerScript'}{/block}
</body>
</html>